﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Drawing;
using Azalea.Map;

namespace Azalea
{
    internal class CommonActions
    {

        private Point _point1;
        private Point _point2;
        private DateTime dateTimeNow;

        public void ExecuteJavaScript(IWebDriver driver, string script)
        {
            IJavaScriptExecutor javaScriptExecutor = driver as IJavaScriptExecutor;
            javaScriptExecutor.ExecuteScript(script);
        }
        public T ExecuteScript<T>(IWebDriver driver, string script)
        {
            IJavaScriptExecutor javaScriptExecutor = driver as IJavaScriptExecutor;
            return (T)javaScriptExecutor.ExecuteScript(script);
        }

        public void Delay(IWebDriver driver, int milliseconds = 1000)
        {
            //Main.tracker.SetInfo("Delay " + milliseconds + "ms");
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMilliseconds(milliseconds);
            Main.Reporter.Record_Message("Delay for " + milliseconds + "ms");
            //Thread.Sleep(milliseconds);
            //Main.tracker.ClearInfo();
        }
        public void Delay(int milliseconds = 1000)
        {
            //Main.tracker.SetInfo("Delay " + milliseconds + "ms");
            Thread.Sleep(milliseconds);
            Main.Reporter.Record_Message("Delay for " + milliseconds + "ms");
            //Main.tracker.ClearInfo();
        }

        public void HoverEvent(IWebDriver driver, IWebElement moveToElement)
        {
            Actions actions = new Actions(driver);
            _point1 = moveToElement.Location;
            dateTimeNow = DateTime.Now;
            actions.MoveToElement(moveToElement).Perform();
            Main.Reporter.Record_Hover(ReportLogItemType.EVENT, dateTimeNow, _point1);
        }
        public void HoverEvent(IWebDriver driver, IWebElement moveToElement, int xCordVal, int yCordVal)
        {
            Actions actions = new Actions(driver);
            _point1 = new Point(xCordVal, yCordVal);
            dateTimeNow = DateTime.Now;
            actions.MoveToElement(moveToElement, xCordVal, yCordVal).Build().Perform();
            Main.Reporter.Record_Hover(ReportLogItemType.EVENT, dateTimeNow, _point1);
        }

        public void DragEvent(IWebDriver driver, IWebElement moveToElement, int startXVal, int startYVal, int endXVal, int endYVal)
        {
            Actions actions = new Actions(driver);
            _point1 = new Point(startXVal, startYVal);
            _point2 = new Point(endXVal, endYVal);
            dateTimeNow = DateTime.Now;
            actions.MoveToElement(moveToElement, startXVal, startYVal).Perform();
            actions.ClickAndHold().Perform();
            actions.MoveByOffset(endXVal, endYVal).Release().Build().Perform();
            Main.Reporter.Record_Drag(ReportLogItemType.EVENT, dateTimeNow, _point1, _point2);
        }

        public void RightClickEvent(IWebDriver driver, IWebElement moveToElement)
        {
            Actions actions = new Actions(driver);
            _point1 = moveToElement.Location;
            dateTimeNow = DateTime.Now;
            actions.MoveToElement(moveToElement).ContextClick().Perform();
            Main.Reporter.Record_RgtClick(ReportLogItemType.EVENT, dateTimeNow, _point1);
        }
        public void RightClickEvent(IWebDriver driver, IWebElement moveToElement, int xCordVal, int yCordVal)
        {
            Actions actions = new Actions(driver);
            _point1 = new Point(xCordVal, yCordVal);
            dateTimeNow = DateTime.Now;
            actions.MoveToElement(moveToElement, xCordVal, yCordVal).ContextClick().Build().Perform();
            Main.Reporter.Record_RgtClick(ReportLogItemType.EVENT, dateTimeNow, _point1);
        }

        public void ClickEvent(IWebDriver driver, IWebElement moveToElement)
        {
            Actions actions = new Actions(driver);
            _point1 = moveToElement.Location;
            dateTimeNow = DateTime.Now;
            actions.MoveToElement(moveToElement).Click(moveToElement).Build().Perform();
            Main.Reporter.Record_Click(ReportLogItemType.EVENT, dateTimeNow, _point1);
        }
        public void ClickEvent(IWebDriver driver, IWebElement moveToElement, IWebElement clickElement)
        {
            Actions actions = new Actions(driver);
            _point1 = clickElement.Location;
            dateTimeNow = DateTime.Now;
            actions.MoveToElement(moveToElement).Click(clickElement).Build().Perform();
            Main.Reporter.Record_Click(ReportLogItemType.EVENT, dateTimeNow, _point1);
        }
        public void ClickEvent(IWebDriver driver, IWebElement moveToElement, int xCordVal, int yCordVal)
        {
            Actions actions = new Actions(driver);
            _point1 = new Point(xCordVal, yCordVal);
            dateTimeNow = DateTime.Now;
            actions.MoveToElement(moveToElement, xCordVal, yCordVal).Click().Build().Perform();
            Main.Reporter.Record_Click(ReportLogItemType.EVENT, dateTimeNow, _point1);
        }

        public void DblClickEvent(IWebDriver driver, IWebElement moveToElement)
        {
            Actions actions = new Actions(driver);
            _point1 = moveToElement.Location;
            dateTimeNow = DateTime.Now;
            actions.MoveToElement(moveToElement).DoubleClick(moveToElement).Build().Perform();
            Main.Reporter.Record_DblClick(ReportLogItemType.EVENT, dateTimeNow, _point1);
        }
        public void DblClickEvent(IWebDriver driver, IWebElement moveToElement, IWebElement clickElement)
        {
            Actions actions = new Actions(driver);
            _point1 = clickElement.Location;
            dateTimeNow = DateTime.Now;
            actions.MoveToElement(moveToElement).DoubleClick(clickElement).Build().Perform();
            Main.Reporter.Record_DblClick(ReportLogItemType.EVENT, dateTimeNow, _point1);
        }
        public void DblClickEvent(IWebDriver driver, IWebElement moveToElement, int xCordVal, int yCordVal)
        {
            Actions actions = new Actions(driver);
            _point1 = new Point(xCordVal, yCordVal);
            dateTimeNow = DateTime.Now;
            actions.MoveToElement(moveToElement, xCordVal, yCordVal).DoubleClick().Build().Perform();
            Main.Reporter.Record_DblClick(ReportLogItemType.EVENT, dateTimeNow, _point1);
        }

        public void ClickInputEvent(IWebDriver driver, IWebElement moveToElement, int xCordVal, int yCordVal, string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                Actions actions = new Actions(driver);
                _point1 = new Point(xCordVal, yCordVal);
                dateTimeNow = DateTime.Now;
                actions.MoveToElement(moveToElement, xCordVal, yCordVal).Click().KeyDown(OpenQA.Selenium.Keys.Control).SendKeys("a").KeyUp(OpenQA.Selenium.Keys.Control).SendKeys(OpenQA.Selenium.Keys.Backspace).SendKeys(value).SendKeys(OpenQA.Selenium.Keys.Tab).Perform();
                //actions.MoveToElement(moveToElement, xCordVal, yCordVal).Click().SendKeys(Keys.Control + "a" + Keys.Null).SendKeys(Keys.Backspace).SendKeys(value).SendKeys(Keys.Tab).Perform();
                Main.Reporter.Record_Input(ReportLogItemType.EVENT, dateTimeNow, _point1, value);
            }
        }

        public void InputEvent(IWebDriver driver, IWebElement inputElement, string inputValue)
        {
            Actions inputAction = new Actions(driver);
            _point1 = inputElement.Location;
            dateTimeNow = DateTime.Now;
            inputAction.Click(inputElement).SendKeys(inputElement, inputValue).Build().Perform();
            Main.Reporter.Record_Input(ReportLogItemType.EVENT, dateTimeNow, _point1, inputValue);
        }
        public void InputEvent(IWebDriver driver, IWebElement clickToElement, IWebElement inputElement, string inputValue)
        {
            Actions inputAction = new Actions(driver);
            _point1 = clickToElement.Location;
            _point2 = inputElement.Location;
            dateTimeNow = DateTime.Now;
            inputAction.Click(clickToElement).SendKeys(inputElement, inputValue).Build().Perform();
            Main.Reporter.Record_Click(ReportLogItemType.EVENT, dateTimeNow, _point1);
            Main.Reporter.Record_Input(ReportLogItemType.EVENT, dateTimeNow, _point2, inputValue);
        }

        public string GetDateUsingExcel(string excelDateValue)
        {

            var currDate = DateTime.Today;
            var currYear = currDate.Year;

            string finalValue = null;
            var FM_value = 1;
            var FD_Value = 1;
            DateTime inputDateValue = currDate;
            string dateFormat = "yyyyMMdd"; //CONST.DATE_FORMAT_YYYYMMDD;;

            string regex_1_1;
            string regex_1_2;
            string regex_1_3;
            //Regex for Y+M+D, Y+M-D, Y-M+D,Y-M-D
            string regex_1_4 = @"^Y[+-][0-9]{1,2}M[+-][0-9]{1,2}D$";

            //Regex for YMD+1, YMD-1
            string regex_1_5 = @"^YMD[+-][0-9]{1,2}$";

            //Regex for YM+FD, YM+LD, YM+D,YM-FD, YM-LD, YM-D
            string regex_1_6 = @"/ ^YM[+-][0-9]{1,2}(F|L|)D$";

            string regex_1_7 = @"^Y[0-9]{1,2}$";
            string regex_1_8;
            string regex_1_9 = @"//";

            if ((excelDateValue.Contains("+")) || (excelDateValue.Contains("-")))
            {
                //Regex for Y+, Y-
                regex_1_1 = @"^Y[+-][0-9]{1,2}$";

                //Regex for Y+FM, Y-FM, Y+LM, Y-LM, Y+M, Y-M
                regex_1_2 = @"^Y[+-][0-9]{1,2} (F|L|)M$";

                //Regex for Y+FMFD, Y+MD, Y-MD
                regex_1_3 = @"^Y[+-][0-9]{1,2} (F|L|)M(F|)D$";
                regex_1_8 = @"^YY[+-][0-9]{1,2}$";
                regex_1_9 = @"^YM(F|L)D[+-][0-9]{1,2}$";
            }
            else
            {
                //Regex for Y+, Y-
                regex_1_1 = @"^Y$";

                //Regex for YFM, YLM
                regex_1_2 = @"^Y(F|L|)M$";
                //Regex for YFMFD, YFMD, YLMLD, YMD
                regex_1_3 = @"^Y(F|L|)M(F|L|)D$";

                regex_1_8 = @"^YY$";
            }
            var subRegex_Year = @"Y";
            var subRegex_Month = @"(F|L|)M$";
            var subRegex_Day = @"(F|L|)D$";


            if (Regex.IsMatch(excelDateValue, regex_1_1))
            {
                dateFormat = "yyyy";

                var tempY = Regex.Split(excelDateValue, subRegex_Year)[1];
                var yInputVal = 0;
                if (!string.IsNullOrEmpty(tempY))
                {
                    yInputVal = Convert.ToInt32(tempY);
                    currYear += yInputVal;
                }
                inputDateValue = new DateTime(currYear, FM_value, FD_Value);


            }
            else if (Regex.IsMatch(excelDateValue, regex_1_2))  //if (excelDateValue.search(regex_1_2) == 0)
            {
                dateFormat = "yyyyMM";

                var value1 = Regex.Split(excelDateValue, subRegex_Month); //excelDateValue.split(subRegex_Month);
                //identify First Month, Last Month or current Month
                var tempM = value1[1];

                var tempY = Regex.Split(value1[0], subRegex_Month)[1]; //value1[0].split(subRegex_Year)[1];
                var finalYear = currYear;
                if (!string.IsNullOrEmpty(tempY))
                {
                    var yInputVal = Convert.ToInt32(tempY); //aqConvert.StrToInt(tempY);
                    finalYear += yInputVal;
                }
                var finalMonth = GetDateMonthValue(tempM);

                inputDateValue = new DateTime(finalYear, finalMonth, FD_Value); //aqDateTime.SetDateElements(finalYear, finalMonth, FD_Value);


            }
            else if (Regex.IsMatch(excelDateValue, regex_1_3))
            {
                var value = Regex.Split(excelDateValue, subRegex_Day);
                var value2 = Regex.Split(value[0], subRegex_Month); //value[0].split(subRegex_Month);


                //identify First Month, Last Month or current Month
                var tempM = value2[1];
                var tempD = value[1];

                var tempY = Regex.Split(value2[0], subRegex_Year)[1]; // value2[0].split(subRegex_Year)[1];
                var finalYear = currYear;
                if (!string.IsNullOrEmpty(tempY))
                {
                    var yInputVal = Convert.ToInt32(tempY); //aqConvert.StrToInt(tempY);
                    finalYear += yInputVal;
                }

                var finalMonth = GetDateMonthValue(tempM);
                var finalDay = GetDateDayValue(tempD);

                inputDateValue = new DateTime(finalYear, finalMonth, finalDay); //aqDateTime.SetDateElements(finalYear, finalMonth, finalDay);
            }
            else if (Regex.IsMatch(excelDateValue, regex_1_4))//excelDateValue.search(regex_1_4) == 0)
            {
                var subRegex_1_4_2 = @"D$";
                var subRegex_1_4_3 = @"M[+-][0-9]{1,2}$";
                var subRegex_1_4_4 = @"^Y[+-][0-9]{1,2}M";

                var value = Regex.Split(excelDateValue, subRegex_1_4_2); //excelDateValue.split(subRegex_1_4_2);
                var value1 = Regex.Split(value[0], subRegex_1_4_3); //value[0].split(subRegex_1_4_3);
                var value2 = Regex.Split(value[0], subRegex_1_4_4)[1]; //value[0].split(subRegex_1_4_4)[1];


                var tempY = Regex.Split(value1[0], subRegex_Year)[1]; //value1[0].split(subRegex_Year)[1];
                var yInputVal = Convert.ToInt32(tempY); //aqConvert.StrToInt(tempY);

                var finalYear = currYear + yInputVal;

                var mInputVal = Convert.ToInt32(value2); //aqConvert.StrToInt(value2);

                var tempMonthValue = currDate.AddMonths(mInputVal); //aqDateTime.AddMonths(currDate, mInputVal);
                var finalMonth = tempMonthValue.Month; //aqDateTime.GetMonth(tempMonthValue);

                var finalDay = GetDateDayValue();
                inputDateValue = new DateTime(finalYear, finalMonth, FD_Value); //aqDateTime.SetDateElements(finalYear, finalMonth, finalDay);


            }
            else if (Regex.IsMatch(excelDateValue, regex_1_5)) //excelDateValue.search(regex_1_5) == 0)
            {
                var subRegex_1_5_1 = @"^YMD";
                var splitValue = Regex.Split(excelDateValue, subRegex_1_5_1)[1]; //excelDateValue.split(subRegex_1_5_1)[1];
                var daysVal = Convert.ToInt32(splitValue); //aqConvert.StrToInt(splitValue);
                inputDateValue = currDate.AddDays(daysVal); //aqDateTime.AddDays(currDate, daysVal);

            }
            else if (Regex.IsMatch(excelDateValue, regex_1_6)) //excelDateValue.search(regex_1_6) == 0)
            {
                var subRegex_1_6_1 = @"M[+-][0-9]{1,2}$";
                var subRegex_1_6_2 = @"^Y";
                var value = Regex.Split(excelDateValue, subRegex_Day); //excelDateValue.split(subRegex_Day);
                var yearRegexValue = Regex.Split(value[0], subRegex_1_6_1)[0]; //value[0].split(subRegex_1_6_1)[0];
                var monthRegexValue = Regex.Split(value[0], subRegex_1_6_2)[1]; //value[0].split(subRegex_1_6_2)[1];


                //identify First Month, Last Month or current Month
                var tempM = Regex.Split(monthRegexValue, @"M")[1]; // monthRegexValue.split(/ M /)[1];
                var tempD = value[1];

                var tempY = Regex.Split(yearRegexValue, subRegex_Year)[1]; //yearRegexValue.split(subRegex_Year)[1];
                var finalYear = currYear;
                int finalMonth = currDate.Month;
                if (!string.IsNullOrEmpty(tempY))
                {
                    var yInputVal = Convert.ToInt32(tempY); //aqConvert.StrToInt(tempY);
                    finalYear += yInputVal;
                }
                DateTime newDate;
                if (!string.IsNullOrEmpty(tempM))
                {
                    newDate = currDate.AddMonths(int.Parse(tempM)); //aqDateTime.AddMonths(currDate, tempM);

                    finalYear = newDate.Year;//aqDateTime.GetYear(newDate);
                    finalMonth = newDate.Month;
                }
                var finalDay = GetDateDayValue(tempD);

                inputDateValue = new DateTime(finalYear, finalMonth, FD_Value); //aqDateTime.SetDateElements(finalYear, finalMonth, finalDay);
            }
            else if (Regex.IsMatch(excelDateValue, regex_1_7)) //excelDateValue.search(regex_1_7) == 0)
            {
                dateFormat = "yyyyMM"; //CONST.DATE_FORMAT_YYYYMM;

                var finalMonth = int.Parse(Regex.Split(excelDateValue, subRegex_Year)[1]); //excelDateValue.split(subRegex_Year)[1];

                var finalYear = currYear;

                inputDateValue = new DateTime(finalYear, finalMonth, FD_Value); //aqDateTime.SetDateElements(finalYear, finalMonth, FD_Value);
            }
            else if (Regex.IsMatch(excelDateValue, regex_1_8)) //excelDateValue.search(regex_1_8) == 0)
            {
                dateFormat = "yyyy";

                var tempY = Regex.Split(excelDateValue, @"YY")[1]; //excelDateValue.split(@"/YY/")[1];
                var yInputVal = 0;
                if (!string.IsNullOrEmpty(tempY))
                {
                    yInputVal = Convert.ToInt32(tempY); //aqConvert.StrToInt(tempY);
                }

                currYear += yInputVal;

                inputDateValue = new DateTime(currYear, FM_value, FD_Value); //aqDateTime.SetDateElements(currYear, FM_value, FD_Value);
            }
            else if (Regex.IsMatch(excelDateValue, regex_1_9)) //excelDateValue.search(regex_1_9) == 0)
            {
                var subRegex_1_9_1 = @"^YM(F|L)D";
                var splitedDateVal = Regex.Split(excelDateValue, subRegex_1_9_1); //excelDateValue.split(subRegex_1_9_1);
                var tempD = splitedDateVal[1];
                var daysVal = Convert.ToInt32(splitedDateVal[2]); //aqConvert.StrToInt(splitedDateVal[2])
                var finalYear = currYear;
                var finalMonth = currDate.Month; //aqDateTime.GetMonth(currDate);
                var finalDay = GetDateDayValue(tempD);

                var newDateValue = new DateTime(finalYear, finalMonth, finalDay); //aqDateTime.SetDateElements(finalYear, finalMonth, finalDay);

                inputDateValue = newDateValue.AddDays(daysVal); //aqDateTime.AddDays(newDateValue, daysVal);

            }

            finalValue = inputDateValue.ToString(dateFormat); //aqConvert.DateTimeToFormatStr(inputDateValue, dateFormat)
            Main.Reporter.Record_Message("Format " + excelDateValue + " : " + finalValue);

            return finalValue;
        }
        public DateTime GetDateLastMonth(DateTime inputDate)
        {
            var FM_value = 1;
            var FD_Value = 1;
            DateTime outputDate;
            var year = inputDate.Year + 1; //aqDateTime.GetYear(inputDate) + 1;
            outputDate = new DateTime(year, FM_value, FD_Value); //aqDateTime.SetDateElements(year, FM_value, FD_Value);
            outputDate = outputDate.AddDays(-1); //aqDateTime.AddDays(outputDate, -1);
            return outputDate;
        }
        public int GetDateMonthValue(string monthType)
        {
            int monthValue;
            var FM_value = 1;
            var currDate = DateTime.Today; //aqDateTime.Today();
            var currMonth = currDate.Month; //aqDateTime.GetMonth(currDate)
            switch (monthType)
            {
                case "F":
                    monthValue = FM_value;
                    break;
                case "L":
                    DateTime tempMonthValue = GetDateLastMonth(currDate);
                    monthValue = tempMonthValue.Month; //aqDateTime.GetMonth(tempMonthValue);
                    break;
                default:
                    monthValue = currMonth;
                    break;
            }
            return monthValue;
        }
        public int GetDateDayValue(string dayType = "")
        {

            var FD_Value = 1;
            var currDate = DateTime.Today; //aqDateTime.Today();
            var currDay = currDate.Day; //aqDateTime.GetDay(currDate);
            int dayValue;
            switch (dayType)
            {
                case "F":
                    dayValue = FD_Value;
                    break;
                case "L":
                    var nextMonth = currDate.AddMonths(1).Month; //aqDateTime.GetMonth(aqDateTime.AddMonths(currDate, 1));
                    var year = currDate.Year; //aqDateTime.GetYear(currDate);

                    var outputDate = new DateTime(year, nextMonth, FD_Value); //aqDateTime.SetDateElements(year, nextMonth, FD_Value);
                    outputDate = outputDate.AddDays(-1); //aqDateTime.AddDays(outputDate, -1);
                    dayValue = outputDate.Day; // aqDateTime.GetDay(outputDate);

                    break;
                default:
                    dayValue = currDay;
                    break;
            }
            return dayValue;

        }

        internal int GetTempFileLineCount(string fileName)
        {
            var pathToFile = Path.Combine(Global.TempFileDirectory, fileName);
            return File.ReadLines(pathToFile).Count();
        }

        public void WriteTempFile(string fileName, string textVal, bool isNewLine)
        {
            if (!Directory.Exists(Global.TempFileDirectory))
            {
                Directory.CreateDirectory(Global.TempFileDirectory);
            }
            var pathToFile = Path.Combine(Global.TempFileDirectory, fileName);
            if (!File.Exists(pathToFile))
            {
                File.AppendAllText(pathToFile, textVal);
                return;
            }

            var delimiter = Keys.TAB;
            if (isNewLine)
            {
                delimiter = Environment.NewLine;
            }
            File.AppendAllText(pathToFile, string.Format("{0}{1}", delimiter, textVal));

            Main.Reporter.Record_Message("Written memory data <" + textVal + "> to file <" + fileName + ">");
        }

        public string ReadTempFile(string fileName, int rowIndex, int colIndex)
        {
            var pathToFile = Path.Combine(Global.TempFileDirectory, fileName);

            string[][] cells = File.ReadLines(pathToFile).Select(x => x.Split(new[] {Keys.TAB}, StringSplitOptions.RemoveEmptyEntries).ToArray()).ToArray();

            return cells[rowIndex][colIndex];
        }

        public string GetFilePath()
        {
            return Global.TempFileDirectory;
        }
        public string GetFileName(string suffix = "")
        {
            string methodName = Global.CurrentMethodName;
            string className = Global.CurrentClassName;
            string fileName = string.Format("{0}.{1}{2}{3}", className, methodName, suffix, FileExtension.TEXT_FILE);
            return fileName;
        }


        public void SetInputDataSetCount(IWebDriver driver, EventHandler eH, SheetActions cSA, int INDEX, string pgmId, string workbookId, int rowCount, int seqVal)
        {
            IJavaScriptExecutor javaScriptExecutor = driver as IJavaScriptExecutor;
            var prefix = seqVal + "_" + workbookId + "_";
            Int64 colCount = ExecuteScript<Int64>(driver, cSA.Script_ColCount(workbookId));

            string val = string.Format("{0}{1},{2}{3}", prefix, rowCount, prefix, colCount);
            eH.SetWSMUpdateJson(INDEX, WorksheetMeta.DataSetCount, val, seqVal);
        }

        //Message Buttons Click
        public IWebElement ClickMesssageButton(IWebDriver driver, ButtonType buttonType)
        {
            IWebElement btnObj = null;
            string message = "";
            /*updated to click buttons for any type of popupmessagebox*/
            driver.SwitchTo().DefaultContent();

            MessagePopupMap messagePopupMap = new MessagePopupMap(driver);

            switch (buttonType)
            {
                case ButtonType.MSG_BTN_OK:
                    btnObj = messagePopupMap.okMsgBtn;
                    message = "Message button <Id:msgBtnOk> clicked";
                    break;
                case ButtonType.MSG_BTN_NO:
                    btnObj = messagePopupMap.noMsgBtn;
                    message = "Message button <Id:msgBtnNo> clicked";
                    break;
                case ButtonType.MSG_BTN_CANCEL:
                    btnObj = messagePopupMap.cancelMsgBtn;
                    message = "Message button <Id:msgBtnCancel> clicked";
                    break;
                default: break;
            }

            ClickEvent(driver, btnObj);
            //btnObj.Click();
            Main.Reporter.Record_Message(message);
            Delay();

            return btnObj;
        }

        public string GetMessageDescription(IWebDriver driver)
        {
            MessagePopupMap messagePopupMap = new MessagePopupMap(driver);

            return messagePopupMap.descriptionText.Text;
        }
        public string GetPgmSeqFrame(string pgmSeq)
        {
            return pgmSeq + "_iframe";
        }

        public void ClickJumpPgm(IWebDriver driver, EventHandler eH, string pgmId, string jumpColId, string jumpPgmId, int INDEX = -1)
        {
            string pgmSeqFrame = GetPgmSeqFrame(pgmId);
            string jumpPgmSeqFrame = GetPgmSeqFrame(jumpPgmId);

            ToolbarMap toolbarMap = new ToolbarMap(driver);
            ClickEvent(driver, toolbarMap.JumpPgmBtnBy(jumpColId));

            Main.Reporter.Record_Message("Clicked jump pgm<seq:" + pgmId + ">");

            var isPopUpVisible = Validations.IsPopupVisible(driver, "messagePopup");
            if (!isPopUpVisible)
            {
                Stopwatch stopWatch = new Stopwatch();
                driver.SwitchTo().DefaultContent();
                stopWatch.Start();

                Validations.is_loading_hidden(driver);

                stopWatch.Stop();
                if (!string.IsNullOrEmpty(jumpPgmId) && INDEX != -1)
                {
                    var isPgmOpened = Validations.is_pgm_openedSelected(driver, jumpPgmId);
                    driver.SwitchTo().Frame(driver.FindElement(By.Id(jumpPgmSeqFrame)));
                    if (isPgmOpened)
                    {
                        eH.SetWSMUpdateJson(INDEX, WorksheetMeta.PgmLoadingTime, GetSeconds(stopWatch));
                    }
                }
            }
        }

        public void ClosePgm(IWebDriver driver, string pgmSeq)
        {
            driver.SwitchTo().DefaultContent();

            HeaderMap headerMap = new HeaderMap(driver);
            var tabItem = headerMap.tabItemBy(pgmSeq);
            if (tabItem == null)
            {
                Main.Reporter.Record_Message("Pgm<" + pgmSeq + "> Already Closed.");
                return;
            }
            ClickEvent(driver, headerMap.closeTabBy(pgmSeq));
            Main.Reporter.Record_Message("Clicked close pgm<seq:" + pgmSeq + ">");
            Delay();
            MessagePopupMap messagePopupMap = new MessagePopupMap(driver);
            if (messagePopupMap.messageBox.Displayed)
            {
                ClickMesssageButton(driver, ButtonType.MSG_BTN_OK);
            }
            Main.Reporter.Record_Message("Pgm<" + pgmSeq + "> Already Closed.");


        }



        public void OpenPgm(IWebDriver driver, EventHandler eH, string pgmSeq, int INDEX = -1)
        {
            driver.SwitchTo().DefaultContent();
            WaitMethodUsingId(driver, "btnProgramQueryArea");

            HeaderMap headerMap = new HeaderMap(driver);
            HoverEvent(driver, headerMap.programQueryBtn);
            Delay();
            var inputVal = pgmSeq + OpenQA.Selenium.Keys.Enter;
            InputEvent(driver, headerMap.programQueryText, inputVal);
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            Validations.is_loading_hidden(driver);
            Main.Reporter.Record_Message("Opened pgm<seq:" + pgmSeq + ">");

            stopWatch.Stop();
            if (INDEX != -1)
            {
                eH.SetWSMUpdateJson(INDEX, WorksheetMeta.PgmLoadingTime, GetSeconds(stopWatch));
            }
        }

        public string GetSeconds(Stopwatch stopWatch)
        {
            return ((float)stopWatch.ElapsedMilliseconds / 1000).ToString();
        }

        public void NewPgm(IWebDriver driver, string pgmSeq)
        {
            string expressionVal = ".//img[@src ='/Images/Toolbar/T_New.png']";
            WaitMethodUsingXPath(driver, expressionVal);
            ToolbarMap toolbarMap = new ToolbarMap(driver);
            ClickEvent(driver, toolbarMap.NewPgmBtn);
            Main.Reporter.Record_Message("Clicked new pgm<seq:" + pgmSeq + ">");

        }
        public void QueryPgm(IWebDriver driver, EventHandler eH, string pgmSeq, int INDEX = -1, string workbookId = "", bool hasTotalRow = false, int seqVal = -1)
        {
            string expressionVal = ".//img[@src ='/Images/Toolbar/T_Query.png']";
            WaitMethodUsingXPath(driver, expressionVal);
            ToolbarMap toolbarMap = new ToolbarMap(driver);
            ClickEvent(driver, toolbarMap.QueryPgmBtn);
            Main.Reporter.Record_Message("Clicked query pgm<seq:" + pgmSeq + ">");
            if (INDEX != -1)
            {
                Stopwatch stopWatch = new Stopwatch();
                stopWatch.Start();
                Validations.is_loading_hidden(driver);

                stopWatch.Stop();

                eH.SetWSMUpdateJson(INDEX, WorksheetMeta.InquiryTime, GetSeconds(stopWatch));
                eH.SetQueryDataSetCount(driver, INDEX, pgmSeq, workbookId, hasTotalRow, seqVal);
            }
            driver.SwitchTo().DefaultContent();
            string pgmSeqFrame = GetPgmSeqFrame(pgmSeq);
            driver.SwitchTo().Frame(driver.FindElement(By.Id(pgmSeqFrame)));
            Delay();
        }

        public void SavePgm(IWebDriver driver, EventHandler eH, string pgmSeq, string subFormId = null, int INDEX = -1)
        {
            string expressionVal = ".//img[@src ='/Images/Toolbar/T_Save.png']";
            WaitMethodUsingXPath(driver, expressionVal);
            ToolbarMap toolbarMap = new ToolbarMap(driver);
            ClickEvent(driver, toolbarMap.SavePgmBtn);
            Main.Reporter.Record_Message("Clicked save pgm<seq:" + pgmSeq + ">");
            if (INDEX != -1)
            {
                Stopwatch stopWatch = new Stopwatch();
                stopWatch.Start();
                Validations.is_loading_hidden(driver);

                stopWatch.Stop();

                eH.SetWSMUpdateJson(INDEX, WorksheetMeta.SaveTime, GetSeconds(stopWatch));
            }
        }

        public void WaitMethodUsingXPath(IWebDriver driver, string expression, double timeSpanVal = 15)
        {
            var wait2 = new WebDriverWait(driver, TimeSpan.FromSeconds(timeSpanVal));
            wait2.Until(d => d.FindElement(By.XPath(expression)));
        }
        public void WaitMethodUsingId(IWebDriver driver, string idVal, double timeSpanVal = 15)
        {
            var wait2 = new WebDriverWait(driver, TimeSpan.FromSeconds(timeSpanVal));
            wait2.Until(d => d.FindElement(By.Id(idVal)));
        }


        public void SetPgmControlData(IWebDriver driver, string fieldId, string value)
        {
            WaitMethodUsingId(driver, fieldId);

            var fieldObj = driver.FindElement(By.Id(fieldId));
            string script = string.Empty;

            if (fieldObj.TagName == "input" || fieldObj.TagName == "textarea")
            {
                if (!string.IsNullOrEmpty(value))
                {
                    Actions inputAction = new Actions(driver);
                    inputAction.MoveToElement(fieldObj).Click(fieldObj).Build().Perform();
                    inputAction.KeyDown(OpenQA.Selenium.Keys.Control).SendKeys("a").KeyUp(OpenQA.Selenium.Keys.Control).SendKeys(OpenQA.Selenium.Keys.Backspace).Build().Perform();
                    //inputAction.SendKeys(Keys.Control + "a" + Keys.Null).SendKeys(Keys.Backspace).Build().Perform();
                    inputAction.SendKeys(value);
                    inputAction.SendKeys(OpenQA.Selenium.Keys.Tab).Build().Perform();
                }

            }
            else if (fieldObj.TagName == "div")
            {

                script = "return $('#" + fieldId + "').hasClass('check');";

                bool isCheckbox = ExecuteScript<bool>(driver, script);
                if (isCheckbox)
                {
                    script = "return $('#" + fieldId + "').hasClass('Checked').toString();";
                    value = ExecuteScript<string>(driver, script);
                }
                else
                {
                    script = "return $('#" + fieldId + "').hasClass('comboBox');";
                    bool isCombobox = ExecuteScript<bool>(driver, script);
                    if (isCombobox)
                    {
                        fieldObj.Click();
                        var itemObj = fieldObj.FindElement(By.XPath(".//option[@value='" + value + "']"));
                        itemObj.Click();
                    }
                }
            }
            Main.Reporter.Record_Message("Set control<id:" + fieldId + "> value<value:" + value + ">");
        }

        public string GetControlValue(IWebDriver driver, string pgmSeq, string fieldId)
        {

            string script, value;
            script = value = string.Empty;
            dynamic fieldObj;
            if (!string.IsNullOrEmpty(pgmSeq))
            {
                driver.SwitchTo().DefaultContent();
                string pgmSeqFrame = GetPgmSeqFrame(pgmSeq);
                driver.SwitchTo().Frame(driver.FindElement(By.Id(pgmSeqFrame)));
            }
            fieldObj = driver.FindElement(By.Id(fieldId));
            IJavaScriptExecutor jsExecutor = driver as IJavaScriptExecutor;
            if (fieldObj.TagName == "input")
            {
                script = "return $('#" + fieldId + "').val();";
                value = ExecuteScript<string>(driver, script);
            }
            else if (fieldObj.TagName == "div") //logic to get value of checkbox control state
            {
                script = "return $('#" + fieldId + "').hasClass('check');";
                bool isCheckbox = ExecuteScript<bool>(driver, script);
                if (isCheckbox)
                {
                    script = "return $('#" + fieldId + "').hasClass('Checked').toString();";
                    value = ExecuteScript<string>(driver, script);
                }
            }

            return value;

        }

        public void SelectPgmControl(IWebDriver driver, EventHandler eH, string pgmSeq, string fieldId, bool isDblClick = false, string jumpPgmId = "", string INDEX = null)
        {
            IJavaScriptExecutor jsExecutor = driver as IJavaScriptExecutor;
            driver.SwitchTo().DefaultContent();
            if (!string.IsNullOrEmpty(pgmSeq))
            {
                string pgmSeqFrame = GetPgmSeqFrame(pgmSeq);
                driver.SwitchTo().Frame(driver.FindElement(By.Id(pgmSeqFrame)));
            }


            var fieldObj = driver.FindElement(By.Id(fieldId));
            if (isDblClick)
            {
                DblClickEvent(driver, fieldObj);
            }
            else
            {
                ClickEvent(driver, fieldObj);
            }
            Main.Reporter.Record_Message("Selected control<id:" + fieldId + ">");

            if (!string.IsNullOrEmpty(jumpPgmId))
            {
                /*Updated recording PgmLoadingTime*/
                Stopwatch stopWatch = new Stopwatch();
                stopWatch.Start();
                Validations.is_loading_hidden(driver); //cV.is_loading_hidden("Popup Program control is clicked.");
                stopWatch.Stop();
                var isPgmOpened = Validations.is_pgm_openedSelected(driver, jumpPgmId);
                if (isPgmOpened)
                {
                    int indexInt = int.Parse(INDEX);
                    eH.SetWSMUpdateJson(indexInt, WorksheetMeta.PgmLoadingTime, GetSeconds(stopWatch));
                }
            }

        }


        //Read control dataList and input the value in corresponding inputfield
        public void ReadPgmControlDataList(IWebDriver driver, string pgmSeq, List<List<string>> dataList)
        {
            var colList = dataList[0];
            var pgmControlId = string.Empty;
            var pgmControlVal = string.Empty;
            var dateType = "Date_";

            for (var i = 1; i < dataList.Count; i++)
            {
                for (var j = 0; j < colList.Count; j++)
                {
                    pgmControlId = colList[j];
                    pgmControlVal = dataList[i][j];

                    /*Set/Clear values on control input*/
                    if (!string.IsNullOrEmpty(pgmControlVal))
                    {
                        if (pgmControlVal == "NULL")
                        {
                            pgmControlVal = string.Empty;
                        }
                        if (pgmControlVal.Contains(dateType))
                        {
                            var excelDateValue = pgmControlVal.Substring(dateType.Length);
                            pgmControlVal = GetDateUsingExcel(excelDateValue);
                        }
                        TestContext.WriteLine("Entered  the " + pgmControlVal + " On inputFile:" + pgmControlId);
                        Main.Reporter.Record_Message(string.Format("Entered  the {0} On inputFile: {1}.", pgmControlVal, pgmControlId));
                        SetPgmControlData(driver, pgmControlId, pgmControlVal);
                    }
                }
            }
        }
        public string SnapScreenshot(IWebDriver driver)
        {
            var path = Global.ImageLogDirectory;
            string timeanddate = System.DateTime.Now.ToString("yyyymmddhhmmss");
            DirectoryInfo DirectPath = new DirectoryInfo(path);
            if (!DirectPath.Exists)
            {
                DirectPath.Create();
            }
            ITakesScreenshot takesScreenshot = driver as ITakesScreenshot;
            Screenshot ss = takesScreenshot.GetScreenshot();
            string filePath = Path.Combine(path, timeanddate + FileExtension.IMAGE_FILE);
            ss.SaveAsFile(filePath, ScreenshotImageFormat.Jpeg);
            Main.Reporter.Record_Message("Saved screen snapshot");
            return timeanddate + FileExtension.IMAGE_FILE;
        }

        //select TreeMenu Nodes
        public void SelectTreeMenuNodes(IWebDriver driver, string pgmSeq, string treeId, List<string> nodeSeqList)
        {
            string pgmSeqFrame = GetPgmSeqFrame(pgmSeq);

            IWebElement frameObj = driver.FindElement(By.Id(pgmSeqFrame));
            driver.SwitchTo().Frame(frameObj);

            WaitMethodUsingId(driver, treeId);

            ContentsMap contentsMap = new ContentsMap(driver);
            contentsMap.TreeOpenBtnBy(treeId).Click();

            string seq;
            IWebElement nodeObj;
            for (var i = 0; i < nodeSeqList.Count; i++)
            {
                seq = nodeSeqList[i];
                nodeObj = contentsMap.TreeNodeObjBy(treeId, seq);
                nodeObj.Click();
                Main.Reporter.Record_Message("Selected treemenu node<seq:" + seq + ">");
            }
            Validations.is_loading_hidden(driver);

            driver.SwitchTo().DefaultContent();
        }

        internal void SelectSpanControl(IWebDriver driver, string pgmSeq, string parentId, string fieldClassStr)
        {
            driver.SwitchTo().DefaultContent();
            IWebElement parentObj = null;
            IWebElement fieldObj = null;
            if (!string.IsNullOrEmpty(pgmSeq))
            {
                string pgmSeqFrame = GetPgmSeqFrame(pgmSeq);
                driver.SwitchTo().Frame(driver.FindElement(By.Id(pgmSeqFrame)));
            }
            parentObj = driver.FindElement(By.CssSelector("#" + parentId));
            fieldObj = parentObj.FindElement(By.CssSelector("#" + parentId + " " + fieldClassStr));
            int offSetLeftVal = int.Parse(fieldObj.GetAttribute("offsetLeft"));
            int offsetWidthVal = int.Parse(fieldObj.GetAttribute("offsetWidth"));
            int offsetTopVal = int.Parse(fieldObj.GetAttribute("offsetTop"));
            int offsetHeightVal = int.Parse(fieldObj.GetAttribute("offsetHeight"));
            int x = offSetLeftVal + offsetWidthVal / 2;
            int y = offsetTopVal + offsetHeightVal / 2;

            ClickEvent(driver, parentObj, x, y);


        }

        internal void ClickExpander(IWebDriver driver, string pgmSeq, string expanderId)
        {
            driver.SwitchTo().DefaultContent();
            if (!string.IsNullOrEmpty(pgmSeq))
            {
                string pgmSeqFrame = GetPgmSeqFrame(pgmSeq);
                driver.SwitchTo().Frame(driver.FindElement(By.Id(pgmSeqFrame)));
            }
            IWebElement fieldObj = null;
            do
            {
                fieldObj = driver.FindElement(By.Id(expanderId));
            } while (fieldObj != null);

            fieldObj.Click();
        }

        internal string GetSheetErrMessageDescription(IWebDriver driver)
        {
            string innerTextVal = driver.FindElement(By.Id("sheetErrMessageDescription")).GetAttribute("innerText");
            return innerTextVal;
        }

        internal void BringPgmFront(IWebDriver driver, string pgmSeq)
        {
            IWebElement pgmTabLabelObj = driver.FindElement(By.CssSelector("li[pgmseq='" + pgmSeq + "'] a"));
            if(pgmTabLabelObj == null)
            {
                Main.Reporter.Record_Message("PgmSeq<" + pgmSeq + "> Is Not Selected.");
            } else
            {
                pgmTabLabelObj.Click();
                Main.Reporter.Record_Message("PgmSeq<" + pgmSeq + "> Is Selected.");
            }
        }

        internal string GetMessageTitle(IWebDriver driver, string fieldId)
        {
            string innerTextVal = driver.FindElement(By.CssSelector("#" + fieldId + " .txtTitle")).GetAttribute("innerText");
            return innerTextVal;
        }
    }
}
