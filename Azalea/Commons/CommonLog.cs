﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Azalea.Commons
{
    class CommonLog
    {


        LogReport CLR = new LogReport();


        //Object/Control is visible
        public void is_visible(bool state, string objName)
        {
            string message = "";
            if (state)
            {
                message = "<" + objName + "> Is Visible.";
                Main.Reporter.Record_Checkpoint(message);
            }
            else
            {
                message = "<" + objName + "> Is Not Visible.";
                Main.Reporter.Record_Warning(message);
            }
            CLR.SetSubLogRecord(message, state);
        }

        //Object/Control is invisible
        public void is_invisible(bool state, string objName)
        {
            string message = "";
            if (state)
            {
                message = "<" + objName + "> Is Invisible.";
                Main.Reporter.Record_Checkpoint(message);
            }
            else
            {
                message = "<" + objName + "> Is Not Invisible.";
                Main.Reporter.Record_Warning(message);
            }
            CLR.SetSubLogRecord(message, state);
        }

        //Object/Control is opened
        public void is_opened(bool state, string objName)
        {
            string message = "";
            if (state)
            {
                message = "<" + objName + "> Is Opened.";
                Main.Reporter.Record_Checkpoint(message);
            }
            else
            {
                message = "<" + objName + "> Is Not Opened.";
                Main.Reporter.Record_Warning(message);
            }
            CLR.SetSubLogRecord(message, state);
        }

        //Object/Control is closed
        public void is_closed(bool state, string objName)
        {
            string message = "";
            if (state)
            {
                message = "<" + objName + "> Is Closed.";
                Main.Reporter.Record_Checkpoint(message);
            }
            else
            {
                message = "<" + objName + "> Is Not Closed.";
                Main.Reporter.Record_Warning(message);
            }
            CLR.SetSubLogRecord(message, state);
        }

        //Object/Conrol is expanded
        public void is_expanded(bool state, string objName)
        {
            string message = "";
            if (state)
            {
                message = "<" + objName + "> Is Expanded.";
                Main.Reporter.Record_Checkpoint(message);
            }
            else
            {
                message = "<" + objName + "> Is Not Expanded.";
                Main.Reporter.Record_Warning(message);
            }
            CLR.SetSubLogRecord(message, state);
        }

        //Object/Control fore color match
        public void match_forecolor(bool state, string objName, string color)
        {
            string message = "";
            if (state)
            {
                message = "<" + objName + "> Text Color Is <" + color + ">.";
                Main.Reporter.Record_Checkpoint(message);
            }
            else
            {
                message = "<" + objName + "> Text Color Is Not <" + color + ">.";
                Main.Reporter.Record_Warning(message);
            }
            CLR.SetSubLogRecord(message, state);
        }

        //Object/Control fore color not match
        public void not_match_forecolor(bool state, string objName, string color)
        {
            string message = "";
            if (state)
            {
                message = "<" + objName + "> Text Color Is Not <" + color + ">.";
                Main.Reporter.Record_Checkpoint(message);
            }
            else
            {
                message = "<" + objName + "> Text Color Is <" + color + ">.";
                Main.Reporter.Record_Warning(message);
            }
            CLR.SetSubLogRecord(message, state);
        }

        //Object/Control values match
        public void match_controlValue(bool state, string objName, string value)
        {
            string message = "";
            if (state)
            {
                message = "<" + objName + "> Text Value Matched With <" + value + ">.";
                Main.Reporter.Record_Checkpoint(message);
            }
            else
            {
                message = "<" + objName + "> Text Value Not Matched With <" + value + ">.";
                Main.Reporter.Record_Warning(message);
            }
            CLR.SetSubLogRecord(message, state);
        }

        //Object/Control values not match
        public void notMatch_controlValue(bool state, string objName, string value)
        {
            string message = "";
            if (state)
            {
                message = "<" + objName + "> Text Value Not Matched With <" + value + ">.";
                Main.Reporter.Record_Checkpoint(message);
            }
            else
            {
                message = "<" + objName + "> Text Value Matched With <" + value + ">.";
                Main.Reporter.Record_Warning(message);
            }
            CLR.SetSubLogRecord(message, state);
        }

        //Object/Control property-value match
        public void match_control_propertyValue(bool state, string objName, string propName, string propValue)
        {
            string message = "";
            if (state)
            {
                message = "<" + objName + "> Property<" + propName + "> Value Matched With <" + propValue + ">.";
                Main.Reporter.Record_Checkpoint(message);
            }
            else
            {
                message = "<" + objName + "> Property<" + propName + "> Value Not Matched With <" + propValue + ">.";
                Main.Reporter.Record_Warning(message);
            }
            CLR.SetSubLogRecord(message, state);
        }

        //Object/Control values length match
        public void match_controlValue_length(bool state, string objName, int length)
        {
            string message = "";
            if (state)
            {
                message = "<" + objName + "> Text Value Length Matched With <" + length + ">.";
                Main.Reporter.Record_Checkpoint(message);
            }
            else
            {
                message = "<" + objName + "> Text Value Length Not Matched With <" + length + ">.";
                Main.Reporter.Record_Warning(message);
            }
            CLR.SetSubLogRecord(message, state);
        }

        //Object/Control is focused
        public void is_controlFocus(bool state, string objName)
        {
            string message = "";
            if (state)
            {
                message = "<" + objName + "> Is Focused.";
                Main.Reporter.Record_Checkpoint(message);
            }
            else
            {
                message = "<" + objName + "> Is Not Focused.";
                Main.Reporter.Record_Warning(message);
            }
            CLR.SetSubLogRecord(message, state);
        }

        //Cursor line match
        public void match_cursorLine(bool state, int line)
        {
            string message = "";
            if (state)
            {
                message = "Cursor Is At Line<" + line + ">.";
                Main.Reporter.Record_Checkpoint(message);
            }
            else
            {
                message = "Cursor Is Not At Line<" + line + ">.";
                Main.Reporter.Record_Warning(message);
            }
            CLR.SetSubLogRecord(message, state);
        }

        //Cursor position match
        public void match_cursorPosition(bool state, string objName, int position)
        {
            string message = "";
            if (state)
            {
                message = "Cursor Is At Position<" + position + "> In <" + objName + ">.";
                Main.Reporter.Record_Checkpoint(message);
            }
            else
            {
                message = "Cursor Is Not At Position<" + position + "> In <" + objName + ">.";
                Main.Reporter.Record_Warning(message);
            }
            CLR.SetSubLogRecord(message, state);
        }

        //Cursor state match
        public void match_cursorName(bool state, string cursorName)
        {
            string message = "";
            if (state)
            {
                message = "Current Cursor State Is <" + cursorName + ">.";
                Main.Reporter.Record_Checkpoint(message);
            }
            else
            {
                message = "Current Cursor State Is Not <" + cursorName + ">.";
                Main.Reporter.Record_Warning(message);
            }
            CLR.SetSubLogRecord(message, state);
        }

        //Object/Control is activated
        public void is_activated(bool state, string objName)
        {
            string message = "";
            if (state)
            {
                message = "<" + objName + "> Is Activated.";
                Main.Reporter.Record_Checkpoint(message);
            }
            else
            {
                message = "<" + objName + "> Is Not Activated.";
                Main.Reporter.Record_Warning(message);
            }
            CLR.SetSubLogRecord(message, state);
        }

        //Object/Control is deactivated
        public void is_deactivated(bool state, string objName)
        {
            string message = "";
            if (state)
            {
                message = "<" + objName + "> Is Deactivated.";
                Main.Reporter.Record_Checkpoint(message);
            }
            else
            {
                message = "<" + objName + "> Is Not Deactivated.";
                Main.Reporter.Record_Warning(message);
            }
            CLR.SetSubLogRecord(message, state);
        }

        //Object/Control is loaded
        public void is_loaded(bool state, string objName)
        {
            string message = "";
            if (state)
            {
                message = "<" + objName + "> Is Loaded.";
                Main.Reporter.Record_Checkpoint(message);
            }
            else
            {
                message = "<" + objName + "> Is Not Loaded.";
                Main.Reporter.Record_Warning(message);
            }
            CLR.SetSubLogRecord(message, state);
        }

        //Operation failed
        public void is_failed(bool state, string opName)
        {
            string message = "";
            if (state)
            {
                message = "<" + opName + "> Operation Failed.";
                Main.Reporter.Record_Checkpoint(message);
            }
            else
            {
                message = "<" + opName + "> Operation Not Failed.";
                Main.Reporter.Record_Warning(message);
            }
            CLR.SetSubLogRecord(message, state);
        }

        //Operation success
        public void is_success(bool state, string opName)
        {
            string message = "";
            if (state)
            {
                message = "<" + opName + "> Operation Success.";
                Main.Reporter.Record_Checkpoint(message);
            }
            else
            {
                message = "<" + opName + "> Operation Not Success.";
                Main.Reporter.Record_Warning(message);
            }
            CLR.SetSubLogRecord(message, state);
        }

        //Have class
        public void have_class(bool state, string objName, string className)
        {
            string message = "";
            if (state)
            {
                message = "<" + objName + "> Have Class<" + className + ">.";
                Main.Reporter.Record_Checkpoint(message);
            }
            else
            {
                message = "<" + objName + "> Does Not Have Class<" + className + ">.";
                Main.Reporter.Record_Warning(message);
            }
            CLR.SetSubLogRecord(message, state);
        }

        //Object/Control is selected
        public void is_selected(bool state, string objName)
        {
            string message = "";
            if (state)
            {
                message = "<" + objName + "> Is Selected.";
                Main.Reporter.Record_Checkpoint(message);
            }
            else
            {
                message = "<" + objName + "> Is Not Selected.";
                Main.Reporter.Record_Warning(message);
            }
            CLR.SetSubLogRecord(message, state);
        }

        //PgmSeq in list are existed
        public void is_pgmseq_exists_in_list(bool state, string pgmSeqList, string listName)
        {
            string message = "";
            if (state)
            {
                message = "PgmSeq<" + pgmSeqList + "> Existed In <" + listName + ">.";
                Main.Reporter.Record_Checkpoint(message);
            }
            else
            {
                message = "PgmSeq<" + pgmSeqList + "> Not Existed In <" + listName + ">.";
                Main.Reporter.Record_Warning(message);
            }
            CLR.SetSubLogRecord(message, state);
        }

        //PgmSeq in list are not existed
        public void is_pgmseq_notExists_in_list(bool state, string pgmSeqList, string listName)
        {
            string message = "";
            if (state)
            {
                message = "PgmSeq<" + pgmSeqList + "> Not Existed In <" + listName + ">.";
                Main.Reporter.Record_Checkpoint(message);
            }
            else
            {
                message = "PgmSeq<" + pgmSeqList + "> Existed In <" + listName + ">.";
                Main.Reporter.Record_Warning(message);
            }
            CLR.SetSubLogRecord(message, state);
        }

        //PgmSeq in list are in ordered
        public void is_pgmseq_ordered_in_list(bool state, string pgmSeqList, string listName)
        {
            string message = "";
            if (state)
            {
                message = "PgmSeq<" + pgmSeqList + "> Are In Ordered In <" + listName + ">.";
                Main.Reporter.Record_Checkpoint(message);
            }
            else
            {
                message = "PgmSeq<" + pgmSeqList + "> Are Not In Ordered In <" + listName + ">.";
                Main.Reporter.Record_Warning(message);
            }
            CLR.SetSubLogRecord(message, state);
        }

        //Pgm is opened
        public void is_pgm_opened(bool state, string pgmSeq)
        {
            string message = "";
            if (state)
            {
                message = "Pgm<" + pgmSeq + "> Is Opened.";
                Main.Reporter.Record_Checkpoint(message);
            }
            else
            {
                message = "Pgm<" + pgmSeq + "> Is Not Opened.";
                Main.Reporter.Record_Warning(message);
            }
            CLR.SetSubLogRecord(message, state);
        }

        //Pgm is selected
        public void is_pgm_selected(bool state, string pgmSeq)
        {
            string message = "";
            if (state)
            {
                message = "Pgm<" + pgmSeq + "> Is Selected.";
                Main.Reporter.Record_Checkpoint(message);
            }
            else
            {
                message = "Pgm<" + pgmSeq + "> Is Not Selected.";
                Main.Reporter.Record_Warning(message);
            }
            CLR.SetSubLogRecord(message, state);
        }

        //Pgm is closed
        public void is_pgm_closed(bool state, string pgmSeq)
        {
            string message = "";
            if (state)
            {
                message = "Pgm<" + pgmSeq + "> Is Closed.";
                Main.Reporter.Record_Checkpoint(message);
            }
            else
            {
                message = "Pgm<" + pgmSeq + "> Is Not Closed.";
                Main.Reporter.Record_Warning(message);
            }
            CLR.SetSubLogRecord(message, state);
        }

        //ItemsCount in Object/Control
        public void have_itemsCount(bool state, string objName, int count, string itemName)
        {
            string message = "";
            if (state)
            {
                message = "<" + objName + "> Have <" + count + "> " + itemName + ".";
                Main.Reporter.Record_Checkpoint(message);
            }
            else
            {
                message = "<" + objName + "> Have No <" + count + "> " + itemName + ".";
                Main.Reporter.Record_Warning(message);
            }
            CLR.SetSubLogRecord(message, state);
        }

        //Items in Object/Control
        public void have_items(bool state, string objName, string itemName)
        {
            string message = "";
            if (state)
            {
                message = "<" + objName + "> Have " + itemName + ".";
                Main.Reporter.Record_Checkpoint(message);
            }
            else
            {
                message = "<" + objName + "> Have No " + itemName + ".";
                Main.Reporter.Record_Warning(message);
            }
            CLR.SetSubLogRecord(message, state);
        }

        //Items in Object/Control are empty
        public void have_empty_Items(bool state, string objName)
        {
            string message = "";
            if (state)
            {
                message = "<" + objName + "> Have No Items.";
                Main.Reporter.Record_Checkpoint(message);
            }
            else
            {
                message = "<" + objName + "> Have Items.";
                Main.Reporter.Record_Warning(message);
            }
            CLR.SetSubLogRecord(message, state);
        }

        //All links are collapsed
        public void is_allLinks_collapsed(bool state)
        {
            string message = "";
            if (state)
            {
                message = "All Links Are Collapsed.";
                Main.Reporter.Record_Checkpoint(message);
            }
            else
            {
                message = "All Links Are Not Collapsed.";
                Main.Reporter.Record_Warning(message);
            }
            CLR.SetSubLogRecord(message, state);
        }

        //ModuleMenuItem position index match
        public void match_moduleMenuItem_index(bool state, string seq, int index)
        {
            string message = "";
            if (state)
            {
                message = "ModuleMenuItem Seq<" + seq + "> Is Positioned At Index<" + index + ">.";
                Main.Reporter.Record_Checkpoint(message);
            }
            else
            {
                message = "ModuleMenuItem Seq<" + seq + "> Is Not Positioned At Index<" + index + ">.";
                Main.Reporter.Record_Warning(message);
            }
            CLR.SetSubLogRecord(message, state);
        }

        //ModuleMenuItem position index not match
        public void notMatch_moduleMenuItem_index(bool state, string seq, int index)
        {
            string message = "";
            if (state)
            {
                message = "ModuleMenuItem Seq<" + seq + "> Is Not Positioned At Index<" + index + ">.";
                Main.Reporter.Record_Checkpoint(message);
            }
            else
            {
                message = "ModuleMenuItem Seq<" + seq + "> Is Positioned At Index<" + index + ">.";
                Main.Reporter.Record_Warning(message);
            }
            CLR.SetSubLogRecord(message, state);
        }

        //combo_optionCount_value match
        public void match_combo_optionCount_value(bool state, string objName, int optionCount, string value)
        {
            string optionStr = "";
            if (optionCount == 0)
            {
                optionStr = "Last";
            }
            else if (optionCount == 1)
            {
                optionStr = "First";
            }
            else
            {
                optionStr = "<" + optionCount + ">";
            }
            
            string message = "";
            if (state)
            {
                message = "<" + objName + "> " + optionStr + " Option Value Matched With <" + value + ">.";
                Main.Reporter.Record_Checkpoint(message);
            }
            else
            {
                message = "<" + objName + "> " + optionStr + " Option Value Matched With <" + value + ">.";
                Main.Reporter.Record_Warning(message);
            }
            CLR.SetSubLogRecord(message, state);
        }

        //Object/Control/Value is in between
        public void is_in_between(bool state, string objName, string condition)
        {
            string message = "";
            if (state)
            {
                message = "<" + objName + "> Lies In Between <" + condition + ">.";
                Main.Reporter.Record_Checkpoint(message);
            }
            else
            {
                message = "<" + objName + "> Lies In Between <" + condition + ">.";
                Main.Reporter.Record_Warning(message);
            }
            CLR.SetSubLogRecord(message, state);
        }
    }
}
