﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Azalea
{
    static class RowDataType
    {
        public const string ADDED_ROWS = "A";
        public const string DELETED_ROWS = "D";
        public const string UPDATED_ROWS = "U";
        public const string SAVED_ROWS = "S";
        public const string DIRTY_ROWS = "X";
    }

    static class DateFormat
    {
        public const string DATE_FORMAT_YYYY = "yyyy";
        public const string DATE_FORMAT_YYYYMM = "yyyyMM";
        public const string DATE_FORMAT_YYYYMMDD = "yyyyMMdd";
    }

    static class SheetCellType
    {
        public const string TEXT_CELL = "1";
        public const string CHECK_CELL = "5";
        public const string COMBO_CELL = "7";
    }
    static class Condition
    {
        public const string EQUALSTO = "=";
        public const string GREATERTHAN = ">";
        public const string SMALLERTHAN = "<";
    }

    static class WorkSheet
    {
        public const string GENERAL = "WorkSheet_Meta";
        public const string QUERY = "Query";
        public const string INPUT = "Input";
        public const string SYSTEM = "System Log";
        public const string IMAGE = "Image Log";

    }

    static class Keys
    {
        public const string TAB = "\t";
        public const string SUCCESS = "O";

    }

    static class FileExtension
    {
        public const string TEXT_FILE = ".txt";
        public const string IMAGE_FILE = ".jpg";

    }


    /*
        static class ButtonType
        {
            public const string MSG_BTN_OK = "OK";
            public const string MSG_BTN_NO = "NO";
            public const string MSG_BTN_CANCEL = "CANCEL";
        }
        */

    public enum ButtonType
    {
        MSG_BTN_OK,
        MSG_BTN_NO,
        MSG_BTN_CANCEL
    }

    public enum WorksheetMeta
    {
        Success,
        Execute,
        StartTime,
        TimeDiff,
        Log,
        PgmLoadingTime,
        InquiryTime,
        SaveTime,
        JumpDataSetCount,
        JumpSheetRowsCount,
        DataSetCount,
        SheetRowsCount,
        SheetColsCount,
        ImageLog
    }

    static class ReportLogItemType
    {
        public const string ERROR = "Failed";
        public const string WARNING = "Warning";
        public const string SUCCESS = "Passed";
        public const string MESSAGE = "Message";
        public const string EVENT = "Event";
        public const string CHECKPOINT = "Checkpoint";
    }

    static class ActionType
    {
        public const string CLICK = "Click";
        public const string DBLCLICK = "DblClick";
        public const string INPUT = "Input";
    }

    static class ExcelLog {
        public const int ColumnCount = 14;
    }

}
