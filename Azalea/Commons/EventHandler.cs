﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Azalea
{
    internal class EventHandler
    {
        private CommonActions cA = new CommonActions();
        private SheetActions cSA = new SheetActions();


        public static DateTime[] timeArr = new DateTime[52];
        public static int TIME_IDX = 0;
        public static List<int> idxArr = new List<int>();
        public static int IDX = 0;
        public static int[,] temp_JumpDataSetCount = new int[52, 14];
        public static int[,] temp_DataSetCount = new int[52, 14];
        //Set Execute/StartTime In WSM_UpdateJson

        public void ResetEventVars(int workerSize)
        {
            EventHandler.timeArr = new DateTime[workerSize];
            EventHandler.TIME_IDX = 0;
            EventHandler.idxArr = new List<int>();
            EventHandler.IDX = 0;
            EventHandler.temp_JumpDataSetCount = new int[workerSize, ExcelLog.ColumnCount];
            EventHandler.temp_DataSetCount = new int[workerSize, ExcelLog.ColumnCount];
        }

        public void SetWSMUpdateJson(int INDEX, WorksheetMeta updateCol, string value = "", int seqVal = -1)
        {

            int updateColInt = (int)updateCol;
            switch (updateCol)
            {
                case WorksheetMeta.Success:
                    Global.WSM_UpdateData[INDEX - 1, updateColInt] = value;
                    break;
                case WorksheetMeta.Log:
                case WorksheetMeta.PgmLoadingTime:
                case WorksheetMeta.InquiryTime:
                case WorksheetMeta.SaveTime:
                    
                    if (String.IsNullOrEmpty(Global.WSM_UpdateData[INDEX - 1, updateColInt]))
                    {
                        Global.WSM_UpdateData[INDEX - 1, updateColInt] = value;
                    }
                    else
                    {
                        Global.WSM_UpdateData[INDEX - 1, updateColInt] = String.Concat(Global.WSM_UpdateData[INDEX - 1, updateColInt], "\r\n") + value;
                    }
                    break;
                case WorksheetMeta.JumpDataSetCount:
                    var rowColCountArr = value.ToString().Split(',');
                    var currJumpDataSetCountStr = Global.WSM_UpdateData[INDEX - 1, updateColInt];
                    int currJumpDataSetCount = -1;
                    if (!string.IsNullOrEmpty(currJumpDataSetCountStr))
                    {
                        currJumpDataSetCount = 1;
                        Global.WSM_UpdateData[INDEX - 1, updateColInt] = currJumpDataSetCount.ToString();
                        Global.WSM_UpdateData[INDEX - 1, (int)WorksheetMeta.JumpDataSetCount] = currJumpDataSetCount + "_" + rowColCountArr[0];
                        Global.WSM_UpdateData[INDEX - 1, (int)WorksheetMeta.SheetColsCount] = currJumpDataSetCount + "_" + rowColCountArr[1];
                    }
                    else
                    {
                        currJumpDataSetCount += 1;
                        Global.WSM_UpdateData[INDEX - 1, updateColInt] = currJumpDataSetCount.ToString();
                        Global.WSM_UpdateData[INDEX - 1, (int)WorksheetMeta.JumpDataSetCount] = String.Concat(Global.WSM_UpdateData[INDEX - 1, (int)WorksheetMeta.JumpDataSetCount], "\r\n") + currJumpDataSetCount + "_" + rowColCountArr[0];
                        //setSheetColsCount(INDEX, currJumpDataSetCount + "_" + rowColCountArr[1]);
                    }

                    break;
                case WorksheetMeta.DataSetCount:
                    rowColCountArr = value.ToString().Split(',');

                    var currJumpDataSetCountStr1 = Global.WSM_UpdateData[INDEX - 1, updateColInt];
                    if (string.IsNullOrEmpty(currJumpDataSetCountStr1))
                    {
                        SetDataSetCount(INDEX, seqVal);
                        Global.WSM_UpdateData[INDEX - 1, (int)WorksheetMeta.SheetRowsCount] = rowColCountArr[0];
                        Global.WSM_UpdateData[INDEX - 1, (int)WorksheetMeta.SheetColsCount] = Regex.Split(rowColCountArr[1], @"_(.+)")[1];

                    }
                    else
                    {
                        SetDataSetCount(INDEX, seqVal);
                        Global.WSM_UpdateData[INDEX - 1, (int)WorksheetMeta.SheetRowsCount] = String.Concat(Global.WSM_UpdateData[INDEX - 1, (int)WorksheetMeta.SheetRowsCount], "\r\n") + rowColCountArr[0];
                        SetSheetColsCount(INDEX, rowColCountArr[1]);
                    }
                    break;
                case WorksheetMeta.ImageLog:
                    Global.WSM_UpdateData[INDEX - 1, updateColInt] = value;
                    break;
                default:
                    var dt = DateTime.Now;
                    if (string.IsNullOrEmpty(Global.WSM_UpdateData[INDEX - 1, (int)WorksheetMeta.Execute]))
                    {
                        Global.WSM_UpdateData[INDEX - 1, (int)WorksheetMeta.Execute] = Keys.SUCCESS;
                        Global.WSM_UpdateData[INDEX - 1, (int)WorksheetMeta.StartTime] = dt.ToString("yyyy/MM/dd hh:mm:ss");
                    }
                    else
                    {
                        Global.WSM_UpdateData[INDEX - 1, (int)WorksheetMeta.StartTime] = String.Concat(Global.WSM_UpdateData[INDEX - 1, (int)WorksheetMeta.StartTime], "\r\n") + dt.ToString("yyyy/MM/dd hh:mm:ss");
                    }

                    SetTimeDiff(INDEX, dt);
                    break;
            }

        }


        
        public void SetJumpDataSetCount(IWebDriver driver, int INDEX, string pgmId, string workbookId, bool hasTotalRow, string requiredColName, int seqVal)
        {
            IJavaScriptExecutor javaScriptExecutor = driver as IJavaScriptExecutor;
            var colCount = cSA.Script_ColCount(workbookId);
            Int64 rowCount = cSA.CountDataRow(driver, pgmId, workbookId, RowDataType.SAVED_ROWS);
            if (rowCount == 0)
            {
                rowCount = cSA.CountDataRow(driver, pgmId, workbookId, RowDataType.DIRTY_ROWS);
            }
            if (rowCount == 0 && !string.IsNullOrEmpty(requiredColName))
            {
                rowCount = cSA.GetRowIndex(driver, workbookId, requiredColName, string.Empty);
                if (rowCount == -1)
                {
                    rowCount = int.Parse(cSA.Script_RowCount(workbookId));
                }
            }
            if (hasTotalRow)
            {
                rowCount -= 1;
            }
            SetWSMUpdateJson(INDEX, WorksheetMeta.JumpDataSetCount, workbookId + "_" + rowCount + "," + workbookId + "_" + colCount, seqVal);
            Global.WSM_UpdateData[INDEX - 1, (int)WorksheetMeta.JumpDataSetCount] += 1;
        }

        public void SetDataSetCount(int INDEX, int seqVal)
        {

            if (temp_DataSetCount[INDEX - 1, seqVal - 1] == 0)
            {
                temp_DataSetCount[INDEX - 1, seqVal - 1] = 1;
                Global.WSM_UpdateData[INDEX - 1, (int)WorksheetMeta.DataSetCount] = "1";
            }
            else if (temp_DataSetCount[INDEX - 1, seqVal - 1] != 0)
            {
                temp_DataSetCount[INDEX - 1, seqVal - 1] = 1;
                Global.WSM_UpdateData[INDEX - 1, (int)WorksheetMeta.DataSetCount] += 1;
            }

        }

        public void SetSheetColsCount(int INDEX, string value)
        {
            var currColCountValue = Global.WSM_UpdateData[INDEX - 1, (int)WorksheetMeta.SheetColsCount];
            var currColCountArr = Regex.Split(Global.WSM_UpdateData[INDEX - 1, (int)WorksheetMeta.SheetColsCount], "\r\n");
            value = Regex.Split(value, @"(_(.+))")[1];
            for (var i = 0; i < currColCountArr.Length; i++)
            {
                if (currColCountArr[i] == value)
                {
                    return;
                }
            }
            Global.WSM_UpdateData[INDEX - 1, (int)WorksheetMeta.SheetColsCount] = String.Concat(currColCountValue, "\r\n") + value;
        }


        public void SetQueryDataSetCount(IWebDriver driver, int INDEX, string pgmId, string workbookId, bool hasTotalRow, int seqVal)
        {
            var prefix = seqVal + "_" + workbookId + "_";
            Int64 rowCount = cSA.CountDataRow(driver, pgmId, workbookId, RowDataType.SAVED_ROWS);
            if (rowCount == 0)
            {
                rowCount = cSA.CountDataRow(driver, pgmId, workbookId, RowDataType.DIRTY_ROWS);
            }
            if (rowCount == 0)
            {
                rowCount = cA.ExecuteScript<Int64>(driver, cSA.Script_RowCount(workbookId));
            }
            Int64 colCount = cA.ExecuteScript<Int64>(driver, cSA.Script_ColCount(workbookId));
            if (hasTotalRow)
            {
                rowCount -= 1;
            }
            string value = string.Format("{0}{1},{2}{3}", prefix, rowCount, prefix, colCount);
            SetWSMUpdateJson(INDEX, WorksheetMeta.DataSetCount, value, seqVal);
        }

        public void SetInputDataSetCount(IWebDriver driver, int INDEX, string pgmId, string workbookId, Int64 rowCount, int seqVal)
        {
            var prefix = seqVal + "_" + workbookId + "_";
            Int64 colCount = cA.ExecuteScript<Int64>(driver, cSA.Script_ColCount(workbookId));

            string value = string.Format("{0}{1},{2}{3}", prefix, rowCount, prefix, colCount);
            SetWSMUpdateJson(INDEX, WorksheetMeta.DataSetCount, value, seqVal);
        }

        public void SetTimeDiff(int INDEX, DateTime dt)
        {
            var currIdx = TIME_IDX;
            timeArr[currIdx] = dt;
            TIME_IDX++;

            idxArr.Add(INDEX - 1);

            if (idxArr.Count > 1)
            {
                var t1 = timeArr[TIME_IDX - 2];
                var t2 = timeArr[TIME_IDX - 1];

                double timeDiffInSec = (t2 - t1).TotalSeconds;


                if (String.IsNullOrEmpty(Global.WSM_UpdateData[idxArr[IDX - 1], (int)WorksheetMeta.TimeDiff]))
                {
                    Global.WSM_UpdateData[idxArr[IDX - 1], (int)WorksheetMeta.TimeDiff] = timeDiffInSec.ToString();
                }
                else
                {
                    Global.WSM_UpdateData[idxArr[IDX - 1], (int)WorksheetMeta.TimeDiff] = String.Concat(Global.WSM_UpdateData[idxArr[IDX - 1], (int)WorksheetMeta.TimeDiff], "\r\n") + timeDiffInSec;
                }
            }
            IDX++;
        }

    }
}
