﻿using Azalea.Model;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Azalea
{
    public static class Global
    {
        public static string SiteAddr;
        public static string User;
        public static string Pass;
        public static string Language;
        public static string Browser;

        public static string CreateLogTime;
        public static string ScenarioIndex;
        public static string[,] WSM_UpdateData = new string[52,14];

        public static SysLogDto SL_UpdateData = new SysLogDto();

        public static bool isRememberMeChecked;

       
        public static string CurrDirectoryPath = @"E:\BitBucket\azaleawpf\Azalea\";
        public readonly static string ClassMethodDirectory = @"E:\BitBucket\azaleawpf\AzaleaRunner\Data";
        public readonly static string ClassMethodName = "RunnerData.csv";
        public readonly static string ReportLogRootName = "Root.csv";
        public readonly static string ExportLogDetailName = "ExportLogDetails.csv";

        public readonly static string DllLocation = Path.Combine(Global.CurrDirectoryPath, @"bin\Debug\Azalea.dll");

        public readonly static string TempFileDirectory = Path.Combine(CurrDirectoryPath, Environment.MachineName + "_TempData");
        public readonly static string DataDirectory = Path.Combine(CurrDirectoryPath, "Data");
        public readonly static string ExcelLogDirectory = Path.Combine(CurrDirectoryPath, "Log", "Excel");
        public readonly static string ImageLogDirectory = Path.Combine(CurrDirectoryPath, "Log", "Image");
        public readonly static string TextLogDirectory = Path.Combine(CurrDirectoryPath, "Log", "Text");
        public readonly static string ReportDirectory = @"E:\BitBucket\azaleawpf\AzaleaRunner\Report";

        public static string CurrentClassName = string.Empty;
        public static string CurrentMethodName = string.Empty;

        public static string DateFormatString = "yyyy/MM/dd HH:mm:ss";

    }
}
