﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Azalea
{
    internal class LogReport
    {
        private EventHandler eH = new EventHandler();

        private string ExportLogDetail = Path.Combine(Global.DataDirectory, Global.ExportLogDetailName);
        private int stepCount = 0;

        public void SetLogRecord(int INDEX, string message, bool state = true, string exception = "")
        {
            
            if (!Directory.Exists(Global.TextLogDirectory))
            {
                Directory.CreateDirectory(Global.TextLogDirectory);
            }
            var executeDateTime = Global.CreateLogTime;
            var LogFileName = Path.Combine(Global.TextLogDirectory, executeDateTime + FileExtension.TEXT_FILE);

            var messageVal = string.Format("{0} - {1} {2}", Global.CurrentClassName, message, Environment.NewLine);

            if (!state)
            {
                var errmessageVal = string.Format("{0}{1}", exception, Environment.NewLine);
                File.AppendAllText(LogFileName, errmessageVal);
            }
            else
            {
                File.AppendAllText(LogFileName, messageVal);
            }
            //Update WSM_UpdateJson
            if (INDEX != 0)
            {
                eH.SetWSMUpdateJson(INDEX, WorksheetMeta.Log, message);
            }


            if (!state)
            {
                Main.Reporter.Record_Error(exception.Replace("\r\n", Keys.TAB).Replace("\n", Keys.TAB));
            }

             SetSubLogRecord(message, state);
        }

        public void SetSubLogRecord(string message, bool state = true)
        {
            stepCount++;
            string testCaseId = Global.CurrentClassName;
            string testCaseSubId = Regex.Match((testCaseId + "." + Global.CurrentMethodName), @"[-+]?[0-9]*\.?[0-9]+").Groups[1].Value;
            message = message.Replace(",", "|||");
            string isFail = (state ? "0" : "1");
            string executeDateTime = Global.CreateLogTime;

            string csvLine = string.Format("{0},{1},{2},{3},{4},{5}", Environment.NewLine + stepCount, testCaseId, testCaseSubId, message, isFail, executeDateTime);

            File.AppendAllText(ExportLogDetail, csvLine);
        }
    }
}
