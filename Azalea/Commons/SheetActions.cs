﻿using Azalea.Map;
using NUnit.Framework;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Azalea
{
    internal class SheetActions
    {
        private CommonActions cA = new CommonActions();

        public int GetColumnIndex(IWebDriver driver, string workBookId, string dataField)
        {
            var colIndex = -1;
            //var colName = ExecuteScript("var wordSeqVal = $('[datafield=" + dataField + "]').attr('wordseq'); alert(wordSeqVal); return dictionaryData['dic_'+ wordSeqVal];");
            string colName = cA.ExecuteScript<string>(driver, "return dictionaryData['dic_' + this.Page.CtrlData[$('[datafield=" + dataField + "]').attr('cid')].split('`!')[2]];");
            //string colName =cA.ExecuteScript<string>(driver, "var wordSeqVal = $('[datafield=" + dataField + "]').attr('wordseq'); return dictionaryData['dic_'+ wordSeqVal];");
            Int64 colCount = cA.ExecuteScript<Int64>(driver, Script_ColCount(workBookId));
            for (var i = 0; i < colCount; i++)
            {
                string colNameExact = cA.ExecuteScript<string>(driver, "return $('#" + workBookId + "').data('workbook').getActiveSheet().getCell(0," + i + ",GC.Spread.Sheets.SheetArea.colHeader).text();").ToString();
                if (colNameExact == colName)
                {
                    colIndex = i;
                    break;
                }
            }
            return colIndex;
        }

        public int GetRowIndex(IWebDriver driver, string workBookId, string colName, string colVal, string condition = "=")
        {
            var colIndex = GetColumnIndex(driver, workBookId, colName);

            var rowIndex = -1;
            Int64 rowCount = cA.ExecuteScript<Int64>(driver, Script_RowCount(workBookId));
            bool isRowVisible = false;

            for (var i = 0; i < rowCount; i++)
            {
                var colValExact = cA.ExecuteScript<string>(driver, "return $('#" + workBookId + "').data('workbook').getActiveSheet().getCell(" + i + "," + colIndex + ").text()");

                if (String.Equals(colVal, colValExact))
                {
                    rowIndex = i;
                }
                isRowVisible = cA.ExecuteScript<bool>(driver, "return $('#" + workBookId + "').data('workbook').getActiveSheet().getRowVisible(" + rowIndex + ")");
                if (rowIndex != -1 && isRowVisible)
                {
                    break;
                }
            }
            return rowIndex;
        }

        internal string Script_RowCount(string workBookId)
        {
            return "return $('#" + workBookId + "').data('workbook').getActiveSheet().getRowCount()";
        }
        internal string Script_ColCount(string workBookId)
        {
            return "return $('#" + workBookId + "').data('workbook').getActiveSheet().getColumnCount()";
        }
        internal string Script_SheetCellObject(string workBookId, int rowIndex, int colIndex)
        {
            return "var activeSheet = $('#" + workBookId + "').data('workbook').getActiveSheet();activeSheet.showRow(" + rowIndex + ");activeSheet.showColumn(" + colIndex + "); return activeSheet.getCellRect(" + rowIndex + "," + colIndex + ");";
        }
        internal string Script_CellValue(string workBookId, int rowIndex, int colIndex)
        {
            return "var activeSheet = $('#" + workBookId + "').data('workbook').getActiveSheet();activeSheet.showRow(" + rowIndex + ");activeSheet.showColumn(" + colIndex + "); return activeSheet.getValue(" + rowIndex + "," + colIndex + ");";
        }
        internal string Script_CellType(string workBookId, int rowIndex, int colIndex)
        {
            return "var activeSheet = $('#" + workBookId + "').data('workbook').getActiveSheet();activeSheet.showRow(" + rowIndex + ");activeSheet.showColumn(" + colIndex + "); return activeSheet.getCellType(" + rowIndex + ", " + colIndex + ").typeName;";
        }
        internal string Script_CellDisabled(string workBookId, int rowIndex, int colIndex)
        {
            return "var activeSheet = $('#" + workBookId + "').data('workbook').getActiveSheet();activeSheet.showRow(" + rowIndex + ");activeSheet.showColumn(" + colIndex + "); return activeSheet.getCell(" + rowIndex + ", " + colIndex + ").locked();";
        }
        public string GetCellValue(IWebDriver driver, string workBookId, int rowIndex, string colName, bool isCheckCell = false)
        {
            var colIndex = GetColumnIndex(driver, workBookId, colName);
            string cellVal = "";
            if (!isCheckCell)
            {
                cellVal = cA.ExecuteScript<string>(driver, Script_CellValue(workBookId, rowIndex, colIndex));
            }
            else
            {
                cellVal = Convert.ToString(cA.ExecuteScript<bool>(driver, Script_CellValue(workBookId, rowIndex, colIndex)));
            }

            return cellVal;
        }

        public IWebElement GetCanvasObj(IWebDriver driver, string workbookId)
        {
            ContentsMap contentsMap = new ContentsMap(driver);
            return contentsMap.CanvasObjBy(workbookId);
        }

        public void CellInput(IWebDriver driver, string pgmSeq, string workbookId, int rowIndex, string colName, string cellVal)
        {
            int colIndex = 0;
            if (!string.IsNullOrEmpty(colName))
            {
                colIndex = GetColumnIndex(driver, workbookId, colName);
            }
            var cellObjDictionary = cA.ExecuteScript<Dictionary<string, object>>(driver, Script_SheetCellObject(workbookId, rowIndex, colIndex));

            var xVal = int.Parse(cellObjDictionary.First(m => m.Key == "x").Value.ToString());
            var yVal = int.Parse(cellObjDictionary.First(m => m.Key == "y").Value.ToString());
            var widthVal = int.Parse(cellObjDictionary.First(m => m.Key == "width").Value.ToString());
            var heightVal = int.Parse(cellObjDictionary.First(m => m.Key == "height").Value.ToString());
            var xCordVal = (xVal + widthVal / 2);
            var yCordVal = yVal + heightVal / 2;
            IWebElement canvasObj = GetCanvasObj(driver, workbookId);
            cA.ClickInputEvent(driver, canvasObj, xCordVal, yCordVal, cellVal);
            Main.Reporter.Record_Message("Set cell<row:" + rowIndex + ", col:" + colIndex + "> value<value:" + cellVal + ">");
        }

        public void CellClick(IWebDriver driver, string pgmSeq, string workbookId, int rowIndex = 0, string colName = "", bool isDblClick = false, bool isRgtClick = false, bool isHeaderCell = false, bool isComboCell = false)
        {
            IWebElement canvasObj = GetCanvasObj(driver, workbookId);

            int colIndex = 0;
            if (!string.IsNullOrEmpty(colName))
            {
                colIndex = GetColumnIndex(driver, workbookId, colName);
            }
            Dictionary<string, object> cellObjDictionary = new Dictionary<string, object>();
            if (isHeaderCell)
            {
                cellObjDictionary = cA.ExecuteScript<Dictionary<string, object>>(driver, Script_HeaderCellObj(workbookId, rowIndex, colIndex));
            }
            else
            {
                cellObjDictionary = cA.ExecuteScript<Dictionary<string, object>>(driver, Script_SheetCellObject(workbookId, rowIndex, colIndex));
            }

            var xVal = int.Parse(cellObjDictionary.First(m => m.Key == "x").Value.ToString());
            var yVal = int.Parse(cellObjDictionary.First(m => m.Key == "y").Value.ToString());
            var widthVal = int.Parse(cellObjDictionary.First(m => m.Key == "width").Value.ToString());
            var heightVal = int.Parse(cellObjDictionary.First(m => m.Key == "height").Value.ToString());
            var xCordVal = (xVal + widthVal / 2);
            var yCordVal = yVal + heightVal / 2;


            var cellType = GetCellType(driver, pgmSeq, workbookId, rowIndex, colName);
            bool isCellDisabled = IsCellDisabled(driver, pgmSeq, workbookId, rowIndex, colName);
            if (!isCellDisabled)
            {
                if (cellType == SheetCellType.CHECK_CELL)
                {
                    cA.HoverEvent(driver, canvasObj, xCordVal, yCordVal);
                    cA.Delay(500);

                    if (isRgtClick)
                    {
                        cA.RightClickEvent(driver, canvasObj, xCordVal, yCordVal);
                        Main.Reporter.Record_Message("Right clicked checkbox cell<row:" + rowIndex + ", col:" + colIndex + ">");
                    }
                    else
                    {
                        bool chkState;
                        chkState = Convert.ToBoolean(cA.ExecuteScript<object>(driver, Script_CellValue(workbookId, rowIndex, colIndex)));
                        var newState = chkState;
                        //Utils.Timers.Add(300000, "EventHandler.TimeoutTimer", true);
                        do
                        {
                            cA.ClickEvent(driver, canvasObj, xCordVal, yCordVal);
                            Main.Reporter.Record_Message("Clicked checkbox cell<row:" + rowIndex + ", col:" + colIndex + ">");
                            newState = Convert.ToBoolean(cA.ExecuteScript<object>(driver, Script_CellValue(workbookId, rowIndex, colIndex)));
                        }
                        while (chkState == newState);
                        //Utils.Timers.Delete(0);
                    }
                }

                else if (isDblClick)
                {
                    cA.DblClickEvent(driver, canvasObj, xCordVal, yCordVal);
                    Main.Reporter.Record_Message("Double clicked cell<row:" + rowIndex + ", col:" + colIndex + ">");
                }
                else if (isRgtClick)
                {
                    cA.RightClickEvent(driver, canvasObj, xCordVal, yCordVal);
                    Main.Reporter.Record_Message("Right clicked cell<row:" + rowIndex + ", col:" + colIndex + ">");
                }
                else if (isComboCell)
                {
                    cA.ClickEvent(driver, canvasObj, xVal + widthVal - 8, yCordVal);
                    Main.Reporter.Record_Message("Combobox clicked cell<row:" + rowIndex + ", col:" + colIndex + ">");
                }
                else
                {
                    cA.ClickEvent(driver, canvasObj, xCordVal, yCordVal);
                    Main.Reporter.Record_Message("Clicked cell<row:" + rowIndex + ", col:" + colIndex + ">");
                }
                cA.Delay();
            }

        }

        public string GetCellType(IWebDriver driver, string pgmId, string workbookId, int rowIndex, string colName)
        {
            var colIndex = GetColumnIndex(driver, workbookId, colName);

            var cellType = cA.ExecuteScript<string>(driver, Script_CellType(workbookId, rowIndex, colIndex));
            return cellType;
        }
        public bool IsCellDisabled(IWebDriver driver, string pgmId, string workbookId, int rowIndex, string colName)
        {
            IJavaScriptExecutor jSExecutor = driver as IJavaScriptExecutor;
            var colIndex = GetColumnIndex(driver, workbookId, colName);

            var cellType = cA.ExecuteScript<bool>(driver, Script_CellDisabled(workbookId, rowIndex, colIndex));
            return cellType;
        }

        public void ReadSheetDataList(IWebDriver driver, string pgmSeq, string workBookId, List<List<string>> dataList)
        {
            var colList = dataList[0];
            var keyColumnValue = string.Empty;
            var workingTagValue = string.Empty;
            var keyColumnIndex = -1;
            var rowIndex = -1;
            var dateType = "Date_";
            var dateValue = string.Empty;
            for (var i = 1; i < dataList.Count; i++)
            {
                keyColumnValue = dataList[i][0];
                workingTagValue = dataList[i][1];
                if (workingTagValue == "A")
                {
                    rowIndex = GetRowIndex(driver, workBookId, keyColumnValue, string.Empty);
                    rowIndex = (rowIndex == -1) ? 0 : rowIndex;
                    keyColumnIndex = -1;
                }
                else if (workingTagValue == "U" || workingTagValue == "D")
                {
                    keyColumnIndex = colList.FindIndex(a => a.Contains(keyColumnValue));
                    rowIndex = GetRowIndex(driver, workBookId, keyColumnValue, dataList[i][keyColumnIndex]);
                }

                if (rowIndex != -1)
                {
                    for (var j = 2; j < colList.Count; j++)
                    {
                        if (keyColumnIndex == j)
                        {
                            continue;
                        }

                        var colName = colList[j];
                        var cellVal = dataList[i][j];
                        if (!string.IsNullOrEmpty(cellVal))
                        {
                            if (cellVal == "NULL")
                            {
                                cellVal = string.Empty;
                            }
                            if (cellVal.Contains(dateType))
                            {
                                dateValue = cellVal.Substring(dateType.Length);
                                cellVal = cA.GetDateUsingExcel(dateValue);
                            }
                            TestContext.WriteLine("Entering  the " + cellVal + " On Sheet's inputFile:" + colName);
                            CellInput(driver, pgmSeq, workBookId, rowIndex, colName, cellVal);
                        }
                    }
                }
            }
        }

        public int CountDataRow(IWebDriver driver, string pgmSeq, string workBookId, string rowType = "X")
        {
            driver.SwitchTo().DefaultContent();
            string pgmSeqFrame = cA.GetPgmSeqFrame(pgmSeq);
            driver.SwitchTo().Frame(driver.FindElement(By.Id(pgmSeqFrame)));


            Int64 rowCount = -1;
            rowCount = cA.ExecuteScript<Int64>(driver, Script_RowCount(workBookId));
            if (!String.IsNullOrEmpty(rowType))
            {
                if (rowType == "X")
                {
                    rowCount = cA.ExecuteScript<Int64>(driver, "return $('#" + workBookId + "').data('workbook').getActiveSheet().getDirtyRows().length");
                    return Convert.ToInt32(rowCount);
                }
                int aRows, dRows, uRows, sRows;
                aRows = dRows = uRows = sRows = 0;
                string rowHeaderValue = string.Empty;
                for (int i = 0; i < rowCount; i++)
                {
                    rowHeaderValue = cA.ExecuteScript<string>(driver, "return $('#" + workBookId + "').data('workbook').getActiveSheet().getCell(" + i + ", 0, GC.Spread.Sheets.SheetArea.rowHeader).text()");
                    if (rowHeaderValue == RowDataType.ADDED_ROWS)
                    {
                        aRows++;
                    }
                    else if (rowHeaderValue == RowDataType.DELETED_ROWS)
                    {
                        dRows++;
                    }
                    else if (rowHeaderValue == RowDataType.UPDATED_ROWS)
                    {
                        uRows++;
                    }
                    else
                    {
                        sRows++;
                    }
                }
                if (rowType == RowDataType.ADDED_ROWS)
                {
                    return aRows;
                }
                else if (rowType == RowDataType.DELETED_ROWS)
                {
                    return dRows;
                }
                else if (rowType == RowDataType.UPDATED_ROWS)
                {
                    return uRows;
                }
                else
                {
                    return sRows;
                }

            }
            else
            {
                return Convert.ToInt32(rowCount);
            }

        }

        private static string Script_HeaderCellObj(string workBookId, int rowIndex, int colIndex)
        {
            return "var activeSheet = $('#" + workBookId + "').data('workbook').getActiveSheet();activeSheet.showRow(" + rowIndex + ");activeSheet.showColumn(" + colIndex + "); return activeSheet.getCellRect(" + rowIndex + "," + colIndex + ", -1);";
        }

        private static string Script_HeaderCellSortImage(string workBookId, int colIndex)
        {
            return "var activeSheet = $('#" + workBookId + "').data('workbook').getActiveSheet();activeSheet.getStyle(-1, " + colIndex + ", ns.SheetArea.colHeader).backgroundImage;";
        }

        public void SetColSortOrder(IWebDriver driver, string pgmSeq, string workbookId, string colName, bool isAsc)
        {
            IWebElement canvasObj = GetCanvasObj(driver, workbookId);

            int colIndex = 0;
            int rowIndex = 0;
            if (!string.IsNullOrEmpty(colName))
            {
                colIndex = GetColumnIndex(driver, workbookId, colName);
            }

            /*Updated code to make cell-click on header-cell too.*/
            var cellObj = cA.ExecuteScript<Dictionary<string, object>>(driver, Script_HeaderCellObj(workbookId, rowIndex, colIndex));


            var xVal = int.Parse(cellObj.First(m => m.Key == "x").Value.ToString());
            var yVal = int.Parse(cellObj.First(m => m.Key == "y").Value.ToString());
            var widthVal = int.Parse(cellObj.First(m => m.Key == "width").Value.ToString());
            var heightVal = int.Parse(cellObj.First(m => m.Key == "height").Value.ToString());
            var xCordVal = (xVal + widthVal / 2);
            var yCordVal = yVal + heightVal / 2;

            cA.ClickEvent(driver, canvasObj, xCordVal, yCordVal);
            cA.Delay();
            cA.ClickEvent(driver, canvasObj, xVal + 8, yVal + 6);

            //Get current sorted state relative to sortImage rendered and sort
            var sortImage = cA.ExecuteScript<string>(driver, Script_HeaderCellSortImage(workbookId, colIndex));
            if ((sortImage == "/Images/iconSheetSortDescending.png" && isAsc) || (sortImage == "/Images/iconSheetSortAscending.png" && !isAsc))
            {
                cA.ClickEvent(driver, canvasObj, xVal + 8, yVal + 6);
                cA.Delay();
            }
            Main.Reporter.Record_Message("Set col<index:" + colIndex + "> order " + (isAsc ? "ascending" :  "descending"));
        }

        public void RowSelection(IWebDriver driver, string pgmId, string workbookId, int startRowIndex, int endRowIndex, bool makeCopy = false, bool makePaste = false, bool isDblClick = false)
        {
            IWebElement canvasObj = GetCanvasObj(driver, workbookId);
            int colIndex = 0;
            //Making start row visible on viewport and at top && column visible within viewport
            string scriptVal = string.Empty;
            if (startRowIndex < endRowIndex)
            {
                scriptVal = Script_InitalizeActiveSheet(workbookId) + "activeSheet.showRow(" + startRowIndex + ");activeSheet.showColumn(" + colIndex + ");";
            }
            else
            {
                scriptVal = Script_InitalizeActiveSheet(workbookId) + "activeSheet.showColumn(" + colIndex + ");activeSheet.showRow(" + startRowIndex + ", ns.VerticalAlign.bottom);";
            }
            cA.ExecuteJavaScript(driver, scriptVal);

            scriptVal = Script_InitalizeActiveSheet(workbookId) + "return activeSheet.getColumnWidth(0, ns.SheetArea.rowHeader)/2;";
            int startX = Convert.ToInt32(cA.ExecuteScript<Int64>(driver, scriptVal));
            scriptVal = Script_InitalizeActiveSheet(workbookId) + "return activeSheet.getCellRect(" + startRowIndex + ", ns.SheetArea.rowHeader).y + activeSheet.getCellRect(" + startRowIndex + ", ns.SheetArea.rowHeader).height/2;";
            int startY = Convert.ToInt32(cA.ExecuteScript<Int64>(driver, scriptVal));
            scriptVal = Script_InitalizeActiveSheet(workbookId) + "return activeSheet.getCellRect(" + endRowIndex + ", ns.SheetArea.rowHeader).y + activeSheet.getCellRect(" + endRowIndex + ", ns.SheetArea.rowHeader).height / 2; ";
            int endY = Convert.ToInt32(cA.ExecuteScript<Int64>(driver, scriptVal));
            try
            {
                if (startRowIndex != endRowIndex)
                {
                    cA.DragEvent(driver, canvasObj, startX, startY, 0, endY - startY);
                    Main.Reporter.Record_Message("Selected row indices from" + startRowIndex + " to " + endRowIndex);
                }
                else if (isDblClick)
                {
                    cA.DblClickEvent(driver, canvasObj, startX, startY);
                    Main.Reporter.Record_Message("Double clicked row<index:" + startRowIndex + ">");
                }
                else
                {
                    cA.ClickEvent(driver, canvasObj, startX, startY);
                    Main.Reporter.Record_Message("Selected row<index:" + startRowIndex + ">");
                }
            }
            catch
            {
                if (startRowIndex > endRowIndex)
                {
                    var temp = startRowIndex;
                    startRowIndex = endRowIndex;
                    endRowIndex = temp;
                }
                scriptVal = Script_InitalizeActiveSheet(workbookId) + "activeSheet.setSelection(" + startRowIndex + ", 0, " + (endRowIndex - startRowIndex + 1) + ", activeSheet.getColumnCount());";
                cA.ExecuteJavaScript(driver, scriptVal);
                Main.Reporter.Record_Message("Selected row indices(using spreadjs method) from" + startRowIndex + " to " + endRowIndex);
            }
        }

        //Select Sheet Context Menu Item
        /*Parameter itemText changed to itemId*/
        public void SelectSheetContextMenu(IWebDriver driver, string pgmId, string workbookId, string itemId)
        {
            ContentsMap contentsMap = new ContentsMap(driver);
            IWebElement contextItem = contentsMap.ContextItemBy(itemId);
            string contextItemText = contextItem.Text;
            contextItem.Click();
            Main.Reporter.Record_Message("Selected contextmenu item<text:" + contextItemText + ">");
        }

        private static string Script_InitalizeActiveSheet(string workbookId)
        {
            return "var activeSheet = $('#" + workbookId + "').data('workbook').getActiveSheet();";
        }

        public void FillCellsUsingDragging(IWebDriver driver, string pgmId, string workbookId, int startRowIndex, string startColName, int endRowIndex, string endColName)
        {
            Validations.is_loading_hidden(driver);

            IWebElement canvasObj = GetCanvasObj(driver, workbookId);
            var startColIndex = GetColumnIndex(driver, workbookId, startColName);
            var endColIndex = GetColumnIndex(driver, workbookId, endColName);
            var startCellObj = cA.ExecuteScript<Dictionary<string, object>>(driver, Script_SheetCellObject(workbookId, startRowIndex, startColIndex));

            var xVal1 = int.Parse(startCellObj.First(m => m.Key == "x").Value.ToString());
            var yVal1 = int.Parse(startCellObj.First(m => m.Key == "y").Value.ToString());
            var widthVal1 = int.Parse(startCellObj.First(m => m.Key == "width").Value.ToString());
            var heightVal1 = int.Parse(startCellObj.First(m => m.Key == "height").Value.ToString());

            var endCellObj = cA.ExecuteScript<Dictionary<string, object>>(driver, Script_SheetCellObject(workbookId, endRowIndex, endColIndex));
            var xVal2 = int.Parse(endCellObj.First(m => m.Key == "x").Value.ToString());
            var yVal2 = int.Parse(endCellObj.First(m => m.Key == "y").Value.ToString());
            var widthVal2 = int.Parse(endCellObj.First(m => m.Key == "width").Value.ToString());
            var heightVal2 = int.Parse(endCellObj.First(m => m.Key == "height").Value.ToString());

            var startX = xVal1 + widthVal1;
            var startY = yVal1 + heightVal1;
            var endX = xVal2 + widthVal2;
            var endY = yVal2 + heightVal2 / 2;
            var scriptVal = string.Empty;
            if (endY != 0)
            {
                cA.DragEvent(driver, canvasObj, startX, startY, endX - startX, endY - startY);
                Main.Reporter.Record_Message("Filled cells dragging from cell<row:" + startRowIndex + ", col:" + startColIndex + "> to cell<row:" + endRowIndex + ", col:" + endColIndex + ">");
                scriptVal = Script_InitalizeActiveSheet(workbookId) + " return activeSheet.getSelections()[0].rowCount;";
                int rowCount = Convert.ToInt32(cA.ExecuteScript<Int64>(driver, scriptVal));

                var lastSelectedRowIndex = rowCount + startRowIndex - 1;
                if (lastSelectedRowIndex < endRowIndex)
                {
                    CellClick(driver, pgmId, workbookId, lastSelectedRowIndex, startColName);
                    FillCellsUsingDragging(driver, pgmId, workbookId, lastSelectedRowIndex, startColName, endRowIndex, endColName);
                }
            }
            else
            {
                int visibleEndRow = -1;
                for (var i = (endRowIndex - 1); i > startRowIndex; i--)
                {
                    endCellObj = cA.ExecuteScript<Dictionary<string, object>>(driver, Script_SheetCellObject(workbookId, i, endColIndex));
                    heightVal2 = int.Parse(endCellObj.First(m => m.Key == "height").Value.ToString());
                    if (heightVal2 != 0)
                    {
                        visibleEndRow = i - 1;
                        break;
                    }
                }
                endCellObj = cA.ExecuteScript<Dictionary<string, object>>(driver, Script_SheetCellObject(workbookId, visibleEndRow, endColIndex));
                xVal2 = int.Parse(endCellObj.First(m => m.Key == "x").Value.ToString());
                yVal2 = int.Parse(endCellObj.First(m => m.Key == "y").Value.ToString());
                widthVal2 = int.Parse(endCellObj.First(m => m.Key == "width").Value.ToString());
                heightVal2 = int.Parse(endCellObj.First(m => m.Key == "height").Value.ToString());
                endX = xVal2 + widthVal2;
                endY = yVal2 + heightVal2 / 2;

                cA.DragEvent(driver, canvasObj, startX, startY, endX - startX, endY - startY);
                Main.Reporter.Record_Message("Filled cells dragging from cell<row:" + startRowIndex + ", col:" + startColIndex + "> to cell<row:" + endRowIndex + ", col:" + endColIndex + ">");

                CellClick(driver, pgmId, workbookId, visibleEndRow, startColName);
                FillCellsUsingDragging(driver, pgmId, workbookId, visibleEndRow, startColName, endRowIndex, endColName);
            }

        }

        internal void SetColFilter(IWebDriver currWebDriver, string pgmSeq, string workbookId, string v, List<string> delvInNoValArr)
        {
            throw new NotImplementedException();
        }
    }
}
