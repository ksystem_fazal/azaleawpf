﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Azalea.Commons
{
    class SheetLog
    {


        LogReport CLR = new LogReport();


        //Cell values matched state
        public void match_cellValue(bool state, int rowIndex, string colName, string value)
        {
            string message = "";
            if (state)
            {
                message = "Cell<" + rowIndex + ", " + colName + "> Value Matched With <" + value + ">.";
                Main.Reporter.Record_Checkpoint(message);
            }
            else
            {
                message = "Cell<" + rowIndex + ", " + colName + "> Value Not Matched With <" + value + ">.";
                Main.Reporter.Record_Warning(message);
            }
            CLR.SetSubLogRecord(message, state);
        }

        //Cells values matched state
        public void match_cells_value(bool state, int rowIndex1, string colName1, int rowIndex2, string colName2)
        {
            string message = "";
            if (state)
            {
                message = "Cells <" + rowIndex1 + ", " + colName1 + "> And <" + rowIndex2 + ", " + colName2 + "> Values Are Matched.";
                Main.Reporter.Record_Checkpoint(message);
            }
            else
            {
                message = "Cells <" + rowIndex1 + ", " + colName1 + "> And <" + rowIndex2 + ", " + colName2 + "> Values Are Not Matched.";
                Main.Reporter.Record_Warning(message);
            }
            CLR.SetSubLogRecord(message, state);
        }

        //Cell fore color state
        public void match_cell_forecolor(bool state, int rowIndex, string colName, string color)
        {
            string message = "";
            if (state)
            {
                message = "Cell<" + rowIndex + ", " + colName + "> Text Color Is <" + color + ">.";
                Main.Reporter.Record_Checkpoint(message);
            }
            else
            {
                message = "Cell<" + rowIndex + ", " + colName + "> Text Color Is Not <" + color + ">.";
                Main.Reporter.Record_Warning(message);
            }
            CLR.SetSubLogRecord(message, state);
        }

        //Cell is active/focused
        public void is_cellActive(bool state, int rowIndex, string colName)
        {
            string message = "";
            if (state)
            {
                message = "Cell<" + rowIndex + ", " + colName + "> Is Focused.";
                Main.Reporter.Record_Checkpoint(message);
            }
            else
            {
                message = "Cell<" + rowIndex + ", " + colName + "> Is Not Focused.";
                Main.Reporter.Record_Warning(message);
            }
            CLR.SetSubLogRecord(message, state);
        }

        //Cell is Editing
        public void is_cellEditing(bool state, int rowIndex, string colName)
        {
            string message = "";
            if (state)
            {
                message = "Cell<" + rowIndex + ", " + colName + "> Is Editing.";
                Main.Reporter.Record_Checkpoint(message);
            }
            else
            {
                message = "Cell<" + rowIndex + ", " + colName + "> Is Not Editing.";
                Main.Reporter.Record_Warning(message);
            }
            CLR.SetSubLogRecord(message, state);
        }

        //ContextMenu sum-value match
        public void match_contextMenu_sumValue(bool state, string value)
        {
            string message = "";
            if (state)
            {
                message = "Summation Of Cell Values Matched With Value<" + value + ">.";
                Main.Reporter.Record_Checkpoint(message);
            }
            else
            {
                message = "Summation Of Cell Values Not Matched With Value<" + value + ">.";
                Main.Reporter.Record_Warning(message);
            }
            CLR.SetSubLogRecord(message, state);
        }

        //Column DividerLine was visible
        public void colDivider_visible(bool state)
        {
            string message = "";
            if (state)
            {
                message = "Column DividerLine Was Visible.";
                Main.Reporter.Record_Checkpoint(message);
            }
            else
            {
                message = "Column DividerLine Was Not Visible.";
                Main.Reporter.Record_Warning(message);
            }
            CLR.SetSubLogRecord(message, state);
        }

        //Column is right next to another column
        public void is_nextCol(bool state, string colName, string leftColName)
        {
            string message = "";
            if (state)
            {
                message = "Col<" + colName + "> Is Right Next to Col<" + leftColName + ">.";
                Main.Reporter.Record_Checkpoint(message);
            }
            else
            {
                message = "Col<" + colName + "> Is Not Right Next to Col<" + leftColName + ">.";
                Main.Reporter.Record_Warning(message);
            }
            CLR.SetSubLogRecord(message, state);
        }

        //Count DataRow match
        public void match_count_dataRow(bool state, int count, string rowType = "")
        {
            string rowTypeStr = "";
            if (rowType != "")
            {
                switch (rowType)
                {
                    case RowDataType.ADDED_ROWS: rowTypeStr = "<Added> "; break;
                    case RowDataType.DELETED_ROWS: rowTypeStr = "<Deleted> "; break;
                    case RowDataType.UPDATED_ROWS: rowTypeStr = "<Updated> "; break;
                    case RowDataType.SAVED_ROWS: rowTypeStr = "<Saved> "; break;
                    default: break;
                }
            }

            string message = "";
            if (state)
            {
                message = rowTypeStr + "DataRow Count Is <" + count + ">.";
                Main.Reporter.Record_Checkpoint(message);
            }
            else
            {
                message = rowTypeStr + "DataRow Count Is <" + count + ">.";
                Main.Reporter.Record_Warning(message);
            }
            CLR.SetSubLogRecord(message, state);
        }

        //Column width change
        public void change_colWidth(bool state, string colName, int colWidth, int newColWidth)
        {
            string message = "";
            if (state)
            {
                message = "Col<" + colName + "> Width Is Changed From <" + colWidth + "> To <" + newColWidth + ">.";
                Main.Reporter.Record_Checkpoint(message);
            }
            else
            {
                message = "Col<" + colName + "> Width Is Not Changed From <" + colWidth + "> To <" + newColWidth + ">.";
                Main.Reporter.Record_Warning(message);
            }
            CLR.SetSubLogRecord(message, state);
        }

        //Column width match
        public void match_colWidth(bool state, string colName, int colWidth)
        {
            string message = "";
            if (state)
            {
                message = "Col<" + colName + "> Width Is <" + colWidth + ">.";
                Main.Reporter.Record_Checkpoint(message);
            }
            else
            {
                message = "Col<" + colName + "> Width Is Not <" + colWidth + ">.";
                Main.Reporter.Record_Warning(message);
            }
            CLR.SetSubLogRecord(message, state);
        }

        //Columns width match
        public void match_colsWidth(bool state, string colName1, int colWidth1, string colName2, int colWidth2)
        {
            string message = "";
            if (state)
            {
                message = "Col<" + colName1 + "> Width Is <" + colWidth1 + "> And Col<" + colName2 + "> Width Is <" + colWidth2 + ">.";
                Main.Reporter.Record_Checkpoint(message);
            }
            else
            {
                message = "Col<" + colName1 + "> Width Is <" + colWidth1 + "> And Col<" + colName2 + "> Width Is <" + colWidth2 + ">.";
                Main.Reporter.Record_Warning(message);
            }
            CLR.SetSubLogRecord(message, state);
        }

        //Column position match
        public void match_colPos(bool state, string colName, int colPos)
        {
            string message = "";
            if (state)
            {
                message = "Col<" + colName + "> Is At Position<" + colPos + ">.";
                Main.Reporter.Record_Checkpoint(message);
            }
            else
            {
                message = "Col<" + colName + "> Is Not At Position<" + colPos + ">.";
                Main.Reporter.Record_Warning(message);
            }
            CLR.SetSubLogRecord(message, state);
        }


    }
}
