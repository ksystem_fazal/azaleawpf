﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Azalea.Map;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace Azalea
{
    class SubFormActions
    {
        private CommonActions cA = new CommonActions();
        private SheetActions cSA = new SheetActions();

        public string GetPgmSeqSubFrame(string subFormId)
        {
            return subFormId + "_iframe_subform";
        }

        public int GetColumnIndex(IWebDriver driver, string subFormId, string subWorkbookId, string dataField)
        {
            string pgmSeqSubFrame = GetPgmSeqSubFrame(subFormId);
            var colIndex = -1;
            string executeScript = "return dictionaryData['dic_' + this.Page.CtrlData[$('[datafield=" + dataField + "]').attr('cid')].split('`!')[2]];";

            string colName = cA.ExecuteScript<string>(driver, executeScript);
            Int64 colCount = cA.ExecuteScript<Int64>(driver, cSA.Script_ColCount(subWorkbookId));
            for (var i = 0; i < colCount; i++)
            {
                string colNameExact = cA.ExecuteScript<string>(driver, "return $('#" + subWorkbookId + "').data('workbook').getActiveSheet().getCell(0," + i + ",GC.Spread.Sheets.SheetArea.colHeader).text();").ToString();
                if (colNameExact == colName)
                {
                    colIndex = i;
                    break;
                }
            }
            return colIndex;
        }

        public int GetRowIndex(IWebDriver driver, string pgmSeq, string subFormId, string subWorkbookId, string dataField, string value)
        {
            string pgmSeqFrame = cA.GetPgmSeqFrame(pgmSeq);
            IWebElement frameObj = driver.FindElement(By.Id(pgmSeqFrame));
            driver.SwitchTo().Frame(frameObj);
            string pgmSeqSubFrame = GetPgmSeqSubFrame(subFormId);
            IWebElement subFrameObj = driver.FindElement(By.Id(pgmSeqSubFrame));
            driver.SwitchTo().Frame(subFrameObj);

            int colIndex = GetColumnIndex(driver, subFormId, subWorkbookId, dataField);

            int rowIndex = -1;
            Int64 rowCount = cA.ExecuteScript<Int64>(driver, cSA.Script_RowCount(subWorkbookId));
            bool isRowVisible = false;

            for (var i = 0; i < rowCount; i++)
            {
                var colValExact = cA.ExecuteScript<string>(driver, "return $('#" + subWorkbookId + "').data('workbook').getActiveSheet().getCell(" + i + "," + colIndex + ").text()");

                if (String.Equals(value, colValExact))
                {
                    rowIndex = i;
                }
                isRowVisible = cA.ExecuteScript<bool>(driver, "return $('#" + subWorkbookId + "').data('workbook').getActiveSheet().getRowVisible(" + rowIndex + ")");
                if (rowIndex != -1 && isRowVisible)
                {
                    break;
                }
            }

            driver.SwitchTo().DefaultContent();
            return rowIndex;
        }

        public void cellInput(IWebDriver driver, string pgmSeq, string subFormId, string subWorkbookId, int rowIndex, string dataField, string value)
        {
            string pgmSeqFrame = cA.GetPgmSeqFrame(pgmSeq);
            IWebElement frameObj = driver.FindElement(By.Id(pgmSeqFrame));
            driver.SwitchTo().Frame(frameObj);
            string pgmSeqSubFrame = GetPgmSeqSubFrame(subFormId);
            IWebElement subFrameObj = driver.FindElement(By.Id(pgmSeqSubFrame));
            driver.SwitchTo().Frame(subFrameObj);

            if (value == "NULL")
            {
                value = string.Empty;
            }

            int colIndex = 0;
            if (!string.IsNullOrEmpty(dataField))
            {
                colIndex = GetColumnIndex(driver, subFormId, subWorkbookId, dataField);
            }
            var cellObjDictionary = cA.ExecuteScript<Dictionary<string, object>>(driver, cSA.Script_SheetCellObject(subWorkbookId, rowIndex, colIndex));

            var xVal = int.Parse(cellObjDictionary.First(m => m.Key == "x").Value.ToString());
            var yVal = int.Parse(cellObjDictionary.First(m => m.Key == "y").Value.ToString());
            var widthVal = int.Parse(cellObjDictionary.First(m => m.Key == "width").Value.ToString());
            var heightVal = int.Parse(cellObjDictionary.First(m => m.Key == "height").Value.ToString());
            var xCordVal = (xVal + widthVal / 2);
            var yCordVal = yVal + heightVal / 2;
            IWebElement canvasObj = cSA.GetCanvasObj(driver, subWorkbookId);
            cA.ClickInputEvent(driver, canvasObj, xCordVal, yCordVal, value);
            Main.Reporter.Record_Message("Set cell<row:" + rowIndex + ", col:" + colIndex + "> value<value:" + value + ">");

            driver.SwitchTo().DefaultContent();
        }

        public void SavePgm(IWebDriver driver, string pgmSeq, string subFormId, int INDEX = -1)
        {
            string pgmSeqFrame = cA.GetPgmSeqFrame(pgmSeq);
            IWebElement frameObj = driver.FindElement(By.Id(pgmSeqFrame));
            driver.SwitchTo().Frame(frameObj);
            string pgmSeqSubFrame = GetPgmSeqSubFrame(subFormId);
            IWebElement subFrameObj = driver.FindElement(By.Id(pgmSeqSubFrame));
            driver.SwitchTo().Frame(subFrameObj);

            string expressionVal = ".//img[@src ='/Images/Toolbar/T_Save.png']";
            cA.WaitMethodUsingXPath(driver, expressionVal);
            ToolbarMap toolbarMap = new ToolbarMap(driver);
            cA.ClickEvent(driver, toolbarMap.SavePgmBtn);
            Main.Reporter.Record_Message("Clicked save pgm<seq:" + pgmSeq + "> subform<id:" + subFormId + ">");
            if (INDEX != -1)
            {
                Stopwatch stopWatch = new Stopwatch();
                stopWatch.Start();
                Validations.is_loading_hidden(driver);

                stopWatch.Stop();
            }

            driver.SwitchTo().DefaultContent();
        }

    }
}
