﻿using Azalea.Map;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Azalea
{
    class Validations
    {
        public static bool IsPopupVisible(IWebDriver driver,string popupId)
        {
            driver.SwitchTo().DefaultContent();
            return driver.FindElement(By.Id(popupId)).Displayed;
        }

        public static bool is_pgm_openedSelected(IWebDriver driver, string pgmId)
        {
            driver.SwitchTo().DefaultContent();
            var iframeItem = is_pgm_opened(driver, pgmId);

            return iframeItem.Displayed && iframeItem.Enabled;
        }

        public static IWebElement is_pgm_opened(IWebDriver driver, string pgmId)
        {
            driver.SwitchTo().DefaultContent();
            var iframeItem = driver.FindElement(By.Id(new CommonActions().GetPgmSeqFrame(pgmId)));
            return iframeItem;
        }

        public static bool is_loading_hidden(IWebDriver driver)
        {
            IWebElement pageLoadingObj = null;
            driver.SwitchTo().DefaultContent();

            GeneralMap generalMap = new GeneralMap(driver);

            do
            {
                pageLoadingObj = generalMap.PageLoadingObj;
                if (pageLoadingObj != null)
                {
                    bool msgObjVisible = generalMap.PageLoadingMsgObj.Displayed;
                    if (msgObjVisible)
                    {
                        return false;
                    }
                }
            } while (pageLoadingObj == null || pageLoadingObj.Displayed);
            return true;
        }
    }
}
