﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Azalea.Helper
{
    class AzaleaMainException : Exception
    {
        public AzaleaMainException()
        {

        }
        public AzaleaMainException(string message, Exception innerException) : base(string.Format("Azalea Main Exception: {0}", message), innerException)
        {

        }
    }
}
