﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Azalea.Helper
{
    class AzaleaMethodException : Exception
    {
        public AzaleaMethodException()
        {

        }
        public AzaleaMethodException(string message, Exception innerException) : base(string.Format("Azalea Method Exception: {0}", message), innerException)
        {

        }
    }
}
