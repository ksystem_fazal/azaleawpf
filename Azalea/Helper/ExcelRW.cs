﻿using Azalea.Model;
using ClosedXML.Excel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace Azalea
{
    public class ExcelRW
    {
        public ReadExcelDto ReadExcel(string sourceFilePath)
        {
            ReadExcelDto excelDto = new ReadExcelDto();
            using (XLWorkbook wb = new XLWorkbook(sourceFilePath))
            {
                string workSheetName = string.Empty;
                IXLWorksheet ws;
                var inputDataList = new NestedMultiDimDictList<string, string, MultiDictList<string, List<string>>>();

                var queryHeaderDataList = new MultiDictList<string, List<string>>();

                var stepListTemp = new MultiDictList<string, string>();

                for (int i = 1; i <= wb.Worksheets.Count; i++)
                {
                    workSheetName = wb.Worksheet(i).Name;

                    if (workSheetName == WorkSheet.GENERAL)
                    {

                        ws = wb.Worksheet(workSheetName);
                        int startRowIdx = 2;
                        int excelRowDataCnt = ws.LastRowUsed().RowNumber();

                        string indexVal = string.Empty;
                        for (int x = startRowIdx; x <= excelRowDataCnt; x++)
                        {
                            IXLRow currRow = ws.Row(x);
                            indexVal = currRow.Cell(1).GetString();
                            stepListTemp.Add(indexVal, currRow.Cell(2).GetString());
                            stepListTemp.Add(indexVal, currRow.Cell(3).GetString());
                        }


                    }
                    else if (workSheetName == WorkSheet.INPUT)
                    {
                        ws = wb.Worksheet(workSheetName);
                        int startRowIdx = 2;
                        int excelRowDataCnt = ws.LastRowUsed().RowNumber() + startRowIdx;

                        int currentRunningIdx = startRowIdx;
                        List<string> colNameList = null;
                        List<string> colValList = null;

                        string dataFor, indexCellVal, seqCellVal, prevIndexCellVal, prevSeqVal;
                        dataFor = indexCellVal = seqCellVal = prevIndexCellVal = prevSeqVal = string.Empty;

                        var myMultiDictList = new MultiDictList<string, List<string>>();

                        while (currentRunningIdx < excelRowDataCnt)
                        {
                            IXLRow currRow = ws.Row(currentRunningIdx);
                            IXLCell currCell = currRow.Cell(1);

                            indexCellVal = currCell.GetString();
                            if (IsUnicodeValue(indexCellVal) || string.IsNullOrEmpty(indexCellVal))
                            {
                                currentRunningIdx++;
                                continue;
                            }

                            seqCellVal = currCell.CellRight(1).GetString();
                            int colCnt = currRow.CellsUsed().Count();
                            int startDataColIdx = 3;
                            if (!string.IsNullOrEmpty(prevSeqVal) && prevSeqVal != seqCellVal)
                            {
                                inputDataList.Add(prevIndexCellVal, prevSeqVal, myMultiDictList);
                                myMultiDictList = new MultiDictList<string, List<string>>();

                            }
                            if (indexCellVal.StartsWith("Control") || indexCellVal.StartsWith("SS"))
                            {
                                dataFor = indexCellVal;

                                colNameList = new List<string>();
                                for (int x = startDataColIdx; x <= colCnt + 1; x++)
                                {
                                    colNameList.Add(currRow.Cell(x).GetString());
                                }
                            }
                            else
                            {

                                colValList = new List<string>();
                                for (int y = startDataColIdx; y < colNameList.Count + startDataColIdx; y++)
                                {
                                    string presentCellVal = currRow.Cell(y).GetString();
                                    colValList.Add(presentCellVal);
                                }

                                if (myMultiDictList.Count == 0)
                                {
                                    myMultiDictList.Add(dataFor, colNameList);
                                }
                                myMultiDictList.Add(dataFor, colValList);

                                prevSeqVal = seqCellVal;
                            }
                            prevIndexCellVal = indexCellVal;
                            currentRunningIdx++;
                            if (currentRunningIdx == excelRowDataCnt - 1)
                            {
                                inputDataList.Add(prevIndexCellVal, prevSeqVal, myMultiDictList);
                                myMultiDictList = new MultiDictList<string, List<string>>();
                            }
                        }
                    }
                    else if (workSheetName == WorkSheet.QUERY)
                    {
                        ws = wb.Worksheet(workSheetName);
                        int startRowIdx = 2;
                        int queryExcelRowDataCnt = ws.LastRowUsed().RowNumber() + startRowIdx;

                        int currentRunningIdx = startRowIdx;
                        List<string> colNameList = null;
                        List<string> colValList = null;

                        string indexCellVal = string.Empty;


                        while (currentRunningIdx < queryExcelRowDataCnt)
                        {
                            IXLRow currRow = ws.Row(currentRunningIdx);
                            IXLCell currCell = currRow.Cell(1);

                            indexCellVal = currCell.GetString();
                            if (IsUnicodeValue(indexCellVal) || string.IsNullOrEmpty(indexCellVal))
                            {
                                currentRunningIdx++;
                                continue;
                            }

                            int colCnt = currRow.CellsUsed().Count();
                            int startDataColIdx = 3;

                            if (indexCellVal.StartsWith("Control"))
                            {
                                colNameList = new List<string>();
                                for (int x = startDataColIdx; x <= colCnt + 1; x++)
                                {
                                    string cellVal = currRow.Cell(x).GetString();
                                    if (!string.IsNullOrEmpty(cellVal))
                                    {
                                        colNameList.Add(currRow.Cell(x).GetString());
                                    }
                                }
                            }
                            else
                            {
                                colValList = new List<string>();
                                for (int y = startDataColIdx; y < colNameList.Count + startDataColIdx; y++)
                                {
                                    colValList.Add(currRow.Cell(y).GetString());
                                }
                                queryHeaderDataList.Add(indexCellVal, colNameList);
                                queryHeaderDataList.Add(indexCellVal, colValList);
                            }
                            currentRunningIdx++;


                        }
                    }
                    excelDto.InputDataList = inputDataList;
                    excelDto.QueryDataList = queryHeaderDataList;
                    excelDto.StepList = stepListTemp;

                }

            }
            return excelDto;
        }


        public void WriteExcel(string sourceFilePath, string destFilePath, string[,] logDataList, SysLogDto sysLogDto)
        {
            if (!File.Exists(sourceFilePath))
            {
                return;
            }
            using (XLWorkbook wb = new XLWorkbook(sourceFilePath))
            {
                string workSheetName = string.Empty;
                IXLWorksheet ws;
                for (int i = 1; i <= wb.Worksheets.Count; i++)
                {
                    workSheetName = wb.Worksheet(i).Name;
                    string imgLogLoc = string.Empty;
                    if (workSheetName == WorkSheet.GENERAL)
                    {
                        // Get upper bounds for the array
                        int rowCnt = logDataList.GetUpperBound(0);
                        int colCnt = logDataList.GetUpperBound(1);

                        ws = wb.Worksheet(workSheetName);
                        string colName_WS = "Success";
                        IXLCell updateHeaderCell = wb.Worksheet(workSheetName).RangeUsed().CellsUsed(c => c.Value.ToString() == colName_WS).FirstOrDefault();
                        int startColIndx = updateHeaderCell.WorksheetColumn().ColumnNumber();

                        for (int ii = 0; ii <= rowCnt; ii++)
                        {
                            for (int jj = 0; jj <= colCnt; jj++)
                            {
                                if (!string.IsNullOrEmpty(logDataList[ii, jj]))
                                {
                                    ws.Cell(ii + 2, jj + startColIndx).Value = logDataList[ii, jj];
                                }

                            }
                            if (!string.IsNullOrEmpty(logDataList[ii, colCnt]))
                            {
                                imgLogLoc = logDataList[ii, colCnt];
                                ws.Cell(ii + 2, colCnt + startColIndx).Value = imgLogLoc;
                                ws.Cell(ii + 2, colCnt + startColIndx).Hyperlink = new XLHyperlink("'Image Log'!A1");
                                IXLWorksheet imageLogSheet = wb.AddWorksheet(WorkSheet.IMAGE);
                                string filePath = Path.Combine(Global.ImageLogDirectory, imgLogLoc);
                                ClosedXML.Excel.Drawings.IXLPicture pic = imageLogSheet.AddPicture(@filePath);
                                pic.MoveTo(imageLogSheet.Cell(2, 2));
                                pic.Scale(1);
                            }
                        }

                        ws.ColumnsUsed().AdjustToContents();
                        ws.RowsUsed().AdjustToContents();
                        ws.Row(1).AdjustToContents();
                        ws.Row(1).ClearHeight();
                        ws.Cell("A2").Select();
                    }
                    else if (workSheetName == WorkSheet.SYSTEM)
                    {
                        ws = wb.Worksheet(workSheetName);
                        string colName_Sys = "Value";
                        IXLCell rowCell = ws.RangeUsed().CellsUsed(c => c.Value.ToString() == colName_Sys).FirstOrDefault();
                        int cnt = 0;
                        rowCell.CellBelow(++cnt).Value = sysLogDto.SiteAddr;
                        rowCell.CellBelow(++cnt).Value = sysLogDto.ID;
                        rowCell.CellBelow(++cnt).Value = sysLogDto.Language;
                        rowCell.CellBelow(++cnt).Value = sysLogDto.Browser;
                        rowCell.CellBelow(++cnt).Value = sysLogDto.ProductName;
                        rowCell.CellBelow(++cnt).Value = sysLogDto.WebERPVersion;
                        rowCell.CellBelow(++cnt).Value = sysLogDto.IsEssUser;
                        rowCell.CellBelow(++cnt).Value = sysLogDto.IsLayoutAdmin;
                        rowCell.CellBelow(++cnt).Value = sysLogDto.IsDev;
                        rowCell.CellBelow(++cnt).Value = sysLogDto.IsRememberLoginId;
                        rowCell.CellBelow(++cnt).Value = sysLogDto.Company;
                        rowCell.CellBelow(++cnt).Value = sysLogDto.LoginSeq;
                        rowCell.CellBelow(cnt).DataType = XLDataType.Number;
                        rowCell.CellBelow(cnt).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;

                        rowCell.CellBelow(++cnt).Value = sysLogDto.UserSeq;
                        rowCell.CellBelow(cnt).DataType = XLDataType.Number;
                        rowCell.CellBelow(cnt).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                        rowCell.CellBelow(++cnt).Value = sysLogDto.UserName;
                        rowCell.CellBelow(++cnt).Value = sysLogDto.DeptName;
                        rowCell.CellBelow(++cnt).Value = sysLogDto.LoginDateTime;
                        rowCell.CellBelow(cnt).DataType = XLDataType.DateTime;
                        rowCell.CellBelow(cnt).Style.DateFormat.Format = "YYYY/MM/DD HH:MM:SS";
                        rowCell.CellBelow(++cnt).Value = sysLogDto.LicenseType;
                        rowCell.CellBelow(++cnt).Value = sysLogDto.IsUseBI;
                        rowCell.CellBelow(++cnt).Value = sysLogDto.BrowserSize;
                        rowCell.CellBelow(++cnt).Value = sysLogDto.BrowserPoint;
                        rowCell.CellBelow(++cnt).Value = sysLogDto.DSN;
                        rowCell.CellBelow(++cnt).Value = sysLogDto.PCLanguage;
                        rowCell.CellBelow(++cnt).Value = sysLogDto.PCLocale;
                        rowCell.CellBelow(++cnt).Value = sysLogDto.PCName;
                        rowCell.CellBelow(++cnt).Value = sysLogDto.PCMacAddress;
                        rowCell.CellBelow(++cnt).Value = sysLogDto.PCMonitorResolution;
                        rowCell.CellBelow(++cnt).Value = sysLogDto.PCMonitorSize;
                        rowCell.CellBelow(++cnt).Value = sysLogDto.PCMemory;
                        rowCell.CellBelow(++cnt).Value = sysLogDto.OSVersion;

                        ws.ColumnsUsed().AdjustToContents();
                        ws.RowsUsed().AdjustToContents();
                    }
                    else if (workSheetName == WorkSheet.IMAGE)
                    {
                        ws = wb.Worksheet(workSheetName);
                    }
                    wb.Worksheet(i).SetTabSelected(false);
                }
                wb.Worksheet(1).SetTabActive();
                wb.Worksheet(1).SetTabSelected();
                wb.SaveAs(destFilePath);
            }
        }


        private static bool IsUnicodeValue(string input)
        {
            var asciiBytesCount = Encoding.ASCII.GetByteCount(input);
            var unicodeBytesCount = Encoding.UTF8.GetByteCount(input);
            return asciiBytesCount != unicodeBytesCount;
        }

    }
}
