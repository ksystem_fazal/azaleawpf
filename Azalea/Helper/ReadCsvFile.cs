﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Azalea
{

    public static class DataDrivenTestPurpose
    {
        public static Dictionary<string, string> ConvertCsvToDictionary(string filePath)
        {
            DataTable dtCsv = new DataTable();
            string Fulltext;
            using (StreamReader sr = new StreamReader(File.OpenRead(filePath)))
            {
                while (!sr.EndOfStream)
                {
                    Fulltext = sr.ReadToEnd().ToString();
                    string[] stringSeparators = new string[] { "\r\n" };
                    string[] rows = Fulltext.Split(stringSeparators, StringSplitOptions.None);
                    for (int i = 0; i < rows.Count(); i++)
                    {
                        string[] rowValues = rows[i].Split(',');
                        if (i == 0)
                        {
                            for (int j = 0; j < rowValues.Count(); j++)
                            {
                                dtCsv.Columns.Add(rowValues[j]);
                            }
                        }
                        else
                        {
                            DataRow dr = dtCsv.NewRow();
                            for (int j = 0; j < rowValues.Count(); j++)
                            {
                                if (string.IsNullOrEmpty(rowValues[0].ToString()))
                                {
                                    break;
                                }
                                dr[j] = rowValues[j].ToString();
                            }
                            dtCsv.Rows.Add(dr);
                        }
                    }
                }
            }
            Dictionary<string, string> dataList = new Dictionary<string, string>();

            for (int j = 0; j < dtCsv.Columns.Count; j++)
            {
                string key = Convert.ToString(dtCsv.Columns[j]);
                string value = Convert.ToString(dtCsv.Rows[0].ItemArray[j]);
                dataList.Add(key, value);

            }
            return dataList;
        }

        public static DataTable ConvertCsvToDataTable(string filePath)
        {
            DataTable dtCsv = new DataTable();
            string Fulltext;
            using (StreamReader sr = new StreamReader(File.OpenRead(filePath)))
            {
                while (!sr.EndOfStream)
                {
                    Fulltext = sr.ReadToEnd().ToString();
                    string[] stringSeparators = new string[] { "\r\n" };
                    string[] rows = Fulltext.Split(stringSeparators, StringSplitOptions.None);
                    for (int i = 0; i < rows.Count(); i++)
                    {
                        string[] rowValues = rows[i].Split(',');
                        if (i == 0)
                        {
                            for (int j = 0; j < rowValues.Count(); j++)
                            {
                                dtCsv.Columns.Add(rowValues[j]);
                            }
                        }
                        else
                        {
                            DataRow dr = dtCsv.NewRow();
                            for (int j = 0; j < rowValues.Count(); j++)
                            {
                                if (string.IsNullOrEmpty(rowValues[0].ToString()))
                                {
                                    break;
                                }
                                dr[j] = rowValues[j].ToString();
                            }
                            dtCsv.Rows.Add(dr);
                        }
                    }
                }
            }
            return dtCsv;
        }
    }
}
