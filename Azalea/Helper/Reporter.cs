﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Azalea.Helper
{
    public class Reporter
    {

        private string _linecsv;
        private string _fileDirectory;
        private string _filePath;
        private string _formattedMessage;
        StringBuilder sb = new StringBuilder(25);

        public Reporter(string classMethodId)
        {
            _fileDirectory = Path.Combine(Global.ReportDirectory, Global.CreateLogTime);
            CreateDirectory();
            CreateCsvFile(classMethodId);

        }

        private void CreateDirectory()
        {
            if(!Directory.Exists(_fileDirectory))
            {
                Directory.CreateDirectory(_fileDirectory);
                File.Copy(Path.Combine(Global.ClassMethodDirectory, Global.ClassMethodName), Path.Combine(_fileDirectory, Global.ReportLogRootName));
            }
        }

        private void CreateCsvFile(string classMethodId)
        {
            _filePath = Path.Combine(_fileDirectory, classMethodId + ".csv");
            _linecsv = "Type,Message,Time,Picture";
            File.AppendAllText(_filePath, _linecsv);
        }

        private void AddLine(string reportLogType, string message, DateTime time, string picture = "")
        {
            message = message.Replace(",", "|||");
            _linecsv = string.Format("{0},{1},{2},{3}", Environment.NewLine + reportLogType, message, time.ToString(Global.DateFormatString), picture);
            File.AppendAllText(_filePath, _linecsv);
        }

        public void Record_Error(string message)
        {
            AddLine(ReportLogItemType.ERROR, message, DateTime.Now);
        }
        public void Record_Warning(string message)
        {
            AddLine(ReportLogItemType.WARNING, message, DateTime.Now);
        }
        public void Record_Message(string message)
        {
            AddLine(ReportLogItemType.MESSAGE, message, DateTime.Now);
        }
        public void Record_Checkpoint(string message)
        {
            AddLine(ReportLogItemType.CHECKPOINT, message, DateTime.Now);
        }

        public void Record_Hover(string reportLogType, DateTime time, Point point)
        {
            _formattedMessage = "Moved to (" + point.X + ", " + point.Y + ")";
            AddLine(reportLogType, _formattedMessage, time);
        }

        public void Record_Drag(string reportLogType, DateTime time, Point point1, Point point2)
        {
            _formattedMessage = "Dragged from (" + point1.X + ", " + point1.Y + ") to (" + point2.X + ", " + point2.Y + ")";
            AddLine(reportLogType, _formattedMessage, time);
        }

        public void Record_Click(string reportLogType, DateTime time, Point point)
        {
            _formattedMessage = "Clicked at (" + point.X + ", " + point.Y + ")";
            AddLine(reportLogType, _formattedMessage, time);
        }

        public void Record_RgtClick(string reportLogType, DateTime time, Point point)
        {
            _formattedMessage = "Right clicked at (" + point.X + ", " + point.Y + ")";
            AddLine(reportLogType, _formattedMessage, time);
        }

        public void Record_DblClick(string reportLogType, DateTime time, Point point)
        {
            _formattedMessage = "Double clicked at (" + point.X + ", " + point.Y + ")";
            AddLine(reportLogType, _formattedMessage, time);
        }

        public void Record_Input(string reportLogType, DateTime time, Point point, string value)
        {
            _formattedMessage = "Inputted " + value.Replace(OpenQA.Selenium.Keys.Enter, "[Enter]");
            AddLine(reportLogType, _formattedMessage, time);
        }
    }
}
