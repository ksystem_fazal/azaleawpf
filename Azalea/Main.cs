﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using Azalea.Helper;
using Azalea.TestCases;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Diagnostics;
using Azalea.Model;
using OpenQA.Selenium.Remote;
using System.Drawing;
using System.Threading;
using System.Windows;

namespace Azalea
{


    [TestFixture, Apartment(ApartmentState.STA)]
    public class Main
    {
        public Tracker tracker;
        public static int progressCountMax;

        private static IWebDriver _driver;


        public bool IsBrowserClosed()
        {
            bool isClosed = false;
            try
            {
                tracker.SetInfo("Checking last browser instance");
                string title = _driver.Title;
            }
            catch (Exception)
            {
                isClosed = true;
            }
            return isClosed;
        }

        public IWebDriver CurrWebDriver
        {
            get
            {
                if (_driver == null || IsBrowserClosed())
                {
                    List<Int64> conhostPID = new List<Int64>();
                    foreach (var process in Process.GetProcessesByName("conhost"))
                    {
                        conhostPID.Add(process.Id);
                    }
                    if (Global.Browser == null || Global.Browser == "chrome")
                    {
                        tracker.SetInfo("Opening Browser " + Global.Browser);
                        ChromeOptions options = new ChromeOptions();
                        options.AddArguments("--test-type", "--start-maximized");
                        options.AddArgument("--log-level=3");

                        //options.AddArguments("disable-infobars");
                        options.AddArguments("--incognito");

                        var chromeDriverService = ChromeDriverService.CreateDefaultService();
                        chromeDriverService.HideCommandPromptWindow = true;
                        _driver = new ChromeDriver(chromeDriverService, options);

                        //DesiredCapabilities capabilities = new DesiredCapabilities();
                        //capabilities.SetCapability(CapabilityType.BrowserName, "chrome");
                        //_driver = new RemoteWebDriver(new Uri("http://10.1.3.44:4444/wd/hub"), capabilities, TimeSpan.FromSeconds(600));
                        /*
                        var driverPath = @"E:\BitBucket\azaleawpf\Driver";
                                                var chromeDriverService = ChromeDriverService.CreateDefaultService(driverPath);
                                                chromeDriverService.HideCommandPromptWindow = true;
                                                chromeDriverService.Start();
                                                _driver = new ChromeDriver(chromeDriverService, options);

                                                DesiredCapabilities capabilities = new DesiredCapabilities();
                                                capabilities.SetCapability(CapabilityType.BrowserName, "chrome");
                                                //_driver = new RemoteWebDriver(new Uri("http://10.1.3.44:4444/wd/hub"), capabilities, TimeSpan.FromSeconds(600));
                                                */
                        tracker.ClearInfo();
                    }
                    else
                    {
                        _driver = new FirefoxDriver();
                    }
                    foreach (var process in Process.GetProcessesByName("conhost"))
                    {
                        if (conhostPID.IndexOf(process.Id) == -1)
                        {
                            process.Kill();
                            break;
                        }
                    }

                }
                return _driver;
            }
            set { _driver = value; }
        }

        public static Reporter _reporter;

        public static Reporter Reporter
        {
            get { return _reporter; }
        }


        [OneTimeSetUp]
        public void SetupTest()
        {
            //Init SiteSeq, Language, Browser
            Global.SL_UpdateData = new Model.SysLogDto();
            Global.SiteAddr = Global.SL_UpdateData.SiteAddr = TestContext.Parameters["SiteAddr"];
            Global.User = Global.SL_UpdateData.ID = TestContext.Parameters["User"];
            Global.Pass = TestContext.Parameters["Pass"];
            Global.Language = Global.SL_UpdateData.Language = TestContext.Parameters["Language"];
            Global.Browser = TestContext.Parameters["Browser"];

            //Init CreateLogTime
            Global.CreateLogTime = DateTime.Now.ToString("yyyyMMddHHmmss");

            //Delete Old ExportLogDetail and Create New
            string ExportLogDetail = Path.Combine(Global.DataDirectory, Global.ExportLogDetailName);
            if (File.Exists(ExportLogDetail))
            {
                File.Delete(ExportLogDetail);
            }
            File.Create(ExportLogDetail).Close();
            string csvLine = string.Format("{0},{1},{2},{3},{4},{5}", "Step", "TestCaseId", "TestCaseSubId", "Message", "IsFail", "ExecuteDateTime");
            File.AppendAllText(ExportLogDetail, csvLine);

            //Load Tracker Indicator
            tracker = new Tracker(progressCountMax);
            CurrWebDriver.Navigate().GoToUrl(Global.SiteAddr);
        }

        public static List<ClassMethodDto> ClassAndMethodList()
        {
            string sourcePath = Path.Combine(Global.ClassMethodDirectory, Global.ClassMethodName);
            IEnumerable<ClassMethodDto> strList = from line in File.ReadLines(sourcePath).Skip(1)
                                                  let columns = line.Split(',')
                                                  select new ClassMethodDto
                                                  {
                                                      TestItemPath = columns[0],
                                                      ClassName = columns[1],
                                                      MethodName = columns[2],
                                                      Identity = columns[3]
                                                  };

            progressCountMax = strList.Count();

            return strList.ToList();
        }

        [Test]
        [TestCaseSource("ClassAndMethodList")]
        public void MainTestCase(ClassMethodDto classMethod)
        {
            Thread thread = new Thread(() =>
            {
                RunTestCase(classMethod);
                tracker.CloseForm();
            });
            thread.Start();
            tracker.ShowDialog();
        }

        private void RunTestCase(ClassMethodDto classMethod)
        {
            Exception ex = null;
            string className;
            try
            {
                _reporter = new Reporter(classMethod.Identity.Substring(classMethod.Identity.LastIndexOf("/") + 1));

                Assembly assembly = Assembly.LoadFrom(Global.DllLocation);

                className = classMethod.ClassName;
                Type type = assembly.GetType("Azalea.TestCases." + className);
                ConstructorInfo constructorInfo = type.GetConstructor(new[] { typeof(IWebDriver) });
                object instance = constructorInfo.Invoke(new object[] { CurrWebDriver });

                string methodName = classMethod.MethodName;
                var method = instance.GetType().GetMethod(methodName);

                try
                {
                    tracker.SetInfo("Running " + className + "." + methodName);
                    method.Invoke(instance, new object[] { });
                    tracker.StepUpProgress();
                }
                catch (Exception e)
                {
                    ex = e;
                    throw new AzaleaMethodException(e.Message, e.InnerException);
                }
            }
            catch (Exception e)
            {
                ex = e;
                throw new AzaleaMainException(e.Message, e.InnerException);
            }
            finally
            {
                if (ex == null)
                {
                    Assert.IsTrue(true);
                }
                else
                {
                    string errMsg;
                    string errDetail;
                    if (ex.InnerException == null)
                    {
                        errMsg = ex.Message;
                        errDetail = ex.StackTrace;
                    }
                    else
                    {
                        errMsg = ex.InnerException.Message;
                        errDetail = ex.InnerException.StackTrace;
                    }
                    errMsg = errMsg.Replace("\r\n", Keys.TAB).Replace("\n", Keys.TAB);
                    errDetail = errDetail.Replace("\r\n", "###").Replace("\n", "###");
                    Reporter.Record_Error(errMsg);
                    Reporter.Record_Error(errDetail);
                    Assert.Fail(errMsg);

                }
            }
        }

        [OneTimeTearDown]
        public void TearDownTest()
        {
            //CurrWebDriver.Close();
            //CurrWebDriver.Quit();
            tracker.CloseForm();
        }

    }
}
