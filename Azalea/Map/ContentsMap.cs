﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Azalea.Map
{
    class ContentsMap
    {
        public ContentsMap(IWebDriver driver)
        {
            PageFactory.InitElements(driver, this);
        }

        [FindsBy(How = How.Id, Using = "contentsArea")]
        private IWebElement ContentsBox { get; set; }

        public IWebElement CanvasObjBy(string workbookId)
        {
            return ContentsBox.FindElement(By.CssSelector("#" + workbookId + " canvas"));
        }

        [FindsBy(How = How.Id, Using = "divSheetContext")]
        private IWebElement SheetContext { get; set; }

        public IWebElement ContextItemBy(string itemId)
        {
            return SheetContext.FindElement(By.CssSelector("#" + itemId));
        }

        public IWebElement TreeOpenBtnBy(string treeId)
        {
            return SheetContext.FindElement(By.CssSelector("#" + treeId + " .treeOpenBtn"));
        }

        public IWebElement TreeNodeObjBy(string treeId, string seq)
        {
            return SheetContext.FindElement(By.CssSelector("#" + treeId + " span[seq='" + seq + "']"));
        }
    }
}
