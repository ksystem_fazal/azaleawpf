﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Azalea.Map
{
    class GeneralMap
    {
        public GeneralMap(IWebDriver driver)
        {
            PageFactory.InitElements(driver, this);
        }

        [FindsBy(How = How.Id, Using = "divOpenPageLoading")]
        public IWebElement PageLoadingObj { get; set; }

        public IWebElement PageLoadingMsgObj {
            get {
                return PageLoadingObj.FindElement(By.Id("divPageLoadingMsg"));
            }
        }
    }
}
