﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Azalea.Map
{
    class HeaderMap
    {
        public HeaderMap(IWebDriver driver)
        {
            PageFactory.InitElements(driver, this);
        }

        [FindsBy(How = How.CssSelector, Using = ".CenterHeaderArea")]
        private IWebElement headerBox { get; set; }

        public IWebElement tabItemBy(string pgmSeq)
        {
            return headerBox.FindElement(By.CssSelector(".modeTopOpenList li[pgmseq='" + pgmSeq + "']"));
        }

        public IWebElement closeTabBy(string pgmSeq)
        {
            return headerBox.FindElement(By.CssSelector(".modeTopOpenList li[pgmseq='" + pgmSeq + "'] .menuClose"));
        }

        [FindsBy(How = How.Id, Using = "btnProgramQueryArea")]
        public IWebElement programQueryBtn { get; set; }

        [FindsBy(How = How.Id, Using = "inputProgramQuery")]
        public IWebElement programQueryText { get; set; }
    }
}
