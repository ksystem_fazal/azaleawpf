﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace Azalea.Map
{
    class LeftModuleMap
    {
        public LeftModuleMap(IWebDriver driver)
        {
            PageFactory.InitElements(driver, this);
        }

        [FindsBy(How = How.Id, Using = "UserInfoImg")]
        public IWebElement userInfoObj { get; set; }

        [FindsBy(How = How.CssSelector, Using = "#LogoutFrm .iconLogout")]
        public IWebElement logOutBtn { get; set; }
    }
}
