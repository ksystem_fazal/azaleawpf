﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;


namespace Azalea.Map
{
    class LoginPageMap
    {
        public LoginPageMap(IWebDriver driver)
        {
            PageFactory.InitElements(driver, this);
        }

        [FindsBy(How = How.XPath, Using = "//*[@id='ulLoginLanguage']")]
        private IWebElement langLinksParent { get; set; }

        public IWebElement langLinkBy(string lang)
        {
            return langLinksParent.FindElement(By.LinkText(lang));
        }

        [FindsBy(How = How.Id, Using = "inputLoginId")]
        public IWebElement userText { get; set; }

        [FindsBy(How = How.Id, Using = "inputLogin")]
        public IWebElement passText { get; set; }

        [FindsBy(How = How.Id, Using = "btnLogin")]
        public IWebElement loginBtn { get; set; }

    }
}
