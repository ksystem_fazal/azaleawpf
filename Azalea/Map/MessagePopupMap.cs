﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Azalea.Map
{
    class MessagePopupMap
    {
        public MessagePopupMap(IWebDriver driver)
        {
            PageFactory.InitElements(driver, this);
        }

        [FindsBy(How = How.Id, Using = "messagePopup")]
        public IWebElement messageBox { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='messagePopup']//*[contains(@class, 'divMessageBgBox')][not(contains(@style,'display:none'))]")]
        private IWebElement visibleMessageBox { get; set; }

        public IWebElement okMsgBtn {
            get { return visibleMessageBox.FindElement(By.Id("msgBtnOk")); }
        }

        public IWebElement noMsgBtn
        {
            get { return visibleMessageBox.FindElement(By.Id("msgBtnNo")); }
        }

        public IWebElement cancelMsgBtn
        {
            get { return visibleMessageBox.FindElement(By.Id("msgBtnCancel")); }
        }

        public IWebElement descriptionText
        {
            get { return visibleMessageBox.FindElement(By.CssSelector(".txtDescription")); }
        }
    }
}
