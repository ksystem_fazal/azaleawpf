﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Azalea.Map
{
    class ToolbarMap
    {
        public ToolbarMap(IWebDriver driver)
        {
            PageFactory.InitElements(driver, this);
        }

        [FindsBy(How = How.CssSelector, Using = ".ulToolbar")]
        private IWebElement ToolbarBox { get; set; }

        public IWebElement JumpPgmBtnBy(string jumpColId)
        {
            return ToolbarBox.FindElement(By.CssSelector("li[colindex='" + jumpColId + "'] :nth-child(2)"));
        }

        [FindsBy(How = How.XPath, Using = ".//img[@src ='/Images/Toolbar/T_New.png']")]
        public IWebElement NewPgmBtn { get; set; }

        [FindsBy(How = How.XPath, Using = ".//img[@src ='/Images/Toolbar/T_Query.png']")]
        public IWebElement QueryPgmBtn { get; set; }

        [FindsBy(How = How.XPath, Using = ".//img[@src ='/Images/Toolbar/T_Save.png']")]
        public IWebElement SavePgmBtn { get; set; }

    }
}
