﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Azalea.Model
{
    public class ClassMethodDto
    {
        public string TestItemPath { get; set; }
        public string ClassName { get; set; }
        public string MethodName { get; set; }
        public string Identity { get; set; }
    }
}
