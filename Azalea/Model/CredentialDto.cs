﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Azalea
{
    public class CredentialDto
    {
        public string url { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string language { get; set; }
        public string browser { get; set; }
    }
}
