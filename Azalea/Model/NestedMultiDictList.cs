﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Azalea.Model
{
    public class NestedMultiDimDictList<K, K2, T> :
            Dictionary<K, MultiDictList<K2, T>>
    {
        public void Add(K key, K2 key2, T addObject)
        {
            if (!ContainsKey(key))
            {
                MultiDictList<K2, T> key2List = new MultiDictList<K2, T>
                {
                    { key2, new List<T>() }
                };
                Add(key, key2List);
            }

            if (!base[key].ContainsKey(key2))
                base[key].Add(key2, new List<T>());
            base[key].Add(key2, addObject);


        }
    }
}
