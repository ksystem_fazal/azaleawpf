﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Azalea.Model
{
    public class ReadExcelDto
    {
        public NestedMultiDimDictList<string, string, MultiDictList<string, List<string>>> InputDataList { get; set; }
        public MultiDictList<string, string> StepList { get; set; }
        public MultiDictList<string, List<string>> QueryDataList { get; set; }

    }
}
