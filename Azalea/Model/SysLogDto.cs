﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Azalea.Model
{
    public class SysLogDto
    {
        public string SiteAddr { get; set; }
        public string ID { get; set; }
        public string Language { get; set; }
        public string Browser { get; set; }
        public string ProductName { get; set; }
        public string WebERPVersion { get; set; }
        public string IsEssUser { get; set; }
        public string IsLayoutAdmin { get; set; }
        public string IsDev { get; set; }
        public string IsRememberLoginId { get; set; }
        public string Company { get; set; }
        public string LoginSeq { get; set; }
        public string UserSeq { get; set; }
        public string UserName { get; set; }
        public string DeptName { get; set; }
        public string LoginDateTime { get; set; }
        public string LicenseType { get; set; }
        public string IsUseBI { get; set; }
        public string BrowserSize { get; set; }
        public string BrowserPoint { get; set; }
        public string DSN { get; set; }
        public string PCLanguage { get; set; }
        public string PCLocale { get; set; }
        public string PCName { get; set; }
        public string PCMacAddress { get; set; }
        public string PCMonitorResolution { get; set; }
        public string PCMonitorSize { get; set; }
        public string PCMemory { get; set; }
        public string OSVersion { get; set; }

    }
}
