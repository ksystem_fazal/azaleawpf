﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Azalea
{
    public class WSMetaDto
    {
        public string INDEX { get; set; }
        public string PgmSeq { get; set; }
        public string WorkSheets { get; set; }
        public string Caption { get; set; }

    }
}
