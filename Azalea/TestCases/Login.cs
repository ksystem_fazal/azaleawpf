﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SeleniumExtras.PageObjects;
using Azalea.Map;

namespace Azalea.TestCases
{
    public class Login
    {
        private IWebDriver _driver;
        private LoginPageMap loginPageMap;

        private CommonActions cA = null;

        public Login(IWebDriver driver)
        {
            _driver = driver;
            cA = new CommonActions();
            loginPageMap = new LoginPageMap(_driver);
        }

        public void Execute()
        {
            LoginPage();
        }

        private void LoginPage()
        {            
            cA.ClickEvent(_driver, loginPageMap.langLinkBy(Global.Language));
            cA.InputEvent(_driver, loginPageMap.userText, Global.User);
            cA.InputEvent(_driver, loginPageMap.passText, Global.Pass);

            string scriptVal = "return $('#RememberMe').is(':checked');";
            bool checkRememberMe = cA.ExecuteScript<bool>(_driver, scriptVal);
            Global.SL_UpdateData.IsRememberLoginId = checkRememberMe.ToString();

            cA.ClickEvent(_driver, loginPageMap.loginBtn);

            cA.WaitMethodUsingId(_driver, "btnProgramQueryArea", 30);
            Main.Reporter.Record_Checkpoint("Logged in successfully");
        }
    }
}
