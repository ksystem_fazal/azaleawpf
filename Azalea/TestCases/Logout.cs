﻿using Azalea.Map;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Azalea.TestCases
{
    class Logout
    {
        private IWebDriver _driver;
        private CommonActions cA = null;
        private LeftModuleMap leftModuleMap;

        public Logout(IWebDriver driver)
        {
            _driver = driver;
            cA = new CommonActions();

            leftModuleMap = new LeftModuleMap(_driver);

        }

        public void Execute()
        {
            _driver.SwitchTo().DefaultContent();

            cA.ClickEvent(_driver, leftModuleMap.userInfoObj);
            cA.ClickEvent(_driver, leftModuleMap.logOutBtn);

            cA.ClickMesssageButton(_driver, ButtonType.MSG_BTN_OK);

            cA.WaitMethodUsingId(_driver, "loginForm", 20);

            Main.Reporter.Record_Checkpoint("Logged out successfully");
        }
    }
}
