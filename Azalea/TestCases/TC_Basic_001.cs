﻿using Azalea.Helper;
using Azalea.Model;
using ClosedXML;
using ClosedXML.Excel;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;

namespace Azalea.TestCases
{
    internal class TC_Basic_001
    {
        private readonly IWebDriver currDriver;

        NestedMultiDimDictList<string, string, MultiDictList<string, List<string>>> _inputDataList = null;
        MultiDictList<string, List<string>> _queryDataList = null;
        MultiDictList<string, string> _stepsList = null;
        private readonly string workbookId = "SS1_c";
        private readonly string workbookId2 = "SS_c";
        private string msgVal = string.Empty;

        private readonly LogReport cLR = null;
        private readonly CommonActions cA = null;
        private readonly EventHandler eH = null;
        private readonly SheetActions cSA = null;
        private readonly SubFormActions sFA = null;
        private readonly ExcelRW excelRW = null;

        public TC_Basic_001(IWebDriver currWebDriver)
        {
            this.currDriver = currWebDriver;
            cLR = new LogReport();
            cA = new CommonActions();
            eH = new EventHandler();
            cSA = new SheetActions();
            sFA = new SubFormActions();
            excelRW = new ExcelRW();
        }

        public void _1()
        {
            try
            {
                string pgmSeq = "502951";
                string subFormId = "502948";
                string subWorkbookId = "SS1_c";
                string treeId = "EnvTree_t";
                string dataField1 = "EnvSeq";
                string dataField2 = "EnvValueName";

                //Step-1
                cA.OpenPgm(currDriver, eH, pgmSeq);

                //Init MetaData
                string sourceFilePath = Path.Combine(Global.DataDirectory, "TC_Basic_001.1.xlsx");
                List<List<string>> nodeSeqList = new List<List<string>>();
                using (var excelWorkbook = new XLWorkbook(sourceFilePath))
                {
                    var sheetData = excelWorkbook.Worksheet(pgmSeq + "_md").RangeUsed().RowsUsed().Skip(1);
                    foreach (IXLRangeRow dataRowItem in sheetData)
                    {
                        nodeSeqList.Add(new List<string>() { dataRowItem.Cell(1).Value.ToString() });
                    }

                    string cellValue = "";
                    int rowIndex = -1;
                    IXLRangeRow dataRow;
                    for (int i = 0; i < nodeSeqList.Count; i++)
                    {
                        //Step-2
                        cA.SelectTreeMenuNodes(currDriver, pgmSeq, treeId, nodeSeqList[i]);

                        sheetData = excelWorkbook.Worksheet(nodeSeqList[i][0].ToString()).RangeUsed().RowsUsed();

                        int EnvSeqIndex = 1;
                        int EnvValueNameIndex = 2;

                        for (int j = 1; j < sheetData.Count(); j++)
                        {
                            dataRow = sheetData.ElementAt(j);
                            cellValue = dataRow.Cell(EnvSeqIndex).Value.ToString();
                            if (cellValue == "")
                            {
                                break;
                            }
                            rowIndex = sFA.GetRowIndex(currDriver, pgmSeq, subFormId, subWorkbookId, dataField1, cellValue);

                            if (rowIndex != -1)
                            {
                                //Step-3
                                cellValue = dataRow.Cell(EnvValueNameIndex).Value.ToString();
                                sFA.cellInput(currDriver, pgmSeq, subFormId, subWorkbookId, rowIndex, dataField2, cellValue);
                            }
                        }

                        //Step-4
                        sFA.SavePgm(currDriver, pgmSeq, subFormId);
                        cA.Delay(2000);
                    } //Step-5 (Repeat Step-2,3,4 for each worksheet)

                    //Step-6
                    (new Logout(currDriver)).Execute();

                    //Step-7
                    (new Login(currDriver)).Execute();
                }
            }
            catch (Exception e)
            {
                throw new AzaleaMethodException(e.Message, e.InnerException);
            }
        }

        public void _2()
        {
            GetSysLogInfo();
            string sourceFilePath = Path.Combine(Global.DataDirectory, "TC_Basic_001.2.xlsx");
            string destFilePath = Path.Combine(Global.ExcelLogDirectory, string.Format("TC_Basic_001.2_{0}_Log.xlsx", Global.CreateLogTime));
            var INDEX = "0";
            Global.ScenarioIndex = INDEX;
            int workerSize = typeof(TC_Basic_001).GetMethods(BindingFlags.NonPublic | BindingFlags.Instance).Where(m => m.Name.StartsWith("_2") && m.ReturnType.Name == "Void").Count();
            eH.ResetEventVars(workerSize);
            try
            {
                Main.Reporter.Record_Checkpoint(string.Format("Reading Excel file from {0}", sourceFilePath));
                ReadExcelDto readExcelDto = excelRW.ReadExcel(sourceFilePath);
                if (readExcelDto != null)
                {
                    _inputDataList = readExcelDto.InputDataList;
                    _queryDataList = readExcelDto.QueryDataList;
                    _stepsList = readExcelDto.StepList;
                }
                Global.CurrentClassName = this.GetType().Name;

                Global.CurrentMethodName = MethodBase.GetCurrentMethod().Name;


                _2_S1();

                Global.ScenarioIndex = INDEX;

            }
            catch (Exception e)
            {
                eH.SetWSMUpdateJson(int.Parse(Global.ScenarioIndex), WorksheetMeta.Log, e.Message);
                string screenshot = cA.SnapScreenshot(currDriver);
                eH.SetWSMUpdateJson(int.Parse(Global.ScenarioIndex), WorksheetMeta.ImageLog, screenshot);
                throw new AzaleaMethodException(e.Message, e.InnerException);
            }
            finally
            {
                excelRW.WriteExcel(sourceFilePath, destFilePath, Global.WSM_UpdateData, Global.SL_UpdateData);
                // EH.end_testCaseRun();
            }
        }

        private void _2_S1()
        {
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 1 Started--------------------");
            var INDEX = "1";
            int intINDEX = 1;
            Global.ScenarioIndex = INDEX;
            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Execute);
            var stepData = _stepsList[INDEX];
            var pgmSeq = stepData[0];
            var workSheetType = stepData[1];

            cA.OpenPgm(currDriver, eH, pgmSeq, intINDEX);
            string pgmSeqFrame = cA.GetPgmSeqFrame(pgmSeq);
            currDriver.SwitchTo().Frame(currDriver.FindElement(By.Id(pgmSeqFrame)));

            var fileName = cA.GetFileName("_S1");
            var pathToFile = Path.Combine(cA.GetFilePath(), fileName);
            if (File.Exists(pathToFile))
            {
                File.Delete(pathToFile);
            }

            bool hasError = false;

            if (IsInputModule(workSheetType))
            {
                var dataSetCountKeys = _inputDataList[INDEX].Keys;


                for (var ii = 0; ii < dataSetCountKeys.Count; ii++)
                {
                    if (ii > 0)
                    {
                        cA.NewPgm(currDriver, pgmSeq);
                    }
                    string iiStrVal = dataSetCountKeys.ElementAt(ii);
                    var workSheetsBundle = _inputDataList[INDEX][iiStrVal];
                    MultiDictList<string, List<string>> workSheetsArr = null;
                    Dictionary<string, List<List<string>>>.KeyCollection datasetKeys = null;
                    List<List<string>> workSheet;
                    for (var seq = 0; seq < workSheetsBundle.Count; seq++)
                    {
                        workSheetsArr = workSheetsBundle[seq];
                        datasetKeys = workSheetsArr.Keys;
                        string dataForVal = datasetKeys.ElementAt(0);
                        workSheet = workSheetsArr[dataForVal];
                        if (dataForVal == "Control")
                        {
                            cA.ReadPgmControlDataList(currDriver, pgmSeq, workSheet);
                        }
                        else
                        {

                            cSA.ReadSheetDataList(currDriver, pgmSeq, workbookId, workSheet);

                            int countDirtyRows = cSA.CountDataRow(currDriver, pgmSeq, workbookId, RowDataType.DIRTY_ROWS);
                            cA.SavePgm(currDriver, eH, pgmSeq, null, intINDEX);
                            int countSavedRows = cSA.CountDataRow(currDriver, pgmSeq, workbookId, RowDataType.SAVED_ROWS);

                            cA.SetInputDataSetCount(currDriver, eH, cSA, intINDEX, pgmSeq, workbookId, (workSheet.Count - 1), 1);
                            if (countDirtyRows == countSavedRows)
                            {
                                string copiedVal = cA.GetControlValue(currDriver, pgmSeq, "txtOrderNo_txt");
                                string copiedValWithIdx = (ii + 1) + Keys.TAB + copiedVal;
                                cA.WriteTempFile(fileName, copiedValWithIdx, true);

                                msgVal = "Scenario Index 1 - " + pgmSeq + " - 수주번호 :  " + copiedVal;
                                cLR.SetLogRecord(intINDEX, msgVal);
                            }
                            else
                            {

                                msgVal = "Scenario Index 1 - " + pgmSeq + " - 수주번호 : 저장이 실패하였습니다.";
                                cLR.SetLogRecord(intINDEX, msgVal);
                                var isPopUpVisible = Validations.IsPopupVisible(currDriver, "messagePopup");
                                if (isPopUpVisible)
                                {
                                    var messageText = cA.GetMessageDescription(currDriver);
                                    cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);

                                    msgVal = "Scenario Index 1 - " + pgmSeq + " - 경고메세지: " + messageText;
                                    cLR.SetLogRecord(intINDEX, msgVal);
                                    eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                                    Global.ScenarioIndex = INDEX;
                                    hasError = true;
                                }
                            }
                        }
                    }
                }
            }

            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 1 End--------------------");

            if (!hasError)
            {
                cA.ClosePgm(currDriver, pgmSeq);
                eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                _2_S2();
            }

        }

        private void _2_S2()
        {
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 2 Started--------------------");
            var INDEX = "2";
            int intINDEX = 2;
            Global.ScenarioIndex = INDEX;

            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.StartTime);
            var stepData = _stepsList[INDEX];
            var pgmSeq = stepData[0];
            var workSheetType = stepData[1];
            var fileName = cA.GetFileName("_S1");
            bool hasError = false;

            if (IsQueryModule(workSheetType))
            {
                var workSheet = _queryDataList[INDEX];
                cA.OpenPgm(currDriver, eH, pgmSeq, intINDEX);
                var lineCount = cA.GetTempFileLineCount(fileName);
                string pgmSeqFrame = cA.GetPgmSeqFrame(pgmSeq);
                currDriver.SwitchTo().Frame(currDriver.FindElement(By.Id(pgmSeqFrame)));
                for (var i = 0; i < lineCount; i++)
                {
                    var orderNoVal = cA.ReadTempFile(fileName, i, 1);
                    cA.SetPgmControlData(currDriver, "txtOrderNo_txt", orderNoVal);

                    cA.ReadPgmControlDataList(currDriver, pgmSeq, workSheet);
                    cA.QueryPgm(currDriver, eH, pgmSeq, intINDEX, workbookId, true, i + 1);

                    var rowCount = cSA.CountDataRow(currDriver, pgmSeq, workbookId);
                    if (rowCount > 1)
                    {
                        cSA.CellClick(currDriver, pgmSeq, workbookId, 1, "Sel");
                        //Added code for confirm
                        int colIndex = cSA.GetColumnIndex(currDriver, workbookId, "Confirm");
                        bool isConfirm = Convert.ToBoolean(cA.ExecuteScript<object>(currDriver, cSA.Script_CellValue(workbookId, 1, colIndex)));
                        if (!isConfirm)
                        {
                            cSA.CellClick(currDriver, pgmSeq, workbookId, 1, "Confirm");
                            cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);
                            currDriver.SwitchTo().Frame(currDriver.FindElement(By.Id(pgmSeqFrame)));
                        }


                        cA.ClickJumpPgm(currDriver, eH, pgmSeq, "4", _stepsList["3"][0], 3);

                        var isPopUpVisible = Validations.IsPopupVisible(currDriver, "messagePopup");
                        if (isPopUpVisible)
                        {
                            cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);
                            msgVal = "Scenario Index 2 - " + pgmSeq + " - 생산의뢰할 데이터가 없습니다.";
                            cLR.SetLogRecord(intINDEX, msgVal);
                            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                            _2_S8();
                            hasError = true;
                        }
                        else
                        {
                            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                            _2_S3(i);
                        }
                    }
                    else
                    {
                        msgVal = "Scenario Index 2 - " + pgmSeq + " - " + orderNoVal + " 의 수주건이 없습니다.";
                        cLR.SetLogRecord(intINDEX, msgVal);
                        eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                        _2_S8();
                        hasError = true;
                    }
                    Global.ScenarioIndex = INDEX;
                }
            }

            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 2 End--------------------");

            if (!hasError)
            {
                cA.ClosePgm(currDriver, pgmSeq);
                eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                _2_S4();
            }
        }

        private void _2_S3(int loopIndex)
        {
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 3 Started--------------------");
            var INDEX = "3";
            var intINDEX = 3;
            Global.ScenarioIndex = INDEX;
            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.StartTime);
            var stepData = _stepsList[INDEX];
            var pgmSeq = stepData[0];
            var workSheetType = stepData[1];

            var fileName = cA.GetFileName("_S3");
            var pathToFile = Path.Combine(cA.GetFilePath(), fileName);
            if (File.Exists(pathToFile))
            {
                File.Delete(pathToFile);
            }
            eH.SetJumpDataSetCount(currDriver, intINDEX, pgmSeq, workbookId2, false, null, loopIndex + 1);

            if (IsInputModule(workSheetType))
            {
                var dataSetCountKeys = _inputDataList[INDEX].Keys;

                string iiStrVal = dataSetCountKeys.ElementAt(loopIndex);
                var workSheetsBundle = _inputDataList[INDEX][iiStrVal];
                MultiDictList<string, List<string>> workSheetsArr = null;
                Dictionary<string, List<List<string>>>.KeyCollection datasetKeys = null;
                List<List<string>> workSheet;
                for (var seq = 0; seq < workSheetsBundle.Count; seq++)
                {
                    workSheetsArr = workSheetsBundle[seq];
                    datasetKeys = workSheetsArr.Keys;

                    for (var i = 0; i < datasetKeys.Count; i++)
                    {
                        string dataForVal = datasetKeys.ElementAt(i);
                        workSheet = workSheetsArr[dataForVal];
                        if (dataForVal == "Control")
                        {
                            cA.ReadPgmControlDataList(currDriver, pgmSeq, workSheet);
                        }
                        else
                        {
                            cSA.ReadSheetDataList(currDriver, pgmSeq, workbookId2, workSheet);
                            int countDirtyRows = cSA.CountDataRow(currDriver, pgmSeq, workbookId2, RowDataType.DIRTY_ROWS);
                            cA.SavePgm(currDriver, eH, pgmSeq, null, intINDEX);
                            cA.Delay();
                            int countSavedRows = cSA.CountDataRow(currDriver, pgmSeq, workbookId2, RowDataType.SAVED_ROWS);

                            cA.SetInputDataSetCount(currDriver, eH, cSA, intINDEX, pgmSeq, workbookId2, countSavedRows, loopIndex + 1);
                            if (countDirtyRows == countSavedRows)
                            {
                                string copiedVal = cA.GetControlValue(currDriver, pgmSeq, "txtProdReqNo_txt");
                                string copiedValWithIdx = (loopIndex + 1) + Keys.TAB + copiedVal;
                                cA.WriteTempFile(fileName, copiedValWithIdx, true);
                                cLR.SetLogRecord(intINDEX, "Scenario Index 3 - " + pgmSeq + " - 생산의뢰번호 : " + copiedVal, true);
                            }

                            else
                            {
                                cLR.SetLogRecord(intINDEX, "Scenario Index 3 - " + pgmSeq + " - 생산의뢰번호 : 저장이 실패하였습니다.", true);
                                var isPopUpVisible = Validations.IsPopupVisible(currDriver, "messagePopup");
                                if (isPopUpVisible)
                                {
                                    var messageText = cA.GetMessageDescription(currDriver);
                                    cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);
                                    cLR.SetLogRecord(intINDEX, "Scenario Index 3 - " + pgmSeq + " - 경고메세지: " + messageText, true);
                                    eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                                    _2_S8();
                                    Global.ScenarioIndex = INDEX;
                                }
                            }
                        }
                    }

                }
                cA.ClosePgm(currDriver, pgmSeq);
                eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);

            }
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 3 End--------------------");
        }

        private void _2_S4()
        {
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 4 Started--------------------");
            var INDEX = "4";
            var intINDEX = 4;
            Global.ScenarioIndex = INDEX;

            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.StartTime);
            var stepData = _stepsList[INDEX];
            var pgmSeq = stepData[0];
            var workSheetType = stepData[1];
            var fileName = cA.GetFileName("_S3");
            bool hasError = false;

            if (IsQueryModule(workSheetType))
            {

                var workSheets = _queryDataList[INDEX];
                cA.OpenPgm(currDriver, eH, pgmSeq, intINDEX);
                var lineCount = cA.GetTempFileLineCount(fileName);
                string pgmSeqFrame = cA.GetPgmSeqFrame(pgmSeq);
                currDriver.SwitchTo().Frame(currDriver.FindElement(By.Id(pgmSeqFrame)));
                for (var i = 0; i < lineCount; i++)
                {
                    var prodReqNoVal = cA.ReadTempFile(fileName, i, 1);
                    cA.SetPgmControlData(currDriver, "txtProdReqNo_txt", prodReqNoVal);
                    int currWorksheetIdx = i + 1;
                    cA.ReadPgmControlDataList(currDriver, pgmSeq, workSheets);
                    cA.QueryPgm(currDriver, eH, pgmSeq, intINDEX, workbookId2, true, i + 1);

                    var rowCount = cSA.CountDataRow(currDriver, pgmSeq, workbookId2);
                    if (rowCount > 0)
                    {
                        cSA.CellClick(currDriver, pgmSeq, workbookId2, 0, "Select");
                        cA.ClickJumpPgm(currDriver, eH, pgmSeq, "2", _stepsList["5"][0], 5);

                        cA.Delay(); // "Time for read prompted message"
                        var isPopUpVisible = Validations.IsPopupVisible(currDriver, "messagePopup");
                        if (isPopUpVisible)
                        {
                            var messageText = cA.GetMessageDescription(currDriver);
                            cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);
                            msgVal = "Scenario Index 4 - " + pgmSeq + " - 메세지 : " + messageText + ".";
                            cLR.SetLogRecord(intINDEX, msgVal);
                            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                            hasError = true;
                        }
                        else
                        {
                            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                            _2_S5(i);
                            Global.ScenarioIndex = INDEX;
                        }
                    }
                    else
                    {
                        msgVal = "Scenario Index 4 - " + pgmSeq + " - 조회된 생산의뢰건이 없습니다.";
                        cLR.SetLogRecord(intINDEX, msgVal);
                        eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                        _2_S8();
                        Global.ScenarioIndex = INDEX;
                        hasError = true;
                    }
                }
            }

            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 4 End--------------------");

            if (!hasError)
            {
                cA.ClosePgm(currDriver, pgmSeq);
                eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                _2_S6();
                Global.ScenarioIndex = INDEX;
            }
        }

        private void _2_S5(int loopIndex)
        {
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 5 Started--------------------");
            var INDEX = "5";
            var intINDEX = 5;
            Global.ScenarioIndex = INDEX;
            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.StartTime);
            var stepData = _stepsList[INDEX];
            var pgmSeq = stepData[0];
            var workSheetType = stepData[1];

            var fileName = cA.GetFileName("_S5");
            var pathToFile = Path.Combine(cA.GetFilePath(), fileName);
            string msgVal, copiedVal, copiedValWithIdx;
            msgVal = copiedVal = copiedValWithIdx = string.Empty;
            if (File.Exists(pathToFile) && loopIndex == 0)
            {
                File.Delete(pathToFile);
            }
            bool hasError = false;
            eH.SetJumpDataSetCount(currDriver, intINDEX, pgmSeq, workbookId, false, null, loopIndex + 1);

            int countDirtyRows = cSA.CountDataRow(currDriver, pgmSeq, workbookId, RowDataType.DIRTY_ROWS);
            cA.SavePgm(currDriver, eH, pgmSeq, null, intINDEX);
            cA.Delay();
            int countSavedRows = cSA.CountDataRow(currDriver, pgmSeq, workbookId, RowDataType.SAVED_ROWS);
            cA.SetInputDataSetCount(currDriver, eH, cSA, intINDEX, pgmSeq, workbookId, countSavedRows, loopIndex + 1);


            for (int i = 0; i < countSavedRows; i++)
            {
                copiedVal = cSA.GetCellValue(currDriver, workbookId, i, "ProdPlanNo");
                copiedValWithIdx = (loopIndex + 1) + Keys.TAB + (i + 1) + Keys.TAB + copiedVal;
                cA.WriteTempFile(fileName, copiedValWithIdx, true);
                msgVal = "Scenario Index 5 - " + pgmSeq + " - 생산계획번호 : " + copiedVal;
                cLR.SetLogRecord(intINDEX, msgVal);
            }

            if (countDirtyRows == countSavedRows)
            {
                int isSemiItemCheckedCnt = 0;
                var rowCount = cSA.CountDataRow(currDriver, pgmSeq, workbookId, RowDataType.SAVED_ROWS);
                for (int i = 0; i < rowCount; i++)
                {
                    string isSemiItemChecked = cSA.GetCellValue(currDriver, workbookId, i, "IsSemiItem", true);
                    if (isSemiItemChecked == "True")
                    {
                        isSemiItemCheckedCnt++;
                        cSA.CellClick(currDriver, pgmSeq, workbookId, i, "IsSelect");
                    }
                }
                if (isSemiItemCheckedCnt > 0)
                {
                    cA.SelectPgmControl(currDriver, eH, pgmSeq, "btnSemiItemProdPlanCreate_btn");

                    var isPopUpVisible = Validations.IsPopupVisible(currDriver, "messagePopup");
                    if (isPopUpVisible)
                    {
                        var messageText = cA.GetMessageDescription(currDriver);
                        cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);
                        msgVal = "Scenario Index 5 - " + pgmSeq + " - 메세지 : " + messageText + ".";
                        cLR.SetLogRecord(intINDEX, msgVal);
                    }
                    else
                    {
                        countDirtyRows = cSA.GetRowIndex(currDriver, workbookId, "ItemName", string.Empty) - rowCount;
                        cA.SavePgm(currDriver, eH, pgmSeq, null, intINDEX);
                        cA.Delay();

                        countSavedRows = cSA.CountDataRow(currDriver, pgmSeq, workbookId, RowDataType.SAVED_ROWS);
                        cA.SetInputDataSetCount(currDriver, eH, cSA, intINDEX, pgmSeq, workbookId, (countSavedRows - rowCount), loopIndex + 1);
                        for (int i = rowCount; i < countSavedRows; i++)
                        {
                            copiedVal = cSA.GetCellValue(currDriver, workbookId, i, "ProdPlanNo");
                            copiedValWithIdx = (loopIndex + 1) + Keys.TAB + (i + 1) + Keys.TAB + copiedVal;
                            cA.WriteTempFile(fileName, copiedValWithIdx, true);
                            msgVal = "Scenario Index 5 - " + pgmSeq + " - 생산계획번호 : " + copiedVal;
                            cLR.SetLogRecord(intINDEX, msgVal);
                        }
                        hasError = _2_S5_M5_6(intINDEX, pgmSeq, countDirtyRows, countSavedRows, rowCount);
                    }


                }
                else
                {
                    hasError = _2_S5_M5_6(intINDEX, pgmSeq, countDirtyRows, countSavedRows, rowCount);
                }

            }
            else
            {
                msgVal = "Scenario Index 5 - " + pgmSeq + " - 주간생산계획입력이 실패하였습니다.";
                cLR.SetLogRecord(intINDEX, msgVal);
                var isPopUpVisible = Validations.IsPopupVisible(currDriver, "messagePopup");
                if (isPopUpVisible)
                {
                    var messageText = cA.GetMessageDescription(currDriver);
                    cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);

                    msgVal = "Scenario Index 5 - " + pgmSeq + " - 메세지 : " + messageText;
                    cLR.SetLogRecord(intINDEX, msgVal);


                }
                eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                _2_S8();
                Global.ScenarioIndex = INDEX;
                hasError = true;
            }
            if (!hasError)
            {
                cA.Delay(); // "Waiting for popup"
                currDriver.SwitchTo().Alert().Accept();

                cA.Delay(); // "Waiting for another popup"
                var isPopUpVisible = Validations.IsPopupVisible(currDriver, "messagePopup");
                if (isPopUpVisible)
                {
                    var messageText = cA.GetMessageDescription(currDriver);
                    msgVal = "Scenario Index 5 - " + pgmSeq + " - 메세지 : " + messageText;
                    cLR.SetLogRecord(intINDEX, msgVal);
                    cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);

                }
            }
            cA.ClosePgm(currDriver, pgmSeq);
            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);

            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 5 End--------------------");

        }

        private bool _2_S5_M5_6(int intINDEX, string pgmSeq, int countDirtyRows, int countSavedRows, int rowCount)
        {
            bool hasError = false;
            string msgVal;
            if (countDirtyRows == countSavedRows)
            {
                msgVal = "Scenario Index 5 - " + pgmSeq + " - 반제품 생산계획 생성이 저장 되었습니다.";
                cLR.SetLogRecord(intINDEX, msgVal);
                var rowCount2 = cSA.CountDataRow(currDriver, pgmSeq, workbookId, RowDataType.SAVED_ROWS);
                for (int i = 0; i < rowCount2; i++)
                {
                    cSA.CellClick(currDriver, pgmSeq, workbookId, i, "IsSelect");
                }
                cA.SelectPgmControl(currDriver, eH, pgmSeq, "btnConfirm_btn");
            }
            else
            {
                msgVal = "Scenario Index 5 - " + pgmSeq + " - 반제품 생산계획 저장이 실패하였습니다.";
                cLR.SetLogRecord(intINDEX, msgVal);
                bool isPopUpVisible = Validations.IsPopupVisible(currDriver, "messagePopup");
                if (isPopUpVisible)
                {
                    var messageText = cA.GetMessageDescription(currDriver);
                    cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);
                    msgVal = "Scenario Index 5 - " + pgmSeq + " - 메세지 : " + messageText + ".";
                    cLR.SetLogRecord(intINDEX, msgVal);
                }
                hasError = true;
            }

            return hasError;
        }

        private void _2_S6()
        {
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 6 Started--------------------");
            var INDEX = "6";
            var intINDEX = 6;
            Global.ScenarioIndex = INDEX;

            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.StartTime);
            var stepData = _stepsList[INDEX];
            var pgmSeq = stepData[0];
            var workSheetType = stepData[1];
            var fileName = cA.GetFileName("_S5");
            var workbookId3 = "SSProdPlan_c";
            var workbookId4 = "SSMRP_c";
            bool hasError = false;


            if (IsQueryModule(workSheetType))
            {

                var workSheets = _queryDataList[INDEX];
                var selectRowStartIndex = -1;
                var lineCount = 0;
                var prodPlanNoVal = string.Empty;
                var rowIndex = -1;
                var rowCount = 0;

                cA.OpenPgm(currDriver, eH, pgmSeq, intINDEX);

                string pgmSeqFrame = cA.GetPgmSeqFrame(pgmSeq);
                currDriver.SwitchTo().Frame(currDriver.FindElement(By.Id(pgmSeqFrame)));

                cA.ReadPgmControlDataList(currDriver, pgmSeq, workSheets);
                cA.QueryPgm(currDriver, eH, pgmSeq, intINDEX, workbookId3, false, 1);

                rowCount = cSA.CountDataRow(currDriver, pgmSeq, workbookId3);
                if (rowCount > 0)
                {
                    var isprodPlanNoExists = false;
                    lineCount = cA.GetTempFileLineCount(fileName);

                    for (int j = 0; j < lineCount; j++)
                    {
                        prodPlanNoVal = cA.ReadTempFile(fileName, j, 2);

                        rowIndex = cSA.GetRowIndex(currDriver, workbookId3, "ProdPlanNo", prodPlanNoVal);
                        if (rowIndex != -1)
                        {
                            isprodPlanNoExists = true;
                            break;
                        }
                    }

                    if (!isprodPlanNoExists)
                    {
                        msgVal = "Scenario Index 6 - " + pgmSeq + " - 선택할 자재소요계획건이 없습니다.";
                        cLR.SetLogRecord(intINDEX, msgVal);
                        eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                        _2_S8();
                        Global.ScenarioIndex = INDEX;
                        hasError = true;
                    }
                    else
                    {
                        int iteration = 0;
                        int seq;
                        for (var j = 0; j <= lineCount; j++)
                        {
                            if (j < lineCount)
                            {
                                prodPlanNoVal = cA.ReadTempFile(fileName, j, 2);
                                seq = int.Parse(cA.ReadTempFile(fileName, j, 0));
                            }
                            else
                            {
                                seq = -1;
                            }

                            if (iteration == (seq - 1))
                            {
                                rowIndex = cSA.GetRowIndex(currDriver, workbookId3, "ProdPlanNo", prodPlanNoVal);
                                if (rowIndex != -1)
                                {
                                    cSA.CellClick(currDriver, pgmSeq, workbookId3, rowIndex, "Sel");
                                }
                            }
                            else if (j <= lineCount)
                            {
                                cA.SelectPgmControl(currDriver, eH, pgmSeq, "btnCreateMRP_btn");
                                cA.Delay(); //"Waiting for popup"
                                currDriver.SwitchTo().Alert().Accept();
                                cA.Delay(); // "Waiting for another popup"
                                cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);

                                cSA.SetColSortOrder(currDriver, pgmSeq, workbookId4, "POReqQty", true);

                                selectRowStartIndex = cSA.GetRowIndex(currDriver, workbookId4, "POReqQty", "0", Condition.GREATERTHAN);
                                if (selectRowStartIndex != -1)
                                {
                                    rowCount = cSA.CountDataRow(currDriver, pgmSeq, workbookId4, RowDataType.SAVED_ROWS);

                                    cSA.RowSelection(currDriver, pgmSeq, workbookId4, selectRowStartIndex, (rowCount - 1));
                                    if (selectRowStartIndex == (rowCount - 1))
                                    {
                                        cSA.CellClick(currDriver, pgmSeq, workbookId4, selectRowStartIndex, "Sel");
                                    }
                                    else
                                    {
                                        cSA.RowSelection(currDriver, pgmSeq, workbookId4, selectRowStartIndex, (rowCount - 1));
                                        cSA.CellClick(currDriver, pgmSeq, workbookId4, selectRowStartIndex, "Sel", false, true);
                                        cSA.SelectSheetContextMenu(currDriver, pgmSeq, workbookId4, "9");
                                    }
                                    cA.ClickJumpPgm(currDriver, eH, pgmSeq, "9", _stepsList["7"][0], 7);
                                    cA.Delay(); //"Time to load jumped program"
                                    eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                                    int parentLoopIndex = int.Parse(cA.ReadTempFile(fileName, j - 1, 0));
                                    _2_S7(iteration, parentLoopIndex - 1);
                                    Global.ScenarioIndex = INDEX;
                                }
                                else
                                {
                                    msgVal = "Scenario Index 6 - " + pgmSeq + " - 구매요청할 수량이 없습니다.";
                                    cLR.SetLogRecord(intINDEX, msgVal);
                                    eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                                    _2_S8();
                                    Global.ScenarioIndex = INDEX;
                                    hasError = true;
                                }

                                if (j == lineCount)
                                {
                                    break;
                                }


                                cSA.CellClick(currDriver, pgmSeq, workbookId3, 0, "Sel", false, true);
                                cSA.SelectSheetContextMenu(currDriver, pgmSeq, workbookId3, "10");
                                cA.Delay(); // "Waiting To Clear All"
                                iteration = seq - 1;
                                j--;
                                continue;
                            }
                        }
                    }
                }
                else
                {
                    msgVal = "Scenario Index 6 - " + pgmSeq + " - 조회된 자재소요계획건이 없습니다.";
                    cLR.SetLogRecord(intINDEX, msgVal);
                    eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                    _2_S8();
                    Global.ScenarioIndex = INDEX;
                    hasError = true;
                }
            }
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 6 End--------------------");

            if (!hasError)
            {
                cA.ClosePgm(currDriver, pgmSeq);
                eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                _2_S8();
                Global.ScenarioIndex = INDEX;
            }
        }

        private void _2_S7(int loopIndex, int parentLoopIndex)
        {
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 7 Started--------------------");
            var INDEX = "7";
            var intINDEX = 7;
            Global.ScenarioIndex = INDEX;
            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Execute);
            var stepData = _stepsList[INDEX];
            var pgmSeq = stepData[0];
            var workSheetType = stepData[1];

            cA.OpenPgm(currDriver, eH, pgmSeq, intINDEX);
            string pgmSeqFrame = cA.GetPgmSeqFrame(pgmSeq);
            currDriver.SwitchTo().Frame(currDriver.FindElement(By.Id(pgmSeqFrame)));

            var fileName = cA.GetFileName("_S7");
            var pathToFile = Path.Combine(cA.GetFilePath(), fileName);
            if (File.Exists(pathToFile) && loopIndex == 0)
            {
                File.Delete(pathToFile);
            }
            eH.SetJumpDataSetCount(currDriver, intINDEX, pgmSeq, workbookId2, false, null, loopIndex + 1);

            if (IsInputModule(workSheetType))
            {

                string requestDate, cellValue, copiedVal, copiedValWithIdx, msgVal;
                requestDate = cellValue = copiedVal = copiedValWithIdx = msgVal = string.Empty;
                var dataSetCountKeys = _inputDataList[INDEX].Keys;

                string iiStrVal = dataSetCountKeys.ElementAt(loopIndex);
                var workSheetsBundle = _inputDataList[INDEX][iiStrVal];
                MultiDictList<string, List<string>> workSheetsArr = null;
                Dictionary<string, List<List<string>>>.KeyCollection datasetKeys = null;
                List<List<string>> workSheet;

                workSheetsArr = workSheetsBundle[loopIndex + 1];
                datasetKeys = workSheetsArr.Keys;

                for (var i = 0; i < datasetKeys.Count; i++)
                {
                    string dataForVal = datasetKeys.ElementAt(i);
                    workSheet = workSheetsArr[dataForVal];
                    if (dataForVal == "Control")
                    {
                        cA.ReadPgmControlDataList(currDriver, pgmSeq, workSheet);
                    }
                    requestDate = cA.GetControlValue(currDriver, pgmSeq, "datPOReqDate_dat");
                    var rowCount = cSA.CountDataRow(currDriver, pgmSeq, workbookId2, RowDataType.DIRTY_ROWS);
                    cSA.CellInput(currDriver, pgmSeq, workbookId2, 0, "DelvDate", requestDate);
                    cSA.CellClick(currDriver, pgmSeq, workbookId2, 0, "DelvDate");
                    cSA.FillCellsUsingDragging(currDriver, pgmSeq, workbookId2, 0, "DelvDate", (rowCount - 1), "DelvDate");

                }

                int countDirtyRows = cSA.CountDataRow(currDriver, pgmSeq, workbookId2, RowDataType.DIRTY_ROWS);
                cA.SavePgm(currDriver, eH, pgmSeq, null, intINDEX);
                cA.Delay();
                int countSavedRows = cSA.CountDataRow(currDriver, pgmSeq, workbookId2, RowDataType.SAVED_ROWS);

                cA.SetInputDataSetCount(currDriver, eH, cSA, intINDEX, pgmSeq, workbookId2, countSavedRows, loopIndex + 1);
                if (countDirtyRows == countSavedRows)
                {
                    copiedVal = cA.GetControlValue(currDriver, pgmSeq, "txtPOReqNo_txt");
                    copiedValWithIdx = (parentLoopIndex + 1) + Keys.TAB + (loopIndex + 1) + Keys.TAB + copiedVal;
                    cA.WriteTempFile(fileName, copiedValWithIdx, true);
                    msgVal = "Scenario Index 7 - " + pgmSeq + " - (생산)구매요청번호 : " + copiedVal;
                    cLR.SetLogRecord(intINDEX, msgVal);
                }

                else
                {
                    msgVal = "Scenario Index 7 - " + pgmSeq + " - (생산)구매요청번호 : 저장이 실패하였습니다.";
                    cLR.SetLogRecord(intINDEX, msgVal);
                    var isPopUpVisible = Validations.IsPopupVisible(currDriver, "messagePopup");
                    if (isPopUpVisible)
                    {
                        var messageText = cA.GetMessageDescription(currDriver);
                        cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);
                        msgVal = "Scenario Index 7 - " + pgmSeq + " - 메세지 : " + messageText;
                        cLR.SetLogRecord(intINDEX, msgVal);
                    }
                }

            }



            cA.ClosePgm(currDriver, pgmSeq);
            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);

            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 7 End--------------------");
        }

        private void _2_S8()
        {
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 8 Started--------------------");
            var INDEX = "8";
            var intINDEX = 8;
            Global.ScenarioIndex = INDEX;

            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.StartTime);
            var stepData = _stepsList[INDEX];
            var pgmSeq = stepData[0];
            var workSheetType = stepData[1];
            var fileName = cA.GetFileName("_S1");
            bool hasError = false;

            if (IsQueryModule(workSheetType))
            {

                var workSheet = _queryDataList[INDEX];
                cA.OpenPgm(currDriver, eH, pgmSeq, intINDEX);
                string pgmSeqFrame = cA.GetPgmSeqFrame(pgmSeq);
                currDriver.SwitchTo().Frame(currDriver.FindElement(By.Id(pgmSeqFrame)));

                var lineCount = cA.GetTempFileLineCount(fileName);
                for (var i = 0; i < lineCount; i++)
                {
                    var orderNoVal = cA.ReadTempFile(fileName, i, 1);
                    cA.SetPgmControlData(currDriver, "txtOrderNo_txt", orderNoVal);

                    cA.ReadPgmControlDataList(currDriver, pgmSeq, workSheet);
                    cA.QueryPgm(currDriver, eH, pgmSeq, intINDEX, workbookId, true, i + 1);

                    var rowCount = cSA.CountDataRow(currDriver, pgmSeq, workbookId);
                    if (rowCount < 2)
                    {
                        msgVal = "Scenario Index 8 - " + pgmSeq + " - 수주번호 : " + orderNoVal + " 조회된 데이터가 없습니다.";
                        cLR.SetLogRecord(intINDEX, msgVal);
                        eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                        //_2_S18();
                        Global.ScenarioIndex = INDEX;
                        hasError = true;
                    }
                    else
                    {
                        cSA.CellClick(currDriver, pgmSeq, workbookId, 1, "Sel");
                        cA.ClickJumpPgm(currDriver, eH, pgmSeq, "3", _stepsList["9"][0], 9);

                        cA.Delay(); // "Time to jump into another program"
                        var isPopUpVisible = Validations.IsPopupVisible(currDriver, "messagePopup");
                        if (isPopUpVisible)
                        {
                            cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);
                            msgVal = "Scenario Index 8 - " + pgmSeq + " - 구매요청할 데이터가 없습니다";
                            cLR.SetLogRecord(intINDEX, msgVal);
                            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                            _2_S10();
                            Global.ScenarioIndex = INDEX;
                            hasError = true;
                        }
                        else
                        {
                            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                            _2_S9(i);
                            Global.ScenarioIndex = INDEX;
                        }
                    }

                }
            }
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 2 End--------------------");
            if (!hasError)
            {
                cA.ClosePgm(currDriver, pgmSeq);
                eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                _2_S10();
                Global.ScenarioIndex = INDEX;
            }

        }

        private void _2_S9(int loopIndex)
        {
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 9 Started--------------------");
            var INDEX = "9";
            var intINDEX = 9;
            Global.ScenarioIndex = INDEX;
            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Execute);
            var stepData = _stepsList[INDEX];
            var pgmSeq = stepData[0];
            var workSheetType = stepData[1];


            var fileName = cA.GetFileName("_S9");
            var pathToFile = Path.Combine(cA.GetFilePath(), fileName);
            if (File.Exists(pathToFile) && loopIndex == 0)
            {
                File.Delete(pathToFile);
            }
            eH.SetJumpDataSetCount(currDriver, intINDEX, pgmSeq, workbookId2, false, null, loopIndex + 1);

            if (IsInputModule(workSheetType))
            {
                var dataSetCountKeys = _inputDataList[INDEX].Keys;


                string iiStrVal = dataSetCountKeys.ElementAt(loopIndex);
                var workSheetsBundle = _inputDataList[INDEX][iiStrVal];
                MultiDictList<string, List<string>> workSheetsArr = null;
                Dictionary<string, List<List<string>>>.KeyCollection datasetKeys = null;
                List<List<string>> workSheet;
                workSheetsArr = workSheetsBundle[loopIndex];
                datasetKeys = workSheetsArr.Keys;

                for (var i = 0; i < datasetKeys.Count; i++)
                {
                    string dataForVal = datasetKeys.ElementAt(i);
                    workSheet = workSheetsArr[dataForVal];
                    if (dataForVal == "Control")
                    {
                        cA.ReadPgmControlDataList(currDriver, pgmSeq, workSheet);
                    }
                    else
                    {
                        cSA.ReadSheetDataList(currDriver, pgmSeq, workbookId2, workSheet);
                        int countDirtyRows = cSA.CountDataRow(currDriver, pgmSeq, workbookId2, RowDataType.DIRTY_ROWS);
                        cA.SavePgm(currDriver, eH, pgmSeq, null, intINDEX);
                        cA.Delay();
                        int countSavedRows = cSA.CountDataRow(currDriver, pgmSeq, workbookId2, RowDataType.SAVED_ROWS);

                        cA.SetInputDataSetCount(currDriver, eH, cSA, intINDEX, pgmSeq, workbookId2, countSavedRows, loopIndex + 1);
                        if (countDirtyRows == countSavedRows)
                        {
                            string copiedVal = cA.GetControlValue(currDriver, pgmSeq, "txtProdReqNo_txt");
                            string copiedValWithIdx = (loopIndex + 1) + Keys.TAB + copiedVal;
                            cA.WriteTempFile(fileName, copiedValWithIdx, true);
                            msgVal = "Scenario Index 9 - " + pgmSeq + " - (영업 구매) 요청번호 : " + copiedVal;
                            cLR.SetLogRecord(intINDEX, msgVal);
                        }
                        else
                        {
                            msgVal = "Scenario Index 9 - " + pgmSeq + " - (영업 구매) 요청번호 : 저장이 실패하였습니다.";
                            cLR.SetLogRecord(intINDEX, msgVal);
                            var isPopUpVisible = Validations.IsPopupVisible(currDriver, "messagePopup");
                            if (isPopUpVisible)
                            {
                                var messageText = cA.GetMessageDescription(currDriver);
                                cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);
                                msgVal = "Scenario Index 9 - " + pgmSeq + " - 메세지 : " + messageText;
                                cLR.SetLogRecord(intINDEX, msgVal);
                            }
                        }
                    }
                }


                cA.ClosePgm(currDriver, pgmSeq);
                eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);

            }
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 9 End--------------------");
        }

        private void _2_S10()
        {
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 10 Started--------------------");
            var INDEX = "10";
            var intINDEX = 10;
            Global.ScenarioIndex = INDEX;

            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.StartTime);
            var stepData = _stepsList[INDEX];
            var pgmSeq = stepData[0];
            var workSheetType = stepData[1];
            bool hasError = false;

            if (IsQueryModule(workSheetType))
            {
                var workSheet = _queryDataList[INDEX];

                var cellValue = "";
                var fileName = "";
                var lineCount = 0;
                List<string> pOReqNoValArr = new List<string>();
                var pOReqNoValIndex = -1;
                cA.OpenPgm(currDriver, eH, pgmSeq, intINDEX);
                string pgmSeqFrame = cA.GetPgmSeqFrame(pgmSeq);
                currDriver.SwitchTo().Frame(currDriver.FindElement(By.Id(pgmSeqFrame)));
                cA.ReadPgmControlDataList(currDriver, pgmSeq, workSheet);
                cA.QueryPgm(currDriver, eH, pgmSeq, intINDEX, workbookId2, false, 1);
                int rowCount = 0;
                var datasetCount = cA.GetTempFileLineCount(cA.GetFileName("_S1"));
                for (int i = 0; i < datasetCount; i++)
                {
                    rowCount = cSA.CountDataRow(currDriver, pgmSeq, workbookId2);

                    if (rowCount > 0)
                    {
                        fileName = cA.GetFileName("_S7");
                        if (File.Exists(cA.GetFilePath() + fileName))
                        {
                            lineCount = cA.GetTempFileLineCount(fileName);
                            for (int j = 0; j < lineCount; j++)
                            {
                                if (cA.ReadTempFile(fileName, j, 0) == (i + 1).ToString())
                                {
                                    pOReqNoValArr.Add(cA.ReadTempFile(fileName, j, 2));
                                }
                            }
                        }

                        fileName = cA.GetFileName("_S9");
                        if (File.Exists(cA.GetFilePath() + fileName))
                        {
                            lineCount = cA.GetTempFileLineCount(fileName);
                            for (int j = 0; j < lineCount; j++)
                            {
                                if (cA.ReadTempFile(fileName, j, 0) == (i + 1).ToString())
                                {
                                    pOReqNoValArr.Add(cA.ReadTempFile(fileName, j, 1));
                                }
                            }
                        }
                        if (pOReqNoValArr.Count > 0)
                        {
                            rowCount = cSA.CountDataRow(currDriver, pgmSeq, workbookId2);
                            for (int j = 0; j < rowCount; j++)
                            {
                                cellValue = cSA.GetCellValue(currDriver, workbookId2, j, "POReqNo");

                                pOReqNoValIndex = pOReqNoValArr.IndexOf(cellValue);
                                if (pOReqNoValIndex != -1)
                                {
                                    cSA.CellClick(currDriver, pgmSeq, workbookId2, j, "Select");
                                    pOReqNoValArr.RemoveAt(pOReqNoValIndex);
                                }
                                if (pOReqNoValArr.Count == 0)
                                {
                                    break;
                                }
                            }
                        }

                        cA.ClickJumpPgm(currDriver, eH, pgmSeq, "2", _stepsList["11"][0], 11);
                        cA.Delay(); //"Time to jump into another program"
                        eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                        _2_S11(i);
                        Global.ScenarioIndex = INDEX;

                    }
                    else
                    {
                        msgVal = "Scenario Index 10 - " + pgmSeq + " - 조회된 구매요청건이 없습니다.";
                        cLR.SetLogRecord(intINDEX, msgVal);
                        hasError = true;
                    }


                }
            }
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 10 End--------------------");
            if (!hasError)
            {
                cA.ClosePgm(currDriver, pgmSeq);
                eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                _2_S12();
                Global.ScenarioIndex = INDEX;
            }

        }

        private void _2_S11(int loopIndex)
        {
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 11 Started--------------------");
            var INDEX = "11";
            var intINDEX = 11;
            Global.ScenarioIndex = INDEX;
            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Execute);
            var stepData = _stepsList[INDEX];
            var pgmSeq = stepData[0];
            var workSheetType = stepData[1];


            var fileName = cA.GetFileName("_S11");
            var pathToFile = Path.Combine(cA.GetFilePath(), fileName);
            if (File.Exists(pathToFile) && loopIndex == 0)
            {
                File.Delete(pathToFile);
            }
            eH.SetJumpDataSetCount(currDriver, intINDEX, pgmSeq, workbookId2, false, null, loopIndex + 1);

            if (IsInputModule(workSheetType))
            {
                var dataSetCountKeys = _inputDataList[INDEX].Keys;

                string iiStrVal = dataSetCountKeys.ElementAt(loopIndex);
                var workSheetsBundle = _inputDataList[INDEX][iiStrVal];
                MultiDictList<string, List<string>> workSheetsArr = null;
                Dictionary<string, List<List<string>>>.KeyCollection datasetKeys = null;
                List<List<string>> workSheet;
                workSheetsArr = workSheetsBundle[loopIndex];
                datasetKeys = workSheetsArr.Keys;

                for (var i = 0; i < datasetKeys.Count; i++)
                {
                    string dataForVal = datasetKeys.ElementAt(i);
                    workSheet = workSheetsArr[dataForVal];
                    if (dataForVal == "Control")
                    {
                        cA.ReadPgmControlDataList(currDriver, pgmSeq, workSheet);
                    }
                    else
                    {
                        cSA.ReadSheetDataList(currDriver, pgmSeq, workbookId2, workSheet);
                        int countDirtyRows = cSA.CountDataRow(currDriver, pgmSeq, workbookId2, RowDataType.DIRTY_ROWS);
                        cA.SavePgm(currDriver, eH, pgmSeq, null, intINDEX);
                        cA.Delay();
                        int countSavedRows = cSA.CountDataRow(currDriver, pgmSeq, workbookId2, RowDataType.SAVED_ROWS);

                        cA.SetInputDataSetCount(currDriver, eH, cSA, intINDEX, pgmSeq, workbookId2, countSavedRows, loopIndex + 1);
                        if (countDirtyRows == countSavedRows)
                        {
                            string copiedVal = cA.GetControlValue(currDriver, pgmSeq, "txtApproReqNo_txt");
                            string copiedValWithIdx = (loopIndex + 1) + Keys.TAB + copiedVal;
                            cA.WriteTempFile(fileName, copiedValWithIdx, true);
                            msgVal = "Scenario Index 11 - " + pgmSeq + " - 품의번호 : " + copiedVal;
                            cLR.SetLogRecord(intINDEX, msgVal);
                        }
                        else
                        {
                            msgVal = "Scenario Index 11 - " + pgmSeq + " - 품의번호 : 저장이 실패하였습니다.";
                            cLR.SetLogRecord(intINDEX, msgVal);
                            var isPopUpVisible = Validations.IsPopupVisible(currDriver, "messagePopup");
                            if (isPopUpVisible)
                            {
                                var messageText = cA.GetMessageDescription(currDriver);
                                cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);
                                msgVal = "Scenario Index 11 - " + pgmSeq + " - 메세지 : " + messageText;
                                cLR.SetLogRecord(intINDEX, msgVal);
                            }
                        }
                    }
                }
                cA.ClosePgm(currDriver, pgmSeq);
                eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);

            }
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 11 End--------------------");
        }

        private void _2_S12()
        {
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 12 Started--------------------");
            var INDEX = "12";
            var intINDEX = 12;
            Global.ScenarioIndex = INDEX;

            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.StartTime);
            var stepData = _stepsList[INDEX];
            var pgmSeq = stepData[0];
            string popupPgmSeq = "502682";
            var workSheetType = stepData[1];
            cA.OpenPgm(currDriver, eH, pgmSeq, intINDEX);
            string pgmSeqFrame = cA.GetPgmSeqFrame(pgmSeq);
            currDriver.SwitchTo().Frame(currDriver.FindElement(By.Id(pgmSeqFrame)));

            string fileName1 = cA.GetFileName("_S11");
            string fileName2 = cA.GetFileName("_S12");
            string pathToFile = cA.GetFilePath() + fileName2;
            if (File.Exists(pathToFile))
            {
                File.Delete(pathToFile);
            }

            bool hasError = false;

            if (IsQueryModule(workSheetType))
            {

                var workSheet = _queryDataList[INDEX];

                var approReqNoVal = "";
                string[] tempRowData = new string[3];
                int rowCount = 0;
                var poNoVal = "";
                var sMImpTypeVal = "";
                var lineCount = cA.GetTempFileLineCount(fileName1);
                for (int i = 0; i < lineCount; i++)
                {
                    approReqNoVal = cA.ReadTempFile(fileName1, i, 1);
                    cA.SetPgmControlData(currDriver, "txtApproReqNo_txt", approReqNoVal);
                    cA.ReadPgmControlDataList(currDriver, pgmSeq, workSheet);//cA.read_pgm_control_dataList(pgmSeq, workSheets[i+1]);
                    cA.QueryPgm(currDriver, eH, pgmSeq, intINDEX, workbookId2, true, i + 1);

                    rowCount = cSA.CountDataRow(currDriver, pgmSeq, workbookId2);

                    if (rowCount > 0)
                    {
                        cSA.CellClick(currDriver, pgmSeq, workbookId2, 0, "Select");
                        tempRowData[0] = cSA.GetCellValue(currDriver, workbookId2, 0, "ApproReqDate");
                        tempRowData[1] = cSA.GetCellValue(currDriver, workbookId2, 0, "EmpName");
                        tempRowData[2] = cSA.GetCellValue(currDriver, workbookId2, 0, "DeptName");


                        cA.ClickJumpPgm(currDriver, eH, pgmSeq, "3", popupPgmSeq, intINDEX);
                        cA.Delay(2000); //Time for load popup program
                        cA.SetPgmControlData(currDriver, "datPODate_dat", tempRowData[0]);
                        cA.SetPgmControlData(currDriver, "txtEmpName_txt", tempRowData[1]);
                        cA.SetPgmControlData(currDriver, "txtDeptName_txt", tempRowData[2]);
                        cA.SelectPgmControl(currDriver, eH, pgmSeq, "btnCrtPO_btn");
                        cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);
                        cA.Delay();

                        rowCount = cSA.CountDataRow(currDriver, pgmSeq, workbookId2);
                        for (int j = 0; j < rowCount; j++)
                        {
                            poNoVal = cSA.GetCellValue(currDriver, workbookId2, j, "PONo");
                            sMImpTypeVal = cSA.GetCellValue(currDriver, workbookId2, j, "SMImpType");
                            cA.WriteTempFile(fileName2, (i + 1) + Keys.TAB + (j + 1) + Keys.TAB + poNoVal + Keys.TAB + sMImpTypeVal, true);
                            msgVal = "Scenario Index 12 - " + pgmSeq + " - 발주번호 : " + poNoVal;
                            cLR.SetLogRecord(intINDEX, msgVal);
                        }
                        cA.SelectPgmControl(currDriver, eH, popupPgmSeq, "btnClose_btn");

                    }
                    else
                    {
                        msgVal = "Scenario Index 12 - " + pgmSeq + " - 조회된 구매품의건이 없습니다.";
                        cLR.SetLogRecord(intINDEX, msgVal);
                        _2_S29();
                        Global.ScenarioIndex = INDEX;
                        hasError = true;
                    }
                }
            }
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 12 End--------------------");
            if (!hasError)
            {
                cA.ClosePgm(currDriver, pgmSeq);
                eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                _2_S13();
                Global.ScenarioIndex = INDEX;
            }
        }

        private void _2_S13()
        {
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 13 Started--------------------");
            var INDEX = "13";
            var intINDEX = 13;
            Global.ScenarioIndex = INDEX;

            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.StartTime);
            var stepData = _stepsList[INDEX];
            var pgmSeq = stepData[0];
            var workSheetType = stepData[1];
            var fileName = cA.GetFileName("_S12");
            bool hasError = false;

            if (IsQueryModule(workSheetType))
            {

                var workSheet = _queryDataList[INDEX];
                var poNoVal = "";
                var sMImpTypeVal = "";
                var issMImpTypeExists = false;
                cA.OpenPgm(currDriver, eH, pgmSeq, intINDEX);
                string pgmSeqFrame = cA.GetPgmSeqFrame(pgmSeq);
                currDriver.SwitchTo().Frame(currDriver.FindElement(By.Id(pgmSeqFrame)));


                var datasetCount = cA.GetTempFileLineCount(cA.GetFileName("_S1"));
                for (var i = 0; i < datasetCount; i++)
                {
                    var loopIndex = 0;
                    var rowCount = cA.GetTempFileLineCount(fileName);
                    for (int j = 0; j < rowCount; j++)
                    {
                        if (cA.ReadTempFile(fileName, j, 0) == (i + 1).ToString())
                        {

                            sMImpTypeVal = cA.ReadTempFile(fileName, j, 3);
                            if (sMImpTypeVal == "8008001")
                            {
                                issMImpTypeExists = true;
                                poNoVal = cA.ReadTempFile(fileName, j, 2);
                                cA.SetPgmControlData(currDriver, "txtPONo_txt", poNoVal);
                                cA.ReadPgmControlDataList(currDriver, pgmSeq, workSheet); //cA.read_pgm_control_dataList(pgmSeq, workSheets[i + 1]);
                                cA.QueryPgm(currDriver, eH, pgmSeq, intINDEX, workbookId2, true, loopIndex + 1);

                                cSA.CellClick(currDriver, pgmSeq, workbookId2, 1, "Select");
                                cA.ClickJumpPgm(currDriver, eH, pgmSeq, "2", _stepsList["14"][0], 14);

                                cA.Delay(); // "Time to jump into another program"

                                eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                                _2_S14(loopIndex, i);
                                Global.ScenarioIndex = INDEX;
                                loopIndex++;
                                msgVal = "Scenario Index 13 - " + pgmSeq + " - 발주번호 : " + poNoVal;
                                cLR.SetLogRecord(intINDEX, msgVal);
                            }
                        }
                    }
                    if (!issMImpTypeExists)
                    {
                        msgVal = "Scenario Index 13 - " + pgmSeq + " - 내외자구분이 내수인 건이 존재하지 않습니다.";
                        cLR.SetLogRecord(intINDEX, msgVal);
                        eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                        _2_S18();
                        Global.ScenarioIndex = INDEX;
                        hasError = true;
                    }
                }
            }


            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 13 End--------------------");
            if (!hasError)
            {
                cA.ClosePgm(currDriver, pgmSeq);
                eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                _2_S15();
                Global.ScenarioIndex = INDEX;
            }
        }

        private void _2_S14(int loopIndex, int parentLoopIndex)
        {
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 14 Started--------------------");
            var INDEX = "14";
            var intINDEX = 14;
            Global.ScenarioIndex = INDEX;
            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Execute);
            var stepData = _stepsList[INDEX];
            var pgmSeq = stepData[0];
            var workSheetType = stepData[1];

            cA.OpenPgm(currDriver, eH, pgmSeq, intINDEX);
            string pgmSeqFrame = cA.GetPgmSeqFrame(pgmSeq);
            currDriver.SwitchTo().Frame(currDriver.FindElement(By.Id(pgmSeqFrame)));

            var fileName = cA.GetFileName("_S14");
            var pathToFile = Path.Combine(cA.GetFilePath(), fileName);
            if (File.Exists(pathToFile) && loopIndex == 0 && parentLoopIndex == 0)
            {
                File.Delete(pathToFile);
            }
            eH.SetJumpDataSetCount(currDriver, intINDEX, pgmSeq, workbookId2, false, null, loopIndex + 1);

            if (IsInputModule(workSheetType))
            {
                string requestDate, cellValue, copiedVal, copiedValWithIdx, msgVal;
                requestDate = cellValue = copiedVal = copiedValWithIdx = msgVal = string.Empty;
                var dataSetCountKeys = _inputDataList[INDEX].Keys;

                string iiStrVal = dataSetCountKeys.ElementAt(loopIndex);
                var workSheetsBundle = _inputDataList[INDEX][iiStrVal];
                MultiDictList<string, List<string>> workSheetsArr = null;
                Dictionary<string, List<List<string>>>.KeyCollection datasetKeys = null;
                List<List<string>> workSheet;

                workSheetsArr = workSheetsBundle[loopIndex + 1];
                datasetKeys = workSheetsArr.Keys;
                for (var i = 0; i < datasetKeys.Count; i++)
                {
                    string dataForVal = datasetKeys.ElementAt(i);
                    workSheet = workSheetsArr[dataForVal];
                    if (dataForVal == "Control")
                    {
                        cA.ReadPgmControlDataList(currDriver, pgmSeq, workSheet);
                    }
                    else
                    {
                        cSA.ReadSheetDataList(currDriver, pgmSeq, workbookId2, workSheet);
                    }
                }


                int countDirtyRows = cSA.CountDataRow(currDriver, pgmSeq, workbookId2, RowDataType.DIRTY_ROWS);
                cA.SavePgm(currDriver, eH, pgmSeq, null, intINDEX);
                cA.Delay();
                int countSavedRows = cSA.CountDataRow(currDriver, pgmSeq, workbookId2, RowDataType.SAVED_ROWS);

                cA.SetInputDataSetCount(currDriver, eH, cSA, intINDEX, pgmSeq, workbookId2, countSavedRows, loopIndex + 1);
                if (countDirtyRows == countSavedRows)
                {
                    copiedVal = cA.GetControlValue(currDriver, pgmSeq, "txtDelvNo_txt");
                    copiedValWithIdx = (parentLoopIndex + 1) + Keys.TAB + (loopIndex + 1) + Keys.TAB + copiedVal;
                    cA.WriteTempFile(fileName, copiedValWithIdx, true);
                    cA.ClosePgm(currDriver, pgmSeq);
                    msgVal = "Scenario Index 14 - " + pgmSeq + " - 납품번호 : " + copiedVal;
                    cLR.SetLogRecord(intINDEX, msgVal);
                }

                else
                {
                    msgVal = "Scenario Index 14 - " + pgmSeq + " - 납품번호 : 저장이 실패하였습니다.";
                    cLR.SetLogRecord(intINDEX, msgVal);
                    var isPopUpVisible = Validations.IsPopupVisible(currDriver, "messagePopup");
                    if (isPopUpVisible)
                    {
                        var messageText = cA.GetMessageDescription(currDriver);
                        cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);
                        msgVal = "Scenario Index 14 - " + pgmSeq + " - 메세지 : " + messageText;
                        cLR.SetLogRecord(intINDEX, msgVal);
                    }
                }

            }

            cA.ClosePgm(currDriver, pgmSeq);
            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);

            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 14 End--------------------");
        }

        private void _2_S15()
        {
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 15 Started--------------------");
            var INDEX = "15";
            var intINDEX = 15;
            Global.ScenarioIndex = INDEX;

            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.StartTime);
            var stepData = _stepsList[INDEX];
            var pgmSeq = stepData[0];
            var workSheetType = stepData[1];
            var fileName = cA.GetFileName("_S14");

            if (IsQueryModule(workSheetType))
            {

                var workSheets = _queryDataList[INDEX];

                cA.OpenPgm(currDriver, eH, pgmSeq, intINDEX);

                string pgmSeqFrame = cA.GetPgmSeqFrame(pgmSeq);
                currDriver.SwitchTo().Frame(currDriver.FindElement(By.Id(pgmSeqFrame)));

                var rowCount = cA.GetTempFileLineCount(fileName);
                var datasetCount = "";
                var seq = "";
                var delvNoVal = "";
                for (var j = 0; j < rowCount; j++)
                {
                    datasetCount = cA.ReadTempFile(fileName, j, 0);
                    seq = cA.ReadTempFile(fileName, j, 1);
                    delvNoVal = cA.ReadTempFile(fileName, j, 2);

                    cA.SetPgmControlData(currDriver, "txtDelvNo_txt", delvNoVal);
                    cA.ReadPgmControlDataList(currDriver, pgmSeq, workSheets);
                    cA.QueryPgm(currDriver, eH, pgmSeq, intINDEX, workbookId2, true, int.Parse(seq));


                    cSA.CellClick(currDriver, pgmSeq, workbookId2, 1, "Select");
                    cA.ClickJumpPgm(currDriver, eH, pgmSeq, "5", _stepsList["16"][0], 16);

                    cA.Delay(); // "Time to load jumped program"
                    eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                    _2_S16((int.Parse(seq) - 1), int.Parse(datasetCount) - 1);
                    Global.ScenarioIndex = INDEX;
                    msgVal = "Scenario Index 15 - " + pgmSeq + " - 구매입고 : " + delvNoVal;
                    cLR.SetLogRecord(intINDEX, msgVal);
                }
            }

            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 15 End--------------------");
            cA.ClosePgm(currDriver, pgmSeq);
            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
            _2_S17();
            Global.ScenarioIndex = INDEX;
        }

        private void _2_S16(int loopIndex, int parentLoopIndex)
        {
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 16 Started--------------------");
            var INDEX = "16";
            var intINDEX = 16;
            Global.ScenarioIndex = INDEX;
            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Execute);
            var stepData = _stepsList[INDEX];
            var pgmSeq = stepData[0];
            var workSheetType = stepData[1];

            var fileName = cA.GetFileName("_S16");
            var pathToFile = Path.Combine(cA.GetFilePath(), fileName);
            if (File.Exists(pathToFile) && loopIndex == 0 && parentLoopIndex == 0)
            {
                File.Delete(pathToFile);
            }
            eH.SetJumpDataSetCount(currDriver, intINDEX, pgmSeq, workbookId2, false, null, loopIndex + 1);
            cA.OpenPgm(currDriver, eH, pgmSeq, intINDEX);
            string pgmSeqFrame = cA.GetPgmSeqFrame(pgmSeq);
            currDriver.SwitchTo().Frame(currDriver.FindElement(By.Id(pgmSeqFrame)));



            if (IsInputModule(workSheetType))
            {
                string copiedVal, copiedValWithIdx, msgVal;
                copiedVal = copiedValWithIdx = msgVal = string.Empty;
                var dataSetCountKeys = _inputDataList[INDEX].Keys;

                string iiStrVal = dataSetCountKeys.ElementAt(loopIndex);
                var workSheetsBundle = _inputDataList[INDEX][iiStrVal];
                MultiDictList<string, List<string>> workSheetsArr = null;
                Dictionary<string, List<List<string>>>.KeyCollection datasetKeys = null;
                List<List<string>> workSheet;

                workSheetsArr = workSheetsBundle[loopIndex + 1];
                datasetKeys = workSheetsArr.Keys;
                for (var i = 0; i < datasetKeys.Count; i++)
                {
                    string dataForVal = datasetKeys.ElementAt(i);
                    workSheet = workSheetsArr[dataForVal];
                    if (dataForVal == "Control")
                    {
                        cA.ReadPgmControlDataList(currDriver, pgmSeq, workSheet);
                    }
                    else
                    {
                        cSA.ReadSheetDataList(currDriver, pgmSeq, workbookId2, workSheet);
                    }
                }


                int countDirtyRows = cSA.CountDataRow(currDriver, pgmSeq, workbookId2, RowDataType.DIRTY_ROWS);
                cA.SavePgm(currDriver, eH, pgmSeq, null, intINDEX);
                cA.Delay();
                int countSavedRows = cSA.CountDataRow(currDriver, pgmSeq, workbookId2, RowDataType.SAVED_ROWS);

                cA.SetInputDataSetCount(currDriver, eH, cSA, intINDEX, pgmSeq, workbookId2, countSavedRows, loopIndex + 1);
                if (countDirtyRows == countSavedRows)
                {
                    copiedVal = cA.GetControlValue(currDriver, pgmSeq, "txtDelvInNo_txt");
                    copiedValWithIdx = (parentLoopIndex + 1) + Keys.TAB + (loopIndex + 1) + Keys.TAB + copiedVal;
                    cA.WriteTempFile(fileName, copiedValWithIdx, true);
                    cA.ClosePgm(currDriver, pgmSeq);
                    msgVal = "Scenario Index 16 - " + pgmSeq + " - 입고번호 : " + copiedVal;
                    cLR.SetLogRecord(intINDEX, msgVal);
                }

                else
                {
                    msgVal = "Scenario Index 16 - " + pgmSeq + " - 입고번호: 저장이 실패하였습니다.";
                    cLR.SetLogRecord(intINDEX, msgVal);
                    var isPopUpVisible = Validations.IsPopupVisible(currDriver, "messagePopup");
                    if (isPopUpVisible)
                    {
                        var messageText = cA.GetMessageDescription(currDriver);
                        cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);
                        msgVal = "Scenario Index 16 - " + pgmSeq + " - 메세지 : " + messageText;
                        cLR.SetLogRecord(intINDEX, msgVal);
                    }
                }

            }

            cA.ClosePgm(currDriver, pgmSeq);
            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);

            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 16 End--------------------");
        }

        private void _2_S17()
        {
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 17 Started--------------------");
            var INDEX = "17";
            var intINDEX = 17;
            Global.ScenarioIndex = INDEX;

            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.StartTime);
            var stepData = _stepsList[INDEX];
            var pgmSeq = stepData[0];
            var popupPgmSeq = "502268";
            var workbookId3 = "SS_Row_c";
            var workSheetType = stepData[1];

            var fileName1 = cA.GetFileName("_S16");
            var fileName2 = cA.GetFileName("_S17");
            var pathToFile = cA.GetFilePath() + fileName2;
            if (File.Exists(pathToFile))
            {
                File.Delete(pathToFile);
            }


            if (IsInputModule(workSheetType))
            {
                var dataSetCountKeys = _inputDataList[INDEX].Keys;
                int lineCount = 0;
                List<string> delvInNoValArr = null;
                var delvInNoValIndex = -1;
                var seq = "";

                cA.OpenPgm(currDriver, eH, pgmSeq, intINDEX);

                string pgmSeqFrame = cA.GetPgmSeqFrame(pgmSeq);
                currDriver.SwitchTo().Frame(currDriver.FindElement(By.Id(pgmSeqFrame)));

                var fileName3 = cA.GetFileName("_S1");
                var datasetCount = cA.GetTempFileLineCount(fileName3);
                for (var j = 0; j < datasetCount; j++)
                {
                    seq = cA.ReadTempFile(fileName3, j, 0);

                    string iiStrVal = dataSetCountKeys.ElementAt(1);
                    var workSheetsBundle = _inputDataList[INDEX][iiStrVal];
                    MultiDictList<string, List<string>> workSheetsArr = null;
                    Dictionary<string, List<List<string>>>.KeyCollection datasetKeys = null;
                    List<List<string>> workSheet;

                    workSheetsArr = workSheetsBundle[1];

                    datasetKeys = workSheetsArr.Keys;
                    for (var i = 0; i < datasetKeys.Count; i++)
                    {
                        string dataForVal = datasetKeys.ElementAt(i);
                        workSheet = workSheetsArr[dataForVal];
                        if (dataForVal == "Control")
                        {
                            cA.ReadPgmControlDataList(currDriver, pgmSeq, workSheet);
                            cA.QueryPgm(currDriver, eH, pgmSeq, intINDEX, workbookId, true, int.Parse(seq));
                        }
                        else
                        {
                            delvInNoValArr = new List<string>();
                            lineCount = cA.GetTempFileLineCount(fileName1);
                            for (var k = 0; k < lineCount; k++)
                            {
                                if (cA.ReadTempFile(fileName1, k, 0) == seq)
                                {
                                    delvInNoValArr.Add(cA.ReadTempFile(fileName1, k, 2));
                                }
                            }
                            cSA.SetColFilter(currDriver, pgmSeq, workbookId, "DelvInNo", delvInNoValArr);
                            cA.Delay(); // "Time to filter col data."
                            cSA.ReadSheetDataList(currDriver, pgmSeq, workbookId, workSheet);

                            cA.SavePgm(currDriver, eH, pgmSeq, null, intINDEX);
                            int countSavedRows = cSA.CountDataRow(currDriver, pgmSeq, workbookId, RowDataType.SAVED_ROWS);

                            cA.SetInputDataSetCount(currDriver, eH, cSA, intINDEX, pgmSeq, workbookId, countSavedRows, int.Parse(seq));

                            int rowCount = cSA.CountDataRow(currDriver, pgmSeq, workbookId);
                            string cellValue = string.Empty;
                            for (var m = 0; m < rowCount; m++)
                            {
                                cellValue = cSA.GetCellValue(currDriver, workbookId, m, "DelvInNo");

                                delvInNoValIndex = delvInNoValArr.IndexOf(cellValue);
                                if (delvInNoValIndex != -1)
                                {
                                    cSA.CellClick(currDriver, pgmSeq, workbookId, m, "Sel");
                                }
                            }
                            cA.SelectPgmControl(currDriver, eH, pgmSeq, "btnSlip_btn", false, popupPgmSeq, INDEX);
                            cA.Delay(); //Time to load popup program
                            int countDirtyRows = cSA.CountDataRow(currDriver, popupPgmSeq, workbookId3, RowDataType.ADDED_ROWS);
                            cA.SavePgm(currDriver, eH, pgmSeq, null, intINDEX);
                            cA.Delay(); //Time to save
                            countSavedRows = cSA.CountDataRow(currDriver, popupPgmSeq, workbookId3, RowDataType.SAVED_ROWS);

                            if (countDirtyRows == countSavedRows)
                            {
                                cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);

                                string copiedVal = cA.GetControlValue(currDriver, pgmSeq, "txtSlipMstID_txt");
                                string copiedValWithIdx = seq + Keys.TAB + copiedVal;
                                cA.WriteTempFile(fileName2, copiedValWithIdx, true);
                                cA.SelectSpanControl(currDriver, null, "devDlgArea .divTitleArea", ".btnClose");

                                msgVal = "Scenario Index 17 - " + pgmSeq + " - 전표번호 : " + copiedVal;
                                cLR.SetLogRecord(intINDEX, msgVal);
                            }
                            else
                            {
                                msgVal = "Scenario Index 17 - " + pgmSeq + " - 전표번호 : 저장이 실패하였습니다.";
                                cLR.SetLogRecord(intINDEX, msgVal);
                                var isPopUpVisible = Validations.IsPopupVisible(currDriver, "messagePopup");
                                if (isPopUpVisible)
                                {
                                    var messageText = cA.GetMessageDescription(currDriver);
                                    cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);
                                    msgVal = "Scenario Index 17 - " + pgmSeq + " - 메세지 : " + messageText;
                                    cLR.SetLogRecord(intINDEX, msgVal);
                                }
                            }

                        }
                    }

                }
            }

            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 17 End--------------------");
            cA.ClosePgm(currDriver, pgmSeq);
            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
            _2_S18();
            Global.ScenarioIndex = INDEX;


        }

        private void _2_S18()
        {
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 18 Started--------------------");
            var INDEX = "18";
            var intINDEX = 18;
            Global.ScenarioIndex = INDEX;

            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.StartTime);
            var stepData = _stepsList[INDEX];
            var pgmSeq = stepData[0];
            var workSheetType = stepData[1];
            var fileName = cA.GetFileName("_S12");

            if (IsQueryModule(workSheetType))
            {

                var workSheets = _queryDataList[INDEX];
                var fileLineCount = 0;
                var poNoVal = "";
                var sMImpTypeVal = "";
                var rowCount = 0;
                var loopIndex = 0;
                var issMImpTypeExists = false;
                var startExistCheck = false;
                var seq = "";

                cA.OpenPgm(currDriver, eH, pgmSeq, intINDEX);

                string pgmSeqFrame = cA.GetPgmSeqFrame(pgmSeq);
                currDriver.SwitchTo().Frame(currDriver.FindElement(By.Id(pgmSeqFrame)));

                fileLineCount = cA.GetTempFileLineCount(fileName);
                for (var i = 0; i < fileLineCount; i++)
                {
                    issMImpTypeExists = false;
                    startExistCheck = false;
                    seq = cA.ReadTempFile(fileName, i, 0);

                    for (var j = 0; j < fileLineCount; j++)
                    {
                        if (cA.ReadTempFile(fileName, j, 0) == seq)
                        {
                            i++;
                            startExistCheck = true;
                            sMImpTypeVal = cA.ReadTempFile(fileName, j, 3);

                            if (sMImpTypeVal == "8008004")
                            {
                                issMImpTypeExists = true;
                                poNoVal = cA.ReadTempFile(fileName, j, 2);
                                loopIndex = int.Parse(cA.ReadTempFile(fileName, j, 1)) - 1;

                                cA.SetPgmControlData(currDriver, "txtPONo_txt", poNoVal);
                                cA.ReadPgmControlDataList(currDriver, pgmSeq, workSheets);

                                cA.QueryPgm(currDriver, eH, pgmSeq, intINDEX, workbookId, true, loopIndex + 1);
                                msgVal = "Scenario Index 18 - " + pgmSeq + " - P/O 관리번호: " + poNoVal;
                                cLR.SetLogRecord(intINDEX, msgVal);

                                rowCount = cSA.CountDataRow(currDriver, pgmSeq, workbookId);
                                if (rowCount > 1)
                                {
                                    cSA.CellClick(currDriver, pgmSeq, workbookId, 1, "Sel");
                                    cA.ClickJumpPgm(currDriver, eH, pgmSeq, "3", _stepsList["19"][0], 19);
                                    cA.Delay(); // "Time to load jumped program"
                                    eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                                    _2_S19(loopIndex, int.Parse(seq) - 1);
                                    Global.ScenarioIndex = INDEX;
                                }
                                else
                                {
                                    msgVal = "Scenario Index 18 - " + pgmSeq + " - 조회된 데이터가 없습니다.";
                                    cLR.SetLogRecord(intINDEX, msgVal);
                                    eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                                    _2_S29();
                                    Global.ScenarioIndex = INDEX;
                                }
                            }
                        }
                    }
                    if (!issMImpTypeExists && startExistCheck)
                    {
                        msgVal = "Scenario Index 18 - " + pgmSeq + " - 수입진행할 건이 없습니다.";
                        cLR.SetLogRecord(intINDEX, msgVal);
                        eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                        _2_S29();
                        Global.ScenarioIndex = INDEX;
                        break;
                    }
                    if (startExistCheck)
                    {
                        i--;
                    }

                }
            }

            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 18 End--------------------");
            cA.ClosePgm(currDriver, pgmSeq);
            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
            _2_S21();
            Global.ScenarioIndex = INDEX;
        }

        private void _2_S19(int loopIndex, int parentLoopIndex)
        {
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 19 Started--------------------");
            var INDEX = "19";
            var intINDEX = 19;
            Global.ScenarioIndex = INDEX;
            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Execute);
            var stepData = _stepsList[INDEX];
            var pgmSeq = stepData[0];
            var workSheetType = stepData[1];

            var fileName = cA.GetFileName("_S19");
            var pathToFile = Path.Combine(cA.GetFilePath(), fileName);
            if (File.Exists(pathToFile) && loopIndex == 0 && parentLoopIndex == 0)
            {
                File.Delete(pathToFile);
            }
            eH.SetJumpDataSetCount(currDriver, intINDEX, pgmSeq, workbookId2, false, null, loopIndex + 1);

            if (IsInputModule(workSheetType))
            {

                string copiedVal, copiedValWithIdx, msgVal;
                copiedVal = copiedValWithIdx = msgVal = string.Empty;
                var dataSetCountKeys = _inputDataList[INDEX].Keys;

                string iiStrVal = dataSetCountKeys.ElementAt(loopIndex);
                var workSheetsBundle = _inputDataList[INDEX][iiStrVal];
                MultiDictList<string, List<string>> workSheetsArr = null;
                Dictionary<string, List<List<string>>>.KeyCollection datasetKeys = null;
                List<List<string>> workSheet;

                workSheetsArr = workSheetsBundle[loopIndex + 1];
                datasetKeys = workSheetsArr.Keys;

                for (var i = 0; i < datasetKeys.Count; i++)
                {
                    string dataForVal = datasetKeys.ElementAt(i);
                    workSheet = workSheetsArr[dataForVal];
                    if (dataForVal == "Control")
                    {
                        cA.ReadPgmControlDataList(currDriver, pgmSeq, workSheet);
                    }
                    else
                    {
                        cSA.ReadSheetDataList(currDriver, pgmSeq, workbookId, workSheet);
                    }
                }


                int countDirtyRows = cSA.CountDataRow(currDriver, pgmSeq, workbookId, RowDataType.DIRTY_ROWS);
                cA.SavePgm(currDriver, eH, pgmSeq, null, intINDEX);
                cA.Delay();
                int countSavedRows = cSA.CountDataRow(currDriver, pgmSeq, workbookId, RowDataType.SAVED_ROWS);

                cA.SetInputDataSetCount(currDriver, eH, cSA, intINDEX, pgmSeq, workbookId, countSavedRows, loopIndex + 1);
                if (countDirtyRows == countSavedRows)
                {
                    cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);

                    copiedVal = cA.GetControlValue(currDriver, pgmSeq, "txtInvoiceNo_txt");
                    copiedValWithIdx = (parentLoopIndex + 1) + Keys.TAB + (loopIndex + 1) + Keys.TAB + copiedVal;
                    cA.WriteTempFile(fileName, copiedValWithIdx, true);
                    msgVal = "Scenario Index 19 - " + pgmSeq + " - Invoice관리번호 : " + copiedVal;
                    cLR.SetLogRecord(intINDEX, msgVal);

                    cA.ClickJumpPgm(currDriver, eH, pgmSeq, "18", _stepsList["20"][0], 20);
                    cA.Delay(); // "Time to load jumped program"
                    eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                    _2_S20(loopIndex, parentLoopIndex);
                    Global.ScenarioIndex = INDEX;
                }

                else
                {
                    msgVal = "Scenario Index 19 - " + pgmSeq + " - Invoice관리번호 : 저장이 실패하였습니다.";
                    cLR.SetLogRecord(intINDEX, msgVal);
                    var isPopUpVisible = Validations.IsPopupVisible(currDriver, "messagePopup");
                    if (isPopUpVisible)
                    {
                        var messageText = cA.GetMessageDescription(currDriver);
                        cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);
                        msgVal = "Scenario Index 19 - " + pgmSeq + " - 경고메세지: " + messageText;
                        cLR.SetLogRecord(intINDEX, msgVal);
                        eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                        _2_S29();
                        Global.ScenarioIndex = INDEX;
                    }
                }

            }

            cA.ClosePgm(currDriver, pgmSeq);
            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 19 End--------------------");
        }

        private void _2_S20(int loopIndex, int parentLoopIndex)
        {
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 20 Started--------------------");
            var INDEX = "20";
            var intINDEX = 20;
            Global.ScenarioIndex = INDEX;
            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Execute);
            var stepData = _stepsList[INDEX];
            var pgmSeq = stepData[0];
            var workSheetType = stepData[1];

            var fileName = cA.GetFileName("_S20");
            var pathToFile = Path.Combine(cA.GetFilePath(), fileName);
            if (File.Exists(pathToFile) && loopIndex == 0 && parentLoopIndex == 0)
            {
                File.Delete(pathToFile);
            }
            eH.SetJumpDataSetCount(currDriver, intINDEX, pgmSeq, workbookId, false, null, loopIndex + 1);

            if (IsInputModule(workSheetType))
            {

                string copiedVal, copiedValWithIdx, msgVal;
                copiedVal = copiedValWithIdx = msgVal = string.Empty;
                var dataSetCountKeys = _inputDataList[INDEX].Keys;

                string iiStrVal = dataSetCountKeys.ElementAt(loopIndex);
                var workSheetsBundle = _inputDataList[INDEX][iiStrVal];
                MultiDictList<string, List<string>> workSheetsArr = null;
                Dictionary<string, List<List<string>>>.KeyCollection datasetKeys = null;
                List<List<string>> workSheet;

                workSheetsArr = workSheetsBundle[loopIndex + 1];
                datasetKeys = workSheetsArr.Keys;

                for (var i = 0; i < datasetKeys.Count; i++)
                {
                    string dataForVal = datasetKeys.ElementAt(i);
                    workSheet = workSheetsArr[dataForVal];
                    if (dataForVal == "Control")
                    {
                        cA.ReadPgmControlDataList(currDriver, pgmSeq, workSheet);
                    }
                    else
                    {
                        cSA.ReadSheetDataList(currDriver, pgmSeq, workbookId, workSheet);
                    }
                }


                int countDirtyRows = cSA.CountDataRow(currDriver, pgmSeq, workbookId, RowDataType.DIRTY_ROWS);
                cA.SavePgm(currDriver, eH, pgmSeq, null, intINDEX);
                cA.Delay();
                int countSavedRows = cSA.CountDataRow(currDriver, pgmSeq, workbookId, RowDataType.SAVED_ROWS);

                cA.SetInputDataSetCount(currDriver, eH, cSA, intINDEX, pgmSeq, workbookId, countSavedRows, loopIndex + 1);
                if (countDirtyRows == countSavedRows)
                {
                    cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);

                    copiedVal = cA.GetControlValue(currDriver, pgmSeq, "txtBLNo_txt");
                    copiedValWithIdx = (parentLoopIndex + 1) + Keys.TAB + (loopIndex + 1) + Keys.TAB + copiedVal;
                    cA.WriteTempFile(fileName, copiedValWithIdx, true);
                    msgVal = "Scenario Index 20 - " + pgmSeq + " - B / L 관리번호: " + copiedVal;
                    cLR.SetLogRecord(intINDEX, msgVal);

                }

                else
                {
                    msgVal = "Scenario Index 20 - " + pgmSeq + " - B/L 관리번호 : 저장이 실패하였습니다.";
                    cLR.SetLogRecord(intINDEX, msgVal);
                    var isPopUpVisible = Validations.IsPopupVisible(currDriver, "messagePopup");
                    if (isPopUpVisible)
                    {
                        var messageText = cA.GetMessageDescription(currDriver);
                        cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);
                        msgVal = "Scenario Index 20 - " + pgmSeq + " - 경고메세지: " + messageText;
                        cLR.SetLogRecord(intINDEX, msgVal);
                        eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                        _2_S29();
                        Global.ScenarioIndex = INDEX;
                    }
                }

            }

            cA.ClosePgm(currDriver, pgmSeq);
            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 20 End--------------------");
        }

        private void _2_S21()
        {
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 21 Started--------------------");
            var INDEX = "21";
            var intINDEX = 21;
            Global.ScenarioIndex = INDEX;

            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.StartTime);
            var stepData = _stepsList[INDEX];
            var pgmSeq = stepData[0];
            var workSheetType = stepData[1];
            var fileName = cA.GetFileName("_S20");

            bool hasError = false;
            if (IsQueryModule(workSheetType))
            {
                var workSheets = _queryDataList[INDEX];

                var rowCount = 0;
                cA.OpenPgm(currDriver, eH, pgmSeq, intINDEX);

                string pgmSeqFrame = cA.GetPgmSeqFrame(pgmSeq);
                currDriver.SwitchTo().Frame(currDriver.FindElement(By.Id(pgmSeqFrame)));

                var lineCount = cA.GetTempFileLineCount(fileName);
                for (var j = 0; j < lineCount; j++)
                {
                    var bLNoVal = cA.ReadTempFile(fileName, j, 2);

                    cA.SetPgmControlData(currDriver, "txtBLNo_txt", bLNoVal);
                    cA.ReadPgmControlDataList(currDriver, pgmSeq, workSheets);
                    cA.QueryPgm(currDriver, eH, pgmSeq, intINDEX, workbookId, true, j + 1);

                    msgVal = "Scenario Index 21 - " + pgmSeq + " - B/L 관리번호 : " + bLNoVal;
                    cLR.SetLogRecord(intINDEX, msgVal);

                    rowCount = cSA.CountDataRow(currDriver, pgmSeq, workbookId);
                    if (rowCount > 1)
                    {
                        cSA.CellClick(currDriver, pgmSeq, workbookId, 1, "Sel");
                        cA.ClickJumpPgm(currDriver, eH, pgmSeq, "2", _stepsList["22"][0], 22);

                        cA.Delay(); // "Time to load jumped program"
                        eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                        _2_S22(j);
                        Global.ScenarioIndex = INDEX;
                    }
                    else
                    {
                        msgVal = "Scenario Index 21 - " + pgmSeq + " - 조회된 데이터가 없습니다.";
                        cLR.SetLogRecord(intINDEX, msgVal);
                        eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                        _2_S29();
                        Global.ScenarioIndex = INDEX;
                        hasError = true;
                    }



                }
            }
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 21 End--------------------");
            if (!hasError)
            {
                cA.ClosePgm(currDriver, pgmSeq);
                eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                _2_S23();
                Global.ScenarioIndex = INDEX;
            }

        }

        private void _2_S22(int loopIndex)
        {
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 22 Started--------------------");
            var INDEX = "22";
            var intINDEX = 22;
            Global.ScenarioIndex = INDEX;
            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.StartTime);
            var stepData = _stepsList[INDEX];
            var pgmSeq = stepData[0];
            var workSheetType = stepData[1];

            var fileName = cA.GetFileName("_S22");
            var pathToFile = Path.Combine(cA.GetFilePath(), fileName);
            if (File.Exists(pathToFile) && loopIndex == 0)
            {
                File.Delete(pathToFile);
            }
            eH.SetJumpDataSetCount(currDriver, intINDEX, pgmSeq, workbookId, false, null, loopIndex + 1);

            if (IsInputModule(workSheetType))
            {
                var dataSetCountKeys = _inputDataList[INDEX].Keys;
                var popupPgmSeq = "502268";

                string iiStrVal = dataSetCountKeys.ElementAt(loopIndex);
                var workSheetsBundle = _inputDataList[INDEX][iiStrVal];
                MultiDictList<string, List<string>> workSheetsArr = null;
                Dictionary<string, List<List<string>>>.KeyCollection datasetKeys = null;
                List<List<string>> workSheet;
                for (var seq = 0; seq < workSheetsBundle.Count; seq++)
                {
                    workSheetsArr = workSheetsBundle[seq];
                    datasetKeys = workSheetsArr.Keys;

                    for (var i = 0; i < datasetKeys.Count; i++)
                    {
                        string dataForVal = datasetKeys.ElementAt(i);
                        workSheet = workSheetsArr[dataForVal];
                        if (dataForVal == "Control")
                        {
                            cA.ReadPgmControlDataList(currDriver, pgmSeq, workSheet);
                        }


                    }
                    int countDirtyRows = cSA.CountDataRow(currDriver, pgmSeq, workbookId, RowDataType.DIRTY_ROWS);
                    cA.SavePgm(currDriver, eH, pgmSeq, null, intINDEX);
                    cA.Delay();
                    int countSavedRows = cSA.CountDataRow(currDriver, pgmSeq, workbookId, RowDataType.SAVED_ROWS);

                    cA.SetInputDataSetCount(currDriver, eH, cSA, intINDEX, pgmSeq, workbookId, countSavedRows, loopIndex + 1);
                    if (countDirtyRows == countSavedRows)
                    {
                        string copiedVal = cA.GetControlValue(currDriver, pgmSeq, "txtSourceNo_txt");
                        string copiedValWithIdx = (loopIndex + 1) + Keys.TAB + copiedVal;
                        cA.WriteTempFile(fileName, copiedValWithIdx, true);
                        msgVal = "Scenario Index 22 - " + pgmSeq + " - 원천관리번호 : " + copiedVal;
                        cLR.SetLogRecord(intINDEX, msgVal);

                        for (var i = 0; i < countSavedRows; i++)
                        {
                            cSA.CellClick(currDriver, pgmSeq, workbookId, i, "Select");
                        }

                        cA.SelectPgmControl(currDriver, eH, pgmSeq, "btnSlip_btn", false, popupPgmSeq, INDEX);
                        cA.Delay(2000); //Time to load Popup Module.
                        cA.SavePgm(currDriver, eH, popupPgmSeq);

                        cA.Delay(2000); //Time for write the txtSlipMstID_txt
                        copiedVal = cA.GetControlValue(currDriver, popupPgmSeq, "txtSourceNo_txt");
                        if (!string.IsNullOrEmpty(copiedVal))
                        {
                            cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);
                            cA.WriteTempFile(fileName, copiedVal, true);
                            cA.SelectSpanControl(currDriver, null, "devDlgArea .divTitleArea", ".btnClose");

                            msgVal = "Scenario Index 22 - " + pgmSeq + " - 전표번호 : " + copiedVal;
                            cLR.SetLogRecord(intINDEX, msgVal);
                        }
                        else
                        {
                            msgVal = "Scenario Index 22 - " + pgmSeq + " - 전표번호 :  전표처리가 실패하였습니다.";
                            cLR.SetLogRecord(intINDEX, msgVal);
                            var isPopUpVisible = Validations.IsPopupVisible(currDriver, "messagePopup");
                            if (isPopUpVisible)
                            {
                                var messageText = cA.GetMessageDescription(currDriver);
                                cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);
                                msgVal = "Scenario Index 22 - " + pgmSeq + " - 경고메세지 : " + messageText;
                                cLR.SetLogRecord(intINDEX, msgVal);
                                eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                                _2_S29();
                                Global.ScenarioIndex = INDEX;
                            }
                        }
                    }

                    else
                    {
                        msgVal = "Scenario Index 22 - " + pgmSeq + " - 원천관리번호 :  저장이 실패하였습니다.";
                        cLR.SetLogRecord(intINDEX, msgVal);
                        var isPopUpVisible = Validations.IsPopupVisible(currDriver, "messagePopup");
                        if (isPopUpVisible)
                        {
                            var messageText = cA.GetMessageDescription(currDriver);
                            cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);
                            msgVal = "Scenario Index 22 - " + pgmSeq + " - 경고메세지 : " + messageText;
                            cLR.SetLogRecord(intINDEX, msgVal);
                            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                            _2_S29();
                            Global.ScenarioIndex = INDEX;
                        }
                    }
                }

            }
            cA.ClosePgm(currDriver, pgmSeq);
            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);

            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 22 End--------------------");
        }

        private void _2_S23()
        {
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 23 Started--------------------");
            var INDEX = "23";
            var intINDEX = 23;
            Global.ScenarioIndex = INDEX;

            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.StartTime);
            var stepData = _stepsList[INDEX];
            var pgmSeq = stepData[0];
            var workSheetType = stepData[1];
            var fileName = cA.GetFileName("_S20");

            bool hasError = false;
            if (IsQueryModule(workSheetType))
            {
                var workSheets = _queryDataList[INDEX];

                var rowCount = 0;
                cA.OpenPgm(currDriver, eH, pgmSeq, intINDEX);

                string pgmSeqFrame = cA.GetPgmSeqFrame(pgmSeq);
                currDriver.SwitchTo().Frame(currDriver.FindElement(By.Id(pgmSeqFrame)));

                var lineCount = cA.GetTempFileLineCount(fileName);
                for (var j = 0; j < lineCount; j++)
                {
                    var bLNoVal = cA.ReadTempFile(fileName, j, 2);

                    cA.SetPgmControlData(currDriver, "txtBLNo_txt", bLNoVal);
                    cA.ReadPgmControlDataList(currDriver, pgmSeq, workSheets);
                    cA.QueryPgm(currDriver, eH, pgmSeq, intINDEX, workbookId, true, j + 1);

                    msgVal = "Scenario Index 23 - " + pgmSeq + " - B/L 관리번호 : " + bLNoVal;
                    cLR.SetLogRecord(intINDEX, msgVal);

                    rowCount = cSA.CountDataRow(currDriver, pgmSeq, workbookId);
                    if (rowCount > 1)
                    {
                        cSA.CellClick(currDriver, pgmSeq, workbookId, 1, "Sel");
                        cA.ClickJumpPgm(currDriver, eH, pgmSeq, "4", _stepsList["24"][0], 24);

                        cA.Delay(); // "Time to load jumped program"
                        eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                        _2_S24(j);
                        Global.ScenarioIndex = INDEX;
                    }
                    else
                    {
                        msgVal = "Scenario Index 23 - " + pgmSeq + " - 조회된 데이터가 없습니다.";
                        cLR.SetLogRecord(intINDEX, msgVal);
                        eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                        _2_S29();
                        Global.ScenarioIndex = INDEX;
                        hasError = true;
                    }
                }
            }
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 23 End--------------------");
            if (!hasError)
            {
                cA.ClosePgm(currDriver, pgmSeq);
                eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                _2_S26();
                Global.ScenarioIndex = INDEX;
            }
        }

        private void _2_S24(int loopIndex)
        {
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 24 Started--------------------");
            var INDEX = "24";
            var intINDEX = 24;
            Global.ScenarioIndex = INDEX;
            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.StartTime);
            var stepData = _stepsList[INDEX];
            var pgmSeq = stepData[0];
            var workSheetType = stepData[1];

            var fileName = cA.GetFileName("_S24");
            var pathToFile = Path.Combine(cA.GetFilePath(), fileName);
            if (File.Exists(pathToFile) && loopIndex == 0)
            {
                File.Delete(pathToFile);
            }
            eH.SetJumpDataSetCount(currDriver, intINDEX, pgmSeq, workbookId, false, null, loopIndex + 1);

            if (IsInputModule(workSheetType))
            {
                var dataSetCountKeys = _inputDataList[INDEX].Keys;

                string iiStrVal = dataSetCountKeys.ElementAt(loopIndex);
                var workSheetsBundle = _inputDataList[INDEX][iiStrVal];
                MultiDictList<string, List<string>> workSheetsArr = null;
                Dictionary<string, List<List<string>>>.KeyCollection datasetKeys = null;
                List<List<string>> workSheet;
                for (var seq = 0; seq < workSheetsBundle.Count; seq++)
                {
                    workSheetsArr = workSheetsBundle[seq];
                    datasetKeys = workSheetsArr.Keys;

                    for (var i = 0; i < datasetKeys.Count; i++)
                    {
                        string dataForVal = datasetKeys.ElementAt(i);
                        workSheet = workSheetsArr[dataForVal];
                        if (dataForVal == "Control")
                        {
                            cA.ReadPgmControlDataList(currDriver, pgmSeq, workSheet);
                        }
                        else
                        {
                            cSA.ReadSheetDataList(currDriver, pgmSeq, workbookId, workSheet);
                        }


                    }
                    int countDirtyRows = cSA.CountDataRow(currDriver, pgmSeq, workbookId, RowDataType.DIRTY_ROWS);
                    cA.SavePgm(currDriver, eH, pgmSeq, null, intINDEX);
                    cA.Delay();
                    int countSavedRows = cSA.CountDataRow(currDriver, pgmSeq, workbookId, RowDataType.SAVED_ROWS);

                    cA.SetInputDataSetCount(currDriver, eH, cSA, intINDEX, pgmSeq, workbookId, countSavedRows, loopIndex + 1);
                    if (countDirtyRows == countSavedRows)
                    {
                        cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);

                        string copiedVal = cA.GetControlValue(currDriver, pgmSeq, "txtPermiReftNo_txt");
                        string copiedValWithIdx = (loopIndex + 1) + Keys.TAB + copiedVal;
                        cA.WriteTempFile(fileName, copiedValWithIdx, true);
                        msgVal = "Scenario Index 24 - " + pgmSeq + " - 수입통관 관리번호 : " + copiedVal;
                        cLR.SetLogRecord(intINDEX, msgVal);

                        cA.ClickJumpPgm(currDriver, eH, pgmSeq, "13", _stepsList["25"][0], 25);
                        eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                        _2_S25(loopIndex);
                        Global.ScenarioIndex = INDEX;
                    }
                    else
                    {
                        msgVal = "Scenario Index 24 - " + pgmSeq + " - 수입통관 관리번호 : 저장이 실패하였습니다";
                        cLR.SetLogRecord(intINDEX, msgVal);
                        var isPopUpVisible = Validations.IsPopupVisible(currDriver, "messagePopup");
                        if (isPopUpVisible)
                        {
                            var messageText = cA.GetMessageDescription(currDriver);
                            cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);
                            msgVal = "Scenario Index 24 - " + pgmSeq + " - 경고메세지 : " + messageText;
                            cLR.SetLogRecord(intINDEX, msgVal);
                            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                            _2_S29();
                            Global.ScenarioIndex = INDEX;
                        }
                    }
                }

            }
            cA.ClosePgm(currDriver, pgmSeq);
            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);

            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 24 End--------------------");
        }

        private void _2_S25(int loopIndex)
        {
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 25 Started--------------------");
            var INDEX = "25";
            var intINDEX = 25;
            Global.ScenarioIndex = INDEX;
            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.StartTime);
            var stepData = _stepsList[INDEX];
            var pgmSeq = stepData[0];
            var workSheetType = stepData[1];

            var fileName = cA.GetFileName("_S25");
            var pathToFile = Path.Combine(cA.GetFilePath(), fileName);
            if (File.Exists(pathToFile) && loopIndex == 0)
            {
                File.Delete(pathToFile);
            }
            eH.SetJumpDataSetCount(currDriver, intINDEX, pgmSeq, workbookId, false, "UMExpenseItemName", loopIndex + 1);

            if (IsInputModule(workSheetType))
            {
                var dataSetCountKeys = _inputDataList[INDEX].Keys;

                var popupPgmSeq = "502268";

                string iiStrVal = dataSetCountKeys.ElementAt(loopIndex);
                var workSheetsBundle = _inputDataList[INDEX][iiStrVal];
                MultiDictList<string, List<string>> workSheetsArr = null;
                Dictionary<string, List<List<string>>>.KeyCollection datasetKeys = null;
                List<List<string>> workSheet;
                for (var seq = 0; seq < workSheetsBundle.Count; seq++)
                {
                    workSheetsArr = workSheetsBundle[seq];
                    datasetKeys = workSheetsArr.Keys;

                    for (var i = 0; i < datasetKeys.Count; i++)
                    {
                        string dataForVal = datasetKeys.ElementAt(i);
                        workSheet = workSheetsArr[dataForVal];
                        if (dataForVal == "Control")
                        {
                            cA.ReadPgmControlDataList(currDriver, pgmSeq, workSheet);
                        }
                        else
                        {
                            cSA.ReadSheetDataList(currDriver, pgmSeq, workbookId, workSheet);
                        }


                    }
                    cA.SavePgm(currDriver, eH, pgmSeq, null, intINDEX);
                    cA.Delay();
                    int countSavedRows = cSA.CountDataRow(currDriver, pgmSeq, workbookId, RowDataType.SAVED_ROWS);

                    cA.SetInputDataSetCount(currDriver, eH, cSA, intINDEX, pgmSeq, workbookId, countSavedRows, loopIndex + 1);

                    string copiedVal = cA.GetControlValue(currDriver, pgmSeq, "txtSourceNo_txt");
                    string copiedValWithIdx = (loopIndex + 1) + Keys.TAB + copiedVal;
                    cA.WriteTempFile(fileName, copiedValWithIdx, true);

                    cSA.CellClick(currDriver, pgmSeq, workbookId, 0, "Select", false, true);
                    cSA.SelectSheetContextMenu(currDriver, pgmSeq, workbookId, "9");

                    cA.SelectPgmControl(currDriver, eH, pgmSeq, "btnSlip_btn", false, popupPgmSeq, INDEX);

                    cA.Delay();
                    cA.SavePgm(currDriver, eH, popupPgmSeq);
                    cA.Delay();

                    copiedVal = cA.GetControlValue(currDriver, pgmSeq, "txtSlipMstID_txt");
                    if (!string.IsNullOrEmpty(copiedVal))
                    {
                        cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);
                        cA.WriteTempFile(fileName, copiedVal, true);
                        cA.SelectSpanControl(currDriver, null, "devDlgArea .divTitleArea", ".btnClose");
                        msgVal = "Scenario Index 25 - " + pgmSeq + " - 전표번호 : " + copiedVal;
                        cLR.SetLogRecord(intINDEX, msgVal);
                    }
                    else
                    {
                        msgVal = "Scenario Index 25 - " + pgmSeq + " - 전표번호 :  전표처리가 실패하였습니다.";
                        cLR.SetLogRecord(intINDEX, msgVal);
                        var isPopUpVisible = Validations.IsPopupVisible(currDriver, "messagePopup");
                        if (isPopUpVisible)
                        {
                            var messageText = cA.GetMessageDescription(currDriver);
                            cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);
                            msgVal = "Scenario Index 25 - " + pgmSeq + " - 경고메세지 : " + messageText;
                            cLR.SetLogRecord(intINDEX, msgVal);
                            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                            _2_S29();
                            Global.ScenarioIndex = INDEX;
                        }
                    }
                }
            }
            cA.ClosePgm(currDriver, pgmSeq);
            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);

            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 25 End--------------------");
        }

        private void _2_S26()
        {
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 26 Started--------------------");
            var INDEX = "26";
            var intINDEX = 26;
            Global.ScenarioIndex = INDEX;

            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.StartTime);
            var stepData = _stepsList[INDEX];
            var pgmSeq = stepData[0];
            var workSheetType = stepData[1];
            var fileName = cA.GetFileName("_S24");

            bool hasError = false;
            if (IsQueryModule(workSheetType))
            {
                var workSheets = _queryDataList[INDEX];

                var rowCount = 0;
                cA.OpenPgm(currDriver, eH, pgmSeq, intINDEX);
                string pgmSeqFrame = cA.GetPgmSeqFrame(pgmSeq);
                currDriver.SwitchTo().Frame(currDriver.FindElement(By.Id(pgmSeqFrame)));

                var lineCount = cA.GetTempFileLineCount(fileName);
                for (var j = 0; j < lineCount; j++)
                {
                    var permiReftVal = cA.ReadTempFile(fileName, j, 1);

                    cA.SetPgmControlData(currDriver, "txtPermiReftNo_txt", permiReftVal);
                    cA.ReadPgmControlDataList(currDriver, pgmSeq, workSheets);
                    cA.QueryPgm(currDriver, eH, pgmSeq, intINDEX, workbookId, true, j + 1);

                    msgVal = "Scenario Index 26 - " + pgmSeq + " - 수입통관 관리번호 : " + permiReftVal;
                    cLR.SetLogRecord(intINDEX, msgVal);

                    rowCount = cSA.CountDataRow(currDriver, pgmSeq, workbookId);
                    if (rowCount > 1)
                    {
                        cSA.CellClick(currDriver, pgmSeq, workbookId, 1, "Sel");
                        cA.ClickJumpPgm(currDriver, eH, pgmSeq, "5", _stepsList["27"][0], 27);

                        cA.Delay(); // "Time to load jumped program"
                        eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                        _2_S27(j);
                        Global.ScenarioIndex = INDEX;
                    }
                    else
                    {
                        eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                        _2_S29();
                        Global.ScenarioIndex = INDEX;
                        hasError = true;
                    }
                }
            }
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 26 End--------------------");
            if (!hasError)
            {
                cA.ClosePgm(currDriver, pgmSeq);
                eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                _2_S28();
                Global.ScenarioIndex = INDEX;
            }
        }


        private void _2_S27(int loopIndex)
        {
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 27 Started--------------------");
            var INDEX = "27";
            var intINDEX = 27;
            Global.ScenarioIndex = INDEX;
            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.StartTime);
            var stepData = _stepsList[INDEX];
            var pgmSeq = stepData[0];
            var workSheetType = stepData[1];

            var fileName = cA.GetFileName("_S27");
            var pathToFile = Path.Combine(cA.GetFilePath(), fileName);
            if (File.Exists(pathToFile) && loopIndex == 0)
            {
                File.Delete(pathToFile);
            }
            eH.SetJumpDataSetCount(currDriver, intINDEX, pgmSeq, workbookId, false, null, loopIndex + 1);

            if (IsInputModule(workSheetType))
            {
                var dataSetCountKeys = _inputDataList[INDEX].Keys;

                string iiStrVal = dataSetCountKeys.ElementAt(loopIndex);
                var workSheetsBundle = _inputDataList[INDEX][iiStrVal];
                MultiDictList<string, List<string>> workSheetsArr = null;
                Dictionary<string, List<List<string>>>.KeyCollection datasetKeys = null;
                List<List<string>> workSheet;
                for (var seq = 0; seq < workSheetsBundle.Count; seq++)
                {
                    workSheetsArr = workSheetsBundle[seq];
                    datasetKeys = workSheetsArr.Keys;

                    for (var i = 0; i < datasetKeys.Count; i++)
                    {
                        string dataForVal = datasetKeys.ElementAt(i);
                        workSheet = workSheetsArr[dataForVal];
                        if (dataForVal == "Control")
                        {
                            cA.ReadPgmControlDataList(currDriver, pgmSeq, workSheet);
                        }
                        else
                        {
                            cSA.ReadSheetDataList(currDriver, pgmSeq, workbookId, workSheet);
                        }

                    }
                    int countDirtyRows = cSA.CountDataRow(currDriver, pgmSeq, workbookId, RowDataType.DIRTY_ROWS);
                    cA.SavePgm(currDriver, eH, pgmSeq, null, intINDEX);
                    cA.Delay();
                    int countSavedRows = cSA.CountDataRow(currDriver, pgmSeq, workbookId, RowDataType.SAVED_ROWS);

                    cA.SetInputDataSetCount(currDriver, eH, cSA, intINDEX, pgmSeq, workbookId, countSavedRows, loopIndex + 1);
                    if (countDirtyRows == countSavedRows)
                    {
                        string copiedVal = cA.GetControlValue(currDriver, pgmSeq, "txtDelvNo_txt");
                        string copiedValWithIdx = (loopIndex + 1) + Keys.TAB + copiedVal;
                        cA.WriteTempFile(fileName, copiedValWithIdx, true);
                        msgVal = "Scenario Index 27 - " + pgmSeq + " - 수입입고번호 : " + copiedVal;
                        cLR.SetLogRecord(intINDEX, msgVal);
                    }
                    else
                    {
                        msgVal = "Scenario Index 27 - " + pgmSeq + " - 수입입고번호 :  저장이 실패하였습니다";
                        cLR.SetLogRecord(intINDEX, msgVal);
                        var isPopUpVisible = Validations.IsPopupVisible(currDriver, "messagePopup");
                        if (isPopUpVisible)
                        {
                            var messageText = cA.GetMessageDescription(currDriver);
                            cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);
                            msgVal = "Scenario Index 27 - " + pgmSeq + " - 경고메세지 : " + messageText;
                            cLR.SetLogRecord(intINDEX, msgVal);
                            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                            _2_S29();
                            Global.ScenarioIndex = INDEX;
                        }
                    }
                }

            }
            cA.ClosePgm(currDriver, pgmSeq);
            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);

            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 27 End--------------------");
        }

        private void _2_S28()
        {
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 28 Started--------------------");
            var INDEX = "28";
            var intINDEX = 28;
            Global.ScenarioIndex = INDEX;

            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.StartTime);
            var stepData = _stepsList[INDEX];
            var pgmSeq = stepData[0];
            var workSheetType = stepData[1];
            var workbookId3 = "SS2_c";
            var fileName1 = cA.GetFileName("_S27");
            var fileName2 = cA.GetFileName("_S28");
            var popupPgmSeq = "502268";

            bool hasError = false;
            if (IsQueryModule(workSheetType))
            {
                var workSheets = _queryDataList[INDEX];

                var rowCount = 0;
                cA.OpenPgm(currDriver, eH, pgmSeq, intINDEX);
                string pgmSeqFrame = cA.GetPgmSeqFrame(pgmSeq);
                currDriver.SwitchTo().Frame(currDriver.FindElement(By.Id(pgmSeqFrame)));

                var lineCount = cA.GetTempFileLineCount(fileName1);
                for (var i = 0; i < lineCount; i++)
                {
                    var delvNoVal = cA.ReadTempFile(fileName1, i, 1);

                    cA.SetPgmControlData(currDriver, "txtDelvNo_txt", delvNoVal);
                    cA.ReadPgmControlDataList(currDriver, pgmSeq, workSheets);
                    cA.QueryPgm(currDriver, eH, pgmSeq, intINDEX, workbookId, true, i + 1);

                    msgVal = "Scenario Index 28 - " + pgmSeq + " - 수입입고번호번호 : " + delvNoVal;
                    cLR.SetLogRecord(intINDEX, msgVal);

                    rowCount = cSA.CountDataRow(currDriver, pgmSeq, workbookId);
                    if (rowCount > 1)
                    {
                        cSA.CellClick(currDriver, pgmSeq, workbookId, 1, "Sel");
                        cA.SelectPgmControl(currDriver, eH, pgmSeq, "btnCostCalcDiv_btn");

                        cA.Delay(); // "Waiting for popup"
                        currDriver.SwitchTo().Alert().Accept();

                        cA.Delay(); // "Time to complete operation"
                        cA.SelectPgmControl(currDriver, eH, pgmSeq, "btnGetCost_btn");

                        rowCount = cSA.CountDataRow(currDriver, pgmSeq, workbookId3);
                        if (rowCount > 0)
                        {
                            cA.SelectPgmControl(currDriver, eH, pgmSeq, "btnSlipProc_btn", false, popupPgmSeq, INDEX);
                            cA.Delay(); // "Time to load Popup Module."
                            cA.SavePgm(currDriver, eH, popupPgmSeq, null, intINDEX);
                            cA.Delay(); // "Time for write the txtSlipMstID_txt""

                            string copiedVal = cA.GetControlValue(currDriver, popupPgmSeq, "txtSlipMstID_txt");
                            if (!string.IsNullOrEmpty(copiedVal))
                            {
                                cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);
                                string copiedValWithIdx = (i + 1) + Keys.TAB + copiedVal;
                                cA.WriteTempFile(fileName2, copiedValWithIdx, true);
                                cA.SelectSpanControl(currDriver, null, "devDlgArea .divTitleArea", ".btnClose");
                                msgVal = "Scenario Index 28 - " + pgmSeq + " - 전표번호 : " + copiedVal;
                                cLR.SetLogRecord(intINDEX, msgVal);
                            }
                            else
                            {
                                msgVal = "Scenario Index 28 - " + pgmSeq + " - 전표번호 :  전표처리가 실패하였습니다.";
                                cLR.SetLogRecord(intINDEX, msgVal);
                                var isPopUpVisible = Validations.IsPopupVisible(currDriver, "messagePopup");
                                if (isPopUpVisible)
                                {
                                    var messageText = cA.GetMessageDescription(currDriver);
                                    cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);
                                    msgVal = "Scenario Index 28 - " + pgmSeq + " - 경고메세지 : " + messageText;
                                    cLR.SetLogRecord(intINDEX, msgVal);
                                    eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                                    _2_S29();
                                    Global.ScenarioIndex = INDEX;
                                }
                            }

                        }
                        else
                        {
                            msgVal = "Scenario Index 28 - " + pgmSeq + " - 수입입고번호번호 : " + delvNoVal + " 의 수입원가비용내역이 존재하지 않습니다";
                            cLR.SetLogRecord(intINDEX, msgVal);
                            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                            _2_S29();
                            Global.ScenarioIndex = INDEX;
                            hasError = true;
                        }
                    }
                    else
                    {
                        eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                        _2_S29();
                        Global.ScenarioIndex = INDEX;
                        hasError = true;
                    }
                }
            }
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 28 End--------------------");
            if (!hasError)
            {
                cA.ClosePgm(currDriver, pgmSeq);
                eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                _2_S28();
                Global.ScenarioIndex = INDEX;
            }
        }

        private void _2_S29()
        {
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 29 Started--------------------");
            var INDEX = "29";
            var intINDEX = 29;
            Global.ScenarioIndex = INDEX;

            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.StartTime);
            var stepData = _stepsList[INDEX];
            var pgmSeq = stepData[0];
            var workSheetType = stepData[1];
            var fileName = cA.GetFileName("_S5");
            var pathToFile = cA.GetFilePath() + fileName;

            bool hasError = false;
            if (IsQueryModule(workSheetType))
            {
                var workSheets = _queryDataList[INDEX];

                var rowCount = 0;
                var lineCount = 0;
                var prodPlanNoVal = "";
                var rowIndex = -1;
                cA.OpenPgm(currDriver, eH, pgmSeq, intINDEX);
                string pgmSeqFrame = cA.GetPgmSeqFrame(pgmSeq);
                currDriver.SwitchTo().Frame(currDriver.FindElement(By.Id(pgmSeqFrame)));

                rowCount = cSA.CountDataRow(currDriver, pgmSeq, workbookId);

                if (rowCount == 0 || !File.Exists(pathToFile))
                {
                    msgVal = "Scenario Index 29 - " + pgmSeq + " - 조회된 외주계획건이 없습니다.";
                    cLR.SetLogRecord(intINDEX, msgVal);

                    eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                    _2_S40();
                    Global.ScenarioIndex = INDEX;

                    hasError = true;
                }
                else
                {
                    var datasetCount = cA.GetTempFileLineCount(cA.GetFileName("_S1"));
                    for (var i = 0; i < datasetCount; i++)
                    {
                        if (i > 0)
                        {
                            cSA.CellClick(currDriver, pgmSeq, workbookId, 0, "Sel", false, true);
                            cSA.SelectSheetContextMenu(currDriver, pgmSeq, workbookId, "10");
                            cA.Delay();//"Waiting To Clear All"
                        }
                        var isprodPlanNoExists = false;
                        lineCount = cA.GetTempFileLineCount(fileName);
                        for (var j = 0; j < lineCount; j++)
                        {
                            var seq = cA.ReadTempFile(fileName, j, 0);
                            if ((i + 1).ToString() == seq)
                            {
                                prodPlanNoVal = cA.ReadTempFile(fileName, j, 2);
                                rowIndex = cSA.GetRowIndex(currDriver, workbookId, "ProdPlanNo", prodPlanNoVal);
                                if (rowIndex != -1)
                                {
                                    cSA.CellClick(currDriver, pgmSeq, workbookId, rowIndex, "Sel");
                                    isprodPlanNoExists = true;
                                }
                            }
                        }
                        if (!isprodPlanNoExists)
                        {
                            msgVal = "Scenario Index 29 - " + pgmSeq + " - 선택할 외주계획건이 없습니다.";
                            cLR.SetLogRecord(intINDEX, msgVal);

                            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                            _2_S40();
                            Global.ScenarioIndex = INDEX;
                            hasError = true;
                        }
                        else
                        {
                            cA.ClickJumpPgm(currDriver, eH, pgmSeq, "2", _stepsList["30"][0], 30);
                            cA.Delay(); // "Time for load jumped progrcam"

                            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                            _2_S30(i);
                            Global.ScenarioIndex = INDEX;
                        }

                    }
                }
            }

            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 29 End--------------------");
            if (!hasError)
            {
                cA.ClosePgm(currDriver, pgmSeq);
                eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                _2_S31();
                Global.ScenarioIndex = INDEX;
            }
        }

        private void _2_S30(int loopIndex)
        {
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 30 Started--------------------");
            var INDEX = "30";
            var intINDEX = 30;
            Global.ScenarioIndex = INDEX;
            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.StartTime);
            var stepData = _stepsList[INDEX];
            var pgmSeq = stepData[0];
            var workSheetType = stepData[1];

            var workbookId3 = "SS2_c";
            var fileName = cA.GetFileName("_S30");
            var pathToFile = Path.Combine(cA.GetFilePath(), fileName);
            if (File.Exists(pathToFile) && loopIndex == 0)
            {
                File.Delete(pathToFile);
            }
            eH.SetJumpDataSetCount(currDriver, intINDEX, pgmSeq, workbookId, false, null, loopIndex + 1);
            eH.SetJumpDataSetCount(currDriver, intINDEX, pgmSeq, workbookId3, false, null, loopIndex + 1);

            if (IsInputModule(workSheetType))
            {
                var dataSetCountKeys = _inputDataList[INDEX].Keys;

                string iiStrVal = dataSetCountKeys.ElementAt(loopIndex);
                var workSheetsBundle = _inputDataList[INDEX][iiStrVal];
                MultiDictList<string, List<string>> workSheetsArr = null;
                Dictionary<string, List<List<string>>>.KeyCollection datasetKeys = null;
                List<List<string>> workSheet;
                for (var seq = 0; seq < workSheetsBundle.Count; seq++)
                {
                    workSheetsArr = workSheetsBundle[seq];
                    datasetKeys = workSheetsArr.Keys;

                    for (var i = 0; i < datasetKeys.Count; i++)
                    {
                        string dataForVal = datasetKeys.ElementAt(i);
                        workSheet = workSheetsArr[dataForVal];
                        if (dataForVal == "Control")
                        {
                            cA.ReadPgmControlDataList(currDriver, pgmSeq, workSheet);
                        }
                        else
                        {
                            cSA.ReadSheetDataList(currDriver, pgmSeq, workbookId, workSheet);
                        }

                    }
                    int countDirtyRows = cSA.CountDataRow(currDriver, pgmSeq, workbookId, RowDataType.DIRTY_ROWS);
                    cA.SavePgm(currDriver, eH, pgmSeq, null, intINDEX);
                    cA.Delay();
                    int countSavedRows = cSA.CountDataRow(currDriver, pgmSeq, workbookId, RowDataType.SAVED_ROWS);

                    cA.SetInputDataSetCount(currDriver, eH, cSA, intINDEX, pgmSeq, workbookId, countSavedRows, loopIndex + 1);
                    if (countDirtyRows == countSavedRows)
                    {
                        string copiedVal = cA.GetControlValue(currDriver, pgmSeq, "txtOSPPONo_txt");
                        string copiedValWithIdx = (loopIndex + 1) + Keys.TAB + copiedVal;
                        cA.WriteTempFile(fileName, copiedValWithIdx, true);
                        msgVal = "Scenario Index 30 - " + pgmSeq + " - 외주발주번호 : " + copiedVal;
                        cLR.SetLogRecord(intINDEX, msgVal);
                        cA.SelectPgmControl(currDriver, eH, pgmSeq, "btnGetNeetMat_btn");

                        cA.Delay(); // "Waiting for popup"
                        currDriver.SwitchTo().Alert().Accept();

                        cA.Delay(); // "Time to load data"
                        cA.SavePgm(currDriver, eH, pgmSeq, null, intINDEX);
                        cA.Delay();
                        countSavedRows = cSA.CountDataRow(currDriver, pgmSeq, workbookId3, RowDataType.SAVED_ROWS);
                        cA.SetInputDataSetCount(currDriver, eH, cSA, intINDEX, pgmSeq, workbookId3, countSavedRows, loopIndex + 1);

                    }
                    else
                    {
                        msgVal = "Scenario Index 30 - " + pgmSeq + " - 외주발주번호 : 저장이 실패하였습니다.";
                        cLR.SetLogRecord(intINDEX, msgVal);
                        var isPopUpVisible = Validations.IsPopupVisible(currDriver, "messagePopup");
                        if (isPopUpVisible)
                        {
                            var messageText = cA.GetMessageDescription(currDriver);
                            cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);
                            msgVal = "Scenario Index 30 - " + pgmSeq + " - 경고메세지 : " + messageText;
                            cLR.SetLogRecord(intINDEX, msgVal);
                            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                            _2_S40();
                            Global.ScenarioIndex = INDEX;
                        }
                    }
                }

            }
            cA.ClosePgm(currDriver, pgmSeq);
            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);

            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 30 End--------------------");
        }

        private void _2_S31()
        {
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 31 Started--------------------");
            var INDEX = "31";
            var intINDEX = 31;
            Global.ScenarioIndex = INDEX;

            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.StartTime);
            var stepData = _stepsList[INDEX];
            var pgmSeq = stepData[0];
            var workSheetType = stepData[1];
            var fileName = cA.GetFileName("_S30");

            bool hasError = false;
            if (IsQueryModule(workSheetType))
            {
                var workSheets = _queryDataList[INDEX];

                var rowCount = 0;
                cA.OpenPgm(currDriver, eH, pgmSeq, intINDEX);
                string pgmSeqFrame = cA.GetPgmSeqFrame(pgmSeq);
                currDriver.SwitchTo().Frame(currDriver.FindElement(By.Id(pgmSeqFrame)));


                var lineCount = cA.GetTempFileLineCount(fileName);
                for (var i = 0; i < lineCount; i++)
                {
                    string outReqNoVal = cA.ReadTempFile(fileName, i, 1);
                    cA.SetPgmControlData(currDriver, "txtOutReqNo_txt", outReqNoVal);
                    cA.ReadPgmControlDataList(currDriver, pgmSeq, workSheets);
                    cA.QueryPgm(currDriver, eH, pgmSeq, intINDEX, workbookId, true, i + 1);

                    rowCount = cSA.CountDataRow(currDriver, pgmSeq, workbookId);
                    if (rowCount == 0)
                    {
                        msgVal = "Scenario Index 31 - " + pgmSeq + " - 조회된 자재출고요청품목이 없습니다.";
                        cLR.SetLogRecord(intINDEX, msgVal);

                        eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                        _2_S40();
                        Global.ScenarioIndex = INDEX;
                        hasError = true;
                    }
                    else
                    {
                        cSA.CellClick(currDriver, pgmSeq, workbookId, 1, "Select", false, true);
                        cSA.SelectSheetContextMenu(currDriver, pgmSeq, workbookId, "9");
                        cA.ClickJumpPgm(currDriver, eH, pgmSeq, "4", _stepsList["32"][0], 32);
                        cA.Delay(); // "Time for load jumped progrcam"

                        eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                        _2_S32(i);
                        Global.ScenarioIndex = INDEX;
                    }
                }
            }

            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 31 End--------------------");
            if (!hasError)
            {
                cA.ClosePgm(currDriver, pgmSeq);
                eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                _2_S33();
                Global.ScenarioIndex = INDEX;
            }
        }

        private void _2_S32(int loopIndex)
        {
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 32 Started--------------------");
            var INDEX = "32";
            var intINDEX = 32;
            Global.ScenarioIndex = INDEX;
            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.StartTime);
            var stepData = _stepsList[INDEX];
            var pgmSeq = stepData[0];
            var workSheetType = stepData[1];

            var workbookId3 = "SSMatOutItem_c";
            var workbookId4 = "SSMatOutGood_c";
            var fileName = cA.GetFileName("_S32");
            var pathToFile = Path.Combine(cA.GetFilePath(), fileName);
            if (File.Exists(pathToFile) && loopIndex == 0)
            {
                File.Delete(pathToFile);
            }
            eH.SetJumpDataSetCount(currDriver, intINDEX, pgmSeq, workbookId3, false, null, loopIndex + 1);
            eH.SetJumpDataSetCount(currDriver, intINDEX, pgmSeq, workbookId4, false, null, loopIndex + 1);

            if (IsInputModule(workSheetType))
            {
                var dataSetCountKeys = _inputDataList[INDEX].Keys;

                string iiStrVal = dataSetCountKeys.ElementAt(loopIndex);
                var workSheetsBundle = _inputDataList[INDEX][iiStrVal];
                MultiDictList<string, List<string>> workSheetsArr = null;
                Dictionary<string, List<List<string>>>.KeyCollection datasetKeys = null;
                List<List<string>> workSheet;
                for (var seq = 0; seq < workSheetsBundle.Count; seq++)
                {
                    workSheetsArr = workSheetsBundle[seq];
                    datasetKeys = workSheetsArr.Keys;

                    for (var i = 0; i < datasetKeys.Count; i++)
                    {
                        string dataForVal = datasetKeys.ElementAt(i);
                        workSheet = workSheetsArr[dataForVal];
                        if (dataForVal == "Control")
                        {
                            cA.ReadPgmControlDataList(currDriver, pgmSeq, workSheet);
                        }
                        else
                        {
                            cSA.ReadSheetDataList(currDriver, pgmSeq, workbookId3, workSheet);
                        }

                    }
                    int countDirtyRows = cSA.CountDataRow(currDriver, pgmSeq, workbookId3, RowDataType.DIRTY_ROWS);
                    cA.SavePgm(currDriver, eH, pgmSeq, null, intINDEX);
                    cA.Delay();
                    int countSavedRows = cSA.CountDataRow(currDriver, pgmSeq, workbookId3, RowDataType.SAVED_ROWS);

                    cA.SetInputDataSetCount(currDriver, eH, cSA, intINDEX, pgmSeq, workbookId3, countSavedRows, loopIndex + 1);

                    countSavedRows = cSA.CountDataRow(currDriver, pgmSeq, workbookId4, RowDataType.SAVED_ROWS);

                    cA.SetInputDataSetCount(currDriver, eH, cSA, intINDEX, pgmSeq, workbookId4, countSavedRows, loopIndex + 1);
                    if (countDirtyRows == countSavedRows)
                    {
                        string copiedVal = cA.GetControlValue(currDriver, pgmSeq, "txtMatOutNo_txt");
                        string copiedValWithIdx = (loopIndex + 1) + Keys.TAB + copiedVal;
                        cA.WriteTempFile(fileName, copiedValWithIdx, true);
                        msgVal = "Scenario Index 32 - " + pgmSeq + " - 출고번호 : " + copiedVal;
                        cLR.SetLogRecord(intINDEX, msgVal);
                    }
                    else
                    {
                        msgVal = "Scenario Index 32 - " + pgmSeq + " - 출고번호 :  저장이 실패하였습니다.";
                        cLR.SetLogRecord(intINDEX, msgVal);
                        var isPopUpVisible = Validations.IsPopupVisible(currDriver, "messagePopup");
                        if (isPopUpVisible)
                        {
                            var messageText = cA.GetMessageDescription(currDriver);
                            cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);
                            msgVal = "Scenario Index 32 - " + pgmSeq + " - 경고메세지 : " + messageText;
                            cLR.SetLogRecord(intINDEX, msgVal);
                            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                            _2_S40();
                            Global.ScenarioIndex = INDEX;
                        }
                    }
                }

            }
            cA.ClosePgm(currDriver, pgmSeq);
            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);

            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 32 End--------------------");
        }

        private void _2_S33()
        {
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 33 Started--------------------");
            var INDEX = "33";
            var intINDEX = 33;
            Global.ScenarioIndex = INDEX;

            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.StartTime);
            var stepData = _stepsList[INDEX];
            var pgmSeq = stepData[0];
            var workSheetType = stepData[1];
            var fileName = cA.GetFileName("_S30");

            bool hasError = false;
            if (IsQueryModule(workSheetType))
            {
                var workSheets = _queryDataList[INDEX];

                var rowCount = 0;
                cA.OpenPgm(currDriver, eH, pgmSeq, intINDEX);
                string pgmSeqFrame = cA.GetPgmSeqFrame(pgmSeq);
                currDriver.SwitchTo().Frame(currDriver.FindElement(By.Id(pgmSeqFrame)));


                var lineCount = cA.GetTempFileLineCount(fileName);
                for (var i = 0; i < lineCount; i++)
                {
                    string pONoVal = cA.ReadTempFile(fileName, i, 1);
                    cA.SetPgmControlData(currDriver, "txtPONo_txt", pONoVal);
                    cA.ReadPgmControlDataList(currDriver, pgmSeq, workSheets);
                    cA.QueryPgm(currDriver, eH, pgmSeq, intINDEX, workbookId, true, 1);

                    rowCount = cSA.CountDataRow(currDriver, pgmSeq, workbookId2);
                    if (rowCount == 0)
                    {
                        msgVal = "Scenario Index 33 - " + pgmSeq + " - 조회된 외주발주건이 없습니다.";
                        cLR.SetLogRecord(intINDEX, msgVal);

                        eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                        _2_S40();
                        Global.ScenarioIndex = INDEX;
                        hasError = true;
                    }
                    else
                    {
                        cSA.CellClick(currDriver, pgmSeq, workbookId2, 1, "Sel");
                        cA.ClickJumpPgm(currDriver, eH, pgmSeq, "2", _stepsList["34"][0], 34);
                        cA.Delay(); // "Time for load jumped program"

                        eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                        _2_S34(i);
                        Global.ScenarioIndex = INDEX;
                    }
                }
            }

            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 33 End--------------------");
            if (!hasError)
            {
                cA.ClosePgm(currDriver, pgmSeq);
                eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                _2_S35();
                Global.ScenarioIndex = INDEX;
            }
        }

        private void _2_S34(int loopIndex)
        {
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 34 Started--------------------");
            var INDEX = "34";
            var intINDEX = 34;
            Global.ScenarioIndex = INDEX;
            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.StartTime);
            var stepData = _stepsList[INDEX];
            var pgmSeq = stepData[0];
            var workSheetType = stepData[1];

            var fileName = cA.GetFileName("_S34");
            var pathToFile = Path.Combine(cA.GetFilePath(), fileName);
            if (File.Exists(pathToFile) && loopIndex == 0)
            {
                File.Delete(pathToFile);
            }
            eH.SetJumpDataSetCount(currDriver, intINDEX, pgmSeq, workbookId, false, null, loopIndex + 1);

            if (IsInputModule(workSheetType))
            {
                var dataSetCountKeys = _inputDataList[INDEX].Keys;

                string iiStrVal = dataSetCountKeys.ElementAt(loopIndex);
                var workSheetsBundle = _inputDataList[INDEX][iiStrVal];
                MultiDictList<string, List<string>> workSheetsArr = null;
                Dictionary<string, List<List<string>>>.KeyCollection datasetKeys = null;
                List<List<string>> workSheet;
                for (var seq = 0; seq < workSheetsBundle.Count; seq++)
                {
                    workSheetsArr = workSheetsBundle[seq];
                    datasetKeys = workSheetsArr.Keys;

                    for (var i = 0; i < datasetKeys.Count; i++)
                    {
                        string dataForVal = datasetKeys.ElementAt(i);
                        workSheet = workSheetsArr[dataForVal];
                        if (dataForVal == "Control")
                        {
                            cA.ReadPgmControlDataList(currDriver, pgmSeq, workSheet);
                        }
                        else
                        {
                            cSA.ReadSheetDataList(currDriver, pgmSeq, workbookId, workSheet);
                        }

                    }
                    int countDirtyRows = cSA.CountDataRow(currDriver, pgmSeq, workbookId, RowDataType.DIRTY_ROWS);
                    cA.SavePgm(currDriver, eH, pgmSeq, null, intINDEX);
                    cA.Delay();
                    int countSavedRows = cSA.CountDataRow(currDriver, pgmSeq, workbookId, RowDataType.SAVED_ROWS);

                    cA.SetInputDataSetCount(currDriver, eH, cSA, intINDEX, pgmSeq, workbookId, countSavedRows, loopIndex + 1);

                    if (countDirtyRows == countSavedRows)
                    {
                        string copiedVal = cA.GetControlValue(currDriver, pgmSeq, "txtOSPDelvNo_txt");
                        string copiedValWithIdx = (loopIndex + 1) + Keys.TAB + copiedVal;
                        cA.WriteTempFile(fileName, copiedValWithIdx, true);
                        msgVal = "Scenario Index 34 - " + pgmSeq + " - 외주납품번호 : " + copiedVal;
                        cLR.SetLogRecord(intINDEX, msgVal);
                    }
                    else
                    {
                        msgVal = "Scenario Index 34 - " + pgmSeq + " - 외주납품번호 :  저장이 실패하였습니다.";
                        cLR.SetLogRecord(intINDEX, msgVal);
                        var isPopUpVisible = Validations.IsPopupVisible(currDriver, "messagePopup");
                        if (isPopUpVisible)
                        {
                            var messageText = cA.GetMessageDescription(currDriver);
                            cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);
                            msgVal = "Scenario Index 34 - " + pgmSeq + " - 경고메세지 : " + messageText;
                            cLR.SetLogRecord(intINDEX, msgVal);
                            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                            _2_S40();
                            Global.ScenarioIndex = INDEX;
                        }
                    }
                }

            }
            cA.ClosePgm(currDriver, pgmSeq);
            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);

            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 34 End--------------------");
        }

        private void _2_S35()
        {
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 35 Started--------------------");
            var INDEX = "35";
            var intINDEX = 35;
            Global.ScenarioIndex = INDEX;

            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.StartTime);
            var stepData = _stepsList[INDEX];
            var pgmSeq = stepData[0];
            var workSheetType = stepData[1];
            var fileName = cA.GetFileName("_S34");

            bool hasError = false;
            if (IsQueryModule(workSheetType))
            {
                var workSheets = _queryDataList[INDEX];

                var rowCount = 0;
                cA.OpenPgm(currDriver, eH, pgmSeq, intINDEX);
                string pgmSeqFrame = cA.GetPgmSeqFrame(pgmSeq);
                currDriver.SwitchTo().Frame(currDriver.FindElement(By.Id(pgmSeqFrame)));


                var lineCount = cA.GetTempFileLineCount(fileName);
                for (var i = 0; i < lineCount; i++)
                {
                    string oSPDelvNoVal = cA.ReadTempFile(fileName, i, 1);
                    cA.SetPgmControlData(currDriver, "txtOSPDelvNo_txt", oSPDelvNoVal);
                    cA.ReadPgmControlDataList(currDriver, pgmSeq, workSheets);
                    cA.QueryPgm(currDriver, eH, pgmSeq, intINDEX, workbookId2, true, 1);

                    rowCount = cSA.CountDataRow(currDriver, pgmSeq, workbookId2);
                    if (rowCount == 0)
                    {
                        msgVal = "Scenario Index 35 - " + pgmSeq + " - 조회된 외주납품건이 없습니다.";
                        cLR.SetLogRecord(intINDEX, msgVal);

                        eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                        _2_S40();
                        Global.ScenarioIndex = INDEX;
                        hasError = true;
                    }
                    else
                    {
                        cSA.CellClick(currDriver, pgmSeq, workbookId2, 1, "Sel");
                        cA.ClickJumpPgm(currDriver, eH, pgmSeq, "3", _stepsList["36"][0], 36);
                        cA.Delay();

                        eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                        _2_S36(i);
                        Global.ScenarioIndex = INDEX;
                    }
                }
            }

            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 35 End--------------------");
            if (!hasError)
            {
                cA.ClosePgm(currDriver, pgmSeq);
                eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                _2_S37();
                Global.ScenarioIndex = INDEX;
            }
        }

        private void _2_S36(int loopIndex)
        {
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 36 Started--------------------");
            var INDEX = "36";
            var intINDEX = 36;
            Global.ScenarioIndex = INDEX;
            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.StartTime);
            var stepData = _stepsList[INDEX];
            var pgmSeq = stepData[0];
            var workSheetType = stepData[1];
            var workbookId3 = "SS2_c";
            var fileName = cA.GetFileName("_S36");
            var pathToFile = Path.Combine(cA.GetFilePath(), fileName);
            if (File.Exists(pathToFile) && loopIndex == 0)
            {
                File.Delete(pathToFile);
            }
            eH.SetJumpDataSetCount(currDriver, intINDEX, pgmSeq, workbookId, false, null, loopIndex + 1);
            eH.SetJumpDataSetCount(currDriver, intINDEX, pgmSeq, workbookId3, false, null, loopIndex + 1);

            if (IsInputModule(workSheetType))
            {
                var dataSetCountKeys = _inputDataList[INDEX].Keys;

                string iiStrVal = dataSetCountKeys.ElementAt(loopIndex);
                var workSheetsBundle = _inputDataList[INDEX][iiStrVal];
                MultiDictList<string, List<string>> workSheetsArr = null;
                Dictionary<string, List<List<string>>>.KeyCollection datasetKeys = null;
                List<List<string>> workSheet;
                for (var seq = 0; seq < workSheetsBundle.Count; seq++)
                {
                    workSheetsArr = workSheetsBundle[seq];
                    datasetKeys = workSheetsArr.Keys;

                    for (var i = 0; i < datasetKeys.Count; i++)
                    {
                        string dataForVal = datasetKeys.ElementAt(i);
                        workSheet = workSheetsArr[dataForVal];
                        if (dataForVal == "Control")
                        {
                            cA.ReadPgmControlDataList(currDriver, pgmSeq, workSheet);
                        }
                        else
                        {
                            cSA.ReadSheetDataList(currDriver, pgmSeq, workbookId, workSheet);
                        }

                    }
                    int countDirtyRows = cSA.CountDataRow(currDriver, pgmSeq, workbookId, RowDataType.DIRTY_ROWS);
                    cA.SavePgm(currDriver, eH, pgmSeq, null, intINDEX);
                    cA.Delay();
                    int countSavedRows = cSA.CountDataRow(currDriver, pgmSeq, workbookId, RowDataType.SAVED_ROWS);

                    cA.SetInputDataSetCount(currDriver, eH, cSA, intINDEX, pgmSeq, workbookId, countSavedRows, loopIndex + 1);

                    if (countDirtyRows == countSavedRows)
                    {
                        cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);

                        string copiedVal = cA.GetControlValue(currDriver, pgmSeq, "fltOSPDelvInNo_txt");
                        string copiedValWithIdx = (loopIndex + 1) + Keys.TAB + copiedVal;
                        cA.WriteTempFile(fileName, copiedValWithIdx, true);
                        msgVal = "Scenario Index 36 - " + pgmSeq + " - 외주입고번호 : " + copiedVal;
                        cLR.SetLogRecord(intINDEX, msgVal);

                        cA.SelectPgmControl(currDriver, eH, pgmSeq, "btnMatQuery_btn");
                        cA.Delay();
                        currDriver.SwitchTo().Alert().Accept();
                        cA.Delay(); // "Waiting for another popup"
                        cA.SavePgm(currDriver, eH, pgmSeq, null, intINDEX);
                        cA.Delay();
                        cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);

                        countSavedRows = cSA.CountDataRow(currDriver, pgmSeq, workbookId3, RowDataType.SAVED_ROWS);

                        cA.SetInputDataSetCount(currDriver, eH, cSA, intINDEX, pgmSeq, workbookId3, countSavedRows, loopIndex + 1);
                    }
                    else
                    {
                        msgVal = "Scenario Index 36 - " + pgmSeq + " - 외주입고번호:  저장이 실패하였습니다.";
                        cLR.SetLogRecord(intINDEX, msgVal);
                        var isPopUpVisible = Validations.IsPopupVisible(currDriver, "messagePopup");
                        if (isPopUpVisible)
                        {
                            var messageText = cA.GetMessageDescription(currDriver);
                            cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);
                            msgVal = "Scenario Index 36 - " + pgmSeq + " - 경고메세지 : " + messageText;
                            cLR.SetLogRecord(intINDEX, msgVal);
                            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                            _2_S40();
                            Global.ScenarioIndex = INDEX;
                        }
                    }
                }

            }
            cA.ClosePgm(currDriver, pgmSeq);
            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);

            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 36 End--------------------");
        }

        private void _2_S37()
        {
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 37 Started--------------------");
            var INDEX = "37";
            var intINDEX = 37;
            Global.ScenarioIndex = INDEX;

            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.StartTime);
            var stepData = _stepsList[INDEX];
            var pgmSeq = stepData[0];
            var workSheetType = stepData[1];
            var fileName = cA.GetFileName("_S36");

            bool hasError = false;
            if (IsQueryModule(workSheetType))
            {
                var workSheets = _queryDataList[INDEX];

                var rowCount = 0;
                cA.OpenPgm(currDriver, eH, pgmSeq, intINDEX);
                string pgmSeqFrame = cA.GetPgmSeqFrame(pgmSeq);
                currDriver.SwitchTo().Frame(currDriver.FindElement(By.Id(pgmSeqFrame)));


                var lineCount = cA.GetTempFileLineCount(fileName);
                for (var i = 0; i < lineCount; i++)
                {
                    string oSPDelvNoVal = cA.ReadTempFile(fileName, i, 1);
                    cA.SetPgmControlData(currDriver, "txtOSPDelvNo_txt", oSPDelvNoVal);
                    cA.ReadPgmControlDataList(currDriver, pgmSeq, workSheets);
                    cA.QueryPgm(currDriver, eH, pgmSeq, intINDEX, workbookId, true, 1);

                    rowCount = cSA.CountDataRow(currDriver, pgmSeq, workbookId);
                    if (rowCount == 0)
                    {
                        msgVal = "Scenario Index 37 - " + pgmSeq + " - 조회된 외주납품건이 없습니다.";
                        cLR.SetLogRecord(intINDEX, msgVal);

                        eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                        _2_S40();
                        Global.ScenarioIndex = INDEX;
                        hasError = true;
                    }
                }
            }

            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 37 End--------------------");
            if (!hasError)
            {
                cA.ClosePgm(currDriver, pgmSeq);
                eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                _2_S38();
                Global.ScenarioIndex = INDEX;
            }
        }

        private void _2_S38()
        {
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 38 Started--------------------");
            var INDEX = "38";
            var intINDEX = 38;
            Global.ScenarioIndex = INDEX;

            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.StartTime);
            var stepData = _stepsList[INDEX];
            var pgmSeq = stepData[0];
            var workSheetType = stepData[1];
            var fileName = cA.GetFileName("_S36");

            bool hasError = false;
            if (IsQueryModule(workSheetType))
            {
                var workSheets = _queryDataList[INDEX];

                var lineCount = cA.GetTempFileLineCount(fileName);
                var rowCount = 0;
                cA.OpenPgm(currDriver, eH, pgmSeq, intINDEX);
                string pgmSeqFrame = cA.GetPgmSeqFrame(pgmSeq);
                currDriver.SwitchTo().Frame(currDriver.FindElement(By.Id(pgmSeqFrame)));

                for (var i = 0; i < lineCount; i++)
                {
                    string delvInNoVal = cA.ReadTempFile(fileName, i, 1);
                    cA.SetPgmControlData(currDriver, "txtDelvInNo_txt", delvInNoVal);
                    cA.ReadPgmControlDataList(currDriver, pgmSeq, workSheets);
                    cA.QueryPgm(currDriver, eH, pgmSeq, intINDEX, workbookId, false, 1);

                    rowCount = cSA.CountDataRow(currDriver, pgmSeq, workbookId);
                    if (rowCount == 0)
                    {
                        msgVal = "Scenario Index 38 - " + pgmSeq + " - 조회된 외주납품건이 없습니다.";
                        cLR.SetLogRecord(intINDEX, msgVal);

                        eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                        _2_S40();
                        Global.ScenarioIndex = INDEX;
                        hasError = true;
                    }
                    else
                    {
                        cA.SelectPgmControl(currDriver, eH, pgmSeq, "btnAllCheck_btn");
                        cA.Delay();
                        cA.SelectPgmControl(currDriver, eH, pgmSeq, "btnSlip_btn");
                        cA.Delay();

                        eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                        _2_S39(i);
                        Global.ScenarioIndex = INDEX;
                    }
                }
            }

            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 38 End--------------------");
            if (!hasError)
            {
                cA.ClosePgm(currDriver, pgmSeq);
                eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                _2_S40();
                Global.ScenarioIndex = INDEX;
            }
        }

        private void _2_S39(int loopIndex)
        {
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 39 Started--------------------");
            var INDEX = "39";
            var intINDEX = 39;
            Global.ScenarioIndex = INDEX;
            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.StartTime);
            var stepData = _stepsList[INDEX];
            var pgmSeq = stepData[0];
            var workSheetType = stepData[1];
            var workbookId3 = "SS_Row_c";
            var fileName = cA.GetFileName("_S39");
            var pathToFile = Path.Combine(cA.GetFilePath(), fileName);
            if (File.Exists(pathToFile) && loopIndex == 0)
            {
                File.Delete(pathToFile);
            }
            eH.SetJumpDataSetCount(currDriver, intINDEX, pgmSeq, workbookId3, false, "AccName", loopIndex + 1);

            if (IsInputModule(workSheetType))
            {
                var dataSetCountKeys = _inputDataList[INDEX].Keys;

                string iiStrVal = dataSetCountKeys.ElementAt(loopIndex);
                var workSheetsBundle = _inputDataList[INDEX][iiStrVal];
                MultiDictList<string, List<string>> workSheetsArr = null;
                Dictionary<string, List<List<string>>>.KeyCollection datasetKeys = null;
                List<List<string>> workSheet;
                for (var seq = 0; seq < workSheetsBundle.Count; seq++)
                {
                    workSheetsArr = workSheetsBundle[seq];
                    datasetKeys = workSheetsArr.Keys;

                    for (var i = 0; i < datasetKeys.Count; i++)
                    {
                        string dataForVal = datasetKeys.ElementAt(i);
                        workSheet = workSheetsArr[dataForVal];
                        if (dataForVal != "Control")
                        {
                            cSA.ReadSheetDataList(currDriver, pgmSeq, workbookId3, workSheet);
                        }

                    }
                    cA.SavePgm(currDriver, eH, pgmSeq, null, intINDEX);
                    cA.Delay();
                    int countSavedRows = cSA.CountDataRow(currDriver, pgmSeq, workbookId3, RowDataType.SAVED_ROWS);

                    cA.SetInputDataSetCount(currDriver, eH, cSA, intINDEX, pgmSeq, workbookId3, countSavedRows, loopIndex + 1);
                    string copiedVal = cA.GetControlValue(currDriver, pgmSeq, "txtSlipMstID_txt");
                    if (!string.IsNullOrEmpty(copiedVal))
                    {
                        cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);

                        string copiedValWithIdx = (loopIndex + 1) + Keys.TAB + copiedVal;
                        cA.WriteTempFile(fileName, copiedValWithIdx, true);

                        cA.SelectSpanControl(currDriver, null, "devDlgArea .divTitleArea", ".btnClose");
                        msgVal = "Scenario Index 39 - " + pgmSeq + " - 전표번호 : " + copiedVal;
                        cLR.SetLogRecord(intINDEX, msgVal);
                    }
                    else
                    {
                        msgVal = "Scenario Index 39 - " + pgmSeq + " - 전표번호:  전표처리가 실패하였습니다.";
                        cLR.SetLogRecord(intINDEX, msgVal);
                        var isPopUpVisible = Validations.IsPopupVisible(currDriver, "messagePopup");
                        if (isPopUpVisible)
                        {
                            var messageText = cA.GetMessageDescription(currDriver);
                            cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);
                            msgVal = "Scenario Index 39 - " + pgmSeq + " - 경고메세지 : " + messageText;
                            cLR.SetLogRecord(intINDEX, msgVal);
                        }
                    }
                }

            }
            cA.ClosePgm(currDriver, pgmSeq);
            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);

            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 39 End--------------------");
        }

        private void _2_S40()
        {
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 40 Started--------------------");
            var INDEX = "40";
            var intINDEX = 40;
            Global.ScenarioIndex = INDEX;

            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.StartTime);
            var stepData = _stepsList[INDEX];
            var pgmSeq = stepData[0];
            var workSheetType = stepData[1];
            var workbookId3 = "SSWorkOrder_c";
            var fileName = cA.GetFileName("_S1");

            bool hasError = false;
            if (IsQueryModule(workSheetType))
            {
                var workSheets = _queryDataList[INDEX];


                var rowCount = 0;
                cA.OpenPgm(currDriver, eH, pgmSeq, intINDEX);
                string pgmSeqFrame = cA.GetPgmSeqFrame(pgmSeq);
                currDriver.SwitchTo().Frame(currDriver.FindElement(By.Id(pgmSeqFrame)));

                var lineCount = cA.GetTempFileLineCount(fileName);
                for (var i = 0; i < lineCount; i++)
                {
                    string orderNoVal = cA.ReadTempFile(fileName, i, 1);
                    cA.ClickExpander(currDriver, pgmSeq, "ExpanderC108");
                    cA.SetPgmControlData(currDriver, "txtOrderNo_txt", orderNoVal);
                    cA.ReadPgmControlDataList(currDriver, pgmSeq, workSheets);
                    cA.QueryPgm(currDriver, eH, pgmSeq, intINDEX, workbookId3, false, i + 1);

                    rowCount = cSA.CountDataRow(currDriver, pgmSeq, workbookId3);
                    if (rowCount == 0)
                    {
                        msgVal = "Scenario Index 40 - " + pgmSeq + " - 수주번호 " + orderNoVal + " 의 작업지시건이 없습니다.";
                        cLR.SetLogRecord(intINDEX, msgVal);

                        eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                        _2_S46();
                        Global.ScenarioIndex = INDEX;
                        hasError = true;
                    }
                    else
                    {
                        cSA.CellClick(currDriver, pgmSeq, workbookId3, 0, "Sel", false, true);
                        cSA.SelectSheetContextMenu(currDriver, pgmSeq, workbookId3, "9");
                        cA.ClickJumpPgm(currDriver, eH, pgmSeq, "2", _stepsList["41"][0], 41);
                        cA.Delay(); // "Time for load jumped progrcam"

                        eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                        _2_S41(i);
                        Global.ScenarioIndex = INDEX;
                    }
                }
            }

            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 40 End--------------------");
            if (!hasError)
            {
                cA.ClosePgm(currDriver, pgmSeq);
                eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                _2_S44();
                Global.ScenarioIndex = INDEX;
            }
        }

        private void _2_S41(int loopIndex)
        {
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 41 Started--------------------");
            var INDEX = "41";
            var intINDEX = 41;
            Global.ScenarioIndex = INDEX;
            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.StartTime);
            var stepData = _stepsList[INDEX];
            var pgmSeq = stepData[0];
            var workSheetType = stepData[1];
            var workbookId3 = "SSAssyItem_c";
            var workbookId4 = "SSMatItem_c";
            var fileName = cA.GetFileName("_S41");
            var pathToFile = Path.Combine(cA.GetFilePath(), fileName);
            if (File.Exists(pathToFile) && loopIndex == 0)
            {
                File.Delete(pathToFile);
            }
            eH.SetJumpDataSetCount(currDriver, intINDEX, pgmSeq, workbookId3, false, null, loopIndex + 1);
            eH.SetJumpDataSetCount(currDriver, intINDEX, pgmSeq, workbookId4, false, null, loopIndex + 1);

            if (IsInputModule(workSheetType))
            {
                var dataSetCountKeys = _inputDataList[INDEX].Keys;

                string iiStrVal = dataSetCountKeys.ElementAt(loopIndex);
                var workSheetsBundle = _inputDataList[INDEX][iiStrVal];
                MultiDictList<string, List<string>> workSheetsArr = null;
                Dictionary<string, List<List<string>>>.KeyCollection datasetKeys = null;
                List<List<string>> workSheet;
                for (var seq = 0; seq < workSheetsBundle.Count; seq++)
                {
                    workSheetsArr = workSheetsBundle[seq];
                    datasetKeys = workSheetsArr.Keys;

                    for (var i = 0; i < datasetKeys.Count; i++)
                    {
                        string dataForVal = datasetKeys.ElementAt(i);
                        workSheet = workSheetsArr[dataForVal];
                        if (dataForVal != "Control")
                        {
                            cSA.ReadSheetDataList(currDriver, pgmSeq, workbookId3, workSheet);
                        }

                    }
                    int countDirtyRows = cSA.CountDataRow(currDriver, pgmSeq, workbookId3, RowDataType.DIRTY_ROWS);
                    cA.SavePgm(currDriver, eH, pgmSeq, null, intINDEX);
                    cA.Delay();
                    int countSavedRows = cSA.CountDataRow(currDriver, pgmSeq, workbookId3, RowDataType.SAVED_ROWS);
                    cA.SetInputDataSetCount(currDriver, eH, cSA, intINDEX, pgmSeq, workbookId3, countSavedRows, loopIndex + 1);

                    int countSavedRows2 = cSA.CountDataRow(currDriver, pgmSeq, workbookId4, RowDataType.SAVED_ROWS);
                    cA.SetInputDataSetCount(currDriver, eH, cSA, intINDEX, pgmSeq, workbookId4, countSavedRows2, loopIndex + 1);

                    if (countDirtyRows == countSavedRows)
                    {
                        string copiedVal = cA.GetControlValue(currDriver, pgmSeq, "txtOutReqNo_txt");
                        cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);

                        string copiedValWithIdx = (loopIndex + 1) + Keys.TAB + copiedVal;
                        cA.WriteTempFile(fileName, copiedValWithIdx, true);

                        cA.SelectSpanControl(currDriver, null, "devDlgArea .divTitleArea", ".btnClose");
                        msgVal = "Scenario Index 41 - " + pgmSeq + " - 자재출고요청번호 : " + copiedVal;
                        cLR.SetLogRecord(intINDEX, msgVal);

                        eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                        _2_S42(loopIndex);
                        Global.ScenarioIndex = INDEX;

                    }
                    else
                    {
                        msgVal = "Scenario Index 41 - " + pgmSeq + " - 자재출고요청번호 : 저장이 실패하였습니다.";
                        cLR.SetLogRecord(intINDEX, msgVal);
                        var isPopUpVisible = Validations.IsPopupVisible(currDriver, "messagePopup");
                        if (isPopUpVisible)
                        {
                            var messageText = cA.GetMessageDescription(currDriver);
                            cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);
                            msgVal = "Scenario Index 41 - " + pgmSeq + " - 경고메세지 : " + messageText;
                            cLR.SetLogRecord(intINDEX, msgVal);

                            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                            _2_S46();
                            Global.ScenarioIndex = INDEX;
                        }
                    }
                }

            }
            cA.ClosePgm(currDriver, pgmSeq);
            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);

            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 41 End--------------------");
        }

        private void _2_S42(int loopIndex)
        {
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 42 Started--------------------");
            var INDEX = "42";
            var intINDEX = 42;
            Global.ScenarioIndex = INDEX;
            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.StartTime);
            var stepData = _stepsList[INDEX];
            var pgmSeq = stepData[0];
            var workSheetType = stepData[1];
            var fileName = cA.GetFileName("_S41");


            if (IsQueryModule(workSheetType))
            {
                var workSheets = _queryDataList[INDEX];


                var rowCount = 0;
                cA.OpenPgm(currDriver, eH, pgmSeq, intINDEX);
                string pgmSeqFrame = cA.GetPgmSeqFrame(pgmSeq);
                currDriver.SwitchTo().Frame(currDriver.FindElement(By.Id(pgmSeqFrame)));
                var outReqNoVal = cA.ReadTempFile(fileName, loopIndex, 1);
                cA.SetPgmControlData(currDriver, "txtOutReqNo_txt", outReqNoVal);

                cA.ReadPgmControlDataList(currDriver, pgmSeq, workSheets);
                cA.QueryPgm(currDriver, eH, pgmSeq, intINDEX, workbookId, false, loopIndex + 1);

                rowCount = cSA.CountDataRow(currDriver, pgmSeq, workbookId);
                if (rowCount == 0)
                {
                    msgVal = "Scenario Index 42 - " + pgmSeq + " - " + outReqNoVal + " 의 자재출고요청품목이 없습니다.";
                    cLR.SetLogRecord(intINDEX, msgVal);

                    eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                    _2_S46();
                    Global.ScenarioIndex = INDEX;
                }
                else
                {
                    cSA.CellClick(currDriver, pgmSeq, workbookId, 1, "Select", false, true);
                    cSA.SelectSheetContextMenu(currDriver, pgmSeq, workbookId, "9");
                    cA.ClickJumpPgm(currDriver, eH, pgmSeq, "4", _stepsList["43"][0], 43);
                    cA.Delay();

                    eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                    _2_S43(loopIndex);
                    Global.ScenarioIndex = INDEX;
                }
            }

            cA.ClosePgm(currDriver, pgmSeq);
            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);

            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 42 End--------------------");
        }

        private void _2_S43(int loopIndex)
        {
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 43 Started--------------------");
            var INDEX = "43";
            var intINDEX = 43;
            Global.ScenarioIndex = INDEX;
            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.StartTime);
            var stepData = _stepsList[INDEX];
            var pgmSeq = stepData[0];
            var workSheetType = stepData[1];
            var workbookId3 = "SSMatOutGood_c";
            var workbookId4 = "SSMatOutItem_c";
            var fileName = cA.GetFileName("_S43");
            var pathToFile = Path.Combine(cA.GetFilePath(), fileName);
            if (File.Exists(pathToFile) && loopIndex == 0)
            {
                File.Delete(pathToFile);
            }
            eH.SetJumpDataSetCount(currDriver, intINDEX, pgmSeq, workbookId3, false, null, loopIndex + 1);
            eH.SetJumpDataSetCount(currDriver, intINDEX, pgmSeq, workbookId4, false, null, loopIndex + 1);

            if (IsInputModule(workSheetType))
            {
                var dataSetCountKeys = _inputDataList[INDEX].Keys;

                string iiStrVal = dataSetCountKeys.ElementAt(loopIndex);
                var workSheetsBundle = _inputDataList[INDEX][iiStrVal];
                MultiDictList<string, List<string>> workSheetsArr = null;
                Dictionary<string, List<List<string>>>.KeyCollection datasetKeys = null;
                List<List<string>> workSheet;
                for (var seq = 0; seq < workSheetsBundle.Count; seq++)
                {
                    workSheetsArr = workSheetsBundle[seq];
                    datasetKeys = workSheetsArr.Keys;

                    for (var i = 0; i < datasetKeys.Count; i++)
                    {
                        string dataForVal = datasetKeys.ElementAt(i);
                        workSheet = workSheetsArr[dataForVal];
                        if (dataForVal != "Control")
                        {
                            cSA.ReadSheetDataList(currDriver, pgmSeq, workbookId3, workSheet);
                        }

                    }
                    int countDirtyRows = cSA.CountDataRow(currDriver, pgmSeq, workbookId3, RowDataType.DIRTY_ROWS);
                    cA.SavePgm(currDriver, eH, pgmSeq, null, intINDEX);
                    cA.Delay();
                    int countSavedRows = cSA.CountDataRow(currDriver, pgmSeq, workbookId3, RowDataType.SAVED_ROWS);
                    cA.SetInputDataSetCount(currDriver, eH, cSA, intINDEX, pgmSeq, workbookId3, countSavedRows, loopIndex + 1);

                    int countSavedRows2 = cSA.CountDataRow(currDriver, pgmSeq, workbookId4, RowDataType.SAVED_ROWS);
                    cA.SetInputDataSetCount(currDriver, eH, cSA, intINDEX, pgmSeq, workbookId4, countSavedRows2, loopIndex + 1);

                    if (countDirtyRows == countSavedRows)
                    {
                        string copiedVal = cA.GetControlValue(currDriver, pgmSeq, "txtMatOutNo_txt");
                        cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);

                        string copiedValWithIdx = (loopIndex + 1) + Keys.TAB + copiedVal;
                        cA.WriteTempFile(fileName, copiedValWithIdx, true);

                        msgVal = "Scenario Index 43 - " + pgmSeq + " - 출고번호 : " + copiedVal;
                        cLR.SetLogRecord(intINDEX, msgVal);
                    }
                    else
                    {
                        msgVal = "Scenario Index 43 - " + pgmSeq + " - 출고번호 : 저장이 실패하였습니다.";
                        cLR.SetLogRecord(intINDEX, msgVal);
                        var isPopUpVisible = Validations.IsPopupVisible(currDriver, "messagePopup");
                        if (isPopUpVisible)
                        {
                            var messageText = cA.GetMessageDescription(currDriver);
                            msgVal = "Scenario Index 43 - " + pgmSeq + " - 경고메세지 : " + messageText;
                            cLR.SetLogRecord(intINDEX, msgVal);

                            //Log.Error("TestRun Stopped.");
                            //Runner.Stop();
                            Environment.Exit(0);
                            Global.ScenarioIndex = INDEX;
                        }
                        else
                        {
                            var isSheetErrMessageVisible = Validations.IsPopupVisible(currDriver, "divSheetErrMessageArea");
                            if (isSheetErrMessageVisible)
                            {
                                var messageText = cA.GetSheetErrMessageDescription(currDriver);

                                msgVal = "Scenario Index 43 - " + pgmSeq + " - 경고메세지 : " + messageText;
                                cLR.SetLogRecord(intINDEX, msgVal);
                                //Log.Error("TestRun Stopped.");
                                //Runner.Stop();
                                Environment.Exit(0);
                                Global.ScenarioIndex = INDEX;
                            }
                        }

                    }
                }

            }
            cA.ClosePgm(currDriver, pgmSeq);
            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);

            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 43 End--------------------");
        }

        private void _2_S44()
        {
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 44 Started--------------------");
            var INDEX = "44";
            var intINDEX = 44;
            Global.ScenarioIndex = INDEX;

            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.StartTime);
            var stepData = _stepsList[INDEX];
            var pgmSeq = stepData[0];
            var workSheetType = stepData[1];
            var workbookId3 = "SSWorkOrder_c";
            var fileName = cA.GetFileName("_S1");

            bool hasError = false;
            if (IsQueryModule(workSheetType))
            {
                var workSheets = _queryDataList[INDEX];

                var rowCount = 0;
                cA.OpenPgm(currDriver, eH, pgmSeq, intINDEX);
                string pgmSeqFrame = cA.GetPgmSeqFrame(pgmSeq);
                currDriver.SwitchTo().Frame(currDriver.FindElement(By.Id(pgmSeqFrame)));
                cA.ClickExpander(currDriver, pgmSeq, "ExpanderC108");

                var lineCount = cA.GetTempFileLineCount(fileName);
                for (var i = 0; i < lineCount; i++)
                {
                    string orderNoVal = cA.ReadTempFile(fileName, i, 1);

                    cA.SetPgmControlData(currDriver, "txtOrderNo_txt", orderNoVal);
                    cA.ReadPgmControlDataList(currDriver, pgmSeq, workSheets);
                    cA.QueryPgm(currDriver, eH, pgmSeq, intINDEX, workbookId3, false, 1);

                    rowCount = cSA.CountDataRow(currDriver, pgmSeq, workbookId3);
                    for (var j = 0; j < rowCount; j++)
                    {
                        cA.BringPgmFront(currDriver, pgmSeq);

                        cSA.CellClick(currDriver, pgmSeq, workbookId3, 0, "Sel", false, true);
                        cSA.SelectSheetContextMenu(currDriver, pgmSeq, workbookId3, "10");
                        cA.Delay();

                        cSA.CellInput(currDriver, pgmSeq, workbookId3, j, "Sel", "true");
                        cA.ClickJumpPgm(currDriver, eH, pgmSeq, "1", _stepsList["45"][0], 45);
                        cA.Delay(); // "Time for load jumped progrcam"

                        var isPopUpVisible = Validations.IsPopupVisible(currDriver, "messagePopup");

                        if (isPopUpVisible)
                        {
                            var messageText = cA.GetMessageDescription(currDriver);
                            cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);

                            msgVal = "Scenario Index 44 - " + pgmSeq + " - 경고메세지 : " + messageText;
                            cLR.SetLogRecord(intINDEX, msgVal);
                            hasError = true;
                        }
                        else
                        {
                            cA.Delay();
                            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                            _2_S45(j, i);
                            Global.ScenarioIndex = INDEX;
                        }
                    }
                }
            }

            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 44 End--------------------");
            if (!hasError)
            {
                cA.ClosePgm(currDriver, pgmSeq);
                eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                _2_S46();
                Global.ScenarioIndex = INDEX;
            }
        }

        private void _2_S45(int loopIndex, int parentLoopIndex)
        {
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 45 Started--------------------");
            var INDEX = "45";
            var intINDEX = 45;
            Global.ScenarioIndex = INDEX;
            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Execute);
            var stepData = _stepsList[INDEX];
            var pgmSeq = stepData[0];
            var workSheetType = stepData[1];

            var workbookId3 = "SSWorkReport_c";
            var workbookId4 = "SSMat_c";

            var fileName = cA.GetFileName("_S16");
            var pathToFile = Path.Combine(cA.GetFilePath(), fileName);
            if (File.Exists(pathToFile) && loopIndex == 0 && parentLoopIndex == 0)
            {
                File.Delete(pathToFile);
            }
            eH.SetJumpDataSetCount(currDriver, intINDEX, pgmSeq, workbookId3, false, null, loopIndex + 1);
            eH.SetJumpDataSetCount(currDriver, intINDEX, pgmSeq, workbookId4, false, null, loopIndex + 1);

            cA.OpenPgm(currDriver, eH, pgmSeq, intINDEX);
            string pgmSeqFrame = cA.GetPgmSeqFrame(pgmSeq);
            currDriver.SwitchTo().Frame(currDriver.FindElement(By.Id(pgmSeqFrame)));


            int countDirtyRows = cSA.GetRowIndex(currDriver, pgmSeq, workbookId3, "WorkOrderNo", "");
            cA.SavePgm(currDriver, eH, pgmSeq, null, intINDEX);
            cA.Delay();
            int countSavedRows = cSA.CountDataRow(currDriver, pgmSeq, workbookId3, RowDataType.SAVED_ROWS);

            cA.SetInputDataSetCount(currDriver, eH, cSA, intINDEX, pgmSeq, workbookId3, countSavedRows, loopIndex + 1);
            var goodItemNameVal = cSA.GetCellValue(currDriver, workbookId3, 0, "GoodItemName");
            var realLotNoVal = cSA.GetCellValue(currDriver, workbookId3, 0, "RealLotNo");
            if (countDirtyRows == countSavedRows)
            {


                var isPopUpVisible = false;
                var messageTitle = "";
                var messageText = "";
                var copiedValWithIdx = "";
                for (var i = 0; i < countSavedRows; i++)
                {

                    copiedValWithIdx = (parentLoopIndex + 1) + Keys.TAB + (loopIndex + 1) + Keys.TAB + i + Keys.TAB + goodItemNameVal + Keys.TAB + realLotNoVal;
                    cA.WriteTempFile(fileName, copiedValWithIdx, true);

                    cSA.RowSelection(currDriver, pgmSeq, workbookId3, 0, 0, false, false, true);
                    cA.Delay();
                    cA.SelectPgmControl(currDriver, eH, pgmSeq, "btnQryMat_btn");

                    isPopUpVisible = Validations.IsPopupVisible(currDriver, "messagePopup");
                    if (isPopUpVisible)
                    {
                        messageTitle = cA.GetMessageTitle(currDriver, "messageBoxInfo");
                        messageText = cA.GetMessageDescription(currDriver);
                        cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);
                        msgVal = "Scenario Index 45 - " + pgmSeq + " - 메세지 : " + messageTitle + ", " + messageText;
                        cLR.SetLogRecord(intINDEX, msgVal);
                        cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);
                    }

                    cA.SelectPgmControl(currDriver, eH, pgmSeq, "btnMatInput_btn");
                    cA.Delay();

                    countSavedRows = cSA.CountDataRow(currDriver, pgmSeq, workbookId4, RowDataType.SAVED_ROWS);

                    cA.SetInputDataSetCount(currDriver, eH, cSA, intINDEX, pgmSeq, workbookId4, countSavedRows, loopIndex + 1);

                    isPopUpVisible = Validations.IsPopupVisible(currDriver, "messagePopup");
                    if (isPopUpVisible)
                    {
                        messageTitle = cA.GetMessageTitle(currDriver, "messageBoxInfo");
                        messageText = cA.GetMessageDescription(currDriver);
                        cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);
                        msgVal = "Scenario Index 45 - " + pgmSeq + " - 메세지 : " + messageTitle + ", " + messageText;
                        cLR.SetLogRecord(intINDEX, msgVal);
                        cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);
                    }
                }
            }
            else
            {
                msgVal = "Scenario Index 45 - " + pgmSeq + " - 제품명 : " + goodItemNameVal + ", LotNo : " + realLotNoVal + " 생성이 실패하였습니다";
                cLR.SetLogRecord(intINDEX, msgVal);
                var isPopUpVisible = Validations.IsPopupVisible(currDriver, "messagePopup");
                if (isPopUpVisible)
                {
                    var messageText = cA.GetMessageDescription(currDriver);
                    cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);
                    msgVal = "Scenario Index 45 - " + pgmSeq + " - 메세지 : " + messageText;
                    cLR.SetLogRecord(intINDEX, msgVal);
                }
            }


            cA.ClosePgm(currDriver, pgmSeq);
            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);

            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 45 End--------------------");
        }

        private void _2_S46()
        {
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 46 Started--------------------");
            var INDEX = "46";
            var intINDEX = 46;
            Global.ScenarioIndex = INDEX;

            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.StartTime);
            var stepData = _stepsList[INDEX];
            var pgmSeq = stepData[0];
            var workSheetType = stepData[1];
            var fileName = cA.GetFileName("_S1");

            bool hasError = false;
            if (IsQueryModule(workSheetType))
            {
                var workSheets = _queryDataList[INDEX];


                var rowCount = 0;
                string orderNoVal = "";
                cA.OpenPgm(currDriver, eH, pgmSeq, intINDEX);
                string pgmSeqFrame = cA.GetPgmSeqFrame(pgmSeq);
                currDriver.SwitchTo().Frame(currDriver.FindElement(By.Id(pgmSeqFrame)));

                var lineCount = cA.GetTempFileLineCount(fileName);
                for (var i = 0; i < lineCount; i++)
                {
                    orderNoVal = cA.ReadTempFile(fileName, i, 1);
                    cA.SetPgmControlData(currDriver, "txtOrderNo_txt", orderNoVal);
                    cA.ReadPgmControlDataList(currDriver, pgmSeq, workSheets);
                    cA.QueryPgm(currDriver, eH, pgmSeq, intINDEX, workbookId, false, 1);

                    rowCount = cSA.CountDataRow(currDriver, pgmSeq, workbookId);
                    if (rowCount == 0)
                    {
                        msgVal = "Scenario Index 46 - " + pgmSeq + " - " + orderNoVal + " 의 수주건이 없습니다.";
                        hasError = true;
                    }
                    else
                    {
                        cSA.CellClick(currDriver, pgmSeq, workbookId, 1, "Sel");
                        cA.ClickJumpPgm(currDriver, eH, pgmSeq, "1", _stepsList["47"][0], 47);
                        cA.Delay(); // "Time for load jumped progrcam"

                        eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                        _2_S47(i);
                        Global.ScenarioIndex = INDEX;
                    }
                }
            }

            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 46 End--------------------");
            if (!hasError)
            {
                cA.ClosePgm(currDriver, pgmSeq);
                eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                _2_S48();
                Global.ScenarioIndex = INDEX;
            }
        }

        private void _2_S47(int loopIndex)
        {
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 47 Started--------------------");
            var INDEX = "47";
            var intINDEX = 47;
            Global.ScenarioIndex = INDEX;
            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.StartTime);
            var stepData = _stepsList[INDEX];
            var pgmSeq = stepData[0];
            var workSheetType = stepData[1];
            var fileName = cA.GetFileName("_S47");
            var pathToFile = Path.Combine(cA.GetFilePath(), fileName);
            if (File.Exists(pathToFile) && loopIndex == 0)
            {
                File.Delete(pathToFile);
            }
            eH.SetJumpDataSetCount(currDriver, intINDEX, pgmSeq, workbookId, false, "ItemName", loopIndex + 1);

            if (IsInputModule(workSheetType))
            {
                var dataSetCountKeys = _inputDataList[INDEX].Keys;

                string iiStrVal = dataSetCountKeys.ElementAt(loopIndex);
                var workSheetsBundle = _inputDataList[INDEX][iiStrVal];
                MultiDictList<string, List<string>> workSheetsArr = null;
                Dictionary<string, List<List<string>>>.KeyCollection datasetKeys = null;
                List<List<string>> workSheet;
                for (var seq = 0; seq < workSheetsBundle.Count; seq++)
                {
                    workSheetsArr = workSheetsBundle[seq];
                    datasetKeys = workSheetsArr.Keys;

                    for (var i = 0; i < datasetKeys.Count; i++)
                    {
                        string dataForVal = datasetKeys.ElementAt(i);
                        workSheet = workSheetsArr[dataForVal];
                        if (dataForVal == "Control")
                        {
                            cA.ReadPgmControlDataList(currDriver, pgmSeq, workSheet);
                        }
                        else
                        {
                            cSA.ReadSheetDataList(currDriver, pgmSeq, workbookId, workSheet);
                        }

                    }

                    int countDirtyRows = cSA.GetRowIndex(currDriver, workbookId, "ItemName", "");
                    cA.SavePgm(currDriver, eH, pgmSeq, null, intINDEX);
                    cA.Delay();
                    int countSavedRows = cSA.CountDataRow(currDriver, pgmSeq, workbookId, RowDataType.SAVED_ROWS);

                    cA.SetInputDataSetCount(currDriver, eH, cSA, intINDEX, pgmSeq, workbookId, countSavedRows, loopIndex + 1);

                    if (countDirtyRows == countSavedRows)
                    {

                        string copiedVal = cA.GetControlValue(currDriver, pgmSeq, "txtDVReqNo_txt");
                        string copiedValWithIdx = (loopIndex + 1) + Keys.TAB + copiedVal;
                        cA.WriteTempFile(fileName, copiedValWithIdx, true);

                        msgVal = "Scenario Index 47 - " + pgmSeq + " - 출하의뢰번호 : " + copiedVal;
                        cLR.SetLogRecord(intINDEX, msgVal);
                    }
                    else
                    {
                        msgVal = "Scenario Index 47 - " + pgmSeq + " - 출하의뢰번호 : 저장이 실패하였습니다.";
                        cLR.SetLogRecord(intINDEX, msgVal);
                        var isPopUpVisible = Validations.IsPopupVisible(currDriver, "messagePopup");
                        if (isPopUpVisible)
                        {
                            var messageText = cA.GetMessageDescription(currDriver);
                            cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);
                            msgVal = "Scenario Index 47 - " + pgmSeq + " - 경고메세지 : " + messageText;
                            cLR.SetLogRecord(intINDEX, msgVal);
                        }
                    }
                }

            }
            cA.ClosePgm(currDriver, pgmSeq);
            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);

            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 47 End--------------------");
        }

        private void _2_S48()
        {
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 48 Started--------------------");
            var INDEX = "48";
            var intINDEX = 48;
            Global.ScenarioIndex = INDEX;

            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.StartTime);
            var stepData = _stepsList[INDEX];
            var pgmSeq = stepData[0];
            var workSheetType = stepData[1];
            var fileName = cA.GetFileName("_S47");

            bool hasError = false;
            if (IsQueryModule(workSheetType))
            {
                var workSheets = _queryDataList[INDEX];


                var rowCount = 0;
                string dVReqNoVal = "";
                cA.OpenPgm(currDriver, eH, pgmSeq, intINDEX);
                string pgmSeqFrame = cA.GetPgmSeqFrame(pgmSeq);
                currDriver.SwitchTo().Frame(currDriver.FindElement(By.Id(pgmSeqFrame)));

                var lineCount = cA.GetTempFileLineCount(fileName);
                for (var i = 0; i < lineCount; i++)
                {
                    dVReqNoVal = cA.ReadTempFile(fileName, i, 1);
                    cA.SetPgmControlData(currDriver, "txtDVReqNo_txt", dVReqNoVal);
                    cA.ReadPgmControlDataList(currDriver, pgmSeq, workSheets);
                    cA.QueryPgm(currDriver, eH, pgmSeq, intINDEX, workbookId, true, 1);

                    rowCount = cSA.CountDataRow(currDriver, pgmSeq, workbookId);
                    if (rowCount == 0)
                    {
                        msgVal = "Scenario Index 48 - " + pgmSeq + " - " + dVReqNoVal + " 의 출하의뢰건이 없습니다.";
                        hasError = true;
                    }
                    else
                    {
                        cSA.CellClick(currDriver, pgmSeq, workbookId, 1, "Sel");
                        cA.ClickJumpPgm(currDriver, eH, pgmSeq, "1", _stepsList["49"][0], 49);
                        cA.Delay();

                        eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                        _2_S49(i);
                        Global.ScenarioIndex = INDEX;
                    }
                }
            }

            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 48 End--------------------");
            if (!hasError)
            {
                cA.ClosePgm(currDriver, pgmSeq);
                eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                _2_S50();
                Global.ScenarioIndex = INDEX;
            }
        }

        private void _2_S49(int loopIndex)
        {
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 49 Started--------------------");
            var INDEX = "49";
            var intINDEX = 49;
            Global.ScenarioIndex = INDEX;
            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.StartTime);
            var stepData = _stepsList[INDEX];
            var pgmSeq = stepData[0];
            var workSheetType = stepData[1];
            var fileName1 = cA.GetFileName("_S45");
            var fileName2 = cA.GetFileName("_S49");
            var pathToFile = Path.Combine(cA.GetFilePath(), fileName2);
            if (File.Exists(pathToFile) && loopIndex == 0)
            {
                File.Delete(pathToFile);
            }
            eH.SetJumpDataSetCount(currDriver, intINDEX, pgmSeq, workbookId, false, "ItemName", loopIndex + 1);

            if (IsInputModule(workSheetType))
            {
                var dataSetCountKeys = _inputDataList[INDEX].Keys;

                var goodItemNameVal = "";
                var realLotNoVal = "";
                var lineCount = 0;
                var rowIndex = -1;
                string iiStrVal = dataSetCountKeys.ElementAt(loopIndex);
                var workSheetsBundle = _inputDataList[INDEX][iiStrVal];
                MultiDictList<string, List<string>> workSheetsArr = null;
                Dictionary<string, List<List<string>>>.KeyCollection datasetKeys = null;
                List<List<string>> workSheet;

                workSheetsArr = workSheetsBundle[loopIndex];
                datasetKeys = workSheetsArr.Keys;

                for (var i = 0; i < datasetKeys.Count; i++)
                {
                    string dataForVal = datasetKeys.ElementAt(i);
                    workSheet = workSheetsArr[dataForVal];
                    if (dataForVal == "Control")
                    {
                        cA.ReadPgmControlDataList(currDriver, pgmSeq, workSheet);
                    }
                    else
                    {
                        cSA.ReadSheetDataList(currDriver, pgmSeq, workbookId, workSheet);
                    }

                }

                if (File.Exists(Path.Combine(cA.GetFilePath(), fileName1)))
                {
                    lineCount = cA.GetTempFileLineCount(fileName1);
                    for (var j = 0; j < lineCount; j++)
                    {
                        if ((loopIndex + 1).ToString() == cA.ReadTempFile(fileName1, j, 0))
                        {
                            goodItemNameVal = cA.ReadTempFile(fileName1, j, 3);
                            realLotNoVal = cA.ReadTempFile(fileName1, j, 4);

                            rowIndex = cSA.GetRowIndex(currDriver, workbookId, "ItemName", goodItemNameVal);
                            if (rowIndex != -1)
                            {
                                cSA.CellInput(currDriver, pgmSeq, workbookId, rowIndex, "LotNo", realLotNoVal);
                            }
                        }
                    }
                }
                int countDirtyRows = cSA.GetRowIndex(currDriver, workbookId, "ItemName", "");
                cA.SavePgm(currDriver, eH, pgmSeq, null, intINDEX);
                cA.Delay();
                int countSavedRows = cSA.CountDataRow(currDriver, pgmSeq, workbookId, RowDataType.SAVED_ROWS);

                cA.SetInputDataSetCount(currDriver, eH, cSA, intINDEX, pgmSeq, workbookId, countSavedRows, loopIndex + 1);

                if (countDirtyRows == countSavedRows)
                {

                    string copiedVal = cA.GetControlValue(currDriver, pgmSeq, "txtInvoiceNo_txt");
                    string copiedValWithIdx = (loopIndex + 1) + Keys.TAB + copiedVal;
                    cA.WriteTempFile(fileName2, copiedValWithIdx, true);

                    msgVal = "Scenario Index 49 - " + pgmSeq + " - 거래명세서번호 : " + copiedVal;
                    cLR.SetLogRecord(intINDEX, msgVal);

                    cA.SelectPgmControl(currDriver, eH, pgmSeq, "btnDelvCfm_btn");
                    var isPopUpVisible = Validations.IsPopupVisible(currDriver, "messagePopup");
                    if (isPopUpVisible)
                    {
                        var messageText = cA.GetMessageDescription(currDriver);
                        cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);
                        msgVal = "Scenario Index 49 - " + pgmSeq + " - 메세지 : " + messageText;
                        cLR.SetLogRecord(intINDEX, msgVal);
                    }
                    bool checkState = Convert.ToBoolean(cA.GetControlValue(currDriver, pgmSeq, "chkIsDelvCfm_chk").ToString());
                    if (!checkState)
                    {
                        msgVal = "Scenario Index 49 - " + pgmSeq + " - 경고메세지 : 출고처리 실패하였습니다.";
                        cLR.SetLogRecord(intINDEX, msgVal);
                    }

                }
                else
                {
                    msgVal = "Scenario Index 49 - " + pgmSeq + " - 거래명세서번호 : 저장이 실패하였습니다.";
                    cLR.SetLogRecord(intINDEX, msgVal);
                    var isPopUpVisible = Validations.IsPopupVisible(currDriver, "messagePopup");
                    if (isPopUpVisible)
                    {
                        var messageText = cA.GetMessageDescription(currDriver);
                        cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);
                        msgVal = "Scenario Index 49 - " + pgmSeq + " - 경고메세지 : " + messageText;
                        cLR.SetLogRecord(intINDEX, msgVal);
                    }
                }

            }
            cA.ClosePgm(currDriver, pgmSeq);
            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);

            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 49 End--------------------");
        }

        private void _2_S50()
        {
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 50 Started--------------------");
            var INDEX = "50";
            var intINDEX = 50;
            Global.ScenarioIndex = INDEX;

            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.StartTime);
            var stepData = _stepsList[INDEX];
            var pgmSeq = stepData[0];
            var workSheetType = stepData[1];
            var fileName = cA.GetFileName("_S47");

            bool hasError = false;
            if (IsQueryModule(workSheetType))
            {
                var workSheets = _queryDataList[INDEX];


                var rowCount = 0;
                string invoiceNoVal = "";
                cA.OpenPgm(currDriver, eH, pgmSeq, intINDEX);
                string pgmSeqFrame = cA.GetPgmSeqFrame(pgmSeq);
                currDriver.SwitchTo().Frame(currDriver.FindElement(By.Id(pgmSeqFrame)));

                var lineCount = cA.GetTempFileLineCount(fileName);
                for (var i = 0; i < lineCount; i++)
                {
                    invoiceNoVal = cA.ReadTempFile(fileName, i, 1);
                    cA.SetPgmControlData(currDriver, "txtInvoiceNo_txt", invoiceNoVal);
                    cA.ReadPgmControlDataList(currDriver, pgmSeq, workSheets);
                    cA.QueryPgm(currDriver, eH, pgmSeq, intINDEX, workbookId, true, 1);

                    rowCount = cSA.CountDataRow(currDriver, pgmSeq, workbookId);
                    if (rowCount == 0)
                    {
                        msgVal = "Scenario Index 50 - " + pgmSeq + " - " + invoiceNoVal + " 의 거래명세서건이 없습니다.";
                        hasError = true;
                    }
                    else
                    {
                        cSA.CellClick(currDriver, pgmSeq, workbookId, 1, "Sel");
                        cA.ClickJumpPgm(currDriver, eH, pgmSeq, "3", _stepsList["51"][0], 51);
                        cA.Delay();

                        eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                        _2_S51(i);
                        Global.ScenarioIndex = INDEX;
                    }
                }
            }

            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 50 End--------------------");
            if (!hasError)
            {
                cA.ClosePgm(currDriver, pgmSeq);
                eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                _2_S52();
                Global.ScenarioIndex = INDEX;
            }
        }

        private void _2_S51(int loopIndex)
        {
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 51 Started--------------------");
            var INDEX = "51";
            var intINDEX = 51;
            Global.ScenarioIndex = INDEX;
            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.StartTime);
            var stepData = _stepsList[INDEX];
            var pgmSeq = stepData[0];
            var workSheetType = stepData[1];
            var popupPgmSeq = "502268";
            var fileName = cA.GetFileName("_S51");
            var pathToFile = Path.Combine(cA.GetFilePath(), fileName);
            if (File.Exists(pathToFile) && loopIndex == 0)
            {
                File.Delete(pathToFile);
            }
            eH.SetJumpDataSetCount(currDriver, intINDEX, pgmSeq, workbookId, false, null, loopIndex + 1);

            if (IsInputModule(workSheetType))
            {
                var dataSetCountKeys = _inputDataList[INDEX].Keys;

                string iiStrVal = dataSetCountKeys.ElementAt(loopIndex);
                var workSheetsBundle = _inputDataList[INDEX][iiStrVal];
                MultiDictList<string, List<string>> workSheetsArr = null;
                Dictionary<string, List<List<string>>>.KeyCollection datasetKeys = null;
                List<List<string>> workSheet;

                workSheetsArr = workSheetsBundle[loopIndex];
                datasetKeys = workSheetsArr.Keys;

                for (var i = 0; i < datasetKeys.Count; i++)
                {
                    string dataForVal = datasetKeys.ElementAt(i);
                    workSheet = workSheetsArr[dataForVal];
                    if (dataForVal == "Control")
                    {
                        cA.ReadPgmControlDataList(currDriver, pgmSeq, workSheet);
                    }
                    else
                    {
                        cSA.ReadSheetDataList(currDriver, pgmSeq, workbookId, workSheet);
                    }

                }


                int countDirtyRows = cSA.CountDataRow(currDriver, pgmSeq, workbookId, RowDataType.DIRTY_ROWS);
                cA.SavePgm(currDriver, eH, pgmSeq, null, intINDEX);
                cA.Delay();
                int countSavedRows = cSA.CountDataRow(currDriver, pgmSeq, workbookId, RowDataType.SAVED_ROWS);

                cA.SetInputDataSetCount(currDriver, eH, cSA, intINDEX, pgmSeq, workbookId, countSavedRows, loopIndex + 1);

                if (countDirtyRows == countSavedRows)
                {

                    string copiedVal = cA.GetControlValue(currDriver, pgmSeq, "txtBillNo_txt");
                    string copiedValWithIdx = (loopIndex + 1) + Keys.TAB + copiedVal;
                    cA.WriteTempFile(fileName, copiedValWithIdx, true);

                    msgVal = "Scenario Index 51 - " + pgmSeq + " - 세금계산서번호 : " + copiedVal;
                    cLR.SetLogRecord(intINDEX, msgVal);

                    cA.SelectPgmControl(currDriver, eH, pgmSeq, "btnSlip_btn", false, popupPgmSeq, INDEX);
                    cA.Delay();
                    cA.SavePgm(currDriver, eH, pgmSeq, null, intINDEX);
                    cA.Delay();

                    copiedVal = cA.GetControlValue(currDriver, popupPgmSeq, "txtSlipMstID_txt");
                    if (!string.IsNullOrEmpty(copiedVal))
                    {
                        cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);
                        cA.WriteTempFile(fileName, copiedValWithIdx, true);
                        cA.SelectSpanControl(currDriver, null, "devDlgArea .divTitleArea", ".btnClose");
                        msgVal = "Scenario Index 51 - " + pgmSeq + " - 전표번호 : " + copiedVal;
                        cLR.SetLogRecord(intINDEX, msgVal);
                    }
                    else
                    {
                        msgVal = "Scenario Index 51 - " + pgmSeq + " - 전표번호 : 전표처리가 실패하였습니다.";
                        cLR.SetLogRecord(intINDEX, msgVal);

                        var isPopUpVisible = Validations.IsPopupVisible(currDriver, "messagePopup");
                        if (isPopUpVisible)
                        {
                            var messageText = cA.GetMessageDescription(currDriver);
                            cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);
                            msgVal = "Scenario Index 51 - " + pgmSeq + " - 경고메세지 : " + messageText;
                            cLR.SetLogRecord(intINDEX, msgVal);
                        }
                    }

                }
                else
                {
                    msgVal = "Scenario Index 51 - " + pgmSeq + " - 세금계산서번호 : 저장이 실패하였습니다.";
                    cLR.SetLogRecord(intINDEX, msgVal);
                    var isPopUpVisible = Validations.IsPopupVisible(currDriver, "messagePopup");
                    if (isPopUpVisible)
                    {
                        var messageText = cA.GetMessageDescription(currDriver);
                        cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);
                        msgVal = "Scenario Index 51 - " + pgmSeq + " - 경고메세지 : " + messageText;
                        cLR.SetLogRecord(intINDEX, msgVal);
                    }
                }

            }
            cA.ClosePgm(currDriver, pgmSeq);
            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);

            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 51 End--------------------");
        }

        private void _2_S52()
        {
            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 52 Started--------------------");
            var INDEX = "52";
            var intINDEX = 52;
            Global.ScenarioIndex = INDEX;

            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.StartTime);
            var stepData = _stepsList[INDEX];
            var pgmSeq = stepData[0];
            var workSheetType = stepData[1];
            if (IsInputModule(workSheetType))
            {
                var dataSetCountKeys = _inputDataList[INDEX].Keys;
                var workbookId3 = "SS2_c";
                var fileName1 = cA.GetFileName("_S51");
                var fileName2 = cA.GetFileName("_S52");
                var pathToFile = Path.Combine(cA.GetFilePath(), fileName2);
                if (File.Exists(pathToFile))
                {
                    File.Delete(pathToFile);
                }

                for (var ii = 0; ii < dataSetCountKeys.Count; ii++)
                {
                    if (ii > 0)
                    {
                        cA.NewPgm(currDriver, pgmSeq);
                    }
                    string iiStrVal = dataSetCountKeys.ElementAt(ii);
                    var workSheetsBundle = _inputDataList[INDEX][iiStrVal];
                    MultiDictList<string, List<string>> workSheetsArr = null;
                    Dictionary<string, List<List<string>>>.KeyCollection datasetKeys = null;
                    List<List<string>> workSheet;

                    var copiedVal = "";
                    var copiedValWithIdx = "";
                    var rowCount = 0;
                    var billNoValArr = new List<string>();
                    var billNoValIndex = -1;

                    for (var seq = 0; seq < workSheetsBundle.Count; seq++)
                    {
                        workSheetsArr = workSheetsBundle[seq];
                        datasetKeys = workSheetsArr.Keys;
                        string dataForVal = datasetKeys.ElementAt(0);
                        workSheet = workSheetsArr[dataForVal];
                        if (dataForVal == "Control")
                        {
                            cA.ReadPgmControlDataList(currDriver, pgmSeq, workSheet);
                        }
                        else
                        {

                            cSA.ReadSheetDataList(currDriver, pgmSeq, workbookId, workSheet);
                        }
                    }
                    cA.SelectPgmControl(currDriver, eH, pgmSeq, "btnBillQuery_btn");
                    rowCount = cSA.CountDataRow(currDriver, pgmSeq, workbookId, RowDataType.DIRTY_ROWS);
                    if (rowCount == 0)
                    {
                        msgVal = "Scenario Index 52 - " + pgmSeq + " - 세금계산서내역이 존재하지 않습니다.";
                        cLR.SetLogRecord(intINDEX, msgVal);

                    }
                    else
                    {
                        var lineCount = cA.GetTempFileLineCount(fileName1);
                        for (var j = 0; j < lineCount; j++)
                        {
                            if (cA.ReadTempFile(fileName1, j, 0) == ii.ToString())
                            {
                                billNoValArr.Add(cA.ReadTempFile(fileName1, j, 1));
                            }
                        }
                        rowCount = cSA.CountDataRow(currDriver, pgmSeq, workbookId3);
                        string cellValue = string.Empty;
                        for (int k = 0; k < rowCount; k++)
                        {
                            cellValue = cSA.GetCellValue(currDriver, workbookId3, k, "BillNo");

                            billNoValIndex = billNoValArr.IndexOf(cellValue);
                            if (billNoValIndex != -1)
                            {
                                cSA.CellClick(currDriver, pgmSeq, workbookId3, k, "Sel");

                                cellValue = cSA.GetCellValue(currDriver, workbookId3, k, "BillCurAmt");
                                cSA.CellInput(currDriver, pgmSeq, workbookId, 0, "ReceiptAmt", cellValue);
                                billNoValArr.RemoveAt(billNoValIndex);
                            }
                            if (billNoValArr.Count == 0)
                            {
                                break;
                            }
                        }
                        int countDirtyRows = cSA.CountDataRow(currDriver, pgmSeq, workbookId);
                        cA.SavePgm(currDriver, eH, pgmSeq, null, intINDEX);
                        int countSavedRows = cSA.CountDataRow(currDriver, pgmSeq, workbookId, RowDataType.SAVED_ROWS);

                        cA.SetInputDataSetCount(currDriver, eH, cSA, intINDEX, pgmSeq, workbookId, countSavedRows, ii);
                        countSavedRows = cSA.CountDataRow(currDriver, pgmSeq, workbookId3, RowDataType.SAVED_ROWS);

                        cA.SetInputDataSetCount(currDriver, eH, cSA, intINDEX, pgmSeq, workbookId3, countSavedRows, ii);
                        if (countDirtyRows == countSavedRows)
                        {
                            copiedVal = cA.GetControlValue(currDriver, pgmSeq, "txtReceiptNo_txt");
                            copiedValWithIdx = (ii + 1) + Keys.TAB + copiedVal;
                            cA.WriteTempFile(fileName2, copiedValWithIdx, true);

                            msgVal = "Scenario Index 52 - " + pgmSeq + " - 입금번호 : " + copiedVal;
                            cLR.SetLogRecord(intINDEX, msgVal);
                        }
                        else
                        {

                            msgVal = "Scenario Index 52 - " + pgmSeq + " - 입금번호 : 저장이 실패하였습니다.";
                            cLR.SetLogRecord(intINDEX, msgVal);
                            var isPopUpVisible = Validations.IsPopupVisible(currDriver, "messagePopup");
                            if (isPopUpVisible)
                            {
                                var messageText = cA.GetMessageDescription(currDriver);
                                cA.ClickMesssageButton(currDriver, ButtonType.MSG_BTN_OK);
                                msgVal = "Scenario Index 52 - " + pgmSeq + " - 경고메세지 : " + messageText;
                                cLR.SetLogRecord(intINDEX, msgVal);
                                eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);
                            }

                        }
                    }

                }

            }
            cA.ClosePgm(currDriver, pgmSeq);
            eH.SetWSMUpdateJson(intINDEX, WorksheetMeta.Success, Keys.SUCCESS);

            Main.Reporter.Record_Checkpoint("--------------------Scenario Index 52 End--------------------");
        }

        private void GetSysLogInfo()
        {

            //string scriptVal = "var sysLog = {'ProductionType': _base.ProductType,'IsLayoutAdmin':window.location.href.includes(\"?isadmin=1\"), \"IsDev\" : window.location.href.includes(\'?isdev=1\'),\"WebERPVersion\": Const.WebVersionInfo,\"IsEssUser\": _base.ConnInfo.IsEssUser,\"Company\" : _base.CompanyName,\"LoginSeq\" : _base.ConnInfo.LoginSeq,\"UserSeq\" : _base.UserSeq,\"UserName\" : _base.UserName,\"DeptName\" : _base.DeptName,\"LoginDateTime\" : _base.LoginID,\"IsUseBI\" : _base.useWebBIMode,\"DSN\" : _base.Dsn,\"PCMonitorSize_w\":window.screen.width,\"PCMonitorSize_h\":window.screen.height,\"BrowserSize_w\": document.documentElement.clientWidth,\"BrowserSize_h\": document.documentElement.clientHeight,\"BrowserPoint_L\": window.screenLeft,\"BrowserPoint_T\": window.screenTop }; return sysLog;";
            string scriptVal = "var sysLog = [_base.ProductType,window.location.href.includes(\"?isadmin=1\"),window.location.href.includes(\'?isdev=1\'),Const.WebVersionInfo,_base.ConnInfo.IsEssUser,_base.CompanyName,_base.ConnInfo.LoginSeq,_base.UserSeq, _base.UserName, _base.DeptName, _base.LoginID,_base.useWebBIMode,_base.Dsn,window.screen.width,window.screen.height,document.documentElement.clientWidth,document.documentElement.clientHeight,window.screenLeft,window.screenTop];return sysLog;";
            ReadOnlyCollection<object> returnDto = cA.ExecuteScript<ReadOnlyCollection<object>>(currDriver, scriptVal);


            Global.SL_UpdateData.Browser = GetBrowserNameAndVersion();

            string ProductionType = returnDto[0].ToString(); //Production Type
            if (ProductionType == "274001")
            {
                Global.SL_UpdateData.ProductName = "SystemEver";
            }
            else if (ProductionType == "274002")
            {
                Global.SL_UpdateData.ProductName = "GenuineW";
            }

            Global.SL_UpdateData.IsLayoutAdmin = returnDto[1].ToString();
            Global.SL_UpdateData.IsDev = returnDto[2].ToString();
            Global.SL_UpdateData.WebERPVersion = returnDto[3].ToString();
            Global.SL_UpdateData.IsEssUser = returnDto[4].ToString();
            Global.SL_UpdateData.Company = returnDto[5].ToString();
            Global.SL_UpdateData.LoginSeq = returnDto[6].ToString();
            Global.SL_UpdateData.UserSeq = returnDto[7].ToString();
            Global.SL_UpdateData.UserName = returnDto[8].ToString();
            Global.SL_UpdateData.DeptName = returnDto[9].ToString();
            string loginDateTime = returnDto[10].ToString().Substring(0, 14);
            var regex = "(.{4})(.{2})(.{2})(.{2})(.{2})(.{2})";
            var replace = "$1/$2/$3 $4:$5:$6";
            loginDateTime = Regex.Replace(loginDateTime, regex, replace);

            Global.SL_UpdateData.LoginDateTime = loginDateTime;

            if (Global.SL_UpdateData.IsEssUser == "true")
            {
                Global.SL_UpdateData.LicenseType = "EssUser";
            }
            else
            {
                Global.SL_UpdateData.LicenseType = "GeneralUser";
            }

            Global.SL_UpdateData.IsUseBI = returnDto[11].ToString();
            Global.SL_UpdateData.DSN = returnDto[12].ToString();

            string resolutionX = returnDto[13].ToString();
            string resolutionY = returnDto[14].ToString();
            Global.SL_UpdateData.PCMonitorResolution = string.Format("{0}{1}{2}", resolutionX, " * ", resolutionY);

            string BrowserSize_w = returnDto[15].ToString();
            string BrowserSize_h = returnDto[16].ToString();
            Global.SL_UpdateData.BrowserSize = string.Format("{0}{1}{2}", BrowserSize_w, " * ", BrowserSize_h);

            string BrowserPoint_L = returnDto[17].ToString();
            string BrowserPoint_T = returnDto[18].ToString();

            Global.SL_UpdateData.BrowserPoint = string.Format("{0}{1}{2}", BrowserPoint_L, " * ", BrowserPoint_T);

            System.Drawing.Size windowSize = currDriver.Manage().Window.Size;
            Global.SL_UpdateData.PCMonitorSize = string.Format("{0}{1}{2}", windowSize.Width, " * ", windowSize.Height);

            Global.SL_UpdateData.PCLanguage = GetPCLanguage();
            Global.SL_UpdateData.PCLocale = GetPCLocale();
            Global.SL_UpdateData.PCName = Environment.MachineName;
            Global.SL_UpdateData.PCMacAddress = GetPhysicalAddForEthernet();
            Global.SL_UpdateData.PCMemory = GetPCMemory();
            Global.SL_UpdateData.OSVersion = GetOSNameVersion();
        }
        private string GetPCLanguage()
        {
            Thread.CurrentThread.CurrentCulture.ClearCachedData();
            CultureInfo currentCulture = Thread.CurrentThread.CurrentCulture;
            return currentCulture.NativeName;
        }
        private string GetPCLocale()
        {
            Thread.CurrentThread.CurrentCulture.ClearCachedData();
            CultureInfo currentCulture = Thread.CurrentThread.CurrentCulture;
            return currentCulture.Name;
        }
        private string GetOSNameVersion()
        {
            Microsoft.VisualBasic.Devices.ComputerInfo CI = new Microsoft.VisualBasic.Devices.ComputerInfo();
            return CI.OSFullName + " " + CI.OSVersion;
        }
        private string GetPCMemory()
        {
            Microsoft.VisualBasic.Devices.ComputerInfo CI = new Microsoft.VisualBasic.Devices.ComputerInfo();
            ulong mem = CI.TotalPhysicalMemory;

            var finalMemory = (mem / (1024 * 1024) + " MB").ToString();
            return finalMemory;
        }
        private string GetPhysicalAddForEthernet()
        {
            string retVal = "";
            foreach (var nic in NetworkInterface.GetAllNetworkInterfaces().Where(nic => nic.OperationalStatus == OperationalStatus.Up))
            {
                if (nic.NetworkInterfaceType == NetworkInterfaceType.Ethernet)
                {
                    retVal = nic.GetPhysicalAddress().ToString();
                    break;
                }

            }
            var regex = "(.{2})(.{2})(.{2})(.{2})(.{2})(.{2})";
            var replace = "$1:$2:$3:$4:$5:$6";
            retVal = Regex.Replace(retVal, regex, replace);
            return retVal;
        }
        private string GetBrowserNameAndVersion()
        {
            //Get Browser name and version.
            ICapabilities caps = ((RemoteWebDriver)currDriver).Capabilities;
            string browser = caps.GetCapability("browserName").ToString();
            string browserName = "";
            if (browser == "chrome")
            {
                browserName = "Google Chrome";
            }
            else if (browser == "firefox")
            {
                browserName = "Mozilla FireFox";
            }
            else if (browser == "edge")
            {
                browserName = "Microsoft Edge";
            }
            else if (browser == "iexplore")
            {
                browserName = "Microsoft Internet Explorer";
            }
            String browserVersion = caps.GetCapability("browserVersion").ToString();

            string returnVal = string.Format("{0} Version: {1}", browserName, browserVersion);
            return returnVal;
        }
        private bool IsInputModule(string workSheetType)
        {
            return workSheetType == "Input";
        }
        private bool IsQueryModule(string workSheetType)
        {
            return workSheetType == "Query";
        }

    }


}