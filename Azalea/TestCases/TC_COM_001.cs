﻿using Azalea.Map;
using NUnit.Framework;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Azalea
{
    class TC_COM_001
    {
        private IWebDriver _driver;
        private WebDriverWait wait;
        private CommonActions cA = null;
        private SheetActions cSA = null;

        public TC_COM_001(IWebDriver driver)
        {
            _driver = driver;
            cA = new CommonActions();
            cSA = new SheetActions();
        }

        public void Execute()
        {
            string pgmSeq = "500547";
            string workbookId = "SS1_c";

            HeaderMap headerMap = new HeaderMap(_driver);
            var inputVal = pgmSeq + OpenQA.Selenium.Keys.Enter;
            cA.HoverEvent(_driver, headerMap.programQueryBtn);
            cA.Delay();
            cA.InputEvent(_driver, headerMap.programQueryText, inputVal);

            string pgmSeqFrame = cA.GetPgmSeqFrame(pgmSeq);
            _driver.SwitchTo().DefaultContent();
            _driver.SwitchTo().Frame(_driver.FindElement(By.Id(pgmSeqFrame)));
            wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(20));
            wait.Until(driver => driver.FindElement(By.Id(workbookId)));
            cA.ClickEvent(_driver, _driver.FindElement(By.Id("ExpanderC364_lbl")));

            string path = @"E:\Selenium\SeleniumTestManager\DataDirectory\ComponentInput_500547.csv";
            Dictionary<string, string> keyValuePairs = DataDrivenTestPurpose.ConvertCsvToDictionary(path);

            foreach (KeyValuePair<string, string> item in keyValuePairs)
            {
                string componentId = item.Key;
                string componetValue = item.Value;

                cA.SetPgmControlData(_driver, componentId, componetValue);
            }

            path = @"E:\Selenium\SeleniumTestManager\DataDirectory\SheetInput_500547.csv";
            DataTable sheetDataDataTable = DataDrivenTestPurpose.ConvertCsvToDataTable(path);
            for (int i = 0; i < sheetDataDataTable.Rows.Count; i++)
            {
                for (int j = 0; j < sheetDataDataTable.Columns.Count; j++)
                {
                    var dataFieldVal = sheetDataDataTable.Columns[j].ToString();
                    var cellVal = sheetDataDataTable.Rows[i][j].ToString();

                    var colIndex = cSA.GetColumnIndex(_driver, "SS1_c", dataFieldVal);
                    var rowIndex = i;

                    var cellObjDictionary = cA.ExecuteScript<Dictionary<string, object>>(_driver, cSA.Script_SheetCellObject(workbookId, rowIndex, colIndex));

                    var xVal = int.Parse(cellObjDictionary.First(m => m.Key == "x").Value.ToString());
                    var yVal = int.Parse(cellObjDictionary.First(m => m.Key == "y").Value.ToString());
                    var widthVal = int.Parse(cellObjDictionary.First(m => m.Key == "width").Value.ToString());
                    var heightVal = int.Parse(cellObjDictionary.First(m => m.Key == "height").Value.ToString());
                    var xCordVal = (xVal + widthVal / 2);
                    var yCordVal = yVal + heightVal / 2;

                    ContentsMap contentsMap = new ContentsMap(_driver);
                    var canvasObj = contentsMap.CanvasObjBy(workbookId);

                    cA.ClickInputEvent(_driver, canvasObj, xCordVal, yCordVal, cellVal);
                    TestContext.Out.WriteLine("X: {0}, Y: {1}", xCordVal, yCordVal);
                }
            }
        }
    }
}
