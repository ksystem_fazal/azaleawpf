﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Brushes = System.Windows.Media.Brushes;

namespace Azalea
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class Tracker : Window
    {
        private delegate void SafeProgressMaxDelegate(int maxVal);

        public Tracker(int progressCountMax)
        {
            ShowInTaskbar = false;
            Topmost = true;
            InitializeComponent();

            Rect screenWorkArea = SystemParameters.WorkArea;
            Top = 0;
            Left = (screenWorkArea.Left + screenWorkArea.Right) / 2 - (Width / 2);
            TrackerProgressBar.Maximum = progressCountMax;
            Cursor = Cursors.ScrollAll;
        }

        public void SetInfo(string info)
        {
            Dispatcher.Invoke(new Action(delegate
            {
                TrackerInfoLbl.Content = info;
                var infoTextWidth = new FormattedText(
                                        info,
                                        CultureInfo.CurrentCulture,
                                        FlowDirection.LeftToRight,
                                        new Typeface(TrackerInfoLbl.FontFamily, TrackerInfoLbl.FontStyle, TrackerInfoLbl.FontWeight, TrackerInfoLbl.FontStretch),
                                        TrackerInfoLbl.FontSize,
                                        Brushes.Black,
                                        new NumberSubstitution(),
                                        1).Width;
                if (infoTextWidth > 175)
                {
                    Width = infoTextWidth + 12;
                }
                else
                {
                    Width = 187;
                }
                TrackerInfoLbl.Width = infoTextWidth;
                Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Render, (Action)delegate () { });
            }));
        }

        public void ClearInfo()
        {
            Dispatcher.Invoke(new Action(delegate
            {
                TrackerInfoLbl.Content = "";
                Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Render, (Action)delegate () { });
            }));
        }

        public void StepUpProgress()
        {
            Dispatcher.Invoke(new Action(delegate
            {
                TrackerProgressBar.Value = Convert.ToInt32(TrackerProgressBar.Value) + 1;
                Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Render, (Action)delegate () { });
            }));
        }

        public void SetProgressMaxVal(int maxVal)
        {
            if (!TrackerProgressBar.CheckAccess())
            {
                Dispatcher.Invoke(new SafeProgressMaxDelegate(SetProgressMaxVal), maxVal);
            }
            else
            {
                TrackerProgressBar.Maximum = maxVal;
            }
        }

        public void CloseForm()
        {
            Dispatcher.Invoke(new Action(delegate
            {
                Close();
            }));
        }

        private void Tracker_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            Visibility = Visibility.Hidden;
        }
    }
}
