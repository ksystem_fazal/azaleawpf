﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzaleaRunner
{
    static class ReportLogItemType
    {
        public const string ERROR = "Failed";
        public const string WARNING = "Warning";
        public const string SUCCESS = "Passed";
        public const string MESSAGE = "Message";
        public const string EVENT = "Event";
        public const string CHECKPOINT = "Checkpoint";
    }

    static class CheckBoxType
    {
        public const int UNCHECKEDVALUE = 0;
        public const int CHECKEDVALUE = 1;
        public const int INDETERMINATEVALUE = 2;
    }

    static class CONST
    {
        public const string DEFAULT_PROJECT_NAME = "Direct_TestCases";
        public const string TEST_CASE_ITEM = "TestCaseItem";
    }

}
