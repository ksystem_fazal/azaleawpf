﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzaleaRunner
{
    static class Global
    {
        //public readonly static string CurrDirectoryPath = @"\\TN-ACR-019\AzaleaFolder\Project";
        //public readonly static string DllDirectory = @"\\TN-ACR-019\AzaleaFolder\Project";
        public readonly static string CurrDirectoryPath = Path.GetDirectoryName(Path.GetDirectoryName(Directory.GetCurrentDirectory()));
        public readonly static string AzaleaDirectoryPath = @"E:\BitBucket\azaleawpf\Azalea";
        public readonly static string AzaleaDllDirectory = Path.Combine(AzaleaDirectoryPath, @"bin\Debug");
        public readonly static string DataDirectory = Path.Combine(CurrDirectoryPath, "Data");
        public readonly static string ReportDirectory = Path.Combine(CurrDirectoryPath, "Report");
        public readonly static string ImageDirectory = Path.Combine(CurrDirectoryPath, "Images");
        public readonly static string TemplateDirectory = Path.Combine(CurrDirectoryPath, "Template");
        public readonly static string AzaleaDataDirectory = Path.Combine(AzaleaDirectoryPath, "Data");

        public readonly static string TestManagerDllName = "Azalea.dll";
        public readonly static string ClassMethodName = "RunnerData.csv";
        public readonly static string TCName_Direct = "TC_Direct.xml";
        public readonly static string TCName_Schedule = "TC_Schedule.xml";
        public readonly static string TCName_New_Schedule = "TC_New_Schedule.xml";
        public readonly static string ReportLogRootName = "Root.csv";
        public readonly static string TestRunInfoName = "Main.xml";
        public readonly static string ReportTemplateName = "ReportLog.txt";
        public readonly static string ExportLogDetailName = "ExportLogDetails.csv";

        public static string DateFormatString = "yyyy/MM/dd HH:mm:ss";

        public readonly static Dictionary<string, string> BrowserList = new Dictionary<string, string>() {
            { "chrome", "Chrome" }
        };
        public readonly static Dictionary<string, string> LanguageList = new Dictionary<string, string>() {
            { "한국어", "한국어" },
            { "English", "English" },
            { "日本語", "日本語" },
            { "中文", "中文" }
        };
    }
}
