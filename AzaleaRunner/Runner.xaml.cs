﻿using Aga.Controls.Tree;
using Aga.Controls.Tree.NodeControls;
using Azalea;
using NUnit.Framework;
using NUnitLite;
using Quartz;
using Quartz.Impl;
using Quartz.Impl.Matchers;
using Quartz.Listener;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Web.UI;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.DataVisualization.Charting;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Xml;


namespace AzaleaRunner
{
    class TreeViewLineConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            TreeViewItem item = (TreeViewItem)value;
            ItemsControl ic = ItemsControl.ItemsControlFromItemContainer(item);
            return ic.ItemContainerGenerator.IndexFromContainer(item) == ic.Items.Count - 1;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new Exception("The method or operation is not implemented.");
        }
    }


    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class Runner : Window
    {
        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        protected static readonly DependencyProperty ActualLegendItemStyleProperty = DependencyProperty.Register("ActualLegendItemStyle", typeof(Style), typeof(DataPointSeries), null);
        protected Style ActualLegendItemStyle
        {
            get
            {
                return (base.GetValue(ActualLegendItemStyleProperty) as Style);
            }
            set
            {
                base.SetValue(ActualLegendItemStyleProperty, value);
            }
        }

        [DisallowConcurrentExecutionAttribute]
        private class ScheduleJob : IJob
        {
            public Task Execute(IJobExecutionContext context)
            {
                Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, new Action(() =>
                {
                    Runner runnerObj = context.JobDetail.JobDataMap["runnerObj"] as Runner;
                    runnerObj.scheduleProjectName = context.JobDetail.Key.Name;
                    try
                    {
                        runnerObj.TestRun_Click(null, null);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error Occured", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    finally
                    {
                        runnerObj.scheduleProjectName = null;
                    }
                }));
                return Task.FromResult(0);
            }
        }

        private class ToolTipProvider : IToolTipProvider
        {
            public string GetToolTip(TreeNodeAdv node)
            {
                return "Drag & drop nodes to move them";
            }
        }

        private class ReportsRow
        {
            public string Tag { get; set; }
            public string DisplayName { get; set; }
        }

        private class LogDetailsRow
        {
            public string Type { get; set; }
            public string HiddenType { get; set; }
            public string Message { get; set; }
            public string Time { get; set; }
            public string Picture { get; set; }
        }

        private class LogDetailsPathRow
        {
            public string Type { get; set; }
            public string HiddenType { get; set; }
            public string Name { get; set; }
            public string StartTime { get; set; }
            public string EndTime { get; set; }
            public string Duration { get; set; }
        }

        private class TestSchedulesRow
        {
            public int ScheduleSeq { get; set; }
            public string ScheduleName { get; set; }
            public int SiteSeq { get; set; }
            public string IsMon { get; set; }
            public string IsTue { get; set; }
            public string IsWed { get; set; }
            public string IsThu { get; set; }
            public string IsFri { get; set; }
            public string StartingTime { get; set; }
            public string JobStartAt { get; set; }
            public string JobRunAfter { get; set; }
        }

        class TreeViewLineConverter : IValueConverter
        {
            public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
            {
                TreeViewItem item = (TreeViewItem)value;
                ItemsControl ic = ItemsControl.ItemsControlFromItemContainer(item);
                return ic.ItemContainerGenerator.IndexFromContainer(item) == ic.Items.Count - 1;
            }

            public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }


        private StreamWriter streamWriter;
        private bool _testRun_Executed = false;
        private Dictionary<string, string> _testRun_GeneralInfo = new Dictionary<string, string>();
        private Dictionary<string, Dictionary<string, string>> _testRun_DetailInfo = new Dictionary<string, Dictionary<string, string>>();

        SelectModule selectModule;

        SettingModule settingModule;

        private int SiteSeq = 0;
        private string Browser = "";
        private string LanguageSelect = "";
        private string mailToStr = "";

        LogReportDataSet._TTCTestProjectDataTable testProjectDT;
        private int testProjectNodeOrder = 0;

        private readonly DispatcherTimer timer = new DispatcherTimer();
        private ISchedulerFactory _schedulerFactory = null;
        private readonly string TC_FilePath;
        public ISchedulerFactory SchedulerFactory
        {
            get
            {
                if (_schedulerFactory == null)
                {
                    NameValueCollection props = new NameValueCollection
                {
                    { "quartz.serializer.type", "binary" },
                    { "quartz.scheduler.instanceName", "QuartzAzalea"},
                };
                    _schedulerFactory = new StdSchedulerFactory(props);


                }
                return _schedulerFactory;
            }

        }

        private bool isNewSchedule = false;
        private bool isNewScheduleCreated = false;

        readonly LogReportDataSet logReportDataSet;

        private string scheduleProjectName = null;

        public Runner()
        {
            TC_FilePath = System.IO.Path.Combine(Global.DataDirectory, Global.TCName_Direct);
            InitializeComponent();
            MaxHeight = SystemParameters.MaximizedPrimaryScreenHeight - 2;

            logReportDataSet = new LogReportDataSet();
            LogReportDataSetTableAdapters._TTCSiteTableAdapter _TTCSiteTableAdapter = new LogReportDataSetTableAdapters._TTCSiteTableAdapter();
            _TTCSiteTableAdapter.Fill(logReportDataSet._TTCSite);
            LogReportDataSetTableAdapters._TTCRegularTestTableAdapter _RegularTestTableAdapter = new LogReportDataSetTableAdapters._TTCRegularTestTableAdapter();
            _RegularTestTableAdapter.Fill(logReportDataSet._TTCRegularTest);
            LogReportDataSetTableAdapters._TTCMailAddressTableAdapter _TTCMailAddressTableAdapter = new LogReportDataSetTableAdapters._TTCMailAddressTableAdapter();
            _TTCMailAddressTableAdapter.Fill(logReportDataSet._TTCMailAddress);
            _TTCSiteTableAdapter.Dispose();
            _RegularTestTableAdapter.Dispose();
            _TTCMailAddressTableAdapter.Dispose();

            foreach (var item in logReportDataSet._TTCRegularTest)
            {
                TestScheduleGridView.Items.Add(new TestSchedulesRow() { ScheduleSeq = item.ScheduleSeq, ScheduleName = item.ScheduleName, SiteSeq = item.SiteSeq, IsMon = item.IsMon, IsTue = item.IsTue, IsWed = item.IsWed, IsThu = item.IsThu, IsFri = item.IsFri, StartingTime = item.StartingTime, JobStartAt = "", JobRunAfter = "" });
            }
            TestScheduleGridView_AfterDataBinded();

            ScheduleSiteCMB.ItemsSource = logReportDataSet._TTCSite;
            if (Properties.Settings.Default.SiteSeq == 0)
            {
                if (logReportDataSet._TTCSite.Rows.Count > 0)
                {
                    Properties.Settings.Default.SiteSeq = (logReportDataSet._TTCSite.Rows[0] as LogReportDataSet._TTCSiteRow).SiteSeq;
                    TestRun.IsEnabled = true;
                }
                else
                {
                    TestRun.IsEnabled = false;
                }
            }
            else
            {
                TestRun.IsEnabled = true;
            }

            CheckpointFilterChk.IsChecked = Properties.Settings.Default.CheckpointFilter;
            SuccessFilterChk.IsChecked = Properties.Settings.Default.SuccessFilter;
            EventFilterChk.IsChecked = Properties.Settings.Default.EventFilter;
            WarningFilterChk.IsChecked = Properties.Settings.Default.WarningFilter;
            ErrorFilterChk.IsChecked = Properties.Settings.Default.ErrorFilter;
            MessageFilterChk.IsChecked = Properties.Settings.Default.MessageFilter;

            CheckpointFilterChk.Tag = ReportLogItemType.CHECKPOINT;
            SuccessFilterChk.Tag = ReportLogItemType.SUCCESS;
            EventFilterChk.Tag = ReportLogItemType.EVENT;
            WarningFilterChk.Tag = ReportLogItemType.WARNING;
            ErrorFilterChk.Tag = ReportLogItemType.ERROR;
            MessageFilterChk.Tag = ReportLogItemType.MESSAGE;

            scheduleBrowserCMB.ItemsSource = Global.BrowserList;
            scheduleLanguageCMB.ItemsSource = Global.LanguageList;
            scheduleMailCMB.ItemsSource = logReportDataSet._TTCMailAddress;
        }

        private void TitleBar_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount >= 2)
            {
                if (WindowState == WindowState.Normal)
                {
                    WindowState = WindowState.Maximized;
                    BorderThickness = new System.Windows.Thickness(8);
                }
                else
                {
                    WindowState = WindowState.Normal;
                    BorderThickness = new System.Windows.Thickness(1);
                }
            }
        }

        private void Runner_FormClosing(object sender, RoutedEventArgs e)
        {
            foreach (var process in Process.GetProcessesByName("chromedriver"))
            {
                process.Kill();
            }
            Close();
        }


        /*********************************
        ***********Tool Bar Box***********
        *********************************/

        #region Tool Bar Box

        private void TestRun_Click(object sender, RoutedEventArgs e)
        {
            WindowState windowState = WindowState;

            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                string filePath = TC_FilePath;
                if (scheduleProjectName == null)
                {
                    SiteSeq = Properties.Settings.Default.SiteSeq;
                    Browser = Properties.Settings.Default.Browser;
                    LanguageSelect = Properties.Settings.Default.Language;
                    mailToStr = Properties.Settings.Default.MailTo; 
                }
                else
                {
                    var item = logReportDataSet._TTCRegularTest.Where(x => x.ScheduleName == scheduleProjectName).Single();
                    SiteSeq = item.SiteSeq;
                    Browser = Properties.Settings.Default.Browser;// Hardcoded
                    LanguageSelect = Properties.Settings.Default.Language;// Hardcoded
                    mailToStr = Properties.Settings.Default.MailTo; // Hardcoded
                    filePath = System.IO.Path.Combine(Global.DataDirectory, scheduleProjectName + "_" + Global.TCName_Schedule);
                }

                xmlDoc.Load(filePath);
                XmlNodeList xmlNodeList = xmlDoc.DocumentElement.ChildNodes;
                List<string> csvList = new List<string>() { "TestItemPath,ClassName,MethodName,Identity" };
                if (xmlNodeList.Count > 0)
                {
                    csvList = csvList.Concat(Testcaseitem_XmlToCSV(xmlNodeList, "", "")).ToList();
                }
                File.WriteAllLines(System.IO.Path.Combine(Global.DataDirectory, Global.ClassMethodName), csvList);

                WindowState = WindowState.Minimized;
                TestCaseRunner();

                string latestDirectory = new DirectoryInfo(Global.ReportDirectory).GetDirectories().OrderByDescending(d => d.LastWriteTimeUtc).First().FullName;
                File.Move(System.IO.Path.Combine(Global.ReportDirectory, "Result_" + Environment.MachineName + ".xml"), System.IO.Path.Combine(latestDirectory, Global.TestRunInfoName));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error Occured", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            finally
            {
                if(ReportTab.IsSelected)
                {
                    Init_LogLoad();
                } else
                {
                    ReportTab.IsSelected = true;
                }

                Show();
                WindowState = windowState;
                Activate();

                if (_testRun_Executed)
                {
                    if (Properties.Settings.Default.SendMail)
                    {
                        Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                        {
                            EmailReport_Send();
                        }));
                    }
                    if (scheduleProjectName == null)
                    {
                        ServerRecord_Save();
                    }
                    else
                    {
                        ServerRecord_Save(scheduleProjectName);
                    }
                    _testRun_Executed = false;
                }
            }
        }

        private List<string> Testcaseitem_XmlToCSV(XmlNodeList xmlNodeList, string nameRelation, string idRelation)
        {
            XmlNode xmlNode;
            List<string> ss;
            List<string> csvList = new List<string>();
            string testItemPath;
            string idPath;
            for (int i = 0; i < xmlNodeList.Count; i++)
            {
                xmlNode = xmlNodeList[i];
                if (string.IsNullOrEmpty(idRelation))
                {
                    testItemPath = xmlNode.Attributes["name"].Value;
                    idPath = xmlNode.Attributes["id"].Value;
                }
                else
                {
                    testItemPath = nameRelation + "/" + xmlNode.Attributes["name"].Value;
                    idPath = idRelation + "/" + xmlNode.Attributes["id"].Value;
                }
                int checkStateVal = Convert.ToInt32(xmlNode.Attributes["selected"].Value);
                if (checkStateVal == CheckBoxType.CHECKEDVALUE)
                {
                    if (xmlNode.Attributes["module"].Value != "")
                    {
                        ss = new List<string>
                        {
                            testItemPath + "," + xmlNode.Attributes["module"].Value.Replace("/", ",") + "," + idPath
                        };
                        csvList.Add(string.Join(",", ss));
                    }
                    if (xmlNode.ChildNodes.Count > 0)
                    {
                        csvList = csvList.Concat(Testcaseitem_XmlToCSV(xmlNode.ChildNodes, testItemPath, idPath)).ToList();
                    }
                }
            }
            return csvList;
        }

        public int TestCaseRunner()
        {
            _testRun_Executed = true;
            List<string> tests = new List<string>();
            //load assembly.
            var assembly = Assembly.LoadFile(System.IO.Path.Combine(Global.AzaleaDllDirectory, Global.TestManagerDllName));
            //get testfixture classes in assembly.
            var testTypes = from t in assembly.GetTypes()
                            let attributes = t.GetCustomAttributes(typeof(TestFixtureAttribute), true)
                            where attributes != null && attributes.Length > 0
                            orderby t.Name
                            select t;
            foreach (var type in testTypes)
            {
                //get test method in class.
                var testMethods = from m in type.GetMethods()
                                  let attributes = m.GetCustomAttributes(typeof(TestAttribute), true)
                                  where attributes != null && attributes.Length > 0
                                  select m;

                string fullName = "";
                foreach (var method in testMethods)
                {
                    fullName = method.DeclaringType.FullName + "." + method.Name;
                    tests.Add(fullName);
                }
            }

            string concatStr = string.Join(", ", tests.ToArray());
            string testNameList = string.Concat("/test:", concatStr);

            string str1 = testNameList;
            string str2 = string.Format("/result:{0}{1}", Global.ReportDirectory, "\\Result_" + Environment.MachineName + ".xml");
            string str3 = string.Format("/work:{0}", Global.CurrDirectoryPath);
            var siteInfo = logReportDataSet._TTCSite.Where(x => x.SiteSeq == SiteSeq).Single();
            string siteAddr = siteInfo.SiteAddr;
            string loginId = siteInfo.LoginID;
            string pw = siteInfo.PW;
            string str4 = string.Format("/params:SiteAddr={0};User={1};Pass={2};Language={3};Browser={4}", siteAddr, loginId, pw, LanguageSelect, Browser);
            string[] runTestCases = new string[] { str1, str2, str3, str4 };
            var typeInfo = typeof(Main).GetTypeInfo();
            return new AutoRun(assembly).Execute(runTestCases);
        }

        private void EmailReport_Send()
        {
            try
            {
                using (MailMessage mail = new MailMessage())
                {
                    List<string> mailAddrs = new List<string>();
                    if (mailToStr != "")
                    {
                        mailAddrs = mailToStr.Split(',').ToList();
                    } else
                    {
                        MessageBox.Show("Mail send list was empty", "Infomation", MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    //mailAddrs = new List<string>() { "xyz@gmail.com" };

                    foreach (string mailAddr in mailAddrs)
                    {
                        mail.To.Add(mailAddr);
                    }
                    mail.From = new MailAddress("ksystem.nepal@gmail.com");
                    mail.Subject = "TestRun result";

                    var siteInfo = logReportDataSet._TTCSite.Where(x => x.SiteSeq == SiteSeq).Single();
                    string siteName = siteInfo.SiteName;
                    string siteAddr = siteInfo.SiteAddr;
                    string runDateTime = DateTime.Parse(_testRun_GeneralInfo["StartTime"]).ToString(Global.DateFormatString);
                    string result = _testRun_GeneralInfo["Result"];



                    string runType = ScheduleExecutionTab.IsSelected ? "" : "Direct";
                    mail.IsBodyHtml = true;
                    mail.Body = "<b>Site Name : </b>" + siteName + "<br/><b>Site Address : </b>" + siteAddr + "<br/><b>Run Date Time : </b>" + runDateTime + "<br/><b>Run Type : </b>" + runType + "<br/><b>Status : </b>" + result;

                    mail.Attachments.Add(Attachment.CreateAttachmentFromString(MailAttachment_Build(), "text/html"));
                    mail.Attachments.Last().ContentDisposition.FileName = "ReportLog.html";

                    using (SmtpClient SmtpServer = new SmtpClient())
                    {
                        SmtpServer.Port = 587;
                        SmtpServer.UseDefaultCredentials = false;
                        SmtpServer.Credentials = new System.Net.NetworkCredential("nepalksystem@gmail.com", "Nepalkcing@123");
                        SmtpServer.EnableSsl = true;
                        SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                        SmtpServer.Host = "smtp.gmail.com";
                        SmtpServer.Send(mail);
                    }
                }
                MessageBox.Show("Success sending mail", "Success", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception e)
            {
                MessageBox.Show("Error sending mail due to: " + e.ToString(), "Failed", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private string MailAttachment_Build()
        {
            string templateText = File.ReadAllText(System.IO.Path.Combine(Global.TemplateDirectory, Global.ReportTemplateName));
            string contentHtml = "<ul id='LogItemsTree'>";
            contentHtml += NestedListView_Create(LogItemsTV.Items);
            contentHtml += "</ul>";
            templateText = templateText.Replace("{LogItemsTV}", contentHtml);

            Dictionary<string, dynamic> dataObjList = new Dictionary<string, dynamic>();
            foreach (string file in Directory.EnumerateFiles((ReportsGV.SelectedItem as ReportsRow).Tag.ToString(), "*.csv"))
            {
                var dataObj = new List<string[]>();
                var lines = System.IO.File.ReadAllLines(file);
                foreach (string line in lines)
                {
                    dataObj.Add(line.Split(','));
                }
                dataObjList[System.IO.Path.GetFileName(file)] = dataObj;
            }
            dataObjList["GeneralInfo"] = _testRun_GeneralInfo;
            dataObjList["DetailInfo"] = _testRun_DetailInfo;
            string dataObjListStr = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(dataObjList);
            templateText = templateText.Replace("{testRunData}", dataObjListStr);

            return templateText;
        }

        private void ServerRecord_Save(string projectName = CONST.DEFAULT_PROJECT_NAME)
        {
            //Insert LogReportMaster
            DateTime StartDateTime = DateTime.Parse(_testRun_GeneralInfo["StartTime"]);
            DateTime EndDateTime = DateTime.Parse(_testRun_GeneralInfo["EndTime"]);
            LogReportDataSet._TTCLogReportMasterRow masterDataRow = logReportDataSet._TTCLogReportMaster.New_TTCLogReportMasterRow();
            masterDataRow.SiteSeq = SiteSeq;
            masterDataRow.ProjectName = projectName;
            masterDataRow.StartDateTime = StartDateTime;
            masterDataRow.EndDateTime = EndDateTime;
            logReportDataSet._TTCLogReportMaster.Rows.Add(masterDataRow);
            LogReportDataSetTableAdapters._TTCLogReportMasterTableAdapter _TTCLogReportMasterTableAdapter = new LogReportDataSetTableAdapters._TTCLogReportMasterTableAdapter();
            int LogSeq = (int)_TTCLogReportMasterTableAdapter.Insert_TTCLogReportMaster(SiteSeq, projectName, StartDateTime, EndDateTime);
            _TTCLogReportMasterTableAdapter.Dispose();

            //Insert LogReportSub
            string ExportLogDetail = System.IO.Path.Combine(Global.AzaleaDataDirectory, Global.ExportLogDetailName);
            if (File.Exists(ExportLogDetail))
            {
                string line;
                string[] strArray;
                LogReportDataSet._TTCLogReportSubRow subDataRow;
                using (StreamReader sr = new StreamReader(ExportLogDetail))
                {
                    line = sr.ReadLine();
                    while ((line = sr.ReadLine()) != null)
                    {
                        strArray = line.Split(',');

                        subDataRow = logReportDataSet._TTCLogReportSub.New_TTCLogReportSubRow();
                        subDataRow.LogSeq = LogSeq;
                        subDataRow.LogSubSeq = Int32.Parse(strArray[0]);
                        subDataRow.TestCaseID = strArray[1];
                        subDataRow.TestCaseSubID = strArray[2];
                        subDataRow.LogReport = strArray[3];
                        subDataRow.IsFaile = strArray[4];
                        subDataRow.ExcuteDateTime = TimeZoneInfo.ConvertTimeToUtc(DateTime.ParseExact(strArray[5], "yyyyMMddHHmmss", CultureInfo.InvariantCulture, DateTimeStyles.None));
                        logReportDataSet._TTCLogReportSub.Rows.Add(subDataRow);
                    }
                }
                LogReportDataSetTableAdapters._TTCLogReportSubTableAdapter ttcLogReportSub = new LogReportDataSetTableAdapters._TTCLogReportSubTableAdapter();
                ttcLogReportSub.Update(logReportDataSet._TTCLogReportSub);
                ttcLogReportSub.Dispose();
            }

        }

        private string NestedListView_Create(ItemCollection Nodes)
        {
            string nodeString = "";
            foreach (TreeViewItem node in Nodes)
            {
                if (node.Items.Count > 0)
                {
                    nodeString += "<li><span class='caret caret-down'></span><span class='textSpan' name='" + node.Uid + "' identity='" + node.Tag + "'>" + node.Header + "</span><ul class='nested active'>" + NestedListView_Create(node.Items) + "</ul></li>";
                }
                else
                {
                    nodeString += "<li><span class='textSpan' name='" + node.Uid + "' identity='" + node.Tag + "'>" + node.Header + "</span></li>";
                }
            }
            return nodeString;
        }

        private void Runner_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F5)
            {
                TestRun_Click(this, null);
            }
        }

        private void Settings_FormOpen(object sender, RoutedEventArgs e)
        {
            settingModule = new SettingModule(logReportDataSet);

            Button button = (sender as Button);
            System.Windows.Point locationFromScreen = button.PointToScreen(new System.Windows.Point(0, 0));
            PresentationSource source = PresentationSource.FromVisual(this);
            System.Windows.Point targetPoints = source.CompositionTarget.TransformFromDevice.Transform(locationFromScreen);
            settingModule.Left = targetPoints.X + button.Width - settingModule.Width;
            settingModule.Top = targetPoints.Y + button.Height;
            settingModule.Show();
        }
        #endregion


        /*********************************
        **********Tab TestCases***********
        *********************************/

        #region Tab TestCases
        private void Init_TestCasesTab()
        {
            _nodeTextBox.ToolTipProvider = new ToolTipProvider();
            TreeModel _model = new TreeModel();
            _tree.Model = _model;
            ChangeButtons();
            XmlCreationFromSQL();
            Tree_Load();
        }

        private void XmlCreationFromSQL(bool isSchedule = false, string projectName = CONST.DEFAULT_PROJECT_NAME)
        {
            //Save TestCaseList from sql to xml
            LogReportDataSetTableAdapters._TTCTestProjectTableAdapter _TestProjectTableAdapter = new LogReportDataSetTableAdapters._TTCTestProjectTableAdapter();
            LogReportDataSet._TTCTestProjectDataTable testCaseList = _TestProjectTableAdapter.QueryTestCaseList(projectName);

            string filePath;
            if (!isSchedule)
            {
                filePath = TC_FilePath;
            }
            else
            {
                filePath = System.IO.Path.Combine(Global.DataDirectory, projectName + "_" + Global.TCName_Schedule);
            }


            streamWriter = new StreamWriter(filePath, false, Encoding.UTF8);
            streamWriter.WriteLine("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
            streamWriter.WriteLine("<ArrayOfTestCaseItem xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">");


            foreach (LogReportDataSet._TTCTestProjectRow row in testCaseList.Rows)
            {
                if (row.ParentNodeId == "")
                {
                    streamWriter.WriteLine("<TestCaseItem id=\"" + row.NodeId + "\" name=\"" + row.NodeName + "\" selected=\"" + row.CheckState + "\" expanded=\"" + row.IsExpand + "\" module=\"" + row.Module + "\" description=\"" + row.Description + "\">");
                    streamWriter.WriteLine("</TestCaseItem>");
                }
            }
            streamWriter.WriteLine("</ArrayOfTestCaseItem>");
            streamWriter.Close();
            streamWriter.Dispose();

            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(filePath);

                foreach (LogReportDataSet._TTCTestProjectRow row in testCaseList.Rows)
                {
                    if (row.ParentNodeId != "")
                    {
                        string parentNodeId = row.ParentNodeId;
                        XmlNode parentNode = xmlDoc.SelectSingleNode("//TestCaseItem[@id='" + parentNodeId + "']");
                        XmlElement childNode = xmlDoc.CreateElement(CONST.TEST_CASE_ITEM);
                        childNode.SetAttribute("id", row.NodeId);
                        childNode.SetAttribute("name", row.NodeName);
                        childNode.SetAttribute("selected", row.CheckState.ToString());
                        childNode.SetAttribute("expanded", row.IsExpand.ToString());
                        childNode.SetAttribute("module", row.Module);
                        childNode.SetAttribute("description", row.Description);
                        parentNode.AppendChild(childNode);
                    }
                }
                xmlDoc.Save(filePath);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error Occured", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            _TestProjectTableAdapter.Dispose();
            testCaseList.Dispose();
        }

        private void Tree_Load(bool isSchedule = false, string scheduleName = "")
        {
            TreeModel _model;
            if (!isSchedule)
            {
                _model = (TreeModel)_tree.Model;
            }
            else
            {
                _model = (TreeModel)_tree_Schedule.Model;
            }
            _model.Nodes.Clear();
            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                if (!isSchedule)
                {
                    xmlDoc.Load(TC_FilePath);
                }
                else
                {
                    string scheduleFilePath = System.IO.Path.Combine(Global.DataDirectory, scheduleName + "_" + Global.TCName_Schedule);
                    if (isNewSchedule)
                    {
                        scheduleFilePath = System.IO.Path.Combine(Global.DataDirectory, Global.TCName_New_Schedule);
                        if (!isNewScheduleCreated)
                        {
                            File.Copy(TC_FilePath, scheduleFilePath, true);
                            isNewScheduleCreated = true;
                        }
                    }
                    xmlDoc.Load(scheduleFilePath);
                }
                XmlNodeList xmlNodeList = xmlDoc.DocumentElement.ChildNodes;
                Node node;
                int checkState;
                int expandState;
                foreach (XmlNode xmlNode in xmlNodeList)
                {
                    node = new Node(xmlNode.Attributes["name"].Value, xmlNode.Attributes["module"].Value, xmlNode.Attributes["description"].Value);
                    checkState = Convert.ToInt32(xmlNode.Attributes["selected"].Value);
                    if (checkState == CheckBoxType.CHECKEDVALUE)
                    {
                        node.CheckState = System.Windows.Forms.CheckState.Checked;
                    }
                    else if (checkState == CheckBoxType.INDETERMINATEVALUE)
                    {
                        node.CheckState = System.Windows.Forms.CheckState.Indeterminate;
                    }
                    _model.Nodes.Add(node);
                    expandState = Convert.ToInt32(xmlNode.Attributes["expanded"].Value);
                    if (expandState == 1)
                    {
                        if (!isSchedule)
                        {
                            _tree.Expanded -= Tree_Expanded;
                            _tree.FindNode(_model.GetPath(node)).IsExpanded = true;
                            _tree.Expanded += Tree_Expanded;
                        }
                        else
                        {
                            _tree_Schedule.Expanded -= Tree_Expanded;
                            _tree_Schedule.FindNode(_model.GetPath(node)).IsExpanded = true;
                            _tree_Schedule.Expanded += Tree_Expanded;
                        }
                    }
                    Tree_Load_Nested(xmlNode.ChildNodes, node, isSchedule);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error Occured", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Tree_Load_Nested(XmlNodeList xmlNodeList, Node parentNode, bool isSchedule = false)
        {
            TreeModel _model;
            if (!isSchedule)
            {
                _model = (TreeModel)_tree.Model;
            }
            else
            {
                _model = (TreeModel)_tree_Schedule.Model;
            }
            Node node;
            int checkState;
            int expandState;
            foreach (XmlNode xmlNode in xmlNodeList)
            {
                node = new Node(xmlNode.Attributes["name"].Value, xmlNode.Attributes["module"].Value, xmlNode.Attributes["description"].Value);
                checkState = Convert.ToInt32(xmlNode.Attributes["selected"].Value);
                if (checkState == CheckBoxType.CHECKEDVALUE)
                {
                    node.CheckState = System.Windows.Forms.CheckState.Checked;
                }
                else if (checkState == CheckBoxType.INDETERMINATEVALUE)
                {
                    node.CheckState = System.Windows.Forms.CheckState.Indeterminate;
                }

                parentNode.Nodes.Add(node);
                if (xmlNode.ChildNodes.Count > 0)
                {
                    expandState = Convert.ToInt32(xmlNode.Attributes["expanded"].Value);
                    if (expandState == 1)
                    {
                        if (!isSchedule)
                        {
                            _tree.Expanded -= Tree_Expanded;
                            _tree.FindNode(_model.GetPath(node)).IsExpanded = true;
                            _tree.Expanded += Tree_Expanded;
                        }
                        else
                        {
                            _tree_Schedule.Expanded -= Tree_Expanded;
                            _tree_Schedule.FindNode(_model.GetPath(node)).IsExpanded = true;
                            _tree_Schedule.Expanded += Tree_Expanded;
                        }
                    }
                    Tree_Load_Nested(xmlNode.ChildNodes, node, isSchedule);
                }
            }
        }

        private void Tree_Save()
        {
            TreeViewAdv tree;
            TreeModel model;
            if (!isNewSchedule)
            {
                tree = (TreeViewAdv)_tree;
                model = (TreeModel)_tree.Model;
                streamWriter = new StreamWriter(TC_FilePath, false, Encoding.UTF8);
            }
            else
            {
                tree = (TreeViewAdv)_tree_Schedule;
                model = (TreeModel)_tree_Schedule.Model;
                streamWriter = new StreamWriter(System.IO.Path.Combine(Global.DataDirectory, Global.TCName_New_Schedule), false, Encoding.UTF8);
            }

            streamWriter.WriteLine("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
            streamWriter.WriteLine("<ArrayOfTestCaseItem xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">");
            TreeNodeAdv treeNodeAdv;
            foreach (Node node in model.Nodes)
            {
                treeNodeAdv = tree.FindNode(model.GetPath(node));
                int selected = CheckBoxType.UNCHECKEDVALUE;
                if (node.CheckState == System.Windows.Forms.CheckState.Checked)
                {
                    selected = CheckBoxType.CHECKEDVALUE;
                }
                else if (node.CheckState == System.Windows.Forms.CheckState.Indeterminate)
                {
                    selected = CheckBoxType.INDETERMINATEVALUE;
                }

                streamWriter.WriteLine("<TestCaseItem id=\"" + Guid.NewGuid().ToString("N") + "\" name=\"" + node.Text + "\" selected=\"" + selected + "\" expanded=\"" + Convert.ToInt32(treeNodeAdv.IsExpanded) + "\" module=\"" + node.Module + "\" description=\"" + node.Description + "\">");
                Tree_Save_Nested(node.Nodes);
                streamWriter.WriteLine("</TestCaseItem>");
            }
            streamWriter.WriteLine("</ArrayOfTestCaseItem>");
            streamWriter.Close();
            streamWriter.Dispose();
            if (isNewSchedule)
            {
                return;
            }
            _saveTree.IsEnabled = true;
        }

        private void Tree_Save_Nested(Collection<Node> Nodes)
        {
            TreeViewAdv tree;
            TreeModel model;
            if (!isNewSchedule)
            {
                tree = (TreeViewAdv)_tree;
                model = (TreeModel)_tree.Model;
            }
            else
            {
                tree = (TreeViewAdv)_tree_Schedule;
                model = (TreeModel)_tree_Schedule.Model;
            }

            TreeNodeAdv treeNodeAdv;
            foreach (Node node in Nodes)
            {
                int selected = CheckBoxType.UNCHECKEDVALUE;
                if (node.CheckState == System.Windows.Forms.CheckState.Checked)
                {
                    selected = CheckBoxType.CHECKEDVALUE;
                }
                else if (node.CheckState == System.Windows.Forms.CheckState.Indeterminate)
                {
                    selected = CheckBoxType.INDETERMINATEVALUE;
                }
                if (node.Nodes.Count > 0)
                {
                    treeNodeAdv = tree.FindNode(model.GetPath(node));

                    streamWriter.WriteLine("<TestCaseItem id=\"" + Guid.NewGuid().ToString("N") + "\" name=\"" + node.Text + "\" selected=\"" + selected + "\" expanded=\"" + Convert.ToInt32(treeNodeAdv.IsExpanded) + "\" module=\"" + node.Module + "\" description=\"" + node.Description + "\">");
                    Tree_Save_Nested(node.Nodes);
                    streamWriter.WriteLine("</TestCaseItem>");
                }
                else
                {
                    streamWriter.WriteLine("<TestCaseItem id=\"" + Guid.NewGuid().ToString("N") + "\" name=\"" + node.Text + "\" selected=\"" + selected + "\" expanded=\"0\" module=\"" + node.Module + "\" description=\"" + node.Description + "\"></TestCaseItem>");
                }
            }
        }

        private void Tree_Update(Node node, string attribute, string value, bool isSchedule = false)
        {
            if (node != null)
            {
                List<int> indexList = new List<int>() { node.Index };
                Node parentNode = node.Parent;
                while (parentNode.Index != -1)
                {
                    indexList.Add(parentNode.Index);
                    parentNode = parentNode.Parent;
                }
                indexList.Reverse();

                XmlDocument xmlDoc = new XmlDocument();

                try
                {
                    string tcFilePath = "";
                    if (!isSchedule)
                    {
                        tcFilePath = TC_FilePath;
                    }
                    else
                    {
                        if (isNewSchedule)
                        {
                            tcFilePath = System.IO.Path.Combine(Global.DataDirectory, Global.TCName_New_Schedule);
                        }
                        else
                        {
                            TestSchedulesRow selectedRowData = TestScheduleGridView.SelectedItem as TestSchedulesRow;
                            tcFilePath = System.IO.Path.Combine(Global.DataDirectory, selectedRowData.ScheduleName + "_" + Global.TCName_Schedule);
                        }
                    }
                    xmlDoc.Load(tcFilePath);
                    XmlNodeList xmlNodeList = xmlDoc.DocumentElement.ChildNodes;
                    XmlNode xmlNode = xmlNodeList[indexList[0]];
                    for (int i = 1; i < indexList.Count; i++)
                    {
                        xmlNode = xmlNode.ChildNodes[indexList[i]];
                    }
                    xmlNode.Attributes[attribute].Value = value;
                    xmlDoc.Save(tcFilePath);
                    if (isSchedule)
                    {
                        return;
                    }
                    _saveTree.IsEnabled = true;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error Occured", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void ClearClick(object sender, EventArgs e)
        {
            MessageBoxResult messageBoxResult = MessageBox.Show("Are you sure to clear ?\nUpdates locally only.", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (messageBoxResult != MessageBoxResult.Yes)
            {
                return;
            }

            TreeModel _model = (TreeModel)_tree.Model;
            _model.Nodes.Clear();
            Tree_Save();
        }

        private void AddRootClick(object sender, EventArgs e)
        {
            TreeModel _model = (TreeModel)_tree.Model;
            Node node = new Node(CONST.TEST_CASE_ITEM + (_model.Nodes.Count + 1).ToString(), "", "");
            _model.Nodes.Add(node);
            _tree.SelectedNode = _tree.FindNode(_model.GetPath(node));
            Tree_Save();
        }

        private void AddChildClick(object sender, EventArgs e)
        {
            if (_tree.SelectedNode != null)
            {
                Node parent = _tree.SelectedNode.Tag as Node;
                Node node = new Node(CONST.TEST_CASE_ITEM + (parent.Nodes.Count + 1).ToString(), "", "");
                parent.Nodes.Add(node);
                _tree.Expanded -= Tree_Expanded;
                _tree.SelectedNode.IsExpanded = true;
                _tree.Expanded += Tree_Expanded;
                Tree_Save();
            }
        }

        private void DeleteClick(object sender, EventArgs e)
        {
            if (_tree.SelectedNode != null)
            {
                (_tree.SelectedNode.Tag as Node).Parent = null;
                Tree_Save();
            }
        }

        private void SaveTreeClick(object sender, RoutedEventArgs e)
        {
            MessageBoxResult messageBoxResult = MessageBox.Show("Are you sure to save ?\nUpdates server.", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (messageBoxResult != MessageBoxResult.Yes)
            {
                return;
            }

            //Delete Old Records
            LogReportDataSetTableAdapters._TTCTestProjectTableAdapter _TestProjectTableAdapter = new LogReportDataSetTableAdapters._TTCTestProjectTableAdapter();
            _TestProjectTableAdapter.DeleteOldTestTree(CONST.DEFAULT_PROJECT_NAME);

            //Insert Updated Records
            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                xmlDoc.Load(TC_FilePath);
                XmlNodeList xmlNodeList = xmlDoc.DocumentElement.ChildNodes;
                testProjectNodeOrder = 0;
                testProjectDT = new LogReportDataSet._TTCTestProjectDataTable();
                XmlToSql(xmlNodeList, "");

                _TestProjectTableAdapter.Update(testProjectDT);

                _saveTree.IsEnabled = false;
                MessageBox.Show("Test tree saved", "Success", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error Occured", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            finally
            {
                _TestProjectTableAdapter.Dispose();
                testProjectDT.Dispose();
            }
        }

        private void XmlToSql(XmlNodeList xmlNodeList, string parendNodeId, string projectName = CONST.DEFAULT_PROJECT_NAME)
        {
            LogReportDataSet._TTCTestProjectRow dataRow;
            foreach (XmlNode xmlNode in xmlNodeList)
            {
                dataRow = logReportDataSet._TTCTestProject.New_TTCTestProjectRow();
                dataRow.ProjectName = projectName;
                dataRow.ParentNodeId = parendNodeId;
                dataRow.NodeId = xmlNode.Attributes["id"].Value;
                dataRow.NodeOrder = ++testProjectNodeOrder;
                dataRow.NodeName = xmlNode.Attributes["name"].Value;
                dataRow.CheckState = Convert.ToInt32(xmlNode.Attributes["selected"].Value);
                dataRow.IsExpand = Convert.ToInt32(xmlNode.Attributes["expanded"].Value);
                dataRow.Module = xmlNode.Attributes["module"].Value;
                dataRow.Description = xmlNode.Attributes["description"].Value;

                testProjectDT.Rows.Add(dataRow.ItemArray);

                if (xmlNode.ChildNodes.Count > 0)
                {
                    XmlToSql(xmlNode.ChildNodes, dataRow.NodeId, projectName);
                }
            }
        }

        private void Tree_SelectionChanged(object sender, EventArgs e)
        {
            ChangeButtons();
        }

        private void ChangeButtons()
        {
            _addChild.IsEnabled = _deleteNode.IsEnabled = (_tree.SelectedNode != null);
        }

        private void Tree_ItemDrag(object sender, System.Windows.Forms.ItemDragEventArgs e)
        {
            TreeNodeAdv[] nodes = new TreeNodeAdv[_tree.SelectedNodes.Count];
            _tree.SelectedNodes.CopyTo(nodes, 0);
            _tree.DoDragDrop(nodes, System.Windows.Forms.DragDropEffects.Move);
        }

        private void Tree_DragOver(object sender, System.Windows.Forms.DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(TreeNodeAdv[])) && _tree.DropPosition.Node != null)
                e.Effect = e.AllowedEffect;
            else
                e.Effect = System.Windows.Forms.DragDropEffects.None;
        }

        private void Tree_DragDrop(object sender, System.Windows.Forms.DragEventArgs e)
        {
            TreeNodeAdv[] nodes = (TreeNodeAdv[])e.Data.GetData(typeof(TreeNodeAdv[]));
            Node dropNode = _tree.DropPosition.Node.Tag as Node;
            bool isSelfDrop = true;
            if (_tree.DropPosition.Position == NodePosition.Inside)
            {
                foreach (TreeNodeAdv n in nodes)
                {
                    if (dropNode.Parent != null && isSelfDrop)
                    {
                        Node parentNode = dropNode.Parent;
                        while (parentNode != null)
                        {
                            if (parentNode == (n.Tag as Node))
                            {
                                return;
                            }
                            parentNode = parentNode.Parent;
                        }
                        isSelfDrop = false;
                    }

                    (n.Tag as Node).Parent = dropNode;
                }
                _tree.Expanded -= Tree_Expanded;
                _tree.DropPosition.Node.IsExpanded = true;
                _tree.Expanded += Tree_Expanded;
            }
            else
            {
                Node parent = dropNode.Parent;
                Node nextItem = dropNode;
                if (_tree.DropPosition.Position == NodePosition.After)
                    nextItem = dropNode.NextNode;

                foreach (TreeNodeAdv node in nodes)
                {
                    if (dropNode.Parent != null && isSelfDrop)
                    {
                        Node parentNode = dropNode.Parent;
                        while (parentNode != null)
                        {
                            if (parentNode == (node.Tag as Node))
                            {
                                return;
                            }
                            parentNode = parentNode.Parent;
                        }
                        isSelfDrop = false;
                    }
                    (node.Tag as Node).Parent = null;
                }

                int index = parent.Nodes.IndexOf(nextItem);
                foreach (TreeNodeAdv node in nodes)
                {
                    Node item = node.Tag as Node;
                    if (index == -1)
                        parent.Nodes.Add(item);
                    else
                    {
                        parent.Nodes.Insert(index, item);
                        index++;
                    }
                }
            }
            Tree_Save();
        }

        private void Tree_Collapsed(object sender, TreeViewAdvEventArgs e)
        {
            TreeViewAdv treeView = (TreeViewAdv)sender;
            string treeName = treeView.AccessibleName;

            Node node = (e.Node.Tag as Node);
            if (node != null)
            {
                TreePath path = ((TreeModel)treeView.Model).GetPath(node);
                treeView.FindNode(path).IsExpanded = false;
                Tree_Update(node, "expanded", "0", IsScheduleTreeView(treeName));
            }

        }

        private static bool IsScheduleTreeView(string treeName)
        {
            return !IsDirectTreeView(treeName);
        }

        private static bool IsDirectTreeView(string treeName)
        {
            return treeName == "_tree";
        }

        private void Tree_Expanded(object sender, TreeViewAdvEventArgs e)
        {
            TreeViewAdv treeView = (TreeViewAdv)sender;
            string treeName = treeView.AccessibleName;
            Node node = (e.Node.Tag as Node);
            if (node != null)
            {
                TreePath path = ((TreeModel)treeView.Model).GetPath(node);
                treeView.FindNode(path).IsExpanded = true;
                Tree_Update(node, "expanded", "1", IsScheduleTreeView(treeName));
            }

        }

        private void NodeCheckBox_CheckStateChanged(object sender, TreePathEventArgs e)
        {
            Object[] fullPath = e.Path.FullPath;

            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                xmlDoc.Load(TC_FilePath);
                XmlNodeList xmlNodeList = xmlDoc.DocumentElement.ChildNodes;
                XmlNode xmlNode = xmlNodeList[(fullPath[0] as Node).Index];
                for (int i = 1; i < fullPath.Length; i++)
                {
                    xmlNode = xmlNode.ChildNodes[(fullPath[i] as Node).Index];
                }

                int checkState = Convert.ToInt32(xmlNode.Attributes["selected"].Value);
                if (xmlNode.ParentNode.Attributes["selected"] == null)
                {
                    if (checkState == CheckBoxType.UNCHECKEDVALUE)
                    {
                        xmlNode.Attributes["selected"].Value = CheckBoxType.CHECKEDVALUE.ToString();
                    }
                    else
                    {
                        xmlNode.Attributes["selected"].Value = CheckBoxType.UNCHECKEDVALUE.ToString();
                    }
                }
                else
                {
                    int parentCheckState = Convert.ToInt32(xmlNode.ParentNode.Attributes["selected"].Value);
                    if (parentCheckState == CheckBoxType.CHECKEDVALUE)
                    {
                        if (checkState == CheckBoxType.UNCHECKEDVALUE)
                        {
                            xmlNode.Attributes["selected"].Value = CheckBoxType.CHECKEDVALUE.ToString();
                        }
                        else
                        {
                            xmlNode.Attributes["selected"].Value = CheckBoxType.UNCHECKEDVALUE.ToString();
                        }
                    }
                    else
                    {
                        if (checkState == CheckBoxType.UNCHECKEDVALUE)
                        {
                            xmlNode.Attributes["selected"].Value = CheckBoxType.INDETERMINATEVALUE.ToString();
                        }
                        else
                        {
                            xmlNode.Attributes["selected"].Value = CheckBoxType.UNCHECKEDVALUE.ToString();
                        }
                    }
                }

                checkState = Convert.ToInt32(xmlNode.Attributes["selected"].Value);
                if (xmlNode.ChildNodes.Count > 0 && checkState != CheckBoxType.INDETERMINATEVALUE)
                {
                    SetChildCheckBoxState(xmlNode.ChildNodes, checkState);
                }
                xmlDoc.Save(TC_FilePath);
                _saveTree.IsEnabled = true;
                Tree_Load();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error Occured", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void SetChildCheckBoxState(XmlNodeList xmlNodes, int parentCheckState)
        {
            foreach (XmlNode xmlNode in xmlNodes)
            {
                int checkState = Convert.ToInt32(xmlNode.Attributes["selected"].Value);
                if (checkState == CheckBoxType.CHECKEDVALUE)
                {
                    checkState = CheckBoxType.INDETERMINATEVALUE;
                    xmlNode.Attributes["selected"].Value = checkState.ToString();
                    SetChildCheckBoxState(xmlNode.ChildNodes, checkState);
                }
                else if (checkState == CheckBoxType.INDETERMINATEVALUE)
                {
                    if (parentCheckState == CheckBoxType.UNCHECKEDVALUE)
                    {
                        checkState = CheckBoxType.INDETERMINATEVALUE;
                    }
                    else
                    {
                        checkState = CheckBoxType.CHECKEDVALUE;
                    }

                    xmlNode.Attributes["selected"].Value = checkState.ToString();
                    SetChildCheckBoxState(xmlNode.ChildNodes, checkState);
                }
            }
        }

        private void NodeTextBox_LabelChanged(object sender, EventArgs e)
        {
            TreeViewAdv treeView = ((NodeTextBox)sender).Parent;
            Node node = (treeView.SelectedNode.Tag as Node);
            string newName = node.Text;
            if (newName == "")
            {
                if (node.Parent.Text == "")
                {
                    node.Text = CONST.TEST_CASE_ITEM + ((treeView.Model as TreeModel).Nodes.Count + 1).ToString();
                }
                else
                {
                    node.Text = CONST.TEST_CASE_ITEM + (node.Parent.Nodes.Count + 1).ToString();
                }
                newName = node.Text;
            }
            Tree_Update(node, "name", newName);
        }

        private void Module_EditorShowing(object sender, CancelEventArgs e)
        {
            selectModule = new SelectModule();
            if (selectModule.ShowDialog() == true)
            {
                string classSelected = selectModule._classNameSelected;
                string methodSelected = selectModule._methodNameSelected;

                if (classSelected == "" || methodSelected == "")
                {
                    _module.SetValue(_tree.SelectedNode, "");
                }
                else
                {
                    _module.SetValue(_tree.SelectedNode, classSelected + "/" + methodSelected);
                }

                Node node = (_tree.SelectedNode.Tag as Node);
                Tree_Update(node, "module", node.Module);
            }

            Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() => System.Windows.Forms.SendKeys.SendWait("{Enter}")));
        }

        private void Description_LabelChanged(object sender, EventArgs e)
        {
            TreeViewAdv treeView = ((NodeTextBox)sender).Parent;
            Node node = (treeView.SelectedNode.Tag as Node);
            Tree_Update(node, "description", node.Description);
        }
        #endregion


        /*********************************
        ************Tab Result************
        *********************************/

        #region Tab Result

        private void Workspace_SelectedChanged(object sender, SelectionChangedEventArgs e)
        {
            if (((FrameworkElement)e.OriginalSource).Name == "Workspace")
            {
                string selectedTab = ((HeaderedContentControl)Workspace.SelectedValue).Name;
                switch (selectedTab)
                {
                    case "ReportTab":
                        Init_LogLoad();
                        break;
                    case "ScheduleExecutionTab":
                        Init_ScheduleGridList();
                        break;
                    default:
                        Init_TestCasesTab();
                        break;
                }
            }
        }

        private void Init_LogLoad()
        {
            ReportsGV.Items.Clear();

            string[] reports = Directory.GetDirectories(Global.ReportDirectory);
            string reportName;
            string reportDisplayName;
            for (int i = 0; i < reports.Length; i++)
            {
                try
                {
                    reportName = reports[i].Substring(reports[i].LastIndexOf('\\') + 1);
                    reportDisplayName = DateTime.ParseExact(reportName, "yyyyMMddHHmmss", CultureInfo.InvariantCulture, DateTimeStyles.None).ToString(Global.DateFormatString);

                    ReportsGV.Items.Add(new ReportsRow { Tag = reports[i], DisplayName = reportDisplayName });
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            int reportsCount = ReportsGV.Items.Count;
            if (reportsCount > 0)
            {
                ReportsGV.SelectedIndex = reportsCount - 1;
                ReportsGV_PreviewMouseUp(null, null);
            }
            else
            {
                TestCaseInfoPanel.Visibility = Visibility.Hidden;
                LogItemFilterPanel.Visibility = Visibility.Hidden;
                LogDetailsGV.Visibility = Visibility.Hidden;
                SummaryPanel.Visibility = Visibility.Hidden;
            }
        }

        private void ReportsGV_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            if (!(ReportsGV.SelectedItem is ReportsRow item))
            {
                LogItemsTV.Items.Clear();
                TestCaseInfoPanel.Visibility = Visibility.Hidden;
                LogItemFilterPanel.Visibility = Visibility.Hidden;
                LogDetailsGV.Visibility = Visibility.Hidden;
                SummaryPanel.Visibility = Visibility.Hidden;
            }
            else
            {
                string reportPath = System.IO.Path.Combine(item.Tag, Global.ReportLogRootName);
                var logItems = from line in File.ReadLines(reportPath).Skip(1)
                               let x = line.Split(',')
                               select new
                               {
                                   TestItemPath = x[0],
                                   TestItemPathArray = x[0].Split('/'),
                                   ClassName = x[1],
                                   MethodName = x[2],
                                   IdentityPathArray = x[3].Split('/'),
                               };

                LogItemsTV.Items.Clear();
                TreeViewItem mainNode = new TreeViewItem()
                {
                    Uid = "Summary",
                    Header = "Summary",
                    IsExpanded = true,
                    FontWeight = FontWeights.Bold
                };
                LogItemsTV.Items.Add(mainNode);

                foreach (var logItem in logItems)
                {
                    string[] nodesNamePath = logItem.TestItemPathArray;
                    string[] nodesIdentityPath = logItem.IdentityPathArray;

                    TreeViewItem parentNode = ChildNode_Add(mainNode, nodesIdentityPath[0], nodesNamePath[0]);
                    parentNode.Tag = nodesIdentityPath[0];
                    for (int i = 1; i < nodesNamePath.Length; i++)
                    {
                        parentNode = ChildNode_Add(parentNode, nodesIdentityPath[i], nodesNamePath[i]);
                        parentNode.Tag = nodesIdentityPath[i];
                    }
                    parentNode.Uid = logItem.TestItemPath;
                }
                mainNode.IsSelected = true;
                ReportsGV.Focus();
            }
        }

        private TreeViewItem ChildNode_Add(TreeViewItem parentNode, string childNodeIdentity, string childNodeText)
        {
            TreeViewItem childNode = new TreeViewItem()
            {
                IsExpanded = true,
                FontWeight = FontWeights.Normal
            };
            foreach (TreeViewItem node in parentNode.Items)
            {
                if (node.Tag.ToString() == childNodeIdentity)
                {
                    childNode = node;
                }
            }
            if (childNode.Header == null)
            {
                childNode.Header = childNodeText;
                parentNode.Items.Add(childNode);
            }
            return childNode;
        }

        private void ReportsGV_PreviewMouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            DependencyObject dep = (DependencyObject)e.OriginalSource;
            while ((dep != null) && !(dep is DataGridRow))
            {
                dep = VisualTreeHelper.GetParent(dep);
            }

            if (dep == null)
            {
                ReportsGV.ContextMenu.Visibility = Visibility.Collapsed;
                return;
            }
            else
            {
                ReportsGV.SelectedItem = dep;
                ReportsGV.ContextMenu.Visibility = Visibility.Visible;
            }
        }

        private void ReportsGVContextMenu_ItemClicked(object sender, RoutedEventArgs e)
        {
            if (((HeaderedItemsControl)sender).Header.ToString() == "DeleteItem")
            {
                MessageBoxResult result = MessageBox.Show("Are you sure want to delete selected report log ?", "Confirm Delete", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result == MessageBoxResult.Yes)
                {
                    ReportsRow currItem = ReportsGV.SelectedItem as ReportsRow;
                    string ReportLogDirectory = currItem.Tag;
                    Directory.Delete(ReportLogDirectory, true);
                    int currRowIndex = ReportsGV.SelectedIndex;
                    ReportsGV.Items.RemoveAt(currRowIndex);

                    if (ReportsGV.Items.Count > 0)
                    {
                        if (currRowIndex == ReportsGV.Items.Count)
                        {
                            ReportsGV.SelectedItem = ReportsGV.Items[currRowIndex - 1];
                        }
                        else
                        {
                            ReportsGV.SelectedItem = ReportsGV.Items[currRowIndex];
                        }
                        ReportsGV_PreviewMouseUp(null, null);
                    }
                    else
                    {
                        LogItemsTV.Items.Clear();
                        TestCaseInfoPanel.Visibility = Visibility.Hidden;
                        LogItemFilterPanel.Visibility = Visibility.Hidden;
                        LogDetailsGV.Visibility = Visibility.Hidden;
                        SummaryPanel.Visibility = Visibility.Hidden;
                    }

                }
            }
        }

        private void ReportsGV_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete && ReportsGV.SelectedIndex > -1)
            {
                ReportsGVContextMenu_ItemClicked(ReportsGVContextMenu.Items[0], null);
            }
        }

        private void LogItemsTV_AfterSelect(object sender, RoutedEventArgs e)
        {
            HeaderedItemsControl node = (HeaderedItemsControl)e.OriginalSource;
            object logTag = node.Tag;
            string logUid = node.Uid;
            if (logTag == null)
            {
                SummaryLogItem_Load();
            }
            else if (logUid == "")
            {
                PathLogItem_Load();
                LogItemFilterChk_Changed(null, null);
            }
            else
            {
                LogItem_Load(logTag.ToString());
                LogItemFilterChk_Changed(null, null);
            }
        }

        private string SecondsToHMS(double seconds)
        {
            TimeSpan time = TimeSpan.FromSeconds(seconds);
            if (time.Hours == 0)
            {
                if (time.Minutes == 0)
                {
                    return time.ToString(@"ss\s");
                }
                return time.ToString(@"mm\m\ ss\s");
            }
            else
            {
                return time.ToString(@"hh\h\ mm\m\ ss\s");
            }
        }

        private void GeneralDetailInfo_Init()
        {
            string summaryItemPath = System.IO.Path.Combine((ReportsGV.SelectedItem as ReportsRow).Tag, Global.TestRunInfoName);
            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                xmlDoc.Load(summaryItemPath);
                XmlNodeList xmlNodeList = xmlDoc.DocumentElement.ChildNodes;

                XmlElement testRunNode = xmlDoc.DocumentElement;
                _testRun_GeneralInfo = new Dictionary<string, string>
                {
                    { "Result", testRunNode.Attributes["result"].Value },
                    { "StartTime", testRunNode.Attributes["start-time"].Value },
                    { "EndTime", testRunNode.Attributes["end-time"].Value },
                    { "Duration", testRunNode.Attributes["duration"].Value }
                };

                XmlNode testSuiteNode = testRunNode.SelectSingleNode("//test-suite");
                _testRun_GeneralInfo.Add("Failed", testSuiteNode.Attributes["failed"].Value);
                _testRun_GeneralInfo.Add("Warnings", testSuiteNode.Attributes["warnings"].Value);
                _testRun_GeneralInfo.Add("Passed", testSuiteNode.Attributes["passed"].Value);

                XmlNode environmentNode = testRunNode.SelectSingleNode("//environment");
                _testRun_GeneralInfo.Add("Platform", environmentNode.Attributes["platform"].Value);
                _testRun_GeneralInfo.Add("MachineName", environmentNode.Attributes["machine-name"].Value);
                _testRun_GeneralInfo.Add("User", environmentNode.Attributes["user"].Value);

                string reportPath = System.IO.Path.Combine((ReportsGV.SelectedItem as ReportsRow).Tag, Global.ReportLogRootName);
                List<string> logItems = (from line in File.ReadLines(reportPath).Skip(1)
                                         let x = line.Split(',')
                                         select new
                                         {
                                             Identity = x[3]
                                         }.Identity).ToList();

                XmlNodeList testcaseNodes = testRunNode.SelectNodes("//test-case");
                _testRun_DetailInfo = new Dictionary<string, Dictionary<string, string>>();
                XmlNode testcaseNode;
                string key = "";
                for (int i = 0; i < logItems.Count(); i++)
                {
                    testcaseNode = testcaseNodes[i];
                    key = logItems[i].Substring(logItems[i].LastIndexOf('/') + 1);
                    _testRun_DetailInfo.Add(key, new Dictionary<string, string>
                    {
                        { "Result", testcaseNode.Attributes["result"].Value },
                        { "StartTime", testcaseNode.Attributes["start-time"].Value },
                        { "EndTime", testcaseNode.Attributes["end-time"].Value },
                        { "Duration", testcaseNode.Attributes["duration"].Value }

                    });
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error Occured", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void SummaryLogItem_Load()
        {
            TestCaseInfoPanel.Visibility = Visibility.Hidden;
            LogItemFilterPanel.Visibility = Visibility.Hidden;
            LogDetailsGV.Visibility = Visibility.Hidden;
            SummaryPanel.Visibility = Visibility.Visible;
            Grid.SetRowSpan(LogItemsTV, 3);
            GeneralDetailInfo_Init();
            GeneralSummary_Load();
            PieChart_Load();
        }

        private void GeneralSummary_Load()
        {
            string result = _testRun_GeneralInfo["Result"];
            string imageName = "";
            if (result == "Failed")
            {
                imageName = "Resources/Failed.png";
                ResultValue.Foreground = new SolidColorBrush(Colors.IndianRed);

            }
            else if (result == "Warning")//Not occured yet
            {
                imageName = "Resources/Warning.png";
                ResultValue.Foreground = new SolidColorBrush(Colors.DarkOrange);
            }
            else if (result == "Passed")
            {
                imageName = "Resources/Passed.png";
                ResultValue.Foreground = new SolidColorBrush(Colors.LimeGreen);
            }
            BitmapImage bitmapImage = new BitmapImage(new Uri(imageName, UriKind.RelativeOrAbsolute));
            ResultPicture.Source = bitmapImage;
            ResultValue.Text = result;

            TestStartedValue.Text = DateTime.Parse(_testRun_GeneralInfo["StartTime"]).ToString(Global.DateFormatString);
            TestEndedValue.Text = DateTime.Parse(_testRun_GeneralInfo["EndTime"]).ToString(Global.DateFormatString);
            TestDurationValue.Text = SecondsToHMS(Convert.ToDouble(_testRun_GeneralInfo["Duration"]));
            PlatformValue.Text = _testRun_GeneralInfo["Platform"];
            MachineValue.Text = _testRun_GeneralInfo["MachineName"];
            UserValue.Text = _testRun_GeneralInfo["User"];
        }

        private void PieChart_Load()
        {
            int failedCases = Convert.ToInt32(_testRun_GeneralInfo["Failed"]);
            int warningCases = Convert.ToInt32(_testRun_GeneralInfo["Warnings"]);
            int passedCases = Convert.ToInt32(_testRun_GeneralInfo["Passed"]);
            ((PieSeries)LogReportPC.Series[0]).ItemsSource = new Dictionary<string, int> {
                { "Failed (" + failedCases + ")", failedCases },
                { "Passed with warnings (" + warningCases + ")", warningCases},
                {"Passed (" + passedCases + ")", passedCases }
            };
        }

        private void PathLogItem_Load()
        {
            ChangeLogItemFilterPanel("path");

            IEnumerable<TreeViewItem> logItems = LogItems_Search(LogItemsTV.SelectedItem as TreeViewItem).Where(item => item.Uid != "").Reverse();
            Dictionary<string, string> testCase_DetailInfo;
            string result;
            string totalResult = null;
            string startTime;
            string endTime;
            Double duration;
            string formattedDuration;
            Double totalDuration = 0;
            LogDetailsGV.Items.Clear();
            LogDetailsGV.Items.Filter = null;
            foreach (TreeViewItem logItem in logItems)
            {
                testCase_DetailInfo = _testRun_DetailInfo[logItem.Tag.ToString()];
                result = testCase_DetailInfo["Result"];
                if (totalResult == null)
                {
                    if (result == ReportLogItemType.ERROR || result == ReportLogItemType.WARNING)
                    {
                        totalResult = result;
                    }
                }
                startTime = DateTime.Parse(testCase_DetailInfo["StartTime"]).ToString(Global.DateFormatString).Split(' ')[1];
                endTime = DateTime.Parse(testCase_DetailInfo["EndTime"]).ToString(Global.DateFormatString).Split(' ')[1];
                duration = Convert.ToDouble(testCase_DetailInfo["Duration"]);
                formattedDuration = SecondsToHMS(duration);
                totalDuration += duration;

                LogDetailsGV.Items.Add(new LogDetailsPathRow { Type = GetLogItemIcon(result), HiddenType = result, Name = logItem.Uid, StartTime = startTime, EndTime = endTime, Duration = formattedDuration });
            }

            TestCaseResultValue.Text = totalResult ?? ReportLogItemType.SUCCESS;
            startTime = _testRun_DetailInfo[logItems.First().Tag.ToString()]["StartTime"];
            TestCaseStartedValue.Text = DateTime.Parse(startTime).ToString(Global.DateFormatString);
            endTime = _testRun_DetailInfo[logItems.Last().Tag.ToString()]["EndTime"];
            TestCaseEndedValue.Text = DateTime.Parse(endTime).ToString(Global.DateFormatString);
            TestCaseDurationValue.Text = SecondsToHMS(totalDuration);

            LogDetailsGV.Columns[2].Visibility = Visibility.Collapsed;
            LogDetailsGV.Columns[3].Visibility = Visibility.Collapsed;
            LogDetailsGV.Columns[4].Visibility = Visibility.Collapsed;
            LogDetailsGV.Columns[5].Visibility = Visibility.Visible;
            LogDetailsGV.Columns[6].Visibility = Visibility.Visible;
            LogDetailsGV.Columns[7].Visibility = Visibility.Visible;
            LogDetailsGV.Columns[8].Visibility = Visibility.Visible;
        }

        private string GetLogItemIcon(string logItemType)
        {
            string imageType;
            switch (logItemType)
            {
                case ReportLogItemType.ERROR:
                    imageType = "Resources/ErrorType.png";
                    break;
                case ReportLogItemType.WARNING:
                    imageType = "Resources/WarningType.png";
                    break;
                case ReportLogItemType.SUCCESS:
                    imageType = "Resources/SuccessType.png";
                    break;
                case ReportLogItemType.MESSAGE:
                    imageType = "Resources/MessageType.png";
                    break;
                case ReportLogItemType.EVENT:
                    imageType = "Resources/EventType.png";
                    break;
                default:
                    imageType = "Resources/CheckpointType.png";
                    break;
            }
            return imageType;
        }

        public IEnumerable<TreeViewItem> LogItems_Search(TreeViewItem root)
        {
            var stack = new Stack<TreeViewItem>();
            stack.Push(root);
            while (stack.Count > 0)
            {
                var current = stack.Pop();
                yield return current;
                foreach (TreeViewItem child in current.Items)
                    stack.Push(child);
            }
        }

        private void LogItem_Load(string fileName)
        {
            ChangeLogItemFilterPanel();

            Dictionary<string, string> testcaseItemInfo = _testRun_DetailInfo[fileName];
            TestCaseResultValue.Text = testcaseItemInfo["Result"];
            TestCaseStartedValue.Text = DateTime.Parse(testcaseItemInfo["StartTime"]).ToString(Global.DateFormatString);
            TestCaseEndedValue.Text = DateTime.Parse(testcaseItemInfo["EndTime"]).ToString(Global.DateFormatString);
            TestCaseDurationValue.Text = SecondsToHMS(Convert.ToDouble(testcaseItemInfo["Duration"]));

            string logItemCount = fileName;
            string logItemPath = System.IO.Path.Combine((ReportsGV.SelectedItem as ReportsRow).Tag.ToString(), logItemCount + ".csv");

            var logDetailsItems = from line in File.ReadLines(logItemPath).Skip(1)
                                  let x = line.Split(',')
                                  select new
                                  {
                                      Type = x[0],
                                      Message = x[1].Replace("|||", ",").Replace("###", Environment.NewLine),
                                      Time = x[2].Split(' ')[1],
                                      Picture = x[3]
                                  };
            LogDetailsGV.Items.Clear();
            LogDetailsGV.Items.Filter = null;
            foreach (var logDetailItem in logDetailsItems)
            {
                LogDetailsGV.Items.Add(new LogDetailsRow { Type = GetLogItemIcon(logDetailItem.Type), HiddenType = logDetailItem.Type, Message = logDetailItem.Message, Time = logDetailItem.Time, Picture = logDetailItem.Picture });
            }

            LogDetailsGV.Columns[5].Visibility = Visibility.Collapsed;
            LogDetailsGV.Columns[6].Visibility = Visibility.Collapsed;
            LogDetailsGV.Columns[7].Visibility = Visibility.Collapsed;
            LogDetailsGV.Columns[8].Visibility = Visibility.Collapsed;
            LogDetailsGV.Columns[2].Visibility = Visibility.Visible;
            LogDetailsGV.Columns[3].Visibility = Visibility.Visible;
            LogDetailsGV.Columns[4].Visibility = Visibility.Visible;
        }

        private void ChangeLogItemFilterPanel(string logItem = null)
        {
            if (logItem == "path")
            {
                TestCaseInfoPanel.Visibility = Visibility.Visible;
                ErrorFilterChk.Visibility = Visibility.Visible;
                WarningFilterChk.Visibility = Visibility.Visible;
                SuccessFilterChk.Visibility = Visibility.Visible;
                CheckpointFilterChk.Visibility = Visibility.Collapsed;
                MessageFilterChk.Visibility = Visibility.Collapsed;
                EventFilterChk.Visibility = Visibility.Collapsed;
                SearchFilterTxt.Visibility = Visibility.Collapsed;
            }
            else
            {
                TestCaseInfoPanel.Visibility = Visibility.Visible;

                string controlType;
                foreach (var c in LogItemFilterPanel.Children)
                {
                    controlType = c.GetType().Name;
                    if (controlType == "CheckBox")
                    {
                        (c as CheckBox).Visibility = Visibility.Visible;
                    }
                    else if (controlType == "TextBox")
                    {
                        (c as TextBox).Visibility = Visibility.Visible;
                    }
                }
                SuccessFilterChk.Visibility = Visibility.Collapsed;
            }

            Grid.SetRowSpan(LogItemsTV, 2);
            LogItemFilterPanel.Visibility = Visibility.Visible;
            LogDetailsGV.Visibility = Visibility.Visible;
            SummaryPanel.Visibility = Visibility.Collapsed;
        }

        private void LogItemFilterChk_Changed(object sender, RoutedEventArgs e)
        {
            if (sender != null)
            {
                CheckBox chkSelected = (sender as CheckBox);
                string chkName = chkSelected.Name;
                bool chkState = chkSelected.IsChecked ?? false;
                switch (chkName)
                {
                    case "ErrorFilterChk":
                        Properties.Settings.Default.ErrorFilter = chkState;
                        break;
                    case "WarningFilterChk":
                        Properties.Settings.Default.WarningFilter = chkState;
                        break;
                    case "CheckpointFilterChk":
                        Properties.Settings.Default.CheckpointFilter = chkState;
                        break;
                    case "MessageFilterChk":
                        Properties.Settings.Default.MessageFilter = chkState;
                        break;
                    case "EventFilterChk":
                        Properties.Settings.Default.EventFilter = chkState;
                        break;
                    case "SuccessFilterChk":
                        Properties.Settings.Default.SuccessFilter = chkState;
                        break;
                    default:
                        break;
                }
                Properties.Settings.Default.Save();
            }

            CheckBox chkControl;
            List<string> filterList = new List<string>();
            foreach (var c in LogItemFilterPanel.Children)
            {
                chkControl = (c as CheckBox);
                if (chkControl != null && chkControl.IsVisible && (chkControl.IsChecked ?? false))
                {
                    filterList.Add(chkControl.Tag.ToString());
                }
            }

            if ((LogItemsTV.SelectedItem as HeaderedItemsControl).Uid == "")
            {
                LogDetailsGV.Items.Filter = new Predicate<object>(item =>
                {
                    return (filterList.IndexOf((item as LogDetailsPathRow).HiddenType) != -1);
                });
            }
            else
            {
                var filterText = SearchFilterTxt.Tag.ToString().ToLower();
                if (filterText == "")
                {
                    LogDetailsGV.Items.Filter = new Predicate<object>(item =>
                    {
                        return (filterList.IndexOf((item as LogDetailsRow).HiddenType) != -1);
                    });
                }
                else
                {
                    LogDetailsGV.Items.Filter = new Predicate<object>(item =>
                    {
                        LogDetailsRow logDetailsRow = item as LogDetailsRow;
                        return (filterList.IndexOf(logDetailsRow.HiddenType) != -1 && logDetailsRow.Message.ToLower().Contains(filterText));
                    });
                }
            }
        }

        private void SearchFilterTxt_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            SearchFilterTxt.SelectAll();
            SearchFilterTxt.Focus();
        }

        private void SearchFilterTxt_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            var keyCode = e.Key;
            if (keyCode == Key.Enter)
            {
                string searchText = SearchFilterTxt.Text.ToString();
                SearchFilterTxt.Tag = searchText;
                if (searchText == "")
                {
                    SearchFilterTxt.Text = "Search";
                    SearchFilterTxt.SelectAll();
                }
                LogItemFilterChk_Changed(null, null);
            }
        }
        #endregion


        /*********************************
        ******Tab Schedule Execution******
        *********************************/

        #region Tab Schedule Execution
        private void TestScheduleGridView_AfterDataBinded()
        {
            foreach (TestSchedulesRow item in TestScheduleGridView.Items)
            {
                JobGridListAsync(item);
            }
            timer.Interval = TimeSpan.FromSeconds(10);
            timer.Tick += new EventHandler(Timer_Tick);
            timer.Start();
        }

        private async void JobGridListAsync(TestSchedulesRow item)
        {
            IScheduler scheduler = await SchedulerFactory.GetScheduler();
            if (!scheduler.IsStarted)
            {
                await scheduler.Start();
            }

            string scheduleName = item.ScheduleName;
            JobKey jobKey = new JobKey(scheduleName);

            List<string> dayList = new List<string>();

            string isMon = item.IsMon;
            if (isMon == "1")
            {
                dayList.Add("MON");
            }

            string isTue = item.IsTue;
            if (isTue == "1")
            {
                dayList.Add("TUE");
            }
            string isWed = item.IsWed;
            if (isWed == "1")
            {
                dayList.Add("WED");
            }
            string isThu = item.IsThu;
            if (isThu == "1")
            {
                dayList.Add("THU");
            }
            string isFri = item.IsFri;
            if (isFri == "1")
            {
                dayList.Add("FRI");
            }
            string joinedString = string.Join(",", dayList);
            int executeTime = Convert.ToInt32(item.StartingTime);
            string cronValue = GetCronValue(executeTime, joinedString);

            await CreateJobAndTrigger(scheduleName, scheduler, jobKey, cronValue);

            TriggerKey triggerKey = new TriggerKey(scheduleName + "Trigger");

            var triggerdetails = scheduler.GetTrigger(triggerKey);

            var nextStartAt = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(triggerdetails.Result.GetNextFireTimeUtc().Value, ((Quartz.Impl.Triggers.CronTriggerImpl)triggerdetails.Result).TimeZone.Id).ToString("yyyy/MM/dd hh:mm:ss,ddd");

            item.JobStartAt = nextStartAt;
            item.JobRunAfter = (triggerdetails.Result.GetNextFireTimeUtc().Value - DateTime.Now.ToUniversalTime()).ToString(@"dd\.hh\:mm\:ss");
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            RefreshTestScheduleGridViewAsync();
        }

        private async void RefreshTestScheduleGridViewAsync()
        {
            IScheduler scheduler = await SchedulerFactory.GetScheduler();
            var allTriggerKeys = scheduler.GetTriggerKeys(GroupMatcher<TriggerKey>.AnyGroup());
            foreach (var triggerKey in allTriggerKeys.Result)
            {
                var triggerdetails = scheduler.GetTrigger(triggerKey);
                var jobName = triggerdetails.Result.JobKey.Name;
                foreach (TestSchedulesRow item in TestScheduleGridView.Items)
                {
                    if (jobName == item.ScheduleName)
                    {
                        item.JobRunAfter = (triggerdetails.Result.GetNextFireTimeUtc().Value - DateTime.Now.ToUniversalTime()).ToString(@"dd\.hh\:mm\:ss");
                        break;
                    }
                }
            }
            TestScheduleGridView.Items.Refresh();
        }

        private void Init_ScheduleGridList()
        {
            _nodeTextBox.ToolTipProvider = new ToolTipProvider();
            TreeModel _model = new TreeModel();
            _tree_Schedule.Model = _model;

            ExecuteTimeCMB.ItemsSource = Enumerable.Range(0, 24).ToList();
            LogReportDataSetTableAdapters._TTCTestProjectTableAdapter _TestProjectTableAdapter = new LogReportDataSetTableAdapters._TTCTestProjectTableAdapter();

            List<object> scheduleNameList = (from dt in _TestProjectTableAdapter.GetData().AsEnumerable()
                                             where !dt["ProjectName"].Equals(CONST.DEFAULT_PROJECT_NAME)
                                             select dt["ProjectName"]).Distinct().ToList();

            foreach (string sName in scheduleNameList)
            {
                string filePath = System.IO.Path.Combine(Global.DataDirectory, sName + "_" + Global.TCName_Schedule);
                if (!File.Exists(filePath))
                {
                    XmlCreationFromSQL(true, sName);
                }
            }
            if (TestScheduleGridView.Items.Count != 0)
            {
                TestScheduleGridView.SelectedIndex = 0;
                DataGridCellInfo cellInfo = new DataGridCellInfo(TestScheduleGridView.SelectedItem, TestScheduleGridView.Columns[0]);
                TestScheduleGridView.CurrentCell = cellInfo;
                TestScheduleGridView_PreviewMouseUp(null, null);
            }
            else
            {
                NewBTN_Click(null, null);
            }
            _TestProjectTableAdapter.Dispose();
        }

        internal void UncheckTreeNodes(Collection<Node> nodes)
        {
            foreach (Node node in nodes)
            {
                if (node.Nodes.Count != 0)
                {
                    UncheckTreeNodes(node.Nodes);
                }
                node.IsChecked = false;
            }
        }

        private void TestScheduleGridView_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            DataGridRow row = e.Row;
            row.Header = (row.GetIndex() + 1);
        }

        private void TestScheduleGridView_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            int colIndex = 0;
            var column = TestScheduleGridView.CurrentCell.Column;
            if (column != null)
            {
                colIndex = column.DisplayIndex;
            }

            isNewSchedule = false;
            if (colIndex <= 11)
            {
                SetScheduleCanvas();
            }
            else if (colIndex == 12)
            {
                DeleteScheduleTest();
            }
        }

        private void SetScheduleCanvas()
        {
            ResetScheduleCanvas();

            if (!(TestScheduleGridView.SelectedItem is TestSchedulesRow item))
            {
                return;
            }

            //Set ScheduleName
            //ScheduleSeqLBL.Text = item.ScheduleSeq + "|" + rowIdx + "|" + item.ScheduleName;
            ScheduleNameTXT.Text = item.ScheduleName;

            //Set SiteCombo
            ScheduleSiteCMB.SelectedValue = item.SiteSeq;

            //Set TestCase TreeView
            string projName = ScheduleNameTXT.Text;
            Tree_Load(true, projName);

            //Set Weekly checkboxes
            ScheduleMonChkBox.IsChecked = item.IsMon == "1";
            ScheduleTueChkBox.IsChecked = item.IsTue == "1";
            ScheduleWedChkBox.IsChecked = item.IsWed == "1";
            ScheduleThuChkBox.IsChecked = item.IsThu == "1";
            ScheduleFriChkBox.IsChecked = item.IsFri == "1";

            //Set Execution time
            ExecuteTimeCMB.SelectedIndex = int.Parse(item.StartingTime);
        }

        private void ResetScheduleCanvas()
        {
            //ScheduleSeqLBL.Text = string.Empty;
            ScheduleNameTXT.Text = string.Empty;
            UncheckTreeNodes(((TreeModel)_tree_Schedule.Model).Nodes);
            ScheduleMonChkBox.IsChecked = false;
            ScheduleTueChkBox.IsChecked = false;
            ScheduleWedChkBox.IsChecked = false;
            ScheduleThuChkBox.IsChecked = false;
            ScheduleFriChkBox.IsChecked = false;

            ExecuteTimeCMB.SelectedIndex = 0;
        }

        private async void DeleteScheduleTest()
        {
            IScheduler sched = await SchedulerFactory.GetScheduler();
            if (!sched.IsStarted)
            {
                await sched.Start();
            }

            TestSchedulesRow item = TestScheduleGridView.SelectedItem as TestSchedulesRow;
            int rowIdx = TestScheduleGridView.SelectedIndex;

            //Remove the task from Task Schedule.
            await sched.DeleteJob(new JobKey(item.ScheduleName));
            //Remove the Data from DataGridView.
            TestScheduleGridView.Items.RemoveAt(rowIdx);
            //Delete Record From RegulateTest
            LogReportDataSetTableAdapters._TTCRegularTestTableAdapter _RegularTestTableAdapter = new LogReportDataSetTableAdapters._TTCRegularTestTableAdapter();
            //Remove the Data from DB.
            _RegularTestTableAdapter.Delete_TTCTegularTest(item.ScheduleSeq);
            _RegularTestTableAdapter.Fill(logReportDataSet._TTCRegularTest);
            _RegularTestTableAdapter.Dispose();

            //Delete Old Records
            LogReportDataSetTableAdapters._TTCTestProjectTableAdapter _TestProjectTableAdapter = new LogReportDataSetTableAdapters._TTCTestProjectTableAdapter();
            _TestProjectTableAdapter.DeleteOldTestTree(item.ScheduleName);
            _TestProjectTableAdapter.Dispose();


            if (rowIdx == 0 && TestScheduleGridView.Items.Count == 0)
            {
                Collection<Node> nodes = ((TreeModel)_tree_Schedule.Model).Nodes;
                UncheckTreeNodes(nodes);
            }
            else if (rowIdx == 0)
            {
                TestScheduleGridView.SelectedIndex = 0;
                DataGridCellInfo cellInfo = new DataGridCellInfo(TestScheduleGridView.SelectedItem, TestScheduleGridView.Columns[0]);
                TestScheduleGridView.CurrentCell = cellInfo;
                TestScheduleGridView_PreviewMouseUp(null, null);
            }
            else
            {
                TestScheduleGridView.SelectedIndex = rowIdx - 1;
                DataGridCellInfo cellInfo = new DataGridCellInfo(TestScheduleGridView.SelectedItem, TestScheduleGridView.Columns[0]);
                TestScheduleGridView.CurrentCell = cellInfo;
                TestScheduleGridView_PreviewMouseUp(null, null);
            }

            /*Added By Fazal*/
            DeleteXmlFiles(item.ScheduleName);

            MessageBox.Show(string.Format("Deleted the Task {0}", item.ScheduleName));
            ResetScheduleCanvas();
        }

        private void DeleteXmlFiles(string deletingScheduleName)
        {
            string path = System.IO.Path.Combine(Global.DataDirectory, deletingScheduleName + "_" + Global.TCName_Schedule);
            File.Delete(path);
        }

        private void NewBTN_Click(object sender, EventArgs e)
        {
            isNewSchedule = true;
            isNewScheduleCreated = false;
            Tree_Load(true);

            TestScheduleGridView.SelectedIndex = -1;
            ResetScheduleCanvas();

            Tree_Save();
        }

        private void SaveBTN_Click(object sender, EventArgs e)
        {
            string scheduleProjName = ScheduleNameTXT.Text;

            if (string.IsNullOrEmpty(scheduleProjName))
            {
                MessageBox.Show("Please Enter the Schedule Name");
                return;
            }
            //Creating New File with existed schedule name
            TestSchedulesRow item = TestScheduleGridView.SelectedItem as TestSchedulesRow;
            int rowIdx = TestScheduleGridView.SelectedIndex;
            string schedulePath = System.IO.Path.Combine(Global.DataDirectory, scheduleProjName + "_" + Global.TCName_Schedule);
            if (isNewSchedule)
            {
                schedulePath = System.IO.Path.Combine(Global.DataDirectory, Global.TCName_New_Schedule);
            }
            else if (item.ScheduleName != scheduleProjName && File.Exists(schedulePath))
            {
                MessageBox.Show("TestCase <" + scheduleProjName + "> already exists.");
                return;
            }

            //Added By Fazal
            if (!(ScheduleMonChkBox.IsChecked ?? false) && !(ScheduleTueChkBox.IsChecked ?? false) && !(ScheduleWedChkBox.IsChecked ?? false) && !(ScheduleThuChkBox.IsChecked ?? false) && !(ScheduleFriChkBox.IsChecked ?? false))
            {
                MessageBox.Show("Please select any one day...");
                return;
            }

            if (rowIdx != -1)
            {
                UpdateTaskWeeklyAsync(item.ScheduleSeq);
            }
            else
            {
                CreateTaskWeeklyAsync(scheduleProjName);
            }

            if (isNewSchedule)
            {
                File.Move(schedulePath, schedulePath.Replace(Global.TCName_New_Schedule, scheduleProjName + "_" + Global.TCName_Schedule));
                TestScheduleGridView.SelectedIndex = TestScheduleGridView.Items.Count - 1;
                DataGridCellInfo cellInfo = new DataGridCellInfo(TestScheduleGridView.SelectedItem, TestScheduleGridView.Columns[0]);
                TestScheduleGridView.CurrentCell = cellInfo;
                TestScheduleGridView_PreviewMouseUp(null, null);
            }
            else
            {
                TestScheduleGridView.SelectedIndex = rowIdx;
                DataGridCellInfo cellInfo = new DataGridCellInfo(TestScheduleGridView.SelectedItem, TestScheduleGridView.Columns[0]);
                TestScheduleGridView.CurrentCell = cellInfo;
                TestScheduleGridView_PreviewMouseUp(null, null);
            }
        }

        private async void UpdateTaskWeeklyAsync(int updatingScheduleSeq)
        {
            string scheduleName = ScheduleNameTXT.Text;

            IScheduler scheduler = await SchedulerFactory.GetScheduler();
            JobKey jobKey = new JobKey(scheduleName);
            //Remove the task from Task Schedule.
            await scheduler.DeleteJob(jobKey);


            var selectedDays = DayCheckedListBox.Children.OfType<CheckBox>().Where(x => x.IsChecked == true).Select(x => x.Content.ToString()).ToList();
            //"0 05 10 ? *MON - FRI"
            string joinedString = string.Join(",", selectedDays).ToUpper();


            int SiteSeq = ((LogReportDataSet._TTCSiteRow)(((DataRowView)ScheduleSiteCMB.SelectedItem).Row)).SiteSeq;

            string IsMon = selectedDays.Contains("Mon") ? "1" : "0";
            string IsTue = selectedDays.Contains("Tue") ? "1" : "0";
            string IsWed = selectedDays.Contains("Wed") ? "1" : "0";
            string IsThu = selectedDays.Contains("Thu") ? "1" : "0";
            string IsFri = selectedDays.Contains("Fri") ? "1" : "0";
            string StartingTime = ExecuteTimeCMB.SelectedValue.ToString();

            LogReportDataSetTableAdapters._TTCRegularTestTableAdapter _RegularTestTableAdapter = new LogReportDataSetTableAdapters._TTCRegularTestTableAdapter();
            string PCName = Environment.MachineName;
            string hostIPAdd = Dns.GetHostEntry(PCName).AddressList[1].ToString();
            var list = new List<string>();
            CheckedNames(((TreeModel)_tree_Schedule.Model).Nodes, list);
            string runItemList = string.Join(",", list);
            _RegularTestTableAdapter.Update_TTCRegularTest(scheduleName, SiteSeq, IsMon, IsTue, IsWed, IsThu, IsFri, StartingTime, runItemList, PCName, hostIPAdd, updatingScheduleSeq);

            TestSchedulesRow item = TestScheduleGridView.SelectedItem as TestSchedulesRow;
            item.ScheduleName = scheduleName;
            item.SiteSeq = SiteSeq;
            item.IsMon = IsMon;
            item.IsTue = IsTue;
            item.IsWed = IsWed;
            item.IsThu = IsThu;
            item.IsFri = IsFri;
            item.StartingTime = StartingTime;
            item.JobStartAt = item.JobRunAfter = "";
            JobGridListAsync(item);

            //Update_TTCRegularTestProject
            LogReportDataSetTableAdapters._TTCRegularTestProjectTableAdapter _RegularTestProjectTableAdapter = new LogReportDataSetTableAdapters._TTCRegularTestProjectTableAdapter();
            _RegularTestProjectTableAdapter.Update_TTCRegularTestProject(scheduleName, updatingScheduleSeq);


            ResetScheduleCanvas();
            _RegularTestTableAdapter.Fill(logReportDataSet._TTCRegularTest);
            _RegularTestProjectTableAdapter.Fill(logReportDataSet._TTCRegularTestProject);
            _RegularTestTableAdapter.Dispose();
            _RegularTestProjectTableAdapter.Dispose();

            //Delete Old Records
            LogReportDataSetTableAdapters._TTCTestProjectTableAdapter _TestProjectTableAdapter = new LogReportDataSetTableAdapters._TTCTestProjectTableAdapter();
            _TestProjectTableAdapter.DeleteOldTestTree(scheduleName);

            //Insert Updated Records
            XmlDocument xmlDoc = new XmlDocument();

            xmlDoc.Load(System.IO.Path.Combine(Global.DataDirectory, scheduleName + "_" + Global.TCName_Schedule));
            XmlNodeList xmlNodeList = xmlDoc.DocumentElement.ChildNodes;
            testProjectNodeOrder = 0;
            testProjectDT = new LogReportDataSet._TTCTestProjectDataTable();
            XmlToSql(xmlNodeList, "", scheduleName);

            _TestProjectTableAdapter.Update(testProjectDT);
            _TestProjectTableAdapter.Dispose();
            _RegularTestProjectTableAdapter.Dispose();

            string cronValue = GetCronValue(int.Parse(StartingTime), joinedString);
            await CreateJobAndTrigger(scheduleName, scheduler, jobKey, cronValue);

            string msg = string.Format("Saved {0}. ", scheduleName);
            MessageBox.Show(msg);
        }

        private async void CreateTaskWeeklyAsync(string customProjName, bool isUpdate = false)
        {
            TestSchedulesRow item = new TestSchedulesRow
            {
                ScheduleName = ScheduleNameTXT.Text
            };
            if (string.IsNullOrEmpty(item.ScheduleName))
            {
                MessageBox.Show("Please Enter the ScheduleName");
                return;
            }
            IScheduler scheduler = await SchedulerFactory.GetScheduler();
            if (!scheduler.IsStarted)
            {
                await scheduler.Start();
            }

            var selectedDays = DayCheckedListBox.Children.OfType<CheckBox>().Where(x => x.IsChecked == true).Select(x => x.Content.ToString()).ToList();
            string joinedString = string.Join(",", selectedDays).ToUpper();
            item.StartingTime = ExecuteTimeCMB.SelectedValue.ToString();


            string _selectedDate = DateTime.Now.ToShortDateString();

            string _triggeringTime = item.StartingTime + ":00:00";
            if (selectedDays.Count == 0)
            {
                MessageBox.Show("Please select any one day...");
                return;
            }
            item.SiteSeq = (int)ScheduleSiteCMB.SelectedValue;
            item.IsMon = "0";
            item.IsTue = "0";
            item.IsWed = "0";
            item.IsThu = "0";
            item.IsFri = "0";

            for (var i = 0; i < selectedDays.Count; i++)
            {
                var day = selectedDays[i];
                switch (day)
                {
                    case "Mon":
                        item.IsMon = "1";
                        break;
                    case "Tue":
                        item.IsTue = "1";
                        break;
                    case "Wed":
                        item.IsWed = "1";
                        break;
                    case "Thu":
                        item.IsThu = "1";
                        break;
                    case "Fri":
                        item.IsFri = "1";
                        break;
                }
            }

            if (!isUpdate)
            {
                //Inserting the data in _TTCRegularTest and then _TTCRegularTestProject.
                LogReportDataSetTableAdapters._TTCRegularTestTableAdapter _RegularTestTableAdapter = new LogReportDataSetTableAdapters._TTCRegularTestTableAdapter();
                string PCName = Environment.MachineName;
                string hostIPAdd = Dns.GetHostEntry(PCName).AddressList[1].ToString();
                var list = new List<string>();
                CheckedNames(((TreeModel)_tree_Schedule.Model).Nodes, list);
                string runItemList = string.Join(",", list);
                item.ScheduleSeq = Int32.Parse(_RegularTestTableAdapter.Insert_TTCRegularTest(item.ScheduleName, item.SiteSeq, item.IsMon, item.IsTue, item.IsWed, item.IsThu, item.IsFri, item.StartingTime, PCName, hostIPAdd, runItemList).ToString());
                LogReportDataSet._TTCRegularTestProjectRow subDataRow = logReportDataSet._TTCRegularTestProject.New_TTCRegularTestProjectRow();
                subDataRow.ScheduleSeq = item.ScheduleSeq;
                subDataRow.ProjectName = customProjName;
                logReportDataSet._TTCRegularTestProject.Rows.Add(subDataRow);

                LogReportDataSetTableAdapters._TTCRegularTestProjectTableAdapter _RegularTestProject = new LogReportDataSetTableAdapters._TTCRegularTestProjectTableAdapter();
                _RegularTestProject.Update(logReportDataSet._TTCRegularTestProject);
                _RegularTestProject.Fill(logReportDataSet._TTCRegularTestProject);
                _RegularTestProject.Dispose();
                _RegularTestTableAdapter.Fill(logReportDataSet._TTCRegularTest);

                item.JobStartAt = item.JobRunAfter = "";
                TestScheduleGridView.Items.Add(item);
                JobGridListAsync(item);

                //Insert New Records
                XmlDocument xmlDoc = new XmlDocument();

                xmlDoc.Load(System.IO.Path.Combine(Global.DataDirectory, Global.TCName_New_Schedule));
                XmlNodeList xmlNodeList = xmlDoc.DocumentElement.ChildNodes;
                testProjectNodeOrder = 0;
                testProjectDT = new LogReportDataSet._TTCTestProjectDataTable();
                XmlToSql(xmlNodeList, "", customProjName);

                LogReportDataSetTableAdapters._TTCTestProjectTableAdapter _TestProjectTableAdapter = new LogReportDataSetTableAdapters._TTCTestProjectTableAdapter();
                _TestProjectTableAdapter.Update(testProjectDT);
                _TestProjectTableAdapter.Dispose();
                _RegularTestTableAdapter.Dispose();
            }

            JobKey jobKey = new JobKey(item.ScheduleName);
            string cronValue = GetCronValue(int.Parse(item.StartingTime), joinedString);

            await CreateJobAndTrigger(item.ScheduleName, scheduler, jobKey, cronValue);

            string msg = string.Format("Registered {0}. Starting {1} at {2} every {3} of every week", item.ScheduleName, _selectedDate, _triggeringTime, joinedString);
            MessageBox.Show(msg);

            if (!isUpdate)
            {
                ResetScheduleCanvas();
            }
        }

        internal void CheckedNames(Collection<Node> theNodes, List<string> list)
        {
            if (theNodes != null)
            {
                foreach (Node aNode in theNodes)
                {
                    if (aNode.Nodes.Count == 0 && aNode.IsChecked)
                    {
                        string textVal = aNode.Module;
                        list.Add(textVal);

                    }
                    if (aNode.Nodes.Count != 0 && aNode.IsChecked)
                    {
                        CheckedNames(aNode.Nodes, list);
                    }
                }
            }
        }

        private void DeleteBTN_Click(object sender, EventArgs e)
        {
            int rowIdx = TestScheduleGridView.SelectedIndex;
            if (rowIdx != -1)
            {
                DeleteScheduleTest();
            }
        }

        private void NodeCheckBox_Schedule_CheckStateChanged(object sender, TreePathEventArgs e)
        {
            Object[] fullPath = e.Path.FullPath;
            string scheduleName = ScheduleNameTXT.Text;
            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                string scheduleFilePath = System.IO.Path.Combine(Global.DataDirectory, scheduleName + "_" + Global.TCName_Schedule);
                if (isNewSchedule)
                {
                    scheduleFilePath = System.IO.Path.Combine(Global.DataDirectory, Global.TCName_New_Schedule);
                }
                xmlDoc.Load(scheduleFilePath);
                XmlNodeList xmlNodeList = xmlDoc.DocumentElement.ChildNodes;

                XmlNode xmlNode = xmlNodeList[(fullPath[0] as Node).Index];
                for (int i = 1; i < fullPath.Length; i++)
                {
                    xmlNode = xmlNode.ChildNodes[(fullPath[i] as Node).Index];
                }

                int checkState = Convert.ToInt32(xmlNode.Attributes["selected"].Value);
                if (xmlNode.ParentNode.Attributes["selected"] == null)
                {
                    if (checkState == CheckBoxType.UNCHECKEDVALUE)
                    {
                        xmlNode.Attributes["selected"].Value = CheckBoxType.CHECKEDVALUE.ToString();
                    }
                    else
                    {
                        xmlNode.Attributes["selected"].Value = CheckBoxType.UNCHECKEDVALUE.ToString();
                    }
                }
                else
                {
                    int parentCheckState = Convert.ToInt32(xmlNode.ParentNode.Attributes["selected"].Value);
                    if (parentCheckState == CheckBoxType.CHECKEDVALUE)
                    {
                        if (checkState == CheckBoxType.UNCHECKEDVALUE)
                        {
                            xmlNode.Attributes["selected"].Value = CheckBoxType.CHECKEDVALUE.ToString();
                        }
                        else
                        {
                            xmlNode.Attributes["selected"].Value = CheckBoxType.UNCHECKEDVALUE.ToString();
                        }
                    }
                    else
                    {
                        if (checkState == CheckBoxType.UNCHECKEDVALUE)
                        {
                            xmlNode.Attributes["selected"].Value = CheckBoxType.INDETERMINATEVALUE.ToString();
                        }
                        else
                        {
                            xmlNode.Attributes["selected"].Value = CheckBoxType.UNCHECKEDVALUE.ToString();
                        }
                    }
                }

                checkState = Convert.ToInt32(xmlNode.Attributes["selected"].Value);
                if (xmlNode.ChildNodes.Count > 0 && checkState != CheckBoxType.INDETERMINATEVALUE)
                {
                    SetChildCheckBoxState(xmlNode.ChildNodes, checkState);
                }

                xmlDoc.Save(scheduleFilePath);

                Tree_Load(true, scheduleName);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error Occured", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private async Task CreateJobAndTrigger(string scheduleName, IScheduler scheduler, JobKey jobKey, string cronValue)
        {
            await scheduler.DeleteJob(jobKey);
            IJobDetail job = CreateJob(jobKey);
            ITrigger trigger = CreateTrigger(scheduleName, job, cronValue);

            job.JobDataMap["runnerObj"] = this;

            // Tell quartz to schedule the job using our trigger
            await scheduler.ScheduleJob(job, trigger);
        }

        private IJobDetail CreateJob(JobKey jobKey)
        {
            //define the job and tie it to our ScheduleJob class
            return JobBuilder.CreateForAsync<ScheduleJob>()
                .WithIdentity(jobKey)
                .Build();
        }

        private ITrigger CreateTrigger(string scheduleName, IJobDetail job, string cronValue)
        {
            return TriggerBuilder.Create().WithIdentity(scheduleName + "Trigger")
                            .WithCronSchedule(cronValue, x => x.WithMisfireHandlingInstructionDoNothing()).ForJob(job).Build();
        }

        private static string GetCronValue(int hourVal, string weekDaysVal)
        {
            int minVal = 0;
            //int minVal = (DateTime.Now.Minute) + 1; // For instantaneous trigger(temporarily)
            return "0 " + minVal + " " + hourVal + " ? * " + weekDaysVal;
        }

        #endregion

    }
}
