﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AzaleaRunner
{
    /// <summary>
    /// Interaction logic for SelectModule.xaml
    /// </summary>
    public partial class SelectModule : Window
    {
        Dictionary<string, List<string>> _classMethodList;
        public string _classNameSelected;
        public string _methodNameSelected;

        private class CategoriesRow
        {
            public string Category { get; set; }
        }

        private class TestsRow
        {
            public string Test { get; set; }
        }

        public SelectModule()
        {
            InitializeComponent();
            LoadModuleList();
        }

        private void LoadModuleList()
        {

            int categoriesRowIndex = 0;
            string assemblyPath = System.IO.Path.Combine(Global.AzaleaDllDirectory, Global.TestManagerDllName);
            Assembly assembly = Assembly.Load(File.ReadAllBytes(assemblyPath));
            List<Type> classList = assembly.GetTypes()
                                  .Where(t => t.Namespace == "Azalea.TestCases" && !t.Name.Contains("<>"))
                                  .ToList();
            _classMethodList = new Dictionary<string, List<string>>();
            foreach (Type classType in classList)
            {
                string className = classType.Name;
                IEnumerable<MethodInfo> methodInfoList = ((TypeInfo)classType).DeclaredMethods;
                List<string> methodNameList = methodInfoList.Where(x => x.IsPublic == true).Select(x => x.Name).ToList();
                _classMethodList.Add(className, methodNameList);

                CategoriesGV.Items.Add(new CategoriesRow { Category = className });
                if (categoriesRowIndex == 0)
                {
                    fillMethodList(className);
                    CategoriesGV.SelectedItem = CategoriesGV.Items[0];
                }
                categoriesRowIndex++;
            }
            CategoriesGV.Items.Add(new CategoriesRow { Category = "None" });
            _classMethodList.Add("Empty", new List<string>());
        }

        private void fillMethodList(string className)
        {
            okBtn.IsEnabled = true;

            TestsGV.Items.Clear();
            if (className == "None")
            {
                _classNameSelected = _methodNameSelected = "";
                return;
            }
            _classNameSelected = className;
            List<string> methodNameList = _classMethodList[className];
            int testsRowIndex = 0;
            _methodNameSelected = null;
            foreach (string method in methodNameList)
            {
                TestsGV.Items.Add(new TestsRow { Test = method });
                if (testsRowIndex == 0)
                {
                    _methodNameSelected = method;
                    TestsGV.SelectedItem = TestsGV.Items[0];
                    TestsGV.Focus();
                }
                testsRowIndex++;
            }
        }

        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void OkBtn_Click(object sender, EventArgs e)
        {
            DialogResult = true;
        }

        private void CancelBtn_Click(object sender, EventArgs e)
        {
            DialogResult = false;
        }

        private void CategoriesGV_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            CategoriesRow item = CategoriesGV.CurrentCell.Item as CategoriesRow;
            if (item != null)
            {
                fillMethodList(item.Category);
            }
        }

        private void TestsGV_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            TestsRow item = TestsGV.CurrentCell.Item as TestsRow;
            if (item != null)
            {
                _methodNameSelected = item.Test;
            }
        }

    }
}
