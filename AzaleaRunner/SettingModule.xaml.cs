﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AzaleaRunner
{
    /// <summary>
    /// Interaction logic for SettingModule.xaml
    /// </summary>
    public partial class SettingModule : Window
    {


        List<string> mailList;

        private class MailRow : INotifyPropertyChanged
        {
            public string MailAddr
            {
                get { return mailAddr; }
                set
                {
                    mailAddr = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("MailAddr"));
                    }
                }
            }
            public event PropertyChangedEventHandler PropertyChanged;

            private string mailAddr;
            public bool IsChecked { get; set; }
            public Visibility TextBlockVisibility { get; set; }
            public Visibility CheckBoxVisibility { get; set; }
        }

        public SettingModule(LogReportDataSet logReportDataSet)
        {
            InitializeComponent();

            SiteNameCombo.ItemsSource = logReportDataSet._TTCSite;
            SiteNameCombo.SelectedValue = Properties.Settings.Default.SiteSeq;

            BrowserNameCombo.ItemsSource = Global.BrowserList;
            BrowserNameCombo.SelectedValue = Properties.Settings.Default.Browser;

            LanguageNameCombo.ItemsSource = Global.LanguageList;
            LanguageNameCombo.SelectedValue = Properties.Settings.Default.Language;

            SendMailChk.IsChecked = MailCMB.IsEnabled = Properties.Settings.Default.SendMail;
            mailList = new List<string>();
            string rawMailList = Properties.Settings.Default.MailTo;
            if (rawMailList != "")
            {
                mailList = rawMailList.Split(',').ToList();
            }
            string MailAddr = "";
            MailCMB.Items.Add(new MailRow { MailAddr = "Selected (" + mailList.Count() + ")", IsChecked = false, TextBlockVisibility=Visibility.Visible, CheckBoxVisibility= Visibility.Collapsed });
            foreach (var item in logReportDataSet._TTCMailAddress)
            {
                MailAddr = item.MailAddr;
                MailCMB.Items.Add(new MailRow() { MailAddr = MailAddr, IsChecked = (mailList.IndexOf(MailAddr) != -1), TextBlockVisibility=Visibility.Collapsed, CheckBoxVisibility=Visibility.Visible });
            }
            MailCMB.SelectedIndex = 0;

            ShowInTaskbar = false;
        }

        private void SendMailChk_Click(object sender, RoutedEventArgs e)
        {
            MailCMB.IsEnabled = SendMailChk.IsChecked ?? false;
        }

        private void MailCMB_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            (MailCMB.Items[0] as MailRow).TextBlockVisibility = Visibility.Collapsed;
        }

        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            CheckBox chkBox = (sender as CheckBox);
            if (chkBox.IsChecked ?? false)
            {
                mailList.Add(chkBox.Content.ToString());
            }
            else
            {
                mailList.Remove(chkBox.Content.ToString());
            }
            (MailCMB.Items[0] as MailRow).MailAddr = "Selected (" + mailList.Count() + ")";
            MailCMB.Items.Refresh();
        }

        private void MailCMB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            MailCMB.SelectedIndex = 0;
        }

        private void Setting_FormClosing(object sender, EventArgs e)
        {
            Properties.Settings.Default.SiteSeq = Convert.ToInt32(SiteNameCombo.SelectedValue);
            Properties.Settings.Default.Browser = BrowserNameCombo.SelectedValue.ToString();
            Properties.Settings.Default.Language = LanguageNameCombo.SelectedValue.ToString();
            Properties.Settings.Default.SendMail = SendMailChk.IsChecked ?? false;
            Properties.Settings.Default.MailTo = String.Join(",", mailList);
            Properties.Settings.Default.Save();
            Close();
        }

    }
}
